import {loadMakeAndModelApi} from "../Api.js";


export const LOAD_MAKES_REQUEST       = 'LOAD_MAKES_REQUEST';
export const LOAD_MAKES_SUCCESS       = 'LOAD_MAKES_SUCCESS';
export const LOAD_MAKES_FAILURE       = 'LOAD_MAKES_FAILURE';


function handleFetchRejection(dispatch, action, error) {
  dispatch({type: action, error: error});
  if (error.status === 401 || error.status === 400) { //not authorized request
    // dispatch(logout()).then((response) => {
    //   if(response && response.data && response.data.redirectUrl){
    //     window.location.replace(response.data.redirectUrl);
    //   }else{
    //     window.location.replace(baseRouterUrl + 'login');
    //   }
    // })
    console.log('handling error');
  }
}


export function loadMakeAndModel() {
  return (dispatch) => {

    dispatch({type:LOAD_MAKES_REQUEST});

    return loadMakeAndModelApi().then((response) => {
      dispatch({type:LOAD_MAKES_SUCCESS, payload:response.data});
      return response
    }).catch(ex => {
      handleFetchRejection(dispatch, LOAD_MAKES_FAILURE, ex);
      return ex
    })
  }
}