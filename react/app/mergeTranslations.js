import * as fs from "fs";
import {sync as globSync} from "glob";
import {sync as mkdirpSync} from "mkdirp";
import last from "lodash/last";

const TRANSLATIONS_PATTERN = "./public/translations/**/*.json";
const LANG_DIR = "./public/locales/";
const LANG_PATTERN = "./public/locales/*.json";

try {
  fs.unlinkSync("./public/locales/data.json");
} catch (error) {
  console.log(error);
}

const mergedTranslations = globSync(LANG_PATTERN)
  .map(filename => {
    const locale = last(filename.split("/")).split(".json")[0];
    return {[locale]: JSON.parse(fs.readFileSync(filename, "utf8"))};
  })
  .reduce((acc, localeObj) => {
    return {...acc, ...localeObj};
  }, {});

const defaultMessages = globSync(TRANSLATIONS_PATTERN)
  .map(filename => fs.readFileSync(filename, "utf8"))
  .map(file => JSON.parse(file))
  .reduce((collection, descriptors) => {
    descriptors.forEach(({id, defaultMessage}) => {
      if (collection.hasOwnProperty(id)) {
        throw new Error(`Duplicate message id: ${id}`);
      }
      collection[id] = defaultMessage;
    });

    return collection;
  }, {});

mkdirpSync(LANG_DIR);

fs.writeFileSync(
  `${LANG_DIR}data.json`,
  JSON.stringify({en: defaultMessages, ...mergedTranslations}, null, 2)
);
