import React, {Component} from 'react'

export default class Home extends Component {

  constructor() {
    super();

  }

  componentWillMount() {

  }

  render(){
    return(
      <div>
        {
          this.props.children
        }
      </div>
    )
  }

}