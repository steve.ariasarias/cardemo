import React, {Component} from 'react'
import LayoutHeader from "../common/LayoutHeader.jsx";
import {pathServer} from "../../../../Constants";
import Table from "../../../public/layout/common/Table.jsx";
import {getSavedSearchesApi, addSaveSearch} from "../../../../Api";
import {loadMakeAndModel} from "../../../../action";
import {connect} from "react-redux";
import Checkbox from '@material-ui/core/Checkbox';
import {optionTitle,optionCarStatus,optionSeller,
  optionBody,optionTransmission,optionCylinder,
  optionFuel,optionDrive,optionDoor,optionCondition,
  optionColor,optionMilleage,CAR_YEAR_START, CURRENT_YEAR} from "../../../../Constants";
import {findIndex} from 'lodash/array'
import {find, filter} from 'lodash/collection'
import {cloneDeep} from 'lodash/lang'
import Moment from "moment";
import Outside from "../../../util/Outside.jsx";


const RenderMakes = ({showMake,renderMake,data,makeSelected,onChangeMake,searchInfoSelected,closeMakeorModel}) => {
  let listMakes= makeSelected.filters.make ? makeSelected.filters.make : [];
  let checkedElement;
  return(
    <div className="w-100">
      <div  className={showMake ? "custom-select select-dropdown-expanded" : "custom-select select-dropdown"}   onClick={()=>renderMake()}>
        {!showMake? <h6> {searchInfoSelected.filters["make"] ? '('+ searchInfoSelected.filters["make"].length + ") selected" : "-" } </h6> :null }
      </div>
      {showMake ?
        <Outside onClose={() => closeMakeorModel("make")}>
          <div className="car-box-detail">
                <div className="car-box-scroll">
                  {data.map((make,index) => {
                    checkedElement=  (listMakes.filter(listMake => listMake === make.id)).length >0;
                    return ( <div key={index} className="card-box-check-make">
                          <input  checked={checkedElement? checkedElement : ''} type="checkbox" value={make.id} onClick={(e) => onChangeMake(e.target.value,e.target.checked)} />
                         <b>{make.name}</b>
                    </div> )
                  })
                  }
                </div>
              </div>
        </Outside>
          : null
      }
    </div>
  )
}

const RenderModels = ({showModel,renderModel,data,listMakes, searchInfoSelected,onChangeModel,closeMakeorModel}) => {
  let makesSelected= searchInfoSelected.filters.make ? searchInfoSelected.filters.make : [];
  let modelSelected= searchInfoSelected.filters.model ? searchInfoSelected.filters.model : [];
  let checkedElement;
  return(
    <div className="w-100">
      <div  className={showModel ? "custom-select select-dropdown-expanded" : "custom-select select-dropdown"}   onClick={()=>renderModel()}>
        {!showModel? <h6> {searchInfoSelected.filters["model"] ? '('+ searchInfoSelected.filters["model"].length + ") selected" : "-" } </h6> :null }
      </div>
      {showModel ?
        <Outside onClose={() => closeMakeorModel("model")}>
          <div className="car-box-detail">
              <div className="car-box-scroll">
                {makesSelected.map(function (make,index) {
                  let makeName = listMakes.find(makes => makes.id === make);
                  return(
                    <div key={index}>
                      <h6 className="custom-select-model"> {makeName? makeName.name:""}</h6>
                        {data.map((model,index) => {
                          if(model.makeId == make){
                            checkedElement=  (modelSelected.filter(modelSel =>modelSel  === model.id)).length >0;
                            return (
                              <div key={index} className="card-box-check-make">
                              <input checked={checkedElement? checkedElement : ''} type="checkbox" value={model.id} onClick={(e)=> onChangeModel(e.target.value,e.target.checked)} />
                              <b>{model.name}</b>
                            </div> )
                          }
                        })
                        }
                    </div>
                  )
                })}
              </div>
            </div>
        </Outside>
          : null
      }
    </div>
  )
}
const RenderDropDown = ({data,type,typeSearchInfo,findElement,handleChange,showDropdown,showFilter,searchInfoSelected,closeDropDown}) => {
  return (
    <div className="w-100">

        <div  className={showFilter[type] ? "custom-select select-dropdown-expanded" : "custom-select select-dropdown"}   onClick={()=>showDropdown(type)}>
          {!showFilter[type]? <h6> {searchInfoSelected.filters[typeSearchInfo] ? '('+ searchInfoSelected.filters[typeSearchInfo].length + ") selected" : "-"  } </h6> :null}
        </div>
        {
          showFilter[type] ?
            <Outside onClose={() => closeDropDown(type)}>
              <div className="car-box-detail">
                <div className="car-box-scroll">
                  {data.map((option,index) => {
                    const checkedElement= findElement(option.value,type,typeSearchInfo);
                    return ( <div key={index} className="card-box-check">
                      <input checked={checkedElement? checkedElement : ''} type="checkbox" value={option.value} name={option.value}
                              onClick={(e)=> handleChange(e,type,typeSearchInfo)} />
                      <b>{option.name}</b>
                    </div> )
                  })
                  }
                </div>
              </div>
            </Outside>
            : null
        }

    </div>

  )
};

class LayoutSavedSearches extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searches: [],
      listingSearchCars: [],
      newSearch:{
        name:"",
        filters:{},
        coincidence:0,
        isAlertActive:false
      },
      showFilter: {
        title: false,
        listing:false,
        seller:false,
        body:false,
        transmission:false,
        cylinder:false,
        fuel:false,
        drive:false,
        door:false,
        extCondition:false,
        intCondition:false,
        extColor:false,
        intColor:false
      },
      searchInfoSelected:null,
      selectedView: null,
      selectedCar: null,
      selectedSearchIndex: null,
      selectedMake: '',
      selectedModel: '',
      minYear:2010,
      minMileage:40000,
      checkedA: true,
      expandModal: false,
      showMake:false,
      showModel:false,
    };
    this.renderPanel = this.renderPanel.bind(this);
    this.renderInfoView = this.renderInfoView.bind(this);
    this.renderEditionView = this.renderEditionView.bind(this);
    this.renderModelView= this.renderModelView.bind(this);
    this.renderListSearches=this.renderListSearches.bind(this);
    this.renderYears=this.renderYears.bind(this);
    this.loadSaveSearches= this.loadSaveSearches.bind(this);
    this.selectListSearches=this.selectListSearches.bind(this);
    this.saveOrUpdateSearch= this.saveOrUpdateSearch.bind(this);
    this.onChangeMake=this.onChangeMake.bind(this);
    this.onChangeModel=this.onChangeModel.bind(this);
    this.showDropdown= this.showDropdown.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.findElement = this.findElement.bind(this);
    this.returnMessage= this.returnMessage.bind(this);
    this.cancelEdition = this.cancelEdition.bind(this);
    this.searchCarListing=this.searchCarListing.bind(this);
    this.closeDropDown=this.closeDropDown.bind(this);
    this.closeMakeorModel=this.closeMakeorModel.bind(this);
    this.renderMake=this.renderMake.bind(this);
    this.renderModel=this.renderModel.bind(this);


  }

  componentWillMount() {
    let {makes, models} = this.props;
    if(!makes || !models){
      this.props.loadMakeAndModel()
    }
    this.loadSaveSearches();

  }

  closeDropDown(value){
    let {showFilter} = this.state;
    let filter=showFilter;
    filter[value]= false;
    this.setState({ showFilter : filter});
  }

  closeMakeorModel(type){
    if(type=="make"){
      this.setState({ showMake : false});
    } else if(type == "model"){
      this.setState({ showModel : false});
    }
  }

  showDropdown(value){
    let {showFilter} = this.state;
    let filter=showFilter;
    if(filter[value]){
      filter[value]= false;
    }else {
      filter[value]= true;
    }
    this.setState({ showFilter : filter});

  }
  renderMake(){
    const {showMake}= this.state;
    this.setState({ showMake : !showMake});
  }
  renderModel(){
    const {showModel}= this.state;
    this.setState({ showModel : !showModel});
  }

  searchCarListing(data){
    let {searches,listingSearchCars} = this.state;
    let newListing =[];
    if(data === ""){
      this.setState({listingSearchCars: searches});
    }else{
      searches.find((car) => {
        let yearFrom=car.filters.yearFrom+" ";
        let yearTo=car.filters.yearTo+" ";
        if(car.filters.make.toLowerCase().indexOf(data.toLowerCase()) !== -1 || car.filters.model.toLowerCase().indexOf(data.toLowerCase()) !== -1 || yearFrom.indexOf(data) !== -1  ||   yearTo.indexOf(data) !== -1 ){
          newListing.push(car);
          // valueFind=car.make_id;
        }
      });
      this.setState({listingSearchCars: newListing});
    }
  }

  cancelEdition(listingSearchCars){
    let {showFilter,searches,searchInfoSelected,newSearch} = this.state;
    let newSearches = [];
    let newListingSearches = [];
    showFilter["title"]=false;
    showFilter["listing"]=false;
    showFilter["seller"]=false;
    showFilter["body"]=false;
    showFilter["transmission"]=false;
    showFilter["cylinder"]=false;
    showFilter["fuel"]=false;
    showFilter["drive"]=false;
    showFilter["door"]=false;
    showFilter["extCondition"]=false;
    showFilter["intCondition"]=false;
    showFilter["extColor"]=false;
    showFilter["intColor"]=false;
    showFilter["make"]=false;
    showFilter["model"]=false;
    if(!searchInfoSelected.id){
      searches.shift();
      listingSearchCars.shift();
      this.setState({searches:searches,listingSearchCars:listingSearchCars,newSearch:{name:"", filters:{}, coincidence:0, isAlertActive:false},
                    selectedView:null,searchInfoSelected:null},()=>{
        this.setState({showFilter: showFilter});
      })
    }else{
      let foundSearch = find(listingSearchCars,function(o) {
        return o.id === searchInfoSelected.id;
      });
      this.setState({selectedView: 'INFO_VIEW',searchInfoSelected:foundSearch},()=>{
        this.setState({showFilter: showFilter});
      });
    }
  }

  handleChange (event,type,typeSearchInfo) {
    let {searchInfoSelected}= this.state;
    let searchInfo = searchInfoSelected;
    let array=[];
    let flag=false;
    let typeElement="";
    typeElement=searchInfoSelected.filters[typeSearchInfo];
    if(typeElement){
      typeElement.map ((value,index) => {
        if(value !== event.target.value){
          array.push(value);
        }
        else{
          flag=true;
          delete array[index];
        }
      });
      if(!flag){
        array.push(event.target.value);
      }
      searchInfo.filters[typeSearchInfo]=array;
    }
    else{
      searchInfo.filters[typeSearchInfo] = [event.target.value];
    }
    searchInfoSelected.filters = {...searchInfo.filters};
    this.setState({searchInfoSelected:{...this.state.searchInfoSelected,filters:searchInfoSelected.filters}});
  };

  findElement(element,type,typeSearchInfo){
    let {searchInfoSelected}= this.state;
    let find=false;
    let typeElement="";
    typeElement=searchInfoSelected.filters[typeSearchInfo];
    if(typeof typeElement !== "undefined"){
      typeElement.map ((value,index) => {
        if(value.toLowerCase() === element.toLowerCase()){
          find=true;
        }})
    }
    else{
      find=false;
    }
    return find;
  }


  loadSaveSearches(query){
    let searches = [];
    getSavedSearchesApi().then((response) => {
      searches = response.data.saveSearches;
      searches.forEach(s => {
        s.filters = JSON.parse(s.filters)
      });
      this.setState({searches: searches,listingSearchCars:searches});
    }).catch((error) => {
      console.log(error)
    });
  }

  saveOrUpdateSearch(searchInfoSelected){
    let {minYear, minMileage, selectedMake, selectedModel} = this.state;
    const {models} = this.props;
    /*saveDocument(this.state.searchInfoSelected._source, PortalEndpoints.SEARCHES_COLLECTION_PATH + '/'+idSearch).then((response) => {
   }).catch((error) => {
     console.log('Search DATA ERROR: ' + error)
   });*/
    console.log("searchInfoSelected to save ->",searchInfoSelected);
    searchInfoSelected.filters.yearFrom = ""+minYear;
    searchInfoSelected.filters.mileageFrom = ""+minMileage;
    searchInfoSelected.filters.make = (selectedMake === '')?searchInfoSelected.filters.make:selectedMake;
    searchInfoSelected.filters.model = (selectedModel === '')?searchInfoSelected.filters.model:selectedModel;
    let modelsByMake = filter(models, (x) => { return x.makeId === selectedMake});
    if(modelsByMake.length > 0 && selectedModel === ''){
      searchInfoSelected.filters.model = modelsByMake[0].name;
    }
    searchInfoSelected.filters = JSON.stringify(searchInfoSelected.filters);
    addSaveSearch(searchInfoSelected).then((response) => {
      this.loadSaveSearches("");
      searchInfoSelected.filters = JSON.parse(searchInfoSelected.filters);
      this.setState({selectedView: 'INFO_VIEW',searchInfoSelected:searchInfoSelected,selectedMake:'',selectedModel:''});
    }).catch((error) => {
      console.log(error)
    });
  }

  selectListSearches(id, searches){
    //let {searches}= this.state;
    this.setState({
      selectedView: 'INFO_VIEW',
      selectedSearchIndex: 0,
      selectedCar: {make: 'Acura', model: 'RSX', color: 'Rojo'}
    });
    let searchSelect = find(searches,function(o) {
      return o.id === id;
    });
    if(searchSelect){
      this.setState({searchInfoSelected: searchSelect});
    }
  }

  renderListSearches(searches){
    let listSearches= searches.map(function(search,index){
      if(!this.state.searchInfoSelected){
        return(
          <div key={index} className="search-item col-lg-12">
            <b>{search.name}</b>
            <div className="d-inline-flex w-100" style={(search.name === "")?{paddingTop:30}:{}}>
              <em className="float-left">Created: {Moment(search.createDate).format("YYYY-MM-DD")} | 23 Coincidences</em>
              <div className="float-right option-buttons d-inline-flex">
                <button className="search-button">Alert</button>
                <button className="search-button"> Run Search</button>
                <img className="float-right arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"}
                     alt="" onClick={() => this.selectListSearches(search.id,searches)} />
              </div>
            </div>
          </div>
        )
      }
      else {
        return(
          <div key={index} className="search-item col-lg-12">
            <b>{search.name}</b>
            <div className="d-inline-flex w-100" style={(search.name === "")?{paddingTop:30}:{}}>
              <em className="float-left">Created: {Moment(search.createDate).format("YYYY-MM-DD")} | 23 Coincidences</em>
              <div className="float-right option-buttons d-inline-flex">
                <button className="search-button">Alert</button>
                <button className="search-button"> Run Search</button>
                { this.state.searchInfoSelected.id === search.id  ?
                  <button className="selected-btn"
                          onClick={() => this.setState({
                            selectedView: null,
                            selectedSearchIndex: null,
                            selectedCar: null,
                          })}> Selected
                    <svg className="selected-arrow" width="13" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 30.152 30.152">
                      <path id="ic_arrow_forward_24px"
                            d="M19.076,4,16.419,6.657,26.934,17.191H4V20.96H26.934L16.419,31.495l2.657,2.657L34.152,19.076Z"
                            transform="translate(-4 -4)"/>
                    </svg></button> :
                  <img className="float-right arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"}
                       alt="" onClick={() => this.selectListSearches(search.id,searches)} />}
              </div>
            </div>
          </div>
        )
      }

    },this)

    return listSearches;
  }

  returnMessage(message){
    let result="";
    if(message.length === 0 || typeof  message === "undefined"){
      result = "-";
    }else{
      result = message.map(function (item,index) {
        return item.toLocaleUpperCase() + " ";
      });
    }
    return result;
  }

  renderInfoView() {
    let {searchInfoSelected} = this.state;
    return (<div className="info-view-panel">
      <h5 className="info-view-title d-inline-flex"> {searchInfoSelected.name ? searchInfoSelected.name : ""}</h5>
      <button className="btn dark-btn btn-edition" onClick={() => this.setState({selectedView: 'EDITION_VIEW'})}>Edit</button>
      <p className="info-view-subtitle">{ searchInfoSelected.filters.make ? searchInfoSelected.filters.make.toUpperCase(): "Make not selected" } /  {searchInfoSelected.filters.model ? searchInfoSelected.filters.model : "Model not selected"}</p>
      <Table admin={true}
             rows={
               [ ["Make",  searchInfoSelected.filters.make ? searchInfoSelected.filters.make.toLocaleUpperCase() : "-"],
                 ["Model", searchInfoSelected.filters.model ? searchInfoSelected.filters.model.toLocaleUpperCase() : "-"],
                 ["Trims",searchInfoSelected.filters.trims ? searchInfoSelected.filters.trims : "-"],
                 ["Type of listing", searchInfoSelected.filters.status ? this.returnMessage( searchInfoSelected.filters.status) : "-"],
                 ["Seller type", searchInfoSelected.filters.seller ? this.returnMessage(searchInfoSelected.filters.seller) : "-"],
                 ["Title type", searchInfoSelected.filters.title ? this.returnMessage(searchInfoSelected.filters.title) : "-"],
                 ["Year from", searchInfoSelected.filters.yearFrom ? searchInfoSelected.filters.yearFrom : "-"],
                 ["Year to", searchInfoSelected.filters.yearTo ? searchInfoSelected.filters.yearTo : "-"],
                 ["Milleage from", searchInfoSelected.filters.mileageFrom ? searchInfoSelected.filters.mileageFrom : "-"],
                 ["Milleage to", searchInfoSelected.filters.mileageTo ? searchInfoSelected.filters.mileageTo : "-"],
                 ["Body type", searchInfoSelected.filters.bodyType ? this.returnMessage(searchInfoSelected.filters.bodyType) : "-"],
                 ["Zip Code", searchInfoSelected.filters.zip ? searchInfoSelected.filters.zip : "-"],
                 ["Drive type", searchInfoSelected.filters.driveType ? this.returnMessage(searchInfoSelected.filters.driveType) : "-"],
                 ["Transmission", searchInfoSelected.filters.transmission ? this.returnMessage(searchInfoSelected.filters.transmission) : "-"],
                 ["Cylinder", searchInfoSelected.filters.cylinder ? this.returnMessage(searchInfoSelected.filters.cylinder) : "-"],
                 ["Fuel type", searchInfoSelected.filters.fuelType ? this.returnMessage(searchInfoSelected.filters.fuelType) : "-"],
                 ["Doors", searchInfoSelected.filters.door ? this.returnMessage(searchInfoSelected.filters.door) : "-"],
                 ["Int. color", searchInfoSelected.filters.intColor ? this.returnMessage(searchInfoSelected.filters.intColor) : "-"],
                 ["Int. condition", searchInfoSelected.filters.intCondition ? this.returnMessage(searchInfoSelected.filters.intCondition) : "-"],
                 ["Ext. color", searchInfoSelected.filters.extColor ? this.returnMessage(searchInfoSelected.filters.extColor) : "-"],
                 ["Ext. condition", searchInfoSelected.filters.extCondition ? this.returnMessage(searchInfoSelected.filters.extCondition) : "-"],]}
             emptyTitle={true} equalSizes={true}></Table>
    </div>);
  }

  renderModelView(makeSelected,models){
    return models.map(function(car,index) {
      if(car.makeId===makeSelected) {
        return (<option key={index} value={car.name}>{car.name}</option>);
      }
    },this);
  }

  renderYears(yearFrom, yearTo){
    let iYearFrom=parseInt(yearFrom)+1;
    const numbers = [];
    for(let i=iYearFrom; i<= yearTo; i++){
      numbers.push(i);
    }
    return numbers.map(function (info,index) {
      return (<option key={index} value={info}>{info}</option>);
    });
  }

  onChangeMake(value,flag){
    const {searchInfoSelected}= this.state;
    let newSearchInfo = searchInfoSelected;
    let newMakes=[];
    if(newSearchInfo.filters.make){
      if(flag){
        newSearchInfo.filters.make.push(value);
      }else{
        const indexElement=newSearchInfo.filters.make.indexOf(value);
        newSearchInfo.filters.make.splice(indexElement,1);
      }
    }
    else{
      newMakes.push(value);
      newSearchInfo.filters.make = newMakes;
    }
    this.setState({selectedMake: value, searchInfoSelected: newSearchInfo});
  }

  onChangeModel(value,flag){
    const {searchInfoSelected}= this.state;
    let newSearchInfo = searchInfoSelected;
    let newModels=[];
    if(newSearchInfo.filters.model){
      if(flag){
        newSearchInfo.filters.model.push(value);
      }else{
        const indexElement=newSearchInfo.filters.model.indexOf(value);
        newSearchInfo.filters.model.splice(indexElement,1);
      }
    }
    else{
      newModels.push(value);
      newSearchInfo.filters.model = newModels;
    }
    this.setState({selectedModel: value, searchInfoSelected: newSearchInfo});
  }

  onChangeYearFrom(searchEdit,e){
    searchEdit.filters.yearFrom = e.target.value;
    this.setState({minYear: e.target.value})
  }

  renderEditionView() {
    const {selectedMake,minYear,minMileage,showFilter}=this.state;
    let {searchInfoSelected,expandModal,showMake,showModel} = this.state;
    let searchEdit = cloneDeep(searchInfoSelected);
    const {makes,models}=this.props;
    if(!makes || !models) return null;
    return (<div>
      <div className="tool-section">
        {/*<img className="zoom-icon" onClick={} src={pathServer.PATH_IMG + "zoom.png"}/>*/}
        {this.state.expandModal ?
          <i className="fas fa-compress mr-3" onClick={()=> this.setState({expandModal: false})}/>
          :<i className="fas fa-expand-arrows-alt mr-3" onClick={()=> this.setState({expandModal: true })}/>
        }
        <button className="btn dark-btn cancel-btn" onClick={() => this.cancelEdition(listingSearchCars)}>Cancel</button>
        <button className="btn dark-btn save-btn" onClick={() => this.saveOrUpdateSearch(searchInfoSelected) }>SAVE</button>
      </div>
      <div className="dropdown search-title-label">
        <label className="field-name">Name your Search</label>
        <input className="text-field search-title-field" type="text" id="btnMenu2" contentEditable="true"
               defaultValue={searchInfoSelected.name} onChange={(e) => searchInfoSelected.name = e.target.value} />
      </div>

      <div className="fields-row d-inline-flex">
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Make</label>
          <RenderMakes showMake={showMake} renderMake={this.renderMake} data={makes} makeSelected={searchEdit} onChangeMake={this.onChangeMake}  searchInfoSelected={searchEdit}  closeMakeorModel={this.closeMakeorModel}/>


        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Model</label>
          <RenderModels showModel={showModel} renderModel={this.renderModel} data={models} listMakes={makes} searchInfoSelected={searchEdit} onChangeModel={this.onChangeModel}  closeMakeorModel={this.closeMakeorModel} />


        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Trims</label>
          <input type="text" className="text-field-2 form-field-edit" defaultValue={searchInfoSelected.filters.trims ? searchInfoSelected.filters.trims : "-"} onChange={(e) => searchInfoSelected.filters.trims = e.target.value } />
        </div>
      </div>
      <div className="fields-row d-inline-flex">
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Seller Type</label>

            <RenderDropDown data={optionSeller} type="seller" typeSearchInfo="seller" findElement={this.findElement} handleChange={this.handleChange} searchInfoSelected={searchEdit} showDropdown={this.showDropdown}     showFilter={showFilter} closeDropDown={this.closeDropDown}  />

        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Type of listing</label>
          <RenderDropDown data={optionCarStatus} type="status" typeSearchInfo="status" findElement={this.findElement} handleChange={this.handleChange} searchInfoSelected={searchEdit} showDropdown={this.showDropdown} showFilter={showFilter}  closeDropDown={this.closeDropDown}  />
        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Title type</label>
          <RenderDropDown data={optionTitle} type="title" typeSearchInfo="title" findElement={this.findElement} handleChange={this.handleChange} searchInfoSelected={searchEdit} showDropdown={this.showDropdown} showFilter={showFilter} closeDropDown={this.closeDropDown}  />
        </div>
      </div>
      <div className="fields-row d-inline-flex">
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex short-dropdown">
          <label className="field-name">Years</label>
          <select defaultValue={searchInfoSelected.filters.yearFrom ? searchInfoSelected.filters.yearFrom : 2010} className="custom-select select-dropdown" onChange={this.onChangeYearFrom.bind(this,searchInfoSelected)}>
            {this.renderYears(CAR_YEAR_START,CURRENT_YEAR + 1)}
          </select>
        </div>
        <label className="field-conector">TO</label>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex short-dropdown">
          <select defaultValue={searchInfoSelected.filters.yearTo? searchInfoSelected.filters.yearTo : 2016} className="custom-select select-dropdown" onChange={(e) => searchInfoSelected.filters.yearTo = e.target.value } >
            {this.renderYears(minYear,CURRENT_YEAR + 1)}
          </select>
        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex short-dropdown">
          <label className="field-name">Mileage</label>
          <select defaultValue={searchInfoSelected.filters.mileageFrom? searchInfoSelected.filters.mileageFrom : 20000}  className="custom-select select-dropdown" onChange={(e) => {this.setState({minMileage : e.target.value});searchInfoSelected.filters.mileageFrom = e.target.value}} >
            {optionMilleage.map((option, index) =>{
              return(
                <option key={index} value={option.value}>{option.name}</option>
              )
            })
            }
          </select>

        </div>
        <label className="field-conector">TO</label>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex short-dropdown">
          <select defaultValue={searchInfoSelected.filters.mileageTo? searchInfoSelected.filters.mileageTo : 50000} className="custom-select select-dropdown" onChange={(e) => searchInfoSelected.filters.mileageTo = e.target.value } >
            {optionMilleage.map((option, index) =>{
              if(option.value > minMileage) {
                return (
                  <option key={index} value={option.value}>{option.name}</option>
                )
              }
            })
            }
          </select>
        </div>
      </div>
      <div className="fields-row d-inline-flex">
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Body Type</label>
          <RenderDropDown data={optionBody} type="body" typeSearchInfo="bodyType" findElement={this.findElement} handleChange={this.handleChange}
                          searchInfoSelected={searchEdit} showDropdown={this.showDropdown} showFilter={showFilter} closeDropDown={this.closeDropDown}/>
        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">ZIP Code</label>
          <input type="text" className="text-field form-field" defaultValue={searchInfoSelected.filters.zip ?  searchInfoSelected.filters.zip : "-" }
                 onChange={(e) => searchInfoSelected.filters.zip = e.target.value}/>
        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Transmission</label>
          <RenderDropDown data={optionTransmission} type="transmission" typeSearchInfo="transmission" findElement={this.findElement}
                          handleChange={this.handleChange} searchInfoSelected={searchEdit} showDropdown={this.showDropdown}
                          showFilter={showFilter} closeDropDown={this.closeDropDown}/>
        </div>
      </div>
      <div className="fields-row d-inline-flex">
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Cylinders</label>
          <RenderDropDown data={optionCylinder} type="cylinder" typeSearchInfo="cylinder" findElement={this.findElement}
                          handleChange={this.handleChange} searchInfoSelected={searchEdit} showDropdown={this.showDropdown}
                          showFilter={showFilter} closeDropDown={this.closeDropDown}/>
        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Fuel Type</label>
          <RenderDropDown data={optionFuel} type="fuel" typeSearchInfo="fuelType" findElement={this.findElement} handleChange={this.handleChange}
                          searchInfoSelected={searchEdit} showDropdown={this.showDropdown} showFilter={showFilter} closeDropDown={this.closeDropDown}/>

        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Drive Type</label>
          <RenderDropDown data={optionDrive} type="drive"   typeSearchInfo="driveType" findElement={this.findElement} handleChange={this.handleChange}
                          searchInfoSelected={searchEdit} showDropdown={this.showDropdown} showFilter={showFilter} closeDropDown={this.closeDropDown}/>
        </div>
      </div>
      <div className="fields-row d-inline-flex">
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Number of doors</label>
          <RenderDropDown data={optionDoor} type="door" typeSearchInfo="door" findElement={this.findElement} handleChange={this.handleChange}
                          searchInfoSelected={searchEdit} showDropdown={this.showDropdown} showFilter={showFilter} closeDropDown={this.closeDropDown}/>
        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Ext. Condition</label>
          <RenderDropDown data={optionCondition} typeSearchInfo="extCondition" type="extCondition" findElement={this.findElement}
                          handleChange={this.handleChange} searchInfoSelected={searchEdit} showDropdown={this.showDropdown}
                          showFilter={showFilter}  closeDropDown={this.closeDropDown}/>
        </div>
        <div className="dropdown admin-btn-edition-dropdown d-inline-flex">
          <label className="field-name">Int. Condition</label>
          <RenderDropDown data={optionCondition} typeSearchInfo="intCondition" type="intCondition" findElement={this.findElement}
                          handleChange={this.handleChange} searchInfoSelected={searchEdit} showDropdown={this.showDropdown}
                          showFilter={showFilter} closeDropDown={this.closeDropDown}/>
        </div>
      </div>
      <div className="fields-row d-inline-flex">
        <div className={expandModal ? "dropdown admin-btn-edition-dropdown d-inline-flex w-31" : "dropdown admin-btn-edition-dropdown d-inline-flex"}>
          <label className="field-name">Ext. Color</label>
          <RenderDropDown data={optionColor} typeSearchInfo="extColor" type="extColor" findElement={this.findElement} handleChange={this.handleChange}
                          searchInfoSelected={searchEdit} showDropdown={this.showDropdown} showFilter={showFilter} closeDropDown={this.closeDropDown}/>
        </div>
        <div className={expandModal ? "dropdown admin-btn-edition-dropdown d-inline-flex w-31" : "dropdown admin-btn-edition-dropdown d-inline-flex"}>
          <label className="field-name">Int. Color</label>
          <RenderDropDown data= {optionColor} typeSearchInfo="intColor" type="intColor" findElement={this.findElement} handleChange={this.handleChange}
                          searchInfoSelected={searchEdit} showDropdown={this.showDropdown} showFilter={showFilter} closeDropDown={this.closeDropDown}/>
        </div>
      </div>
    </div>);
  }

  renderPanel(selectedView,listingSearchCars) {
    switch (selectedView) {
      case 'INFO_VIEW':
        return this.renderInfoView();
      case 'EDITION_VIEW':
        return this.renderEditionView(listingSearchCars);
      default:
        return (<div className="content-select text-center">
          <i className="fas fa-pencil-alt"></i>
          <h4>Select a <br/>Saved Search <br/> to view/edit</h4>
        </div>);
    }
  }

  showPanelCreate(searches){
    let {newSearch} = this.state;
    searches.unshift(newSearch);
    this.setState({searches:searches,listingSearchCars:searches,searchInfoSelected:newSearch,selectedView:"EDITION_VIEW"});
  }

  render() {
    let {searchInfoSelected}=this.state;
    let listingSearchCars = cloneDeep(this.state.searches);
    let {expandModal} = this.state;
    return (
      <div className="admin-saved">
        <LayoutHeader colorNav="header-admin" searchCar={this.searchCarListing}  optionSelected="searches"/>
        <div className="bg-admin"/>

        <div className="content-admin">
          <div className="container-fluid">
            <div className="row">
              <div className={ "float-left d-inline-flex " + (expandModal ? "principal-panel-expanded" : "principal-panel" )} style={(listingSearchCars.length > 0)?{width:"54vw"}:{width:"86vw"}}>
                <div className="d-inline-flex w-100 filter">
                  <h4>Saved Searches</h4>
                  <button className="create-listing" onClick={this.showPanelCreate.bind(this,this.state.searches)}>Create Search</button>
                  <div className="dropdown">
                    <button className="btn dropdown-toggle" type="button" id="btnMenu2"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <b>Sort By</b>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                      <a className="dropdown-item" href="#!">Action</a>
                      <a className="dropdown-item" href="#!">Another action</a>
                    </div>
                  </div>
                </div>
                <div className="row list-search-item">
                  {this.renderListSearches(listingSearchCars)}
                </div>
              </div>
              {
                (listingSearchCars.length > 0)? <div className={expandModal ? "float-right d-inline-flex principal-panel2-expanded" : "float-right d-inline-flex principal-panel2"}>
                  {this.renderPanel(this.state.selectedView,listingSearchCars)}
                </div>:null
              }

            </div>

          </div>
        </div>
      </div>
    )
  }

}
const mapStateToProps = (state, ownProps) => {
  return {
    makes: state.makes,
    models:state.models
  }
};

export default connect(mapStateToProps, {loadMakeAndModel})(LayoutSavedSearches)