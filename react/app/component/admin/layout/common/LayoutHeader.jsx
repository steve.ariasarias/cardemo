import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {pathServer} from "../../../../Constants";
import {Link} from "react-router";
import { connect } from 'react-redux';


const LoggedMenu = ({menuOptions, logout, refMethod}) => {
  return(
    <div id="logged-menu-options" ref={refMethod}>
      {
        menuOptions.map((menuOption, index)=>{
          return (
            <a href={menuOption.link}>
              <div key={index} className="logged-menu-option">
                {menuOption.displayName}
              </div>
            </a>
          );
        })
      }
      <div id="logout-menu-option" onClick={logout}>Log out</div>
    </div>
  )
};

class LayoutHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenuOptions: false,
      menuOptions:[
        {link:'/catalog', displayName:'Home'},
        {link:'/catalog/inventory', displayName:'Inventory'},
        {link:'/account/dashboard', displayName:'My Account'}],
    }
    this.openSessionMenu = this.openSessionMenu.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount(){
    window.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    window.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({showMenuOptions: false});
    }
  }

  openSessionMenu(){
    const {showMenuOptions} = this.state;
    this.setState({showMenuOptions:!showMenuOptions});
  }

  logout(){
    sessionStorage.clear();
    location.href = "/logout";
  }

  render() {
    let {showMenuOptions, menuOptions} = this.state;
    let {colorNav,optionSelected, firstName} = this.props;
    if(!firstName){
      firstName = sessionStorage.getItem("firstName")
    }
    return (
      <div id="admin-nav" className={colorNav}>
        <div className="menu-header navbar-fixed-top" ref="headerFixed">
          <div className="container-fluid">
            <div className="row">
              <div className="float-left d-inline-flex " id="menu-head-left">
                <div className='dealer-logo text-center'>
                  <Link to="/catalog">
                    <img className="logo-header" src={pathServer.PATH_IMG + "zoom-logo.png"} width="90"/>
                  </Link>
                </div>
                <div className="input-group ml-5">
                  <div className="input-group-prepend">
                    <img src={pathServer.PATH_IMG + "icons/searchIcon.svg"}/>
                  </div>
                  <input type="text" placeholder="Search"  onKeyUp={(e)=>this.props.searchCar(e.target.value)}/>
                </div>
              </div>

              <div className="d-inline-flex" id="menu-head-right"  style={{paddingRight: showMenuOptions ? "calc(100% - 92vw)":"calc(100% - 92vw)"}}>
                <nav aria-label="breadcrumb" id="breadcrum-nav">
                  <ol className="breadcrumb text-center">
                    <li className="breadcrumb-item"> <Link to="/admin/dashboard"><a className={optionSelected== "dashboard" ? "selected-breadcrumb" : "not-selected-breadcrumb"} href="#">Dashboard </a></Link></li>
                    <li className="breadcrumb-item"> <Link to="/account/dashboard"><a className={optionSelected== "account" ? "selected-breadcrumb" : "not-selected-breadcrumb"} href="#">Account </a></Link></li>
                    <li className="breadcrumb-item"><Link to="/admin/listing"><a className={optionSelected== "listing" ? "selected-breadcrumb" : "not-selected-breadcrumb"} href="#">Listings</a></Link></li>
                    <li className="breadcrumb-item"><Link to="/admin/saved"><a className={optionSelected== "searches" ? "selected-breadcrumb" : "not-selected-breadcrumb"}  href="#">Saved Searches</a></Link></li>
                  </ol>
                </nav>
                <div style={{margin: "6px "+(showMenuOptions ? "-84px":"-7px")+" 0px 15px"}}>
                  <a className="user-greeting" onClick={()=> this.openSessionMenu()}>
                    Welcome, {firstName} <i className="fas fa-caret-down"/>
                  </a>
                </div>
                {showMenuOptions ? <LoggedMenu menuOptions={menuOptions} logout={this.logout} refMethod={this.setWrapperRef}/> : null}
              </div>
            </div>
          </div>

        </div>
      </div>

    )
  }
}

function mapStateToProps(state) {
  return {
    firstName: state.firstName
  };
}

export default connect(mapStateToProps)(LayoutHeader);