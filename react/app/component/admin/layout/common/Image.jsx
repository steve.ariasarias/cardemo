import React, {Component} from 'react';

import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import {
  DragSource,
  DropTarget,
  ConnectDropTarget,
  ConnectDragSource,
  DropTargetMonitor,
  DropTargetConnector,
  DragSourceConnector,
  DragSourceMonitor,
} from 'react-dnd';
import { XYCoord } from 'dnd-core';
import flow from 'lodash/flow';


const imageSource = {
  beginDrag(props) {
    return {
      id: props.id,
      index: props.index,
    }
  },
  endDrag(props, monitor, component) {
    const {updateItemStyle} = props;
    if(updateItemStyle)
      updateItemStyle()
  }
};

const imageTarget = {

  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index
    const hoverIndex = props.index
    if (dragIndex === hoverIndex) {
      return;
    }
    props.moveImage(dragIndex, hoverIndex);
    monitor.getItem().index = hoverIndex;
  }
}

class Image extends Component {

  render() {
    const {
      customClass,
      imageSrc,
      isDragging,
      connectDragSource,
      connectDropTarget,
      isOver,
      options
    } = this.props;
    const cursor = isDragging ? 'move' : 'pointer';
    return (
      connectDragSource &&
      connectDropTarget &&
      connectDragSource(
        connectDropTarget(<div>
          {
            (isOver) ?
              <div className="drag-drop-container">
              </div> : <div>{options}<img className={customClass} style={{cursor}} src={imageSrc}/></div>
          }</div>),
      )
    );
  }
}

export default flow(
  DragSource(
    'image',
    imageSource,
    (connect, monitor) => ({
      connectDragSource: connect.dragSource(),
      connectDragPreview: connect.dragPreview(),
      isDragging: monitor.isDragging(),
    }),
  ),
  DropTarget('image', imageTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  }))
)(Image);