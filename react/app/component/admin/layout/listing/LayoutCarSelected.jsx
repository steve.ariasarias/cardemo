import React, {Component} from 'react'
import {
  pathServer,
  optionBody,
  optionCarStatus,
  optionFuel,
  optionCylinder,
  optionColor,
  optionCondition,
  optionTitle,
  optionTransmission,
  optionDrive, moneySign,
  CURRENT_YEAR,
  CAR_YEAR_START
} from "../../../../Constants";
import {loadMakeAndModel} from "../../../../action";
import {connect} from "react-redux";
import Dropzone from "react-dropzone";
import {PortalEndpoints, saveDocument, saveListing, updateCar, uploadCarImage} from "../../../../Api";
import isEmpty from 'lodash/isEmpty'
import Slider from 'react-slick'

class LayoutCarSelected extends Component {

  constructor(props) {
    super(props);
    this.state = {
      carSelected: false,
      idSelected: -1,
      car: {},
      modelsByMake: [],
      images: [
        "car1.png", "car2.png", "car3.png", "car4.png", "car5.png", "car6.png", "car7.png", "car1.png"
      ],
      expandModal: false,
      mainImage: {},
      imageIndexSelected: 0,
      isLoading: false,
      mainImageIndex: 0,
      sliderView: true,
      hideNextArrow: false,
      hidePrevArrow: true
    };
    this.openCarSelected = this.openCarSelected.bind(this);
    this.showImage = this.showImage.bind(this);
    this.expandCarSelected = this.expandCarSelected.bind(this);
    this.compressCarSelected = this.compressCarSelected.bind(this);
    this.save = this.save.bind(this);
    this.getModels = this.getModels.bind(this);
    this.handleMakeChange = this.handleMakeChange.bind(this);
    this.onDropFile = this.onDropFile.bind(this);
    this.onDropFileToReplace = this.onDropFileToReplace.bind(this);
    this.showImagesSlider = this.showImagesSlider.bind(this);
    this.saveCarAndUpdateListings = this.saveCarAndUpdateListings.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
    this.getNextOrder = this.getNextOrder.bind(this);
    this.saveAsMainImage = this.saveAsMainImage.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
  }

  componentDidMount() {
    let {makes, models} = this.props;
    let makeFound = makes.find(m => m.id === this.props.car.makeId);
    let modelFound = models.find(m => m.id === this.props.car.modelId && m.makeId === this.props.car.makeId);
    if(makeFound && modelFound){
      this.setState({car:{...this.state.car,makeName:makeFound.name,modelName:modelFound.name}})
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState({car: nextProps.car, imageIndexSelected: 0, mainImage: null,
      hideNextArrow: false, hidePrevArrow: true});
  }

  componentWillMount() {
    let {makes, models} = this.props;
    // + 2 cause it starts from 0
    let years = [...Array((CURRENT_YEAR - CAR_YEAR_START)+2).keys()].map(x => x + CAR_YEAR_START);
    if (!makes || !models) {
      this.props.loadMakeAndModel()
    }
    this.years = years;
    this.setState({car: this.props.car});
  }


  expandCarSelected(props) {
    this.setState({expandModal: true});
    this.props.expandCar(this);
  }

  compressCarSelected(props) {
    this.setState({expandModal: false});
    this.props.compressCar(this);
  }

  openCarSelected(idCar) {
    this.setState({carSelected: true, idSelected: idCar});
  }

  save() {
    const {isLoading,mainImage,mainImageIndex,car} = this.state;
    if (!isLoading && (!isEmpty(mainImage) || !isEmpty(car.imagesUrls.filter(image => image.order)) || mainImageIndex > 0)) {
      console.log("upload files");
      this.uploadFiles(car.id)
    } else {
      if(car.mileage) car.mileage = car.mileage.replace(/,/g, '');
      this.saveCarAndUpdateListings(car, car.vin);
    }
  }

  getModels() {
    return this.props.models.filter(model => model.makeId === this.state.car.makeId);
  }

  handleMakeChange(makeId) {
    let {car} = this.state;
    car.makeId = makeId;
    this.setState({car: car});
  }

  onDropFile(files) {
    let {imagesUrls} = this.state.car;
    imagesUrls = (imagesUrls) ? imagesUrls : [];
    let that = this;
    let maxImageName;
    files.forEach((file, index) => {
      maxImageName = this.getNextOrder();
      let blob = new Blob([file], {type: 'image/jpeg'});
      let objectURL = URL.createObjectURL(blob);
      let fileTemp = new File([blob], maxImageName.toString(), {type: blob.type});
      fileTemp.preview = objectURL;
      fileTemp.order = imagesUrls.length + 1;
      fileTemp.imageName = maxImageName;
      imagesUrls.push(fileTemp);
      that.setState({car: {...this.state.car, imagesUrls: imagesUrls}});
    });
  }


  onDropFileToReplace(files) {
    let {imageIndexSelected} = this.state;
    let {imagesUrls} = this.state.car;
    let that = this;
    let maxImageName = this.getNextOrder();
    let blob = new Blob([files[0]], {type: 'image/jpeg'});
    let objectURL = URL.createObjectURL(blob);
    let fileTemp = new File([blob], maxImageName.toString(), {type: blob.type});
    fileTemp.preview = objectURL;
    fileTemp.order = imageIndexSelected;
    fileTemp.imageName = maxImageName;
    if (imageIndexSelected === 0) {
      this.setState({mainImage: fileTemp});
    } else {
      imagesUrls[imageIndexSelected - 1] = fileTemp;
      this.setState({car: {...this.state.car, imagesUrls: imagesUrls}});
    }
  }

  saveAsMainImage(imageIndex) {
    if (imageIndex > 0) {
      let car = this.state.car;
      let newImagesUrls = [car.mainImageUrl].concat(car.imagesUrls.slice(0, imageIndex - 1))
        .concat(car.imagesUrls.slice(imageIndex));
      if (car.imagesUrls[imageIndex - 1].preview) {
        newImagesUrls.forEach((image, index) => {
          if(image.preview && index < imageIndex){
            image.order++;
          }
        });
        let mainImageTemp = car.imagesUrls[imageIndex - 1];
        mainImageTemp.order = 0;
        car.imagesUrls = newImagesUrls;
        return({car: car,mainImage: mainImageTemp, mainImageIndex: 0});
      } else {
        return({car: car, mainImageUrl: car.imagesUrls[imageIndex - 1], imagesUrls: newImagesUrls});
      }
    }
  }

  showImage(images, indexInitial, indexNext) {
    let {imageIndexSelected, mainImageIndex} = this.state;
    return (
      <div className="d-inline-flex">
        {images.slice(indexInitial, indexNext).map(function (image, index) {
          return (
            <div className={"img-secondary mr-1 mb-1"+(indexInitial + index === mainImageIndex ? " main-image-indicator" : "")}>
              {indexInitial + index === mainImageIndex ? <label className="main-image-tag">Main</label> : null}
              <img
                src={image ? (image.imageUrl ? image.imageUrl : image.preview) : null}
                onClick={() => this.setState({imageIndexSelected: indexInitial + index})}
                className={indexInitial + index === imageIndexSelected ? "image-selected-highlight" : ""}
                alt=""/>
            </div>
          )
        }, this)}
      </div>
    )
  }

  showImagesGrid(columnsNumber) {
    let files = [];
    let {car} = this.state;
    if (car.mainImageUrl) {
      !isEmpty(this.state.mainImage) ? files.push(this.state.mainImage) : files.push({imageUrl: car.mainImageUrl});
    }
    if (car.imagesUrls) {
      car.imagesUrls.forEach(imageUrl => {
        !isEmpty(imageUrl) && !isEmpty(imageUrl.preview) ? files.push(imageUrl) : files.push({imageUrl: imageUrl});
      });
    }
    return (
      <div  style={!this.state.expandModal ? {height: "17.33vw"}:  {height: "14.33vw"}}>
        <div className="triangle black-triangle" onClick={() => this.setState({sliderView: true})}/>
        {files.map(function (image, index) {
        if (index % columnsNumber === 0) {
          return (this.showImage(files, index, index + columnsNumber));
        }
      }.bind(this))}</div>);
  }


  showImagesSlider() {
    let files = [];
    let {hidePrevArrow, hideNextArrow, car, imageIndexSelected, mainImageIndex} = this.state;
    function CustomizedPrevArrow(props) {
      const {onClick} = props;
      return (
        <div className="slick-arrow fas fa-arrow-left arrow prev-arrow" onClick={onClick} hidden={hidePrevArrow} />
      );
    }
    function CustomizedNextArrow(props) {
      const {onClick} = props;
      return (
        <div className="slick-arrow fas fa-arrow-right arrow next-arrow" onClick={onClick} hidden={hideNextArrow} />
      );
    }
    const sliderSettings = {
      dots: false,
      infinite: false,
      slidesToShow: 8,
      slidesToScroll: 8,
      speed: 1000,
      adaptiveHeight: true,
      arrows: true,
      prevArrow: <CustomizedPrevArrow/>,
      nextArrow: <CustomizedNextArrow/>,
      afterChange: (currentSlide) => {
        this.setState({
          hideNextArrow: (currentSlide === (Math.ceil(((car.imagesUrls.length + 1)/8))-1)*8),
          hidePrevArrow: (currentSlide === 0)
        });
      }
    };

    if (car.mainImageUrl) {
      !isEmpty(this.state.mainImage) ? files.push(this.state.mainImage) : files.push({imageUrl: car.mainImageUrl});
    }

    if (car.imagesUrls) {
      car.imagesUrls.forEach(imageUrl => {
        !isEmpty(imageUrl) && !isEmpty(imageUrl.preview) ? files.push(imageUrl) : files.push({imageUrl: imageUrl});
      });
    }
    return (
      <div className="justify-content-center">
        <div className="triangle" onClick={() => this.setState({sliderView: false})}/>
        {car.imagesUrls && car.imagesUrls.length+1 >= 8 ?
          <Slider {...sliderSettings}>
            {
              files.map((image, index) => {
                return (
                  <div key={index} className={index === mainImageIndex ? "main-image-indicator" : ""}
                       onClick={() => this.setState({imageIndexSelected: index})}>
                    {index === mainImageIndex ? <label>Main</label> : null}
                    <img src={image ? (image.imageUrl ? image.imageUrl : image.preview) : null}
                         className={"slider-image" + (index === imageIndexSelected && index !== mainImageIndex ? " image-selected-highlight" : "")}/>
                  </div>
                )
              })
            }
          </Slider>:<div className="small-slider row justify-content-center">{
            files.map((image, index) => {
              return (
                <div key={index} className={index === mainImageIndex ? "main-image-indicator" : ""}
                     onClick={() => this.setState({imageIndexSelected: index})}>
                  {index === mainImageIndex ? <label>Main</label> : null}
                  <img src={image ? (image.imageUrl ? image.imageUrl : image.preview) : null}
                       className={"slider-image" + (index === imageIndexSelected && index !== mainImageIndex ? " image-selected-highlight" : "")}/>
                </div>
              )
            })
          }</div>}
        {
          /*files.map(function (image, index) {
            if (index % columnsNumber === 0) {
              return (this.showImage(files, index, index + columnsNumber));
            }
          }.bind(this))*/}

      </div>);
  }

  saveCarAndUpdateListings(car, vin) {
    console.log("car to update -> ",car);
    let carInfo = {
      carId:car.id,
      vin: car.vin,
      makeId: car.makeId,
      makeName: car.makeName,
      modelId: car.modelId,
      modelName: car.modelName,
      year: car.year,
      price: car.price.replace(/,/g, ''),
      mainImageUrl: car.mainImageUrl ? car.mainImageUrl: "",
      description: car.description,
      mileage:car.mileage.replace(/,/g, ''),
      values:{
        "body":car.body,
        "cylinder":car.cylinder,
        "drive": car.drive,
        "exteriorColor":car.exteriorColor,
        "interiorColor":car.interiorColor,
        "exteriorCondition":car.exteriorCondition,
        "interiorCondition":car.interiorCondition,
        "fuel":car.fuel,
        "title":car.title,
        "transmission":car.transmission,
        "trim":car.trim,
        "status":car.status,
        "isTextSeller":(car.isTextSeller) ? car.isTextSeller : false
      }
    };
    saveListing({car:carInfo}).then((response) => {
      console.log('SUCCESS - car updated ' + response.carId + ':::' + JSON.stringify(car));
      //this.uploadFiles(this.state.chosenFiles, response.carId);
      this.props.updateCar(car);
    }).catch((error) => {
      console.log('CAR DATA ERROR: ' + error)
    });

    /*saveDocument({doc: car}, PortalEndpoints.CARS_COLLECTION_PATH + '/' + carId + PortalEndpoints.UPDATE_COMMAND).then((response) => {
      console.log('SUCCESS - car updated : ' + JSON.stringify(car));
      this.setState({isLoading: false});
      this.props.updateCar(car);
    }).catch((error) => {
      console.log('CAR DATA ERROR: ' + JSON.stringify(error))
    });*/
  }

  deleteImage(imageIndex) {
    let {car, mainImageIndex} = this.state;
    if ( ((car.imagesUrls || []).length > 0)) {
      car.imagesUrls.slice(imageIndex + 1).forEach(image => {
        if (image.preview) {
          image.order--;
        }
      });
      if (imageIndex === 0) {
        this.setState({
          car: {...car, mainImageUrl: car.imagesUrls[0], imagesUrls: car.imagesUrls.slice(1)},
          mainImage: null,
          mainImageIndex: mainImageIndex > imageIndex ? mainImageIndex-1:mainImageIndex
        });
      } else {
        this.setState({
          car: {
            ...car,
            imagesUrls: car.imagesUrls.slice(0, imageIndex - 1).concat(car.imagesUrls.slice(imageIndex))
          }, imageIndexSelected: imageIndex - 1,
          mainImageIndex: mainImageIndex > imageIndex ? mainImageIndex-1:mainImageIndex
        });
      }
    }
  }

  uploadFiles(carId) {
    this.setState({isLoading: true});
    const {mainImageIndex} = this.state;
    const {state} = this;
    let updatedData = this.saveAsMainImage(mainImageIndex);
    this.setState(updatedData);
    let changedImagesQuantity = 0;
    let files = [];
    if (updatedData && !isEmpty(updatedData.mainImage)) {
      files.push(updatedData.mainImage);
    }
    let imagesUrlsToSave = (updatedData && updatedData.imagesUrls) ? updatedData.imagesUrls : state.car.imagesUrls;
    let paths = imagesUrlsToSave;
    imagesUrlsToSave.forEach(image => {
      if (image.preview) {
        files.push(image);
        changedImagesQuantity++;
      }
    });
    if (files.length > 0) {
      uploadCarImage(carId, files, 1).then((response) => {
        response.paths.forEach(path => paths.push(path.replace('Success(', '').replace(')', '')));
        let images = {};
        if (updatedData && updatedData.mainImage) {
          files.filter(file => file.order).forEach((changedFile, index) => {
            paths[changedFile.order - 1] = paths[paths.length - changedImagesQuantity + index];
          });
          images = {
            imagesUrls: paths.slice(0, paths.length - 1 - changedImagesQuantity),
            mainImageUrl: paths[paths.length - 1 - changedImagesQuantity]
          };
        } else {
          files.filter(file => file.order).forEach((changedFile, index) => {
            paths[changedFile.order - 1] = paths[paths.length - changedImagesQuantity + index];
          });
          images = {imagesUrls: paths.slice(0, paths.length - changedImagesQuantity)};
          if(updatedData && updatedData.mainImageUrl) images = {...images, mainImageUrl: updatedData.mainImageUrl};
        }
        const car = {...state.car, ...images};
        this.saveCarAndUpdateListings(car, carId);

      }).catch(error => {
        console.error(error);
      })
    } else if(updatedData && updatedData.mainImageUrl){
      const car = {...state.car, ...updatedData};
      this.saveCarAndUpdateListings(car, carId);
    }
  }

  getNextOrder() {
    let {imagesUrls, mainImageUrl} = this.state.car;
    let order = 1;
    imagesUrls = (imagesUrls) ? imagesUrls : [];
    if(mainImageUrl){
      order = Math.max(mainImageUrl.slice(mainImageUrl.lastIndexOf("/") + 1), order);
    }
    imagesUrls.forEach(imageUrl => {
      if (!imageUrl.preview) {
        order = Math.max(imageUrl.slice(imageUrl.lastIndexOf("/") + 1), order);
      } else {
        order = Math.max(imageUrl.imageName, order);
      }
    });
    if (!isEmpty(this.state.mainImage))
      order = Math.max(this.state.mainImage.imageName, order);
    return order + 1;
  }

  render() {
    const {makes, models} = this.props;
    if (!makes || !models) return null;
    let modelsByMake = this.getModels();
    const {car, imageIndexSelected, mainImageIndex, sliderView} = this.state;
    console.log("In Update car: ", car);
    return (
      <div className="car-selected" style={this.state.isLoading ? {"cursor": "progress"} : null}>
        <Dropzone onDrop={(files) => this.onDropFile(files)} accept="image/*">
          {({getRootProps, getInputProps}) => (
            <div className="d-inline-flex" {...getRootProps()}>
              <input {...getInputProps()} />
              <button className="btn btn-upload">Upload Pictures</button>
            </div>
          )}
        </Dropzone>
        <div className="d-inline-flex float-right mr-1">
          {this.state.expandModal ?
            <i className="fas fa-compress mr-3" onClick={this.compressCarSelected}></i>
            : <i className="fas fa-expand-arrows-alt mr-3" onClick={this.expandCarSelected}></i>
          }
          <button className="btn-cancel btn-car" onClick={this.props.hideEditListing}> Cancel</button>
          <button className="btn-save btn-car ml-2" onClick={this.save}> SAVE</button>
        </div>
        <div className="images-car d-inline-flex w-100 mt-3">
          {sliderView ?
            <div className="w-100">
            <div className="float-right img-principal">
              <div className="image-options-container">
                <div className="image-options">

                  <div className="custom-checkbox d-inline-flex">
                    <div className="checkbox-square d-inline-flex"
                         onClick={() => this.setState({mainImageIndex: imageIndexSelected})}
                         style={{"backgroundColor": (mainImageIndex === imageIndexSelected ? "#17DC00" : "#FFFFFF")}}/>
                    <label className="d-inline-flex" htmlFor="customCheck1"
                           onClick={() => this.setState({mainImageIndex: imageIndexSelected})}>Select as Main
                      Image</label>
                  </div>
                  <i className="remove fas fa-trash delete-icon"
                     onClick={() => this.deleteImage(imageIndexSelected)}></i>
                </div>
              </div>
              <img
                src={car.mainImageUrl ? (imageIndexSelected > 0 ? (car.imagesUrls[imageIndexSelected - 1].preview || car.imagesUrls[imageIndexSelected - 1]) : (!isEmpty(this.state.mainImage) ? this.state.mainImage.preview : car.mainImageUrl)) : pathServer.PATH_IMG + "car.png"}
                alt="" onClick={() => {if(imageIndexSelected!=car.imagesUrls.length){this.setState({imageIndexSelected: imageIndexSelected+1})}}}
                style={{cursor:"pointer"}}
              />
            </div>
            <div className="float-left img-items ml-2">
              {!this.state.expandModal ? this.showImagesSlider() : this.showImagesGrid(8)}
            </div>
          </div> : this.showImagesGrid(8)}
        </div>
        <div className="separator"></div>
        <div className="form-inputs ">
          <div className="d-inline-flex w-100">
            <div className="dropdown line-0">
              <label htmlFor="">Make</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.handleMakeChange(e.target.value)}>
                {makes.map((make, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.makeId === make.id}
                                  value={make.id}>{make.name}</option>
                  );
                })}
              </select>
            </div>
            <div className="dropdown line-0">
              <label htmlFor="">Model</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, modelId: e.target.value}})}>
                {modelsByMake.map((model, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.modelId === model.id}
                                  value={model.id}>{model.name}</option>);
                })}
              </select>
            </div>
            <div className="dropdown line-0-1">
              <label htmlFor="">Year</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, year: e.target.value.toString()}})}>
                {this.years.map((year, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.year === year.toString()}
                                  value={year}>{year}</option>);
                })}
              </select>
            </div>
          </div>
          <div className="d-inline-flex w-100">
            <div className="dropdown line-0">
              <label htmlFor="">Int. Condition</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, interiorCondition: e.target.value}})}>
                {optionCondition.map((interiorCondition, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.interiorCondition === interiorCondition.id}
                                  value={interiorCondition.id}>{interiorCondition.value}</option>);
                })}
              </select>
            </div>
            <div className="dropdown line-0">
              <label htmlFor="">Ext. Condition</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, exteriorCondition: e.target.value}})}>
                {optionCondition.map((exteriorCondition, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.exteriorCondition === exteriorCondition.id}
                                  value={exteriorCondition.id}>{exteriorCondition.value}</option>);
                })}
              </select>
            </div>
            <div className="dropdown line-0-1">
              <label htmlFor="">Transmission</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, transmission: e.target.value}})}>
                {optionTransmission.map((transmission, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.transmission === transmission.id}
                                  value={transmission.id}>{transmission.value}</option>);
                })}
              </select>
            </div>
          </div>
          <div className="d-inline-flex w-100">
            <div className="dropdown line-2">
              <label htmlFor="">VIN Number</label>
              <input type="text" className="rounded-input"
                     value={car.vin ? car.vin : ""}
                     onChange={(e) => this.setState({car: {...this.state.car, vin: e.target.value.toUpperCase()}})}/>
            </div>
            <div className="dropdown line-1">
              <label htmlFor="">Int. Color</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, interiorColor: e.target.value}})}>
                {optionColor.map((interiorColor, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.interiorColor === interiorColor.id}
                                  value={interiorColor.id}>{interiorColor.value}</option>);
                })}
              </select>
            </div>
            <div className="dropdown line-1">
              <label htmlFor="">Ext. Color</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, exteriorColor: e.target.value}})}>
                {optionColor.map((exteriorColor, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.exteriorColor === exteriorColor.id}
                                  value={exteriorColor.id}>{exteriorColor.value}</option>);
                })}
              </select>
            </div>
          </div>
          <div className="d-inline-flex w-100">
            <div className="dropdown line-2">
              <label htmlFor="">Mileage</label>
              <input type="text" className="rounded-input"
                     name="mileage"
                     value={car.mileage ? car.mileage.replace(/[^0-9]+/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','): ""}
                     onChange={(e) => this.setState({car: {...this.state.car, mileage: e.target.value.replace(/[^0-9]+/g, '')
                                                                                                          .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}})}/>
            </div>
            <div className="dropdown line-1">
              <label htmlFor="">Body type</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, body: e.target.value}})}>
                {optionBody.map((bodyType, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.body === bodyType.id}
                                  value={bodyType.id}>{bodyType.value}</option>);
                })}
              </select>
            </div>
            <div className="dropdown line-1">
              <label htmlFor="">Title</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, title: e.target.value}})}>
                {optionTitle.map((title, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.title === title.id}
                                  value={title.id}>{title.value}</option>);
                })}
              </select>
            </div>
          </div>
          <div className="d-inline-flex w-100">
            <div className="dropdown line-2">
              <label htmlFor="">Trim</label>
              <input type="text" className="rounded-input"
                     name="mileage"
                     value={car.trim ? car.trim : ""}
                     onChange={(e) => this.setState({car: {...this.state.car, trim: e.target.value}})}/>
            </div>
            <div className="dropdown line-1">
              <label htmlFor="">Status</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, status: e.target.value}})}>
                {optionCarStatus.map((carStatus, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.status === carStatus.id}
                                  value={carStatus.id}>{carStatus.value}</option>);
                })}
              </select>
            </div>

            <div className="dropdown line-3">
              <label htmlFor="">Fuel type</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, fuel: e.target.value}})}>
                {optionFuel.map((fuel, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.fuel === fuel.id}
                                  value={fuel.id}>{fuel.value}</option>);
                })}
              </select>
            </div>
          </div>
          <div className="d-inline-flex w-100">
            <div className="dropdown line-2">
              <label htmlFor="">Price</label>
              <input type="text" className="rounded-input"
                     value={car.price ? moneySign + car.price :moneySign + ""}
                     onChange={(e) => this.setState({car: {...this.state.car, price: e.target.value.replace(',', '')
                                                                                                         .replace('$', '')
                                                                                                         .replace(/[^0-9]+/g, '')
                                                                                                         .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}})}/>
            </div>
            <div className="dropdown line-1">
              <label htmlFor="">Cylinders</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, cylinder: parseInt(e.target.value)}})}>
                {optionCylinder.map((cylinder, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.cylinder == cylinder.id}
                                  value={cylinder.id}>{cylinder.value}</option>);
                })}
              </select>
            </div>
            <div className="dropdown line-1">
              <label htmlFor="">Drive</label>
              <select className="dropdown-selector"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onChange={(e) => this.setState({car: {...this.state.car, drive: e.target.value}})}>
                {optionDrive.map((drive, index) => {
                  return (<option key={index}
                                  className="dropdown-item"
                                  selected={car.drive === drive.id}
                                  value={drive.id}>{drive.value}</option>);
                })}
              </select>
            </div>
          </div>
          <div className="dealer-description w-100">
            <label htmlFor="">Dealer Description</label>
            <textarea className="rounded-textarea"
                      placeholder="Description"
                      rows="8"
                      cols="50"
                      wrap="hard"
                      value={car.description ? car.description : ""}
                      onChange={(e) => this.setState({car: {...this.state.car, description: e.target.value}})}>
            </textarea>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    makes: state.makes,
    models: state.models
  }
};


export default connect(mapStateToProps, {loadMakeAndModel})(LayoutCarSelected)