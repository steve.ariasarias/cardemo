import React, {Component} from 'react'
import LayoutHeader from "../common/LayoutHeader.jsx";
import {pathServer} from "../../../../Constants";
import LayoutFilter from "./LayoutFilter.jsx";
import LayoutCarSelected from "./LayoutCarSelected.jsx";
import {searchInventoryAdminApi} from "../../../../Api";
import {loadMakeAndModel} from "../../../../action";
import {connect} from "react-redux";
import Moment from 'moment';

class LayoutListing extends Component {

  constructor() {
    super();
    this.state = {
      listingCars: [],
      listingSearchCars: [],
      carFind:"",
      carSelected: false,
      idSelected: -1,
      indexSelected: -1,
      showModalCar: false,
      rowsNumber: 5,
      pageNumber: 1,
      isLoading:true,
      panelSelected: false,
      panelSelectedExpanded:false
    };
    this.prevPage = this.prevPage.bind(this);
    this.nextPage = this.nextPage.bind(this);
    this.openCarSelected = this.openCarSelected.bind(this);
    this.showInfoPanel = this.showInfoPanel.bind(this);
    this.hideEditListing = this.hideEditListing.bind(this);
    this.changeStyleListing= this.changeStyleListing.bind(this);
    this.refreshStyleListing= this.refreshStyleListing.bind(this);
    this.updateCar = this.updateCar.bind(this);
    this.searchCarListing=this.searchCarListing.bind(this);
    this.orderData=this.orderData.bind(this);
    this.filterData= this.filterData.bind(this);
    this.reloadData=this.reloadData.bind(this);
    this.setValuesList=this.setValuesList.bind(this);
  }

  getFormatedDate( date ) {
    const newDate = new Date(date);
    return ('0' + (newDate.getMonth() + 1)).slice(-2) + '/' + ('0' + newDate.getDate()).slice(-2) + '/' + newDate.getFullYear();
  }

  setRenewedDate( date ) {
    const newDate = new Date(date);
    const twoWeeksAfterDate = new Date(+ newDate + 12096e5);
    const year = twoWeeksAfterDate.getFullYear();
    const month = ('0' + (twoWeeksAfterDate.getMonth() + 1)).slice(-2);
    const day = ('0' + twoWeeksAfterDate.getDate()).slice(-2);
    const time = newDate.getHours() + ":" + ('0' + newDate.getMinutes()).slice(-2) + ":" + ('0'+newDate.getSeconds()).slice(-2);

    return  year +'-' + month + '-' + day + " " + time;
  }

  setExpirationDate( date ) {
    const newDate = new Date(date);
    newDate.setMonth(newDate.getMonth() + 1);
    const year = newDate.getFullYear();
    const month = ('0' + (newDate.getMonth() + 1)).slice(-2);
    const day = ('0' + newDate.getDate()).slice(-2);
    const time = newDate.getHours() + ":" + ('0' + newDate.getMinutes()).slice(-2) + ":" + ('0'+newDate.getSeconds()).slice(-2);
    return  year +'-' + month + '-' + day + " " + time;
  }

  componentDidMount() {
    this.setState({isLoading: true});
    let that = this
    setTimeout(function() {searchInventoryAdminApi({query:"",offset:0,size:1000}).then((response) => {
      let cars = that.parseDataCars(response.data.cars);
      console.log("response",response);
      this.setState({listingCars: cars, listingSearchCars : cars, isLoading: false});
    }).catch((error) => {
      console.log(error)
    });}.bind(this),2000);
  }

  parseDataCars(carsDB){
    let cars = [];
    let photosQuantity = 0;
    carsDB.forEach(car => {
      photosQuantity = (car.imagesUrls || []).length+1;
      const renewedDate = this.setRenewedDate(car.createdDate);
      const expirationDate = this.setExpirationDate(car.createdDate);
      cars.push({
        id: car.id,
        elasticSearchId:car.elasticSearchId,
        makeId: car.makeId,
        modelId: car.modelId,
        year: car.year? car.year:null,
        vin: car.vin?car.vin:null,
        mileage: car.mileage? car.mileage:null,
        price: car.price?car.price:null,
        createdDate: car.createdDate,
        renewedDate: renewedDate.toString(),
        expirationDate: expirationDate.toString(),
        description: car.description? car.description:null,
        interiorColor: car.interiorColor?car.interiorColor:null,
        exteriorColor: car.exteriorColor?car.exteriorColor:null,
        interiorCondition: car.interiorCondition?car.interiorCondition:null,
        exteriorCondition: car.exteriorCondition?car.exteriorCondition:null,
        body: car.body?car.body:null,
        title: car.title?car.title:null,
        fuel: car.fuel?car.fuel:null,
        cylinder: car.cylinder,
        photos: (photosQuantity > 0 ?
          photosQuantity + (photosQuantity < 2 ? ' picture' : ' pictures') : '0 pictures'),
        drive: car.drive?car.drive:null,
        trim: car.trim?car.trim:null,
        status: car.status?car.status:null,
        transmission: car.transmission?car.transmission:null,
        imagesUrls: car.imagesUrls ? car.imagesUrls :[],
        duration: '3 days',
        stars: car.stars,
        view: car.view,
        //mainImageUrl: (car.mainImageUrl && car.mainImageUrl !== "") ? car.mainImageUrl : (car.imagesUrls.length === 1) ? car.imagesUrls[0] : car.mainImageUrl
        mainImageUrl: car.mainImageUrl
      });
    });
    return cars
  }

  componentWillMount() {
    let {makes, models} = this.props;
    if(!makes || !models){
      this.props.loadMakeAndModel()
    }

  }
  prevPage() {
    if (this.state.pageNumber !== 1) {
      this.setState({pageNumber: this.state.pageNumber - 1});
    }
  }

  nextPage() {
    if (this.state.pageNumber != Math.ceil(this.state.listingCars.length / (this.state.rowsNumber))) {
      this.setState({pageNumber: this.state.pageNumber + 1});
    }
  }

  setValuesList(data){
    this.setState({rowsNumber: data})
  }

  filterData(type){
    let {listingCars}= this.state;
    let filterCars=[];
    let today= new Date();
    if(type === "renewed"){
      listingCars.map(function (car,index) {
        if(car.renewedDate){
          let renewedDate= new Date(car.createdDate);
          var timeDiff = Math.abs(today.getTime() - renewedDate.getTime());
          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
          if(diffDays > 14 && diffDays < 30){
            filterCars.push(car);
          }
        }
      })
    } else if( type === "expire") {
      listingCars.map(function (car,index) {
        if(car.expirationDate){
          let expirationDate= new Date(car.createdDate);
          var timeDiff = Math.abs(today.getTime() - expirationDate.getTime());
          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
          if(diffDays >= 30 && diffDays <= 33){
            filterCars.push(car);
          }
        }
      })
    }
    this.setState({listingSearchCars: filterCars});
  }

  orderData(type){
    let {listingSearchCars}= this.state;
    let listSortCars=[];
    let listFailCars=[];
    listingSearchCars.map(function (car) {
        if(car.createdDate)
          listSortCars.push(car);
        else
          listFailCars.push(car);
    });
    if(type === "asc"){
      listSortCars.sort(function (a, b) {
          a=new Date(a.createdDate);
          b=new Date(b.createdDate);
          return a - b;
       });
    }else if ( type === "desc") {
      listSortCars.sort(function (a, b) {
          a = new Date(a.createdDate);
          b = new Date(b.createdDate);
          return b - a;
      });
    }
    listFailCars.map(function (car) {
      listSortCars.push(car);
    })
    this.setState({listingSearchCars:listSortCars });
  }

  reloadData(){
    const {listingCars}= this.state;
    this.setState({listingSearchCars:listingCars });
  }

  searchCarListing(data,evt){
    let {listingCars,listingSearchCars} = this.state;
    //let newListing =[];
    let valueFind="";
    if(data === ""){
      this.setState({listingSearchCars: listingCars});
    }else{
      /*listingCars.find((car) => {
        let year=car.year+' '
        let price= car.price+ ' ';
        let vin=car.vin+ ' ';
        if(car.makeId.toLowerCase().indexOf(data.toLowerCase()) !== -1 || car.modelId.toLowerCase().indexOf(data.toLowerCase()) !== -1 || year.indexOf(data) !== -1 || vin.indexOf(data) !== -1 || price.indexOf(data) !== -1 ){
          newListing.push(car);
          // valueFind=car.makeId;
        }
      });*/
      searchInventoryAdminApi({keyword:data}).then((response) => {
        let cars = this.parseDataCars(response.data.cars);
        this.setState({listingSearchCars: cars});
      });
    }
  }

  updateCar(car){
    let {listingSearchCars} = this.state;
    let carIndex = listingSearchCars.findIndex(carElement => carElement.id === car.id);
    let allCars =  listingSearchCars;
    allCars[carIndex] = car;
    this.setState({listingSearchCars: allCars, carSelected: false, idSelected:-1, panelSelected: false});
  }

  hideEditListing() {
    this.setState({carSelected: false, idSelected:-1,panelSelected: false,panelSelectedExpanded:false,showModalCar:false});
  }

  openCarSelected(id, index){
    this.setState({carSelected:true, idSelected: id, indexSelected: index ,panelSelected: true});
  }

  changeStyleListing() {
    this.setState({showModalCar: true, panelSelectedExpanded: true, panelSelected: false});

  }

  refreshStyleListing() {
    this.setState({showModalCar: false, panelSelectedExpanded: false, panelSelected: true});
  }

  showInfoPanel(car){
    return(
      <div>
        {car && this.state.idSelected !== -1 ?
          <LayoutCarSelected hideEditListing={this.hideEditListing}  car={car} expandCar={this.changeStyleListing} updateCar={this.updateCar} compressCar={this.refreshStyleListing} />
        : <div className="content-initial text-center">
            <i className="fas fa-pencil-alt"/>
            <h4>Select a car<br/>to Edit</h4>
        </div>}
      </div>
    )
  }

  render() {
    let {listingSearchCars,carFind,panelSelected,panelSelectedExpanded,showModalCar} = this.state;
    const { makes,models} = this.props;
    let firstIndex = (this.state.rowsNumber ) * (this.state.pageNumber - 1);
    let lastIndex = firstIndex + (this.state.rowsNumber );
    if(!makes || !models) return null;

    return (
      <div className="admin-listing">
        <LayoutHeader carFind={carFind.length === 0 ? "" : carFind} searchCar={this.searchCarListing}  colorNav="header-admin" optionSelected="listing"/>
        <div className="bg-admin"/>
        {!this.state.isLoading ?
        <div className="content-admin">
          <div className="container-fluid">
            <div className="row">
              <div className={" float-left d-inline-flex principal-panel " + (panelSelected ? "principal-panel-edit" : "") + (showModalCar ? " principal-panel-expanded " : "")}>
                <LayoutFilter setValuesList={this.setValuesList} reloadData={this.reloadData} filterData={this.filterData}
                              orderData={this.orderData} prevPage={this.prevPage} pageNumber={this.state.pageNumber}
                              listingCars={listingSearchCars} rowsNumber={this.state.rowsNumber} nextPage={this.nextPage}/>
                <div className={"row list-search-item " + (panelSelected ? "list-search-item-edit":null) }>
                  {listingSearchCars.slice(firstIndex,lastIndex).map(function(car,index) {
                    const createdDate = this.getFormatedDate(car.createdDate);
                    const renewedDate = this.getFormatedDate(car.renewedDate);
                    const expirationDate = this.getFormatedDate(car.expirationDate);

                    let now= new Date();
                    const makeName = (makes.find(make => make.id === car.makeId) || {}).name;
                    const modelName = (models.find(model => model.id === car.modelId) || {}).name;
                    return (
                      <div className="search-item col-lg-12">
                        <div className="d-inline-flex w-100">
                          <div className="float-left option-buttons-left">
                            <img className="d-flex align-self-center mr-3 car-img"
                                 src={car.mainImageUrl ? car.mainImageUrl : (pathServer.PATH_IMG + "car.png")}
                                 alt="car image"/>
                            <b>{car.photos}</b>
                          </div>
                          <div className={ "float-right option-buttons "+ (panelSelected ? " option-buttons-edit":null)}>
                            <div className={ this.state.showModalCar ? "title" :"d-inline-flex title"}>
                              <b className="car-name">{`${makeName} ${modelName}`}</b>
                              <b className="model">VIN: {car.vin}</b>
                              <div className="valoration">
                                <i className="far fa-calendar"/> <b>
                                {Moment(now, "YYYY-MM-DD").diff(Moment(car.createdDate, "YYYY-MM-DD"), 'd')} days</b>
                                <i className="ml-2 fas fa-star"/> <b>{car.stars}</b>
                                <i className="ml-2 fas fa-eye"/> <b>{car.view}</b>
                              </div>
                            </div>
                            {this.state.showModalCar ?
                              <h6 className="mt-1">Created: {createdDate} <br/> Renewed: {renewedDate}
                                <br/> Expires: {expirationDate}</h6>
                              :
                              <h6 className="mt-1">Created: {createdDate} | Renewed: {renewedDate} |
                                Expires: {expirationDate}</h6>
                            }
                            <div className="description">
                              <h5>{car.description ? (car.description.slice(0,200)+(car.description.length > 200 ? '...':'')):
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores at commodi dolore,  dolorem ducimus eaque et fuga illo ipsum quam quisquam ratione sit vitae voluptas! Ad atque error maiores!...'}
                              <a hidden={car.description && (car.description.length <= 200)} href="#">more</a></h5>
                            </div>

                            {this.state.idSelected === -1 ?
                            <div className="float-left actions pt-2">
                              <i className="remove fas fa-trash mr-3" ></i>
                              <i className="edit fas fa-pencil-alt" onClick={() => this.openCarSelected(car.id, firstIndex + index)}></i>
                            </div> :  this.state.idSelected !== car.id  &&
                              <div className="float-right actions pt-2">
                                <i className="remove fas fa-trash mr-3" ></i>
                                <img className="arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"}
                                alt="" onClick={() => this.openCarSelected(car.id, firstIndex + index)}/>
                              </div>
                            }

                            {this.state.idSelected === car.id  &&
                              <div className="float-right actions-edited pt-2">
                                <i className="remove fas fa-trash mr-3" ></i>
                                <button className="btn-editCar"> Editing ... <i className="fas fa-arrow-right"/></button>
                              </div>
                            }
                          </div>
                        </div>
                      </div>
                    )
                  }, this)
                  }
                </div>
                <div className={"change-page text-center justify-content-center " + (!panelSelected ? " change-page-edit" :null)} >
                  <a className="btn previus-page" onClick={this.prevPage}>PREVIOUS</a>
                  <a className="btn next-page" onClick={this.nextPage}>NEXT</a>
                </div>
              </div>
              {panelSelected || panelSelectedExpanded?
              <div className={this.state.showModalCar ? "principal-edit-expanded" : "principal-edit"}>
                <div className="principal-panel2">
                  {this.showInfoPanel(this.state.listingSearchCars[this.state.indexSelected])}
                </div>
              </div> : null}
            </div>
          </div>
        </div>:null}
      </div>
    )
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    makes: state.makes,
    models:state.models
  }
};


export default connect(mapStateToProps, {loadMakeAndModel})(LayoutListing)
