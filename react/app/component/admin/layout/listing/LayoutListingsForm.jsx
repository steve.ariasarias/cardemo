import React, {Component} from 'react'
import LayoutHeader from "../common/LayoutHeader.jsx";
import Image from "../common/Image.jsx";
import Dropzone from 'react-dropzone';
import {
  pathServer,
  PERSON_MODE_CODE,
  optionFuel,
  optionDrive,
  optionColor,
  optionCylinder,
  optionTitle,
  optionTransmission,
  optionCondition,
  optionCarStatus,
  optionBody, moneySign, CURRENT_YEAR, CAR_YEAR_START
} from "../../../../Constants";
import {browserHistory, Link} from "react-router";
import {
  PortalEndpoints,
  saveDocument,
  searchInventoryApi,
  getDataFromVinNumber,
  uploadCarImage,
  saveListing, searchInventoryAdminApi, getInfoAccountLogging
} from "../../../../Api";
import {connect} from "react-redux";
import {loadMakeAndModel} from "../../../../action";
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';
import flow from 'lodash/flow';

const update = require('immutability-helper');

class LayoutListingsForm extends Component {

  constructor() {
    super();
    this.state = {
      sectionShowed: 0,
      chosenFiles: [],
      acceptedFirstTerms: false,
      isTextSeller: false,
      isUseZoomInfo: false,
      isUseSellerInfo: false,
      account: {},
      car: {},
      location: {},
      maxImageName: 0,
      isLoading: false,
      testFillBox: false,
      emptyBox: {}
    };
    this.states = ['Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Federated States of Micronesia', 'Florida', 'Georgia', 'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Island', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
    this.sellerTypes = ["Dealership", "For Sale By Owner"];
    this.renderInput = this.renderInput.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderDropdown = this.renderDropdown.bind(this);
    this.renderMoreOrLessIcon = this.renderMoreOrLessIcon.bind(this);
    this.onDropFile = this.onDropFile.bind(this);
    this.save = this.save.bind(this);
    this.getModels = this.getModels.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
    this.getFormattedPhone = this.getFormattedPhone.bind(this);
    this.changeUseInfoFiled = this.changeUseInfoFiled.bind(this);
    this.getDataFromVinNumber = this.getDataFromVinNumber.bind(this);
    this.moveImage = this.moveImage.bind(this);
    this.emptyBox = this.emptyBox.bind(this);
    this.emptyBoxFinal = this.emptyBoxFinal.bind(this);
    this.onClickSave = this.onClickSave.bind(this);
  }

  componentWillUnmount() {
  }

  componentWillMount() {
    let {makes, models} = this.props;
    if (!makes || !models) {
      this.props.loadMakeAndModel().then((response) => {
        console.log('response = ' + response);
      });
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    let {makes, models} = nextProps;
    if (makes && models) {
      let makesNames = [];
      makes.map(function (make, index) {
        makesNames.push(make.name)
      });
      let years = [...Array((CURRENT_YEAR - CAR_YEAR_START)+2).keys()].map(x => x + CAR_YEAR_START);
      this.setState({
        car: {
          makeId: makes[0].id,
          modelId: models[0].id,
          year: years[0],
          title: optionTitle[0].id,
          cylinder: optionCylinder[0].id,
          drive: optionDrive[0].id,
          transmission: optionTransmission[0].id,
          interiorCondition: optionCondition[0].id,
          exteriorCondition: optionCondition[0].id,
          interiorColor: optionColor[0].id,
          exteriorColor: optionColor[0].id,
          status: optionCarStatus[0].id,
          fuel: optionFuel[0].id,
          body: optionBody[0].id
        },
        location: {state: this.states[0]}, account: {typeSeller: this.sellerTypes[0]}
      });
      this.makes = makesNames;
      this.years = years;
    }

  }

  componentDidMount() {
    const {makes, models} = this.props;
    if (!makes || !models) {
      this.props.loadMakeAndModel().then((response) => {
        console.log('response = ' + response);
      });
    }
    let makesNames = [];
    if (makes) {
      makes.map(function (make, index) {
        makesNames.push(make.name)
      });
      let years = [...Array((CURRENT_YEAR - CAR_YEAR_START)+2).keys()].map(x => x + CAR_YEAR_START);
      this.setState({
        car: {
          makeId: makes[0].id,
          modelId: models[0].id,
          year: years[0],
          title: optionTitle[0].id,
          cylinder: optionCylinder[0].id,
          drive: optionDrive[0].id,
          transmission: optionTransmission[0].id,
          interiorCondition: optionCondition[0].id,
          exteriorCondition: optionCondition[0].id,
          interiorColor: optionColor[0].id,
          exteriorColor: optionColor[0].id,
          status: optionCarStatus[0].id,
          fuel: optionFuel[0].id,
          body: optionBody[0].id
        },
        location: {state: this.states[0]}, account: {typeSeller: this.sellerTypes[0]}
      });
      this.makes = makesNames;
      this.years = years;
    }
  }

  onDropFile(files) {
    let {chosenFiles, maxImageName} = this.state;
    let that = this;
    let order = chosenFiles.length + 1;
    let indexImageSelected = null;
    if (chosenFiles.length === 0)
      indexImageSelected = 0;

    files.forEach((file, index) => {
      maxImageName++;
      let blob = new Blob([file], {type: 'image/jpeg'});
      let objectURL = URL.createObjectURL(blob);
      let fileTemp = new File([blob], maxImageName.toString(), {type: blob.type});
      fileTemp.preview = objectURL;
      fileTemp.order = order + index;
      chosenFiles.push(fileTemp);
      that.setState({chosenFiles: chosenFiles, maxImageName: maxImageName})
    });
  }

  renderMoreOrLessIcon(isMoreOptionShown) {
    return (isMoreOptionShown ?
      <svg fill="white" width="6" height="6" viewBox="0 0 24 24">
        <path d="M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z"/>
      </svg> :
      <svg width="8" height="6" id="Layer_1" fill="white" viewBox="0 0 30 6" version="1.1">
        <rect height="6" width="30"/>
      </svg>);
  }

  emptyBox(sectionShowed){
    let {car, emptyBox, maxImageName} = this.state;
    let boxFill = true;
    if (car.vin && car.vin.length===17){
      emptyBox["vin"] = "";
    } else{
      boxFill = false;
      emptyBox["vin"] = "Please fill VIN Number";
    }
    if (car.mileage && car.mileage.length > 0){
      emptyBox["mil"] = "";
    } else{
      boxFill = false;
      emptyBox["mil"] = "Please fill Mileage";
    }
    if (car.trim && car.trim.length > 0){
      emptyBox["tri"] = "";
    } else{
      boxFill = false;
      emptyBox["tri"] = "Please fill Trim";
    }
    if (car.price && car.price.length > 0){
      emptyBox["pri"] = "";
    } else{
      boxFill = false;
      emptyBox["pri"] = "Please fill Price";
    }
    if (car.modelId && car.modelId!=="15157"){
      emptyBox["mod"] = "";
    } else{
      boxFill = false;
      emptyBox["mod"] = "Please select Model";
    }
    if (car.description && car.description.length > 0){
      emptyBox["des"] = "";
    }
    else{
      boxFill = false;
      emptyBox["des"] = "Please fill Description";
    }
    if (maxImageName && maxImageName > 0){
      emptyBox["img"] = "";
    } else{
      boxFill = false;
      emptyBox["img"] = "Please upload Image";
    }
    if (boxFill){
      this.setState({sectionShowed: sectionShowed, emptyBox: emptyBox});
      return false;
    } else{
      this.setState({emptyBox: emptyBox});
      return true;
    }
  }

  emptyBoxSecond(sectionShowed){
    let {location, emptyBox} = this.state;
    let boxFill = true;
    if (location.address1 && location.address1.length > 0){
      emptyBox["add"] = "";
    }
    else{
      boxFill = false;
      emptyBox["add"] = "Please fill Address 1";
    }
    if (location.city && location.city.length > 0){
      emptyBox["cit"] = "";
    }
    else{
      boxFill = false;
      emptyBox["cit"] = "Please fill City";
    }
    if (location.zipCode && location.zipCode.length > 0){
      emptyBox["zip"] = "";
    }
    else{
      boxFill = false;
      emptyBox["zip"] = "Please fill ZIP Code";
    }
    if (boxFill){
      this.setState({sectionShowed: sectionShowed, emptyBox: emptyBox});
      return false;
    } else {
      this.setState({emptyBox: emptyBox});
      return true;
    }
  }

  emptyBoxFinal(sectionShowed){
    let {account, emptyBox,acceptedFirstTerms} = this.state;
    let boxFill = true;
    if(account.typeSeller==="Dealership") {
      if (account.dealerName && account.dealerName.length > 0) {
        emptyBox["dea"] = "";
      } else {
        boxFill = false;
        emptyBox["dea"] = "Please fill Dealer Name";
      }
      if (account.dealerNumber && account.dealerNumber.length > 0) {
        emptyBox["num"] = "";
      } else {
        boxFill = false;
        emptyBox["num"] = "Please fill Dealer Number";
      }
    }else{
      emptyBox["dea"] = "";
      emptyBox["num"] = "";
    }
    if (account.firstName && account.firstName.length > 0){
      emptyBox["fir"] = "";
    } else{
      boxFill = false;
      emptyBox["fir"] = "Please fill First Name";
    }
    if (account.lastName && account.lastName.length > 0){
      emptyBox["las"] = "";
    } else{
      boxFill = false;
      emptyBox["las"] = "Please fill Last Name";
    }
    if (account.phone && account.phone.length > 0){
      emptyBox["pho"] = "";
    } else{
      boxFill = false;
      emptyBox["pho"] = "Please fill Phone Number";
    }
    if (account.email && this.isValidEmail(account.email)){
      emptyBox["ema"] = "";
    } else{
      boxFill = false;
      emptyBox["ema"] = "Please fill Email Address";
    }
    if (acceptedFirstTerms){
      emptyBox["con"] = "";
    } else{
      boxFill = false;
      emptyBox["con"] = "You must accept the conditions";
    }
    if (boxFill){
      this.setState({sectionShowed: sectionShowed, emptyBox: emptyBox});
      return false;
    } else{
      this.setState({emptyBox: emptyBox});
      return true;
    }
  }

  onClickSave(){
    if(this.emptyBox(0)){
      this.setState({sectionShowed:0})
    }else if(this.emptyBoxSecond(1)){
      this.setState({sectionShowed:1})
    }else if(this.emptyBoxFinal(2)){
      this.setState({sectionShowed:2})
    }else if (!this.emptyBox(0) && !this.emptyBoxSecond(1) && !this.emptyBoxFinal(2)) {
      this.save();
    }else{
      console.log("please check emptyBox ",this.state.emptyBox)
    }
  }

  renderInput(inputData) {
    let {emptyBox} = this.state;
    return (<div className="text-input-panel">
      <label className="row" hidden={inputData.hidden}>{inputData.label}</label>
      <input className="text-input currency"
             min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100"
             defaultValue={inputData.defaultValue}
             type={inputData.type} maxLength={inputData.maxLength} value={inputData.value}
             placeholder={inputData.placeholder}
             onChange={inputData.onChange} hidden={inputData.hidden}/>
      {inputData.imagePath ?
        <img className="question-icon" src={pathServer.PATH_IMG + inputData.imagePath}/> : null}
      <spa className="message-error-create-listing">{(inputData.label==="Address 2")?null:
        ((inputData.label==="Dealer Number")?emptyBox["num"]:emptyBox[inputData.label.substring(0,3).toLowerCase()])}</spa>
    </div>);
  }

  renderDropdown(dropdownData) {
    let {emptyBox} = this.state;
    return (
      <div className="dropdown-panel">
        <label className="row">{dropdownData.label}</label>
        <select className="dropdown-selector text-left"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onChange={dropdownData.onChange}
                value={dropdownData.value} disabled={dropdownData.disabled}>
          {dropdownData.options ?
            dropdownData.options.map(function (option, index) {
              return (<option className="dropdown-item">{option}</option>);
            }) : null
          }
          {dropdownData.optionsArray ? dropdownData.optionsArray.map((option, index) => {
            return (<option key={index}
                            className="dropdown-item"
                            selected={dropdownData.carAttribute===option.id}
                            value={option.id}>{option.value}</option>);
          }): null}
        </select>
        <spa className="message-error-create-listing">{emptyBox[dropdownData.label.substring(0,3).toLowerCase()]}</spa>
      </div>
    );
  }

  getFormattedPhone(phoneNumber) {
    let formattedPhone = phoneNumber;
    if (phoneNumber.length >= 1)
      formattedPhone = '(' + phoneNumber.substring(0);
    if (phoneNumber.length > 3)
      formattedPhone = formattedPhone.substring(0, 4) + ') ' + formattedPhone.substring(4);
    if (phoneNumber.length > 6)
      formattedPhone = formattedPhone.substring(0, 9) + '-' + formattedPhone.substring(9);
    return formattedPhone;
  }

  capitalize(sentence) {
    sentence = sentence.toLowerCase().split(' ').map(function (word) {
      return word.replace(word[0], word[0].toUpperCase());
    });
    return sentence.join(' ');
  }

  renderRow(inputDataArray, width = 100, isDescriptionTextAreaShown = false) {
    let {car, emptyBox} = this.state;
    return (
      <div className={"d-inline-flex "+(isDescriptionTextAreaShown ? "last-row":"")} style={{"width": width + "%", "marginBottom": "4px"}}>
        {inputDataArray.map(function (inputData, index) {
          return (inputData.placeholder ? this.renderInput(inputData) : this.renderDropdown(inputData));
        }.bind(this))}
        {isDescriptionTextAreaShown ?
          <div className="dealer-description" style={{width: "200%"}}>
            <label htmlFor="">Description</label>
            <textarea className="rounded-textarea"
                      placeholder="Description"
                      rows="8"
                      wrap="hard"
                      value={car.description ? car.description : ""}
                      onChange={(e) =>{
                        (e.target.value.length>0)?emptyBox["des"]="":emptyBox["des"]="Please fill Description";
                        this.setState({car: {...car, description: e.target.value}})}
                      }>
                      </textarea>
            <spa className="message-error-create-listing">{emptyBox["des"]}</spa>
          </div> : null}
      </div>
    );
  }

  getModels() {
    let models = ["Select one"];
    this.props.models.filter(model => model.makeId === this.state.car.makeId).map(function (model, index) {
      models.push(model.name)
    });
    return models;
  }

  getDataFromVinNumber(vin) {
    getDataFromVinNumber(vin).then((response) => {
      if (response.Results.find(element => element.Variable == 'Error Code').ValueId == 0) {
        let make = this.capitalize(response.Results.find(element => element.Variable == 'Make').Value);
        let model = this.capitalize(response.Results.find(element => element.Variable == 'Model').Value);
        let year = response.Results.find(element => element.Variable == 'Model Year').Value;
        let cylinders = response.Results.find(element => element.Variable == 'Engine Number of Cylinders').Value;
        let makeFound = this.props.makes.find(m => m.name === make);
        let modelFound = this.props.models.find(m => m.name === model && m.makeId === makeFound.id);
        this.setState({
          car: {
            ...this.state.car,
            makeId: makeFound.id,
            makeName: makeFound.name,
            modelId: modelFound.id,
            modelName: modelFound.name,
            year: ""+year.toString(),
            cylinder: cylinders
          }
        });
      }
    });
  }

  uploadFiles(files, carId) {
    if (files.length > 0) {
      uploadCarImage(carId, files, 1).then((response) => {
        this.setState({isLoading: false});
        console.log('SUCCESS - car updated');
        browserHistory.push('/admin/listing');
      }).catch(error => {
        console.error(error);
        this.setState({isLoading: false, showMessage: true});
      })
    }else{
      browserHistory.push('/admin/listing');
    }
  }

  moveImage(dragIndex, hoverIndex){
    const { chosenFiles } = this.state;
    const dragCard = chosenFiles[dragIndex];
      this.setState(
        update(this.state, {
          chosenFiles: {
      $splice: [[dragIndex, 1], [hoverIndex, 0, dragCard]],
        },
    }),
    )
  }

  save() {
    this.setState({isLoading: true});
    let {car, account, isUseZoomInfo, isTextSeller, location, isUseSellerInfo} = this.state;
    const today = new Date();
    const twoWeeksAfterDate = new Date(Date.now() + 12096e5);
    const createdDate = ('0' + (today.getMonth() + 1)).slice(-2) + '/' + ('0' + today.getDate()).slice(-2) + '/' + today.getFullYear();
    const renewedDate = ('0' + (twoWeeksAfterDate.getMonth() + 1)).slice(-2) + '/' + ('0' + twoWeeksAfterDate.getDate()).slice(-2) + '/' + twoWeeksAfterDate.getFullYear();
    today.setMonth(today.getMonth() + 1);
    const expirationDate = ('0' + (today.getMonth() + 1)).slice(-2) + '/' + ('0' + today.getDate()).slice(-2) + '/' + today.getFullYear();
    car = {
      ...car,
      createdDate: createdDate,
      renewedDate: renewedDate,
      expirationDate: expirationDate,
      mileage: (car.mileage!=null && car.mileage!=="") ? car.mileage.replace(',', ''): ''
    };
    account = {
      ...account,
      isUseZoomInfo: isUseZoomInfo,
      isTextSeller: isTextSeller
    };
    location = {
      ...location,
      isUseSellerInfo: isUseSellerInfo,
    };
    console.log("car",car);
    let carInfo = {
      vin: car.vin,
      makeId: car.makeId,
      makeName: car.makeName,
      modelId: car.modelId,
      modelName: car.modelName,
      year: ""+car.year,
      price: (car.price!=null && car.price!=="")?car.price.replace(/,/g, ''):"",
      mainImageUrl: car.mainImageUrl ? car.mainImageUrl: "",
      description: car.description,
      mileage:(car.mileage!=null && car.mileage!=="")?car.mileage.replace(/,/g, ''):"",
      values:{
        "body":car.body,
        "cylinder":car.cylinder,
        "drive": car.drive,
        "exteriorColor":car.exteriorColor,
        "interiorColor":car.interiorColor,
        "exteriorCondition":car.exteriorCondition,
        "interiorCondition":car.interiorCondition,
        "fuel":car.fuel,
        "title":car.title,
        "transmission":car.transmission,
        "trim":car.trim,
        "status":car.status,
        "isTextSeller":account.isTextSeller
      }
    };
    saveListing({car:carInfo,contactInfo:account, sellerLocation:location}).then((response) => {
      console.log('SUCCESS - car saved ' + response.carId + ':::' + JSON.stringify(car));
      this.uploadFiles(this.state.chosenFiles, response.carId);
    }).catch((error) => {
      console.log('CAR DATA ERROR: ' + error)
    });
  }

  changeUseInfoFiled() {
    const emptyAccount = {
      dealerName: '',
      dealerNumber: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      typeSeller: 'Dealership'
    };
    let {isUseZoomInfo, account} = this.state;
    if (!isUseZoomInfo) {
      getInfoAccountLogging().then((response) => {
        let dataAccount = response.data.account;
        emptyAccount.firstName=dataAccount.firstName;
        emptyAccount.lastName= dataAccount.lastName;
        emptyAccount.address = dataAccount.address;
        emptyAccount.email= dataAccount.email;
        emptyAccount.phone= this.getFormattedPhone(dataAccount.phone.replace(/[^0-9]+/g, ''));
        emptyAccount.dealerName= dataAccount.dealerName;
        emptyAccount.dealerNumber= dataAccount.dealerNumber;
        this.setState({account: {...account,...emptyAccount},isUseZoomInfo:!isUseZoomInfo},()=>{
          this.emptyBoxFinal(2);
        });
      }).catch((error) => {
        console.log("error",error);
      });
    } else {
      this.setState({isUseZoomInfo: !this.state.isUseZoomInfo, account: emptyAccount});
    }
  }

  isValidEmail(value) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(value).toLowerCase());
  }

  verifyEmail(event){
    let {account, emptyBox} = this.state;
    if(!this.isValidEmail(event.target.value)) {
      emptyBox["ema"] = "";
      this.setState({account: {...account, email: event.target.value}})
    } else {
      emptyBox["ema"] = "Please fill Email";
    }
  }

  onClickContactInfo(){
    if(this.state.sectionShowed === 0){
      this.emptyBox(1);
      this.emptyBoxSecond(2);
    }else if (this.state.sectionShowed === 1){
      this.emptyBoxSecond(2);
    }
  }

  render() {
    const {makes, models} = this.props;
    let {car, location, account, emptyBox} = this.state;
    if (!makes || !models) return null;
    return (<div className="admin-listing-form">
      <LayoutHeader colorNav="header-admin" optionSelected="listing"/>
      <div className="bg-admin"/>
      <div className="content-admin">
        <div className="container-fluid" style={this.state.isLoading ? {"cursor": "progress"} : null}>
          <div className="row">
            <div className="principal-panel">
              <h4 className="title-header">New Listing</h4>
              <div className="form-panel">
                <div className="square text-center"
                     onClick={() => this.setState({sectionShowed: this.state.sectionShowed == 0 ? -1 : 0})}>{this.renderMoreOrLessIcon(this.state.sectionShowed != 0)}</div>
                <h5>Car information</h5>
                {this.state.sectionShowed == 0 ? (
                  <div>{this.renderRow([{
                    label: "VIN Number",
                    placeholder: "VIN Number",
                    imagePath: "question.png",
                    value: car.vin,
                    maxLength: 17,
                    onChange: (event) => {
                      (event.target.value.length===17)?emptyBox["vin"]="":emptyBox["vin"]="Please fill VIN Number";
                      this.setState({
                        car: {
                          ...car,
                          vin: event.target.value.toUpperCase()
                        }
                      });
                      this.getDataFromVinNumber(event.target.value.toUpperCase());
                    }
                  }, {
                    label: "Year",
                    options: this.years,
                    value: car.year,
                    onChange: (event) => this.setState({car: {...car, year: event.target.value}})
                  }, {
                    label: "Make",
                    options: this.makes,
                    value: makes.find(make => make.id === car.makeId) ? makes.find(make => make.id === car.makeId).name : null,
                    onChange: (event) => {
                      let makeFound = makes.find(make => make.name === event.target.value);
                      this.setState({
                        car: {
                          ...car,
                          makeId:makeFound.id,
                          makeName:makeFound.name
                        }
                      });
                    }
                  }, {
                    label: "Model",
                    options: this.getModels(),
                    value: models.find(model => model.id === car.modelId) ? models.find(model => model.id === car.modelId).name : null,
                    onChange: (event) => {
                      let modelFound = models.find(model => model.name === event.target.value && car.makeId && model.makeId === car.makeId);
                      (event.target.value!=="Select one")?emptyBox["mod"]="":emptyBox["mod"]="Please select Model";
                      this.setState({
                        car: {
                          ...car,
                          modelId: modelFound.id,
                          modelName: modelFound.name
                        }
                      });
                    }
                  }])}
                    {this.renderRow([{
                      label: "Title",
                      optionsArray: optionTitle,
                      value: car.title,
                      onChange: (event) => this.setState({car: {...car, title: event.target.value}})
                    }, {
                      label: "Cylinders",
                      optionsArray: optionCylinder,
                      value: car.cylinder,
                      onChange: (event) => this.setState({
                        car: {
                          ...car,
                          cylinder: event.target.value
                        }
                      })
                    }, {
                      label: "Drive",
                      optionsArray: optionDrive,
                      value: car.drive,
                      onChange: (event) => this.setState({car: {...car, drive: event.target.value}})
                    }, {
                      label: "Transmission",
                      optionsArray: optionTransmission,
                      value: car.transmission,
                      onChange: (event) => this.setState({car: {...car, transmission: event.target.value}})
                    }])}
                    {this.renderRow([{
                      label: "Int. Condition",
                      optionsArray: optionCondition,
                      value: car.interiorCondition,
                      onChange: (event) => this.setState({
                        car: {
                          ...car,
                          interiorCondition: event.target.value
                        }
                      })
                    }, {
                      label: "Ext. Condition",
                      optionsArray: optionCondition,
                      value: car.exteriorCondition,
                      onChange: (event) => this.setState({
                        car: {
                          ...car,
                          exteriorCondition: event.target.value
                        }
                      })
                    }, {
                      label: "Int. Color",
                      optionsArray: optionColor,
                      value: car.interiorColor,
                      onChange: (event) => this.setState({car: {...car, interiorColor: event.target.value}})
                    }, {
                      label: "Ext. Color",
                      optionsArray: optionColor,
                      value: car.exteriorColor,
                      onChange: (event) => this.setState({car: {...car, exteriorColor: event.target.value}})
                    }])}
                    {this.renderRow([{
                      label: "Status",
                      optionsArray: optionCarStatus,
                      value: car.status,
                      onChange: (event) => this.setState({car: {...car, status: event.target.value}})
                    }, {
                      label: "Mileage",
                      placeholder: "Mileage",
                      value: car.mileage,
                      onChange: (event) =>{
                        (parseInt(event.target.value)>-1 && event.target.value.length>0)?emptyBox["mil"]="":emptyBox["mil"]="Please fill Mileage";
                        this.setState({
                        car: {
                          ...car,
                          mileage: (event.target.value!=="" && event.target.value!=null)?event.target.value.replace(/[^0-9]+/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ","):""
                        }
                      })}
                    }, {
                      label: "Trim",
                      placeholder: "Trim",
                      value: car.trim,
                      onChange: (event) =>{
                        (event.target.value.length>0)?emptyBox["tri"]="":emptyBox["tri"]="Please fill Trim";
                        this.setState({
                        car: {
                          ...car,
                          trim: event.target.value
                        }
                      })}
                    }, {
                      label: "Price",
                      placeholder: "Price",
                      value: car.price? moneySign + car.price:moneySign + '',
                      onChange: (event) => {
                        (parseInt(event.target.value.substring(1))>-1 && event.target.value.length>1)?emptyBox["pri"]="":emptyBox["pri"]="Please fill Price";
                        this.setState({
                        car: {
                          ...car,
                          price: event.target.value.replace(',', '').replace('$', '').replace(/[^0-9]+/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        }
                      })}
                    }])}
                    {this.renderRow([{
                      label: "Fuel",
                      optionsArray: optionFuel,
                      carAttribute: car.fuel,
                      onChange: (event) => this.setState({car: {...car, fuel: event.target.value}})
                    },{
                      label: "Body Type",
                      optionsArray: optionBody,
                      carAttribute: car.body,
                      onChange: (event) => this.setState({car: {...car, body: event.target.value}})
                    }], 100, true)}
                    <h6>Upload Photos<img className="question-icon" src={pathServer.PATH_IMG + "question.png"}/></h6>
                    <div className="d-inline-flex">
                      <div style={{display: "flex", flexWrap: "wrap", maxHeight: "395px"}}>
                        {
                          this.state.chosenFiles.map(function(file, index){
                            const options = (<div className="thumbnail-options-container">
                              <div className="thumbnail-options">
                              <img className="move-icon left-arrow" src={pathServer.PATH_IMG + "icons/right-arrow.svg"}
                               onClick={() => this.setState({chosenFiles: this.state.chosenFiles.slice(0, index - 1).concat(this.state.chosenFiles.slice(index - 1, index + 1).reverse()).concat(this.state.chosenFiles.slice(index + 1))})}/>
                              <img className="move-icon" src={pathServer.PATH_IMG + "icons/right-arrow.svg"}
                               onClick={() => this.setState({chosenFiles: this.state.chosenFiles.slice(0, index).concat(this.state.chosenFiles.slice(index, index + 2).reverse()).concat(this.state.chosenFiles.slice(index + 2))})}/>
                              <img className="delete-icon" src={pathServer.PATH_IMG + "icons/trash.svg"}
                               onClick={() => this.setState({chosenFiles: this.state.chosenFiles.filter(file => file != this.state.chosenFiles[index])})}/>
                             </div>
                            </div>)
                            return(
                              <div className="thumbnail-container">
                                <Image id={index} index={index} imageSrc={file.preview} customClass="thumbnail" moveImage={this.moveImage} options={options}/>
                              </div>
                              )
                              }.bind(this))}
                        <Dropzone onDrop={(files) => this.onDropFile(files)} accept="image/*">
                          {({getRootProps, getInputProps}) => (
                            <div className="drop-file-panel" {...getRootProps()}>
                              <input {...getInputProps()} />
                              <div><img className="camera-icon" src={pathServer.PATH_IMG + "camera.png"}/></div>
                              <div className="content"><span className="text">Drag here or </span><p
                                className="link">Upload from Computer</p>
                              </div>
                            </div>
                          )}
                        </Dropzone>
                      </div>

                      <spa className="message-error-img-create-listing">{this.state.maxImageName > 0 ? "" : emptyBox["img"]}</spa>
                    </div>
                    {this.state.chosenFiles.length > 0 ? <label id="id-main-image-tag"  className="main-image-tag">Cover image</label> : null}
                    <button className="btn btn-continue"
                            style={this.state.chosenFiles.length < 1 ? {marginTop: "48px"} : null}
                            onClick={() => this.emptyBox(1)}>CONTINUE

                    </button>
                    <div className="line"/>
                  </div>) : null}

                <div className="square text-center"
                     onClick={() => this.setState({sectionShowed: this.state.sectionShowed == 1 ? -1 : 1})}>{this.renderMoreOrLessIcon(this.state.sectionShowed != 1)}</div>
                <h5 className="d-inline-flex">Seller location</h5> {this.state.sectionShowed == 1 ?
                <div className="custom-checkbox text-left" style={{marginTop: 0, marginBottom: 0}}>
                  <div className="checkbox-square d-inline-flex"
                       onClick={() => this.setState({isUseSellerInfo: !this.state.isUseSellerInfo})}
                       style={{"backgroundColor": (this.state.isUseSellerInfo ? "#17DC00" : "#FFFFFF")}}/>
                  <label id="id-custom-checkbox" className="d-inline-flex" htmlFor="customCheck1"
                         onClick={() => this.setState({isUseSellerInfo: !this.state.isUseSellerInfo})}>Use my Zoom Autos info.</label>
                </div>:null}
                {this.state.sectionShowed == 1 ? (<div>
                  {this.renderRow([{
                    label: "Address 1",
                    placeholder: "Type here",
                    value: location.address1,
                    onChange: (event) =>{
                      (event.target.value.length>0)?emptyBox["add"]="":emptyBox["add"]="Please fill Address 1";
                      this.setState({
                      location: {
                        ...this.state.location,
                        address1: event.target.value
                      }
                    })}
                  }, {
                    label: "Address 2",
                    placeholder: "Type here",
                    value: location.address2,
                    onChange: (event) => this.setState({
                      location: {
                        ...this.state.location,
                        address2: event.target.value
                      }
                    })
                  }])}
                  {this.renderRow([{
                    label: "City",
                    placeholder: "Type here",
                    value: location.city,
                    onChange: (event) => {
                      (event.target.value.length>0)?emptyBox["cit"]="":emptyBox["cit"]="Please fill City";
                      this.setState({location: {...this.state.location, city: event.target.value}})
                  }}, {
                    label: "State",
                    options: this.states,
                    value: location.state,
                    onChange: (event) => this.setState({location: {...this.state.location, state: event.target.value}})
                  }, {
                    label: "ZIP Code",
                    placeholder: "Type here",
                    value: location.zipCode,
                    onChange: (event) =>{
                      (event.target.value.length>0)?emptyBox["zip"]="":emptyBox["zip"]="Please fill ZIP Code";
                      this.setState({location: {...this.state.location,zipCode: event.target.value}
                    })}
                  }], 75)}
                  <div>
                    <button className="btn btn-continue" onClick={() => this.emptyBoxSecond(2)}>CONTINUE
                    </button>
                  </div>
                  <div className="line"/>
                </div>) : null}

                <div className="square text-center"
                     onClick={() => this.setState({sectionShowed: this.state.sectionShowed == 2 ? -1 : 2})}>{this.renderMoreOrLessIcon(this.state.sectionShowed != 2)}</div>
                <h5>Contact info</h5>
                {this.state.sectionShowed == 2 ? (<div>
                  <div className="custom-checkbox text-left">
                    <div className="checkbox-square d-inline-flex"
                         onClick={this.changeUseInfoFiled}
                         style={{"backgroundColor": (this.state.isUseZoomInfo ? "#17DC00" : "#FFFFFF")}}/>
                    <label id="id-custom-checkbox" className="d-inline-flex" htmlFor="customCheck1" style={{marginBottom: "20px"}}
                           onClick={this.changeUseInfoFiled}>Use seller information on file</label>
                  </div>
                  {this.renderRow([{
                    label: "Seller Type",
                    options: this.sellerTypes,
                    value: account.typeSeller,
                    disabled: this.state.isUseZoomInfo,
                    onChange: (event) =>{
                      (event.target.value==="For Sale By Owner")?emptyBox["dea"]="":null;
                      (event.target.value==="For Sale By Owner")?emptyBox["num"]="":null;
                      this.setState({account: {...this.state.account,typeSeller: event.target.value}
                    })}
                  }, {
                    label: "Dealer name",
                    placeholder: "Type here", value: account.dealerName,
                    hidden: this.state.account.typeSeller === this.sellerTypes[1],
                    onChange: (event) =>{
                      (event.target.value.length>0)?emptyBox["dea"]="":emptyBox["dea"]="Please fill Dealer Name";
                      this.setState({account: {...this.state.account,dealerName: event.target.value}
                    })}
                  }, {
                    label: "Dealer Number",
                    placeholder: "## ## ##",
                    hidden: account.typeSeller === this.sellerTypes[1],
                    value: account.dealerNumber,
                    onChange: (event) =>{
                      (event.target.value.length>0)?emptyBox["num"]="":emptyBox["num"]="Please fill Dealer Number";
                      this.setState({account: {...account,dealerNumber: event.target.value}
                    })}
                  }], 75)}
                  {this.renderRow([{
                    label: "First name",
                    placeholder: "Type here",
                    value: account.firstName,
                    onChange: (event) =>{
                      (event.target.value.length>0)?emptyBox["fir"]="":emptyBox["fir"]="Please fill First Name";
                      this.setState({
                      account: {
                        ...account,
                        firstName: event.target.value
                      }
                    })}
                  }, {
                    label: "Last name",
                    placeholder: "Type here",
                    value: account.lastName,
                    onChange: (event) =>{
                      (event.target.value.length>0)?emptyBox["las"]="":emptyBox["las"]="Please fill Last Name";
                      this.setState({account: {...account, lastName: event.target.value}})}
                  }, {
                    label: "Phone number",
                    placeholder: "Type here",
                    maxLength: 14,
                    value: account.phone,
                    onChange: (event) =>{
                      (event.target.value.length>0)?emptyBox["pho"]="":emptyBox["pho"]="Please fill Phone Number";
                      this.setState({
                      account: {
                        ...account,
                        phone: this.getFormattedPhone(event.target.value.replace(/[^0-9]+/g, ''))
                      }
                    })}
                  }, {
                    label: "Email Address",
                    placeholder: "Type here",
                    value: account.email,
                    onChange: (event) =>{
                      (this.isValidEmail(event.target.value) && event.target.value.length>0)?emptyBox["ema"]="":
                        emptyBox["ema"]="Please fill Email Address";
                      this.setState({account: {...account, email: event.target.value}})}
                  }])}
                  <div className="custom-checkbox text-left">
                    <div className="checkbox-square d-inline-flex"
                         onClick={() => this.setState({isTextSeller: !this.state.isTextSeller})}
                         style={{"backgroundColor": (this.state.isTextSeller ? "#17DC00" : "#FFFFFF")}}/>
                    <label id="id-custom-checkbox" className="d-inline-flex" htmlFor="customCheck1"
                           onClick={() => this.setState({isTextSeller: !this.state.isTextSeller})}>A "Text Seller" option will appear on your listing.</label>
                  </div>
                  <div className="custom-checkbox text-left">
                    <div className="checkbox-square d-inline-flex"
                         onClick={() => this.setState({acceptedFirstTerms: !this.state.acceptedFirstTerms})}
                         style={{"backgroundColor": (this.state.acceptedFirstTerms ? "#17DC00" : "#FFFFFF")}}/>
                    <label id="id-custom-checkbox" className="d-inline-flex" htmlFor="customCheck1"
                           onClick={() => this.setState({acceptedFirstTerms: !this.state.acceptedFirstTerms})}>Accept
                      ZoomAutos <b>Terms and
                        Conditions</b></label>
                    <spa className="message-error-create-listing" style={{top:"1px"}}>{(this.state.acceptedFirstTerms)?"":emptyBox["con"]}</spa>
                  </div>
                  <div className="text-center"><Link onClick={this.onClickSave}>
                    <button className="btn btn-colored">CREATE LISTING</button>
                  </Link></div>
                </div>) : null}


              </div>
            </div>
          </div>
        </div>
      </div>
    </div>);
  }
}


const mapStateToProps = (state, ownProps) => {
  return {
    makes: state.makes,
    models: state.models
  }
};


export default flow(connect(mapStateToProps, {loadMakeAndModel}),
  DragDropContext(HTML5Backend))(LayoutListingsForm);