import React, {Component} from 'react'
import {browserHistory, Link} from "react-router";

export default class LayoutFilter extends Component {

  constructor() {
    super();
    this.state = {
      orderList :[5, 10, 20],
      showFilter : {
        orderBy: false,
        filterBy: false,
        paginateBy: false
      },
      paginateSelected: "",
      orderSelected:  "",
      filterSelected: "",
    }
    this.showDropdown=this.showDropdown.bind(this);
    this.setType = this.setType.bind(this);
    this.setPaginate= this.setPaginate.bind(this);

  }


  componentDidMount(){
    this.props.orderData("desc");
    this.setState({orderSelected:"desc"});
  }

  setType(filter,type){
    if(filter === "all"){
        this.props.reloadData();
        if(type === "sort"){
          this.setState({orderSelected:""});
          this.showDropdown("orderBy");
        } else if(type === "filter"){
          this.setState({filterSelected:""});
          this.showDropdown("filterBy");
        }
    }else{
      if(type === "sort"){
        this.props.orderData(filter);
        this.setState({orderSelected:filter});
        this.showDropdown("orderBy");
      } else if(type === "filter"){
        this.props.filterData(filter);
        this.setState({filterSelected:filter});
        this.showDropdown("filterBy");
      }
    }
  }

  setPaginate(value){
    this.showDropdown("paginateBy");
    this.setState({paginateSelected:value});
    this.props.setValuesList(value);
  }


  showDropdown(type){
    let {showFilter} = this.state;
    let filter=showFilter;
    if(type === "orderBy"){
      if(filter["orderBy"]){
        filter["orderBy"] = false;
      }
      else{
        filter["paginateBy"] = false;
        filter["orderBy"] = true;
        filter["filterBy"] = false;
      }
    } else if(type === "filterBy"){
      if(filter["filterBy"]){
        filter["filterBy"] = false;
      }
      else{
        filter["filterBy"] = true;
        filter["orderBy"] = false;
        filter["paginateBy"] = false;

      }
    } else if(type === "paginateBy"){
      if(filter["paginateBy"]){
        filter["paginateBy"] = false;
      }
      else{
        filter["filterBy"] = false;
        filter["orderBy"] = false;
        filter["paginateBy"] = true;

      }
    }
    this.setState({ showFilter : filter});
  }

  render(){
    const {orderList} = this.state;
    let {showFilter, orderSelected, filterSelected,paginateSelected} = this.state;
    let {pageNumber,listingCars,rowsNumber,orderBy,filterBy} = this.props;

    return(
      <div className="d-inline-flex w-100 filter mt-1" >

        <h4>Listings</h4>
        <Link onClick={() => browserHistory.push('/admin/listing/form')} className="btn create-listing">Create Listing</Link>
          <div className="d-inline-flex option-filter">
            <div  className= "dropdown listing-btn-edition-dropdown d-inline-flex filter-paginate"  style={{marginRight:"7px", width:"4.5vw" }}>
              <div className="w-100">
                <div  className={"select-dropdown " + (showFilter["paginateBy"] ? "custom-dropdown-expanded" : "custom-dropdown")}
                      onClick={()=>this.showDropdown("paginateBy")}>
                  <i className="fas fa-list-ul mr-1"></i>{ (paginateSelected== "" ? 5: paginateSelected)}</div>
                {
                  showFilter["paginateBy"] ?
                    <div   className="car-box-detail">
                      {paginateSelected != "5" ?
                        <div className="card-box-check">
                          <a className="dropdown-item main" onClick={() => this.setPaginate("5")} href="#!">5</a>
                        </div>
                        : null}
                      {paginateSelected != "10" ?
                        <div className="card-box-check">
                          <a className="dropdown-item" onClick={() => this.setPaginate("10")} href="#!">10</a>
                        </div> : null}
                      {paginateSelected != "20" ?
                        <div className="card-box-check">
                          <a className="dropdown-item last" onClick={() => this.setPaginate("20")} href="#!">20</a>
                        </div> : null}
                    </div>
                    : null
                }
              </div>
            </div>
            <nav aria-label="Page navigation" className="float-right d-inline-flex ">
              <ul className="pagination">
                <li className="page-item">
                  <a className="page-link" href="#!" aria-label="Previous" onClick={this.props.prevPage}>
                    <svg className="arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.152 30.152">
                      <path id="ic_arrow_forward_24px" d="M15.076,0,12.419,2.657,22.934,13.191H0V16.96H22.934L12.419,27.495l2.657,2.657L30.152,15.076Z" transform="translate(30.152 30.152) rotate(-180)"/>
                    </svg>
                  </a>
                </li>
                <li className="page-text">
                  <a><b>{pageNumber} </b> OF { Math.ceil(listingCars.length / rowsNumber)}</a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#!" aria-label="Next" onClick={this.props.nextPage}>
                    <svg className="arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.152 30.152">
                      <path id="ic_arrow_forward_24px" d="M19.076,4,16.419,6.657,26.934,17.191H4V20.96H26.934L16.419,31.495l2.657,2.657L34.152,19.076Z" transform="translate(-4 -4)"/>
                    </svg>
                  </a>
                </li>
              </ul>
            </nav>

            <div className= {"filter-1 dropdown listing-btn-edition-dropdown d-inline-flex " + (showFilter["filterBy"] ? "dropdown-expanded":" ")}>
              <div className="w-100">
                <div  className={showFilter["filterBy"] ? " select-dropdown custom-dropdown-expanded" : "custom-dropdown select-dropdown" } onClick={()=>this.showDropdown("filterBy")}>{filterSelected.toLowerCase() === "renewed" ? "Renewed" : filterSelected.toLowerCase() === "expire" ? "Expires Date" : "Filter by"}</div>
                {
                  showFilter["filterBy"] ?
                    <div   className="car-box-detail">
                      {filterSelected != "" ?
                        <div className="card-box-check">
                          <a className="dropdown-item main" onClick={() => this.setType("all","filter")} href="#!">Filter By</a>
                        </div>
                      : null}
                      {filterSelected != "renewed" ?
                        <div className="card-box-check">
                          <a className="dropdown-item" onClick={() => this.setType("renewed","filter")} href="#!">Renewed</a>
                        </div> : null}
                      {filterSelected != "expire" ?
                        <div className="card-box-check">
                          <a className="dropdown-item last" onClick={() => this.setType("expire","filter")} href="#!">Expires date</a>
                        </div> : null}
                    </div>
                    : null
                }
              </div>
            </div>

            <div className={"filter-2 dropdown listing-btn-edition-dropdown d-inline-flex ml-2 " + (showFilter["orderBy"] ? "dropdown-expanded ":" ")}>
              <div className="w-100">
                <div  className={showFilter["orderBy"] ? " select-dropdown custom-dropdown-expanded" : "custom-dropdown select-dropdown" } onClick={()=>this.showDropdown("orderBy")}> {orderSelected.toLowerCase() === "asc" ? "Oldest-Newest" : orderSelected.toLowerCase() === "desc" ? "Newest-Oldest" : "Sort by"} </div>
                {
                  showFilter["orderBy"] ?
                    <div className="car-box-detail">
                      {orderSelected != "" ?
                        <div className="card-box-check">
                          <a className="dropdown-item main" onClick={() => this.setType("all","sort")} href="#!">Sort By</a>
                        </div>
                        : null}
                      {orderSelected != "asc" ?
                      <div className="card-box-check">
                        <a className="dropdown-item" onClick={() => this.setType("asc","sort")} href="#!">Oldest - newest</a>
                      </div> : null }

                      {orderSelected != "desc" ?
                      <div className="card-box-check">
                        <a className="dropdown-item last" onClick={() => this.setType("desc","sort")}  href="#!">Newest - oldest</a>
                      </div> : null }

                    </div>
                    : null
                }
              </div>
            </div>

        </div>
      </div>

    )
  }

}