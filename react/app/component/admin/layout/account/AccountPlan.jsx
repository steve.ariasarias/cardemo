import React from 'react';
import {pathServer} from "../../../../Constants";

const optionPlan = [
  {
    name: "Owner",
    img: "owner-plan.png",
    options: ["2 free listings", "1 user", "Track your listings"],
    price: "FREE",
    monthly: ""
  },
  {
    name: "Baby Dealer",
    img: "baby-dealer-plan.png",
    options: ["1 -30 listings", "2 users", "Track your listings"],
    price: "$299",
  },
  {
    name: "Dealer",
    img: "dealer-plan.png",
    options: ["1 - 70 listings", "5 users", "Track your listings"],
    price: "$499"
  },
  {
    name: "Dealer PRO",
    img: "dealer-pro-plan.png",
    options: ["Unlimited listings", "Unlimited users", "Track your listings"],
    price: "$799"
  },
];

const AccountPlan = (props) => {
  return (
    <div className="admin-panel plan-wrapper">
      <p className="horizontal-panel-title">Zoom Autos Plan </p>
      <div className="plan-card-wrapper">
        {
          optionPlan.map((plan, index) => {
            return (
              <div key={index} className="admin-panel plan-card-option">
                <p className="plan-name">{plan.name}</p>
                <div className="plan-content">
                  <img className="plan-img" src={pathServer.PATH_IMG + plan.img}/>
                </div>
                <div className="plan-options">
                  {plan.options.map((option, index) => {
                    return <p key={index} className="plan-option">{option}</p>
                  })}
                  {plan.name === "Dealer" ?
                    <p className="black-underlined-link plan-option">MORE</p> : null
                  }
                </div>
                <div className="plan-price-wrap">
                  <p className="plan-price">{plan.price}</p>
                  {plan.price !== "FREE" ? <p className="plan-monthly">pay every 30 days</p> : null}
                </div>
                {props.user.accountType === plan.name ?
                  <div className=" dark-btn current-plan">CURRENT</div> : null
                }
              </div>
            )
          })
        }
      </div>
    </div>
  )
};

export default AccountPlan;