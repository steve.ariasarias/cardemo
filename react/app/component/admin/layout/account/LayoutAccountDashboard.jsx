import React, {Component} from 'react';
import LayoutHeader from "../common/LayoutHeader.jsx";
import AccountInfo from "./AccountInfo.jsx";
import AccountPlan from "./AccountPlan.jsx";
import AccountBilling from "./AccountBilling.jsx";
import {changePassword, saveAccount, getInfoAccountLogging,updateAccount} from "../../../../Api";
import {EMPTY_FIELD_ERROR_MESSAGE,
  BAD_REQUEST_ERROR,
  PASSWORD_UPDATED_MESSAGE} from "../../../../Constants";
import {browserHistory} from "react-router";


export default class LayoutAccountDashboard extends Component {
  constructor() {
    super(),
      this.state = {
        user: {
          id:"",
          type: "user",
          firstName: "",
          lastName: "",
          email: "",
          address :"",
          phone: "",
          created_at: "feb 3, 1997",
          updated_at: "feb 3, 2019",
          accountType: "Owner",
          paymentMethod: "Card",
          photo: "",
        },
        showPasswordForm:false,
        accountEditValues:{},
        errorMessage:null,
        successMessage:null
      };
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.updateAccount= this.updateAccount.bind(this);
    this.getFormattedPhone=this.getFormattedPhone.bind(this);
  }

  componentDidMount() {
    const {user} = this.state;
    setTimeout(function() { getInfoAccountLogging().then((response) => {
      let userResponse = response.data.account;
      user.id= userResponse.id;
      user.firstName=userResponse.firstName;
      user.lastName= userResponse.lastName;
      user.address = userResponse.address;
      user.email=userResponse.email;
      user.phone= this.getFormattedPhone(userResponse.phone.replace(/[^0-9]+/g, ''));
      this.setState({user: user});
    }).catch((error) => {
      console.log("error",error);
    });}.bind(this),1000);
  }

  switchToDealer() {
    const {user} = this.state;
    let switchUser = user;

    switchUser.type = "dealer";
    switchUser.accountType = "Dealer";

    this.setState({user: switchUser});
  }

  switchToUser() {
    const {user} = this.state;
    let switchUser = user;
    switchUser.type = "user";
    switchUser.accountType = "user";
    this.setState({user: switchUser});
  }

  togglePasswordForm() {
    const {showPasswordForm} = this.state;
    this.setState({showPasswordForm:!showPasswordForm, accountEditValues:{}});
    this.resetMessages();
  }

  resetMessages(){
    this.setState({errorMessage:null, successMessage:null});
  }

  getFormattedPhone(phoneNumber) {
    let formattedPhone = phoneNumber;
    if (phoneNumber.length >= 1)
      formattedPhone = '(' + phoneNumber.substring(0);
    if (phoneNumber.length > 3)
      formattedPhone = formattedPhone.substring(0, 4) + ') ' + formattedPhone.substring(4);
    if (phoneNumber.length > 6)
      formattedPhone = formattedPhone.substring(0, 9) + '-' + formattedPhone.substring(9);
    return formattedPhone;
  }

  inputChangeHandler(event) {
    const {accountEditValues} = this.state;
    if(event.target.name === "phone"){
      let phoneFormatted=this.getFormattedPhone(event.target.value.replace(/[^0-9]+/g, ''));
      this.setState({user: {...this.state.user, phone: phoneFormatted}});
    }
    accountEditValues[event.target.name] = event.target.value;
    this.setState({accountEditValues:accountEditValues});
  }

  updateAccount(){
    const {accountEditValues, user} = this.state;
    const data = {};
    data["id"] = user.id;
    data["firstName"] =  accountEditValues["firstName"] || user.firstName;
    data["lastName"] = accountEditValues["lastName"] || user.lastName;
    data["email"] = accountEditValues["email"]  || user.email ;
    data["phone"] = accountEditValues["phone"]  ||  user.phone;
    data["address"] = accountEditValues["address"] ||  user.address;
    updateAccount(data).then((response) => {
      let userUpdate = user;
      userUpdate.id=data["id"];
      userUpdate.firstName=data["firstName"];
      userUpdate.lastName = data["lastName"];
      userUpdate.email=data["email"];
      userUpdate.phone= data["phone"];
      userUpdate.address = data["address"];
      this.setState({user: userUpdate});
      console.log("Update Sucess",response);
      sessionStorage.setItem('firstName', userUpdate.firstName);
    }).catch((error) => {
      console.log("error",error.status);
    });
    this.switchToUser();
  }

  changePassword(){
    const {accountEditValues} = this.state;
    this.resetMessages();
    if(accountEditValues["oldPassword"] && accountEditValues["newPassword"]){
      const data = {};
      const email = sessionStorage.getItem("email");
      data["email"] = email;
      data["oldPassword"] = accountEditValues["oldPassword"];
      data["newPassword"] = accountEditValues["newPassword"];

      changePassword(data).then((response) => {
        this.setState({successMessage:response.message});
        setTimeout(()=>{
          this.togglePasswordForm();
        }, 3000);
      }).catch((error) => {
        if(error.status === BAD_REQUEST_ERROR){
          this.setState({errorMessage:error.message});
        }
      });
    }else{
      this.setState({errorMessage:EMPTY_FIELD_ERROR_MESSAGE});
    }
  }

  render() {
    const {user,showPasswordForm, errorMessage,successMessage} = this.state;
    return (
      <div id="admin-account">
        <LayoutHeader colorNav="header-admin" optionSelected="account"/>
        <div className="bg-admin"/>
        <div className="account-wrap">
          <AccountInfo user={user}
                       togglePasswordForm={() => this.togglePasswordForm()}
                       updateAccount={() => this.updateAccount()}
                       showPasswordForm={showPasswordForm}
                       inputChangeHandler={this.inputChangeHandler}
                       changePassword={() => this.changePassword()}
                       errorMessage={errorMessage}
                       switchToDealer={()=>this.switchToDealer()}
                       successMessage={successMessage}
          />

          <AccountPlan user={user}/>
          <AccountBilling user={user}/>
        </div>
      </div>
    );
  }
}