import React from 'react';
import {pathServer} from "../../../../Constants";


const AccountBilling = (props) => {
  return (
    <div className="admin-panel billing-wrapper">
      <div className="black-underlined-link see-invoices">See invoices</div>
      <p className="horizontal-panel-title">Billing Information
      </p>
      <div className="credit-card-wrapper">
        <div className="card-wrap">
          <img alt="card" className="credit-card" src={pathServer.PATH_IMG + "mastercard.png"}/>
          <div className="account-card-description">
            <i className="remove-creditcard fas fa-trash"/>
            <div className="card-holder-info">
              <p className="holder-number">XXXX-XXXX-XXXX-5456</p>
              <p>03/21</p>
              <p className="holder-name">THOMAS ANDERSON</p>
            </div>
          </div>
          <div className="card-description">
            <p>786 N 98 W, Logan, Utah. 804595</p>
            <p>{props.user.email}</p>
            <p>{props.user.mobile}</p>
            <p className="selected-card">Selected as Main Card</p>
          </div>
        </div>
        <div className="card-wrap">
          <img alt="card" className="credit-card" src={pathServer.PATH_IMG + "visacard.png"}/>
          <div className="account-card-description">
            <i className="remove-creditcard fas fa-trash"/>
            <div className="card-holder-info">
              <p className="holder-number">XXXX-XXXX-XXXX-5457</p>
              <p>03/22</p>
              <p className="holder-name">THOMAS ANDERSON</p>
            </div>
          </div>
          <div className="card-description">
            <p>786 N 98 W, Logan, Utah. 804595</p>
            <p>{props.user.email}</p>
            <p>{props.user.mobile}</p>
            <p className="black-underlined-link">Select as Main Card</p>
          </div>
        </div>
        <div className="card-wrap">
          <img alt="card" className="credit-card" src={pathServer.PATH_IMG + "newcard.png"}/>
          <div className="new-creditcard-btn-wrapper">
            <button className="selected-dark-btn btn new-creditcard-btn">
              Add new card
            </button>
          </div>
        </div>
      </div>
    </div>
  )
};


export default AccountBilling;