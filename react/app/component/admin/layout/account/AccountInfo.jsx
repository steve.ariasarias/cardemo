import React from 'react';
import {pathServer} from "../../../../Constants";


const UpgradePlan = (user) => {
  return (
    <div className="account-info-plan">
      {
        user.user.accountType === 'Owner'?
          <img alt="plan" className="plan-img" src={pathServer.PATH_IMG + "dealer-pro-plan.png"}/>
          :null
      }
      {
        user.user.accountType === 'Owner' ?
          <p className="account-info-plan-msg">Improve everything by Upgrading</p>
          :null
      }
      <button type="submit" className="btn-upgrade"><span>UPGRADE</span></button>
      {
        user.user.accountType === 'Dealer' ?
          <div className="plan-padding"/>
          :null
      }
    </div>
  )
};


const DealerHeader = ({user, togglePasswordForm,showPasswordForm,inputChangeHandler,updateAccount}) => {
  return (
    <div>
      <div className="account-panel-top">
        <b className="account-panel-title">Personal Information</b>
        <div onClick={updateAccount} className="dark-btn btn account-panel-edit-btn" style={{backgroundColor:"#08C601", color:"white", borderColor:"white"}}>SAVE</div>
      </div>
      <div className="account-info-section d-inline-flex">
        <img alt="avatar" className="account-photo" src={pathServer.PATH_IMG + "avatar.png"}/>
        <div className="account-info">
          <div className="d-inline-flex" style={{width:"15.5vw"}}>
            <input onChange={inputChangeHandler} className="account-input col-r48 mr-2" type="text" name="firstName" defaultValue={user.firstName}/>
            <input onChange={inputChangeHandler} className="account-input col-r48" type="text" name="lastName" defaultValue={user.lastName}/>
          </div>
          <input onChange={inputChangeHandler} className="account-input" type="text" name="address" defaultValue={user.address}/>
          <input onChange={inputChangeHandler} className="account-input" type="text" name="email" defaultValue={user.email}/>
          <input onChange={inputChangeHandler} className="account-input" type="text" name="phone" value={user.phone}/>
          {
            showPasswordForm?
              null
              :<p className="account-change-password mt-1" onClick={togglePasswordForm}><a>Change account's password</a></p>
          }
        </div>
      </div>
    </div>
  )
};

const UserHeader = ({user, togglePasswordForm,showPasswordForm, switchToDealer}) => {
  return (
    <div>
      <div className="account-panel-top">
        <b className="account-panel-title">Personal Information</b>
        <button className="dark-btn btn account-panel-edit-btn" onClick={switchToDealer}>Edit</button>
      </div>
      <div className="account-info-section">
        <img alt="avatar" className="account-photo" src={pathServer.PATH_IMG + "avatar.png"}/>
        <div className="account-info">
          <p className="account-name">{user.firstName + " " + user.lastName}</p>
          <p className="account-location">{user.address}</p>
          <hr/>
          <p className="account-info-description">{user.email}</p>
          <p className="account-info-description">{user.phone}</p>
          {
            showPasswordForm?
              null
              :<p className="account-change-password" onClick={togglePasswordForm}><a>Change account's password</a></p>
          }
        </div>
      </div>
    </div>
  )
};

const AccountTable = (user) => {
  return(
    <div className="account-info-table">
      <table className="table table-responsive-sm table-borderless table-striped">
        <tbody>
        <tr>
          <td>Account Created</td>
          <td>{user.created_at}</td>
        </tr>
        <tr>
          <td>Last Login</td>
          <td>{user.created_at}</td>
        </tr>
        <tr>
          <td>Account Type</td>
          <td>{user.accountType}</td>
        </tr>
        <tr>
          <td>Payment Method</td>
          <td>{user.paymentMethod}</td>
        </tr>
        </tbody>
      </table>
    </div>
  );
};

const PasswordForm = ({inputChangeHandler, togglePasswordForm, changePassword,errorMessage,successMessage}) => {
  return(
    <div className="change-password-section">
      <div className="change-password-form">
        {
          errorMessage!=null?
            <div className="alert alert-danger" role="alert">
              <strong>Error!</strong> {errorMessage}
            </div>:null
        }
        {
          successMessage!=null?
            <div className="alert alert-success" role="alert">
              <strong>Great!</strong> {successMessage}
            </div>:null
        }
        <input className="account-password-input" name="oldPassword" type="password" placeholder="Old Password" onChange={inputChangeHandler}/>
        <input className="account-password-input" name="newPassword" type="password" placeholder="New Password" onChange={inputChangeHandler}/>
        <div className="change-password-form-options">
          <div className="dark-btn btn password-btn change-password-btn" onClick={changePassword} >Save</div>
          <div className="dark-btn btn password-btn cancel-change-password-btn" onClick={togglePasswordForm}>Cancel</div>
        </div>
      </div>
    </div>
  );
};

const AccountInfo = (props) => {
  const user = props.user;
  return (
    <div className="admin-panel account-wrapper">
      <div className="account-info-panel">
        {
          user.type === 'user' ?
          <UserHeader user={user}
                      togglePasswordForm={props.togglePasswordForm}
                      showPasswordForm={props.showPasswordForm}
                      switchToDealer={props.switchToDealer}
                      inputChangeHandler={props.inputChangeHandler}
          />
            :
          <DealerHeader user={user}
                        togglePasswordForm={props.togglePasswordForm}
                        showPasswordForm={props.showPasswordForm}
                        switchToDealer={props.switchToDealer}
                        inputChangeHandler={props.inputChangeHandler}
                        updateAccount = {props.updateAccount}

          />
          }
        {
          props.showPasswordForm?
            <PasswordForm
              inputChangeHandler={props.inputChangeHandler}
              togglePasswordForm={props.togglePasswordForm}
              changePassword={props.changePassword}
              errorMessage={props.errorMessage}
              successMessage={props.successMessage}/>:
            <AccountTable/>
        }
        <UpgradePlan user={user}/>
      </div>
    </div>
  )
};

export default AccountInfo;