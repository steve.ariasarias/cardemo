import React, {Component} from 'react';
import LayoutHeader from "./layout/common/LayoutHeader.jsx";
import Table from "../public/layout/common/Table.jsx";
import Chart from 'react-apexcharts'
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import {connect} from "react-redux";


class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      messages: [{
        status: 'ACTIVE',
        subject: 'Message subject',
        antiquity: 'Today',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard'
      },
        {
          status: 'INACTIVE',
          subject: 'Message subject',
          antiquity: 'Today',
          body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard'
        },
        {
          status: 'ACTIVE',
          subject: 'Message subject',
          antiquity: 'Yesterday',
          body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard'
        },
        {
          status: 'INACTIVE',
          subject: 'Message subject',
          antiquity: 'Wednesday Nov 21',
          body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard'
        },
        {
          status: 'ACTIVE',
          subject: 'Message subject',
          antiquity: 'Monday Nov 19',
          body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard'
        },
        {
          status: 'INACTIVE',
          subject: 'Message subject',
          antiquity: 'Today',
          body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard'
        },
        {
          status: 'INACTIVE',
          subject: 'Message subject',
          antiquity: 'Today',
          body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard'
        }],
      chartOptions: {
        chart: {
          id: 'apexchart-example',
          stacked: false,
          zoom: {
            enabled: false
          }
        },
        legend: {
          show: false
        },
        dataLabels: {
          enabled: false
        }, plotOptions: {
          line: {
            curve: 'smooth'
          }
        },
        markers: {
          size: 2,
          colors: ['#fff'],
          strokeColor: ['#FF7E00', '#4A7AD9'],
          strokeWidth: 2,
          strokeOpacity: 1,
          fillOpacity: 1,
          shape: "circle",
          radius: 2,
          offsetX: 0,
          offsetY: 0,
          hover: {
            size: 4,
            sizeOffset: 3
          }

        },
        xaxis: {
          lines: {
            show: true
          },
          labels: {
            show:true,
            style: {
              fontSize: '0.5rem',
              colors: ['rgba(67, 66, 93, 0.5)','rgba(67, 66, 93, 0.5)','rgba(67, 66, 93, 0.5)','rgba(67, 66, 93, 0.5)','rgba(67, 66, 93, 0.5)','rgba(67, 66, 93, 0.5)','rgba(67, 66, 93, 0.5)']
            }
          },
          categories: ['Dec 01', 'Dec 02', 'Dec 03', 'Dec 04', 'Dec 05', 'Dec 06', 'Dec 07'],
          axisTicks:{
            show: false
          },
          axisBorder:{
            show: true,
            height: 1,
            color: '#EAF0F4'
          },

          tooltip: {
            enabled: false
          }
        },
        yaxis: {
          min: 25,
          max: 45,
          tickAmount: 4,
          decimalsInFloat: false,
          lines: {
            show: true
          },
          labels: {
            style: {
              color: 'rgba(67, 66, 93, 0.5)',
              fontSize: '0.5rem',
            }
          },

          axisTicks: {
            show: false
          }
        },
        toolbar: {
          show: false,
        },
        stroke: {
          show: true,
          curve: 'smooth',
          lineCap: 'butt',
          colors: ['#FF7E00', '#4A7AD9'],
          width: 2,
          dashArray: 0,
        },
        grid: {
          borderColor: '#EAF0F4',
        },
        tooltip: {
          shared: false,
          custom: function ({series, seriesIndex, dataPointIndex, w}) {
            return '<div class="speech-bubble">' +
              '<span>' + series[seriesIndex][dataPointIndex] + ' views'+ '</span>' +
              '</div>'
          },
          fixed:{
            enabled: true,
            position: 'topRight',
            offsetX: 0
          }
        },
        fill: {
          colors: ['#FF7E00', '#4A7AD9'],
          type: 'gradient',
          gradient: {
            shadeIntensity: 1,
            opacityFrom: 0.7,
            opacityTo: 1,
            stops: [0, 70, 100]
          }
        },

      },
      chartSeries: [{
        name: 'By searching',
        data: [32, 28, 36, 35, 33, 36]
      },
        {
          name: 'By advertising',
          data: [32, 35, 34, 37, 37.5, 35]
        }],

    };
    this.renderMessage = this.renderMessage.bind(this);
    this.logout = this.logout.bind(this);
  }

  logout(){
    sessionStorage.clear();
    location.href = "/logout"
  }

  renderMessage(message, index) {
    return (
      <div>
        <div className="row">
          <div className={'square' + (message.status === 'ACTIVE' ? ' active-square' : ' inactive-square')} onClick={() => {
            let allMessages = [...this.state.messages];
            allMessages = {...allMessages[index], status: (message.status === 'ACTIVE') ? 'INACTIVE' : 'ACTIVE'};
            this.setState({ messages: allMessages });
          }}/>
          <span className="message-subject">{message.subject}</span>
          <div className="inner-container"><span className="message-antiquity text-right">{message.antiquity}</span></div>
        </div>
        <div className="row"><p className="message-body">{message.body}</p></div>
      </div>
    );
  }

  render() {
    return (
      <div className="admin-dashboard">
        <LayoutHeader colorNav="header-admin" optionSelected="dashboard"/>
        <div className="bg-admin"/>
        <div className="row">
          <div className="admin-panel inbox-panel">
            <b className="panel-title">Inbox</b>
            <button className="dark-btn btn view-btn">View all</button>
            {this.state.messages.filter(m => m.status == 'ACTIVE').length > 0 ?
              <div className="row tools-panel">
                <div className="col-sm-1" style={{"marginTop":"-2px"}}><span className="badge">{this.state.messages.filter(m => m.status == 'ACTIVE').length}</span></div>
                <div className="col-sm-1"><span className="save-icon"></span></div>
                <div className="col-sm-1"><span className="delete-icon"></span></div>
              </div> : null}
            <div className={"messages-panel " + (this.state.messages.filter(m => m.status == 'ACTIVE').length > 0 ? "small-panel":"big-panel")}>
              {this.state.messages.map(function (message, index) {
                return (<div key={index} className="message-panel">{this.renderMessage(message, index)}</div>);
              }.bind(this))}
            </div>
          </div>

          <div className="admin-panel statistics-panel row">
            <div className="col-sm-9">
              <div className="row"><b className="panel-title">Views</b>
                <div className="legend row">
                  <div className="legend-line" style={{'backgroundColor': '#FF7E00'}}></div>
                  <span className="legend-text">By searching</span>
                  <div className="legend-line" style={{'backgroundColor': '#4A7AD9'}}></div>
                  <span className="legend-text">By advertising</span>
                </div>
                <div className="dropdown admin-btn-dropdown">
                  <button className="btn dropdown-toggle" type="button" id="btnMenu2"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <b>All cars</b>
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a className="dropdown-item" href="#!">Action</a>
                    <a className="dropdown-item" href="#!">Another action</a>
                  </div>
                </div>
              </div>

              <div className="row justify-content-center">
              <Chart options={this.state.chartOptions} series={this.state.chartSeries} type="area" width={500}
                     height={270}/>
              </div>
              <div className="dropdown admin-btn-dropdown">
                <button className="btn dropdown-toggle" type="button" id="btnMenu2"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <b>Daily</b>
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                  <a className="dropdown-item" href="#!">Action</a>
                  <a className="dropdown-item" href="#!">Another action</a>
                </div>
              </div>
            </div>
            <div className="impressions-panel col text-center">
              <b className="title">Today's Impressions</b>
              <div className="dropdown admin-btn-dropdown">
                <button className="btn dropdown-toggle" type="button" id="btnMenu2"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <b>All listings</b>
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                  <a className="dropdown-item" href="#!">Action</a>
                  <a className="dropdown-item" href="#!">Another action</a>
                </div>
              </div>
              <div className="row justify-content-center">
              <div className="circle">
                <b className="impressions-number">35</b>
                <p><b>Today</b></p>
              </div></div>
              <p className="impressions-text">39 <span style={{'color': 'black'}}>Yesterday</span></p>
            </div>

          </div>
        </div>
        <div className="row">
          <div className="admin-panel advertising-panel">
            <b className="panel-title" style={{"marginLeft": "10px"}}>Advertising admin</b>
            <button className="selected-dark-btn btn">New campaign</button>
            <Table admin={true}
                   rows={
                     [["Campaign name", "Budget", "Views", "Impressions", "Cost by impression"],
                       ["Illuminated Vanity Mirrors", "$300", "408", "$1.01", "Not available"],
                       ["Steering Wheel Material", "$300", "408", "$1.01", "Urethane"],
                       ["Gear Shift Knob Trim", "$300", "408", "$1.01", "Metal-look"],
                       ["12-Volt DC Power Outlet", "$300", "408", "$1.01", "2"]]}></Table>
          </div>

          <div className="admin-panel calendar-panel text-center">
            <div className="row">
              <b className="panel-title">Calendar</b>
            </div>
            <DayPicker/>
          </div>
        </div>
      </div>
    )
  }

}


function mapStateToProps(state) {
  return {
    auth: state.auth,
  };
}

export default connect(mapStateToProps)(Dashboard);