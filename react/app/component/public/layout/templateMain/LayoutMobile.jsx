import React, {Component} from 'react'
import {pathServer} from "../../../../Constants"
import Slider from 'react-slick';
import {FormattedMessage} from "react-intl";

export default class LayoutMobile extends Component {
  constructor(props){
    super(props);
  }

  render(){
    const settings = {
      dots: true,
      infinite: true,
      className: "center",
      centerMode: true,
      centerPadding: "30px",
      slidesToShow: 6,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 1000
    };
    return (
      <section id="mobile">
        <div className="container-fluid">
          <div className="row ml-0 mr-0 justify-content-center" >
            <div className="col-lg-5 text-center">
              <img src={pathServer.PATH_IMG + "imagen-mobile.png"} width="250px" alt=""/>
            </div>
            <div className="col-lg-5 text-center mt-2 pr-3">
              <button className="mt-5 bg-white icon-mobile p-2">
                <img src={pathServer.PATH_IMG + "zoom-icon.png"} width="75px" alt=""/>
              </button>
              <h4 className="pt-3"><FormattedMessage id="Home.getTheApp"/></h4>
              <div className="d-inline-block mt-5">
                <img className="img-fluid" src={pathServer.PATH_IMG + "appstore.png"}  alt=""/>
                <img className="img-fluid" src={pathServer.PATH_IMG + "googleplay.png"}  alt=""/>
              </div>
            </div>
          </div>
          <div className="panel mt-5 mr-5 ml-5 bg-white" id="brands">
            <Slider {...settings}>
              <div>
                <img  width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
              </div>
              <div>
                <img   width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
              </div>
              <div>
                <img  width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
              </div>
              <div>
                <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
              </div>
              <div>
                <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
              </div>
              <div>
                <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
              </div>
              <div>
                <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
              </div>
              <div>
                <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
              </div>
            </Slider>
          </div>
        </div>


      </section>
    );

  }

}