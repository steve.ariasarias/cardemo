import React, {Component} from 'react';
import {Link} from 'react-router';
import {pathServer} from "../../../../Constants";
import {FormattedMessage} from "react-intl";
import FormUtils from "../../../util/FormUtils";
import {connect} from 'react-redux'


const LoggedMenu = ({menuOptions, logout, refMethod}) => {
return(
  <div id="logged-menu-options" className="position-absolute" ref={refMethod}>
    {
      menuOptions.map((menuOption, index)=>{
      return (
          <a href={menuOption.link}>
            <div key={index} className="logged-menu-option">
              {menuOption.displayName}
            </div>
          </a>
      );
      })
    }
    <div id="logout-menu-option" onClick={logout}>Log out</div>
  </div>
)
};

class LayoutHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navStatus: true,
      classNav: "row justify-content-center align-items-center",
      classHeader: "header-container",
      menuOptions:[
        {link:'/admin/dashboard', displayName:'Dashboard'},
        {link:'/admin/account', displayName:'Account'},
        {link:'/admin/listing', displayName:'My Listing'}],
      showMenuOptions:false,
      carsType: 1
    };
    this.onChangeCarsType=this.onChangeCarsType.bind(this);
    this.openSessionMenu = this.openSessionMenu.bind(this);
    this.logout = this.logout.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({showMenuOptions: false});
    }
  }

  openSessionMenu(){
    const {showMenuOptions} = this.state;
    this.setState({showMenuOptions:!showMenuOptions});
  }

  componentDidMount() {
    window.addEventListener('scroll', () => {
      let y = FormUtils.getScrollTop();
      if (y < 100) {
        this.setState({
          navStatus: true,
          classNav: "row justify-content-center align-items-center",
          classHeader: "header-container"
        });

      }
      else {
        this.setState({navStatus: false, classNav: "navHidden", classHeader: "header-container2"});
      }
    });
    window.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    window.removeEventListener('mousedown', this.handleClickOutside);
  }

  onChangeCarsType(carsTypeIndex) {
    this.setState({carsType: carsTypeIndex});
  }

  logout(){
    sessionStorage.clear();
    location.href = "/logout";
  }

  render() {
    let {navStatus, classNav, classHeader, menuOptions, showMenuOptions} = this.state;
    let {email,firstName} = this.props;
    if(!email || email === "" && !firstName){
      email = sessionStorage.getItem("email")
      firstName = sessionStorage.getItem("firstName")
    }
    return (
      <header id="header" className={classHeader}>
        <div className="menu-header navbar-fixed-top" >
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-2 col-sm-3 text-center" id="menu-head-left">
                <div className="dropdown">
                  <button className="btn dropdown-toggle" type="button" id="btnMenu1"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src={pathServer.PATH_IMG + 'logo-' + this.props.selectedLanguage + '.png'} alt=""/>
                    <b>{this.props.selectedLanguage == 'en' ? 'ENGLISH' : 'ESPAÑOL'}</b>
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <a
                      className={'dropdown-item ' + (this.props.selectedLanguage == 'en' ? 'selected-dropdown-item' : '')}
                      onClick={() => this.props.onChangeLanguage('en')}>ENGLISH</a>
                    <a
                      className={'dropdown-item ' + (this.props.selectedLanguage == 'es' ? 'selected-dropdown-item' : '')}
                      onClick={() => this.props.onChangeLanguage('es')}>ESPAÑOL</a>
                  </div>
                </div>

              </div>
              <div className="menu-center col-md-8 col-sm-6">
                <div className='dealer-logo text-center'>
                  {navStatus ? <img className="logo-header" src={pathServer.PATH_IMG + "zoom-logo.png"} width="50"/>
                    : <img className="logo-header2" src={pathServer.PATH_IMG + "zoom-logo.png"} width="50"/>
                  }
                </div>

                <nav aria-label="breadcrumb" className={classNav} id="breadcrum-nav">

                  <ol className="breadcrumb text-center">
                    <li className="breadcrumb-item"><a onClick={()=> this.onChangeCarsType(1)} className={this.state.carsType == 1 ? " selected-breadcrumb" : ""} href="#"><FormattedMessage
                      id="Header.home"/></a></li>
                    <li className="breadcrumb-item">
                      <Link to="/catalog/inventory"><a onClick={()=> this.onChangeCarsType(3)} className={this.state.carsType == 3 ? " selected-breadcrumb" : ""} href="#"><FormattedMessage
                        id="Header.inventory"/></a></Link></li>
                    <li className="breadcrumb-item">
                      <Link to="/car-values"><a onClick={()=> this.onChangeCarsType(3)} className={this.state.carsType == 3 ? " selected-breadcrumb" : ""} href="#"><FormattedMessage
                        id="Header.carValues"/></a></Link></li>
                    <li className="breadcrumb-item">
                      <Link to="/sell"><a onClick={()=> this.onChangeCarsType(4)} className={this.state.carsType == 4 ? " selected-breadcrumb" : ""} href="#"><FormattedMessage
                        id="Header.sellFree"/></a></Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link to="/about"><a onClick={()=> this.onChangeCarsType(5)} className={this.state.carsType == 5 ? " selected-breadcrumb" : ""} href="#"><FormattedMessage
                      id="Header.us"/></a> </Link></li>
                  </ol>
                </nav>
              </div>
              {email?
                  <div className="col-md-2 col-sm-3 text-left" id="login-menu-head-right">
                    <div id="logged-menu">
                      <a id="favorites-btn"><i className="far fa-star"/>Favorites</a>
                        <span id="logged-user">
                          <a className="user-greeting" onClick={()=> this.openSessionMenu()}>
                            Welcome, {firstName} <i className="fas fa-caret-down"/>
                          </a>
                        </span>
                    </div>
                    {showMenuOptions? <LoggedMenu menuOptions={menuOptions} logout={this.logout} refMethod={this.setWrapperRef}/> : null}
                  </div>
                  :
                  <div className="col-md-2 col-sm-3 text-right" id="menu-head-right">
                    <Link to="/login" id="btnLogin" className="btn-login">
                      <img src={pathServer.PATH_IMG + "icons/user.svg"}/>
                    </Link>
                  </div>
              }
            </div>
          </div>

        </div>
      </header>

    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    email: state.email,
    firstName:state.firstName
  }
};

export default connect(mapStateToProps)(LayoutHeader)