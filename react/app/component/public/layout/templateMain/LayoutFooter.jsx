import React, {Component} from 'react'
import {pathServer} from "../../../../Constants"
import {FormattedMessage} from "react-intl";

export default class LayoutFooter extends Component {
  constructor(props){
    super(props);
    this.onClick = this.onClick.bind(this);

  }

  onClick(p, x) {
    let el = document.querySelector('back_to_top2');
    let pointTo = getPosition(el);
    let y = getScrollTop();
    //window.scrollTo(point.x, point.y);
    let pointFrom = {
      x: 0,
      y: y //window.scrollY
    };
    scrollTo(pointFrom,pointTo,600);
  }
  render(){
    return (
      <section id="footer">
        <div className="container-fluid">
          <div className="row ml-0 mr-0 justify-content-center" >
            <div className="content-footer text-center">
              <h4> <FormattedMessage id="Footer.emailTitle"/> </h4>
              <small className="mt-1"> <FormattedMessage id="Footer.emailSubTitle"/> </small>

              <div className="row">
                <div className="col-sm-10">
                  <FormattedMessage id="Footer.emailPlaceholder">
                    {placeholder =>
                      <input type="text" className="form-control email-footer mt-4" placeholder={placeholder}></input>
                    }
                  </FormattedMessage>
                </div>
                <div className="col-sm-1 mt-4 ml-2">
                  <a className="p-2 ml-2  text-dark aEmail" href="#" >
                    <i className="fas fa-check text-white"></i>
                  </a>
                </div>
              </div>


            </div>
          </div>
          <div className="nav-footer mt-5 pb-4">
              <div className="row  ml-0 mr-0 justify-content-center">
                <div className="col-lg-2 text-center">
                  <img src={pathServer.PATH_IMG+"zoom-logo.png"} width="170"/>
                </div>
                <div className="col-lg-8">
                  <nav aria-label="breadcrumb" className="row justify-content-center align-items-center">
                    <ol className="breadcrumb text-center">
                      <li className="breadcrumb-item">
                        <a href="/catalog" className={location.pathname === '/catalog'? 'selected-breadcrumb': null}>
                          <FormattedMessage id="Header.home"/>
                          </a>
                      </li>
                      <li className="breadcrumb-item">
                        <a href="/catalog/inventory">
                          <FormattedMessage id="Header.inventory"/>
                        </a>
                      </li>
                      <li className="breadcrumb-item">
                        <a href="/car-values">
                          <FormattedMessage id="Header.carValues"/>
                        </a>
                        </li>
                      <li className="breadcrumb-item">
                        <a href="/sell">
                          <FormattedMessage id="Header.sellFree"/>
                          </a>
                      </li>
                      <li className="breadcrumb-item">
                        <a href="/about">
                          <FormattedMessage id="Header.us"/>
                          </a>
                        </li>
                    </ol>
                  </nav>
                </div>
                <div className="col-lg-2">
                      <a className="p-2" href="#"><i className="fab fa-facebook-f"></i></a>
                      <a className="p-2" href="#"><i className="fab fa-instagram ig"></i></a>
                      <a className="p-2" href="#"><i className="fab fa-twitter"></i></a>
                      {/*<a id="back_to_top2" className="p-2 ml-2 text-dark" href="#" onClick={this.onClick}>*/}
                        {/*<i className="fas fa-arrow-up arrow"></i>*/}
                      {/*</a>*/}
                </div>
              </div>
            </div>
          </div>

      </section>
    );

  }

}