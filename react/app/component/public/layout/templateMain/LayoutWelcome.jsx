import React, {Component} from 'react'
import {pathServer} from "../../../../Constants";
import {FormattedMessage} from "react-intl";

export default class LayoutWelcome extends Component {
  constructor(props){
    super(props);
  }

  render(){

    return (
      <section id="welcome">
        <div id="welcomeSec" className="container-fluid welcome-section">
            <div className="row ml-0 mr-0 justify-content-center welcome-content" >
              <div className="col-lg-5 text-center">
                <div className="panel bg-white p-4 w-100">
                  <h2 className="mb-4 mt-4"><FormattedMessage id="Home.greatDealsTitle1"/><br/><FormattedMessage id="Home.greatDealsTitle2"/></h2>
                  <button type="button" className="btnSearch mb-4"><FormattedMessage id="Home.greatDealsButton"/></button>
                </div>
                <div className="panel options bg-white p-3 w-100 mt-2">
                  <h3 className="text-left mt-3 ml-3"><FormattedMessage id="Home.inventoryOption"/>
                    <img className="float-right arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"} alt=""/>
                  </h3>

                </div>
                <div className="panel options bg-white p-3 w-100 mt-2">
                  <h3 className="text-left mt-3 ml-3"><FormattedMessage id="Home.salesOption"/>
                  <img className="float-right arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"} alt=""/>
                  </h3>
                </div>
                <div className="panel options bg-white p-3 w-100 mt-2">
                  <h3 className="text-left mt-3 ml-3"><FormattedMessage id="Home.nearestOption"/>
                  <img className="float-right arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"} alt=""/>
                  </h3>
                </div>



              </div>

              <div className="ml-1 panel col-lg-6 text-center bg-white p-4">
                <h2 className="mt-4" ><FormattedMessage id="Home.welcomeTitle1"/><br/><FormattedMessage id="Home.welcomeTitle2"/></h2>
                <p className="text-justify p-3"><FormattedMessage id="Home.welcomeText"/>
                  <img className="img-fluid float-right" width="450" src={pathServer.PATH_IMG + "BMW-X6.png"} alt=""/>
                </p>
              </div>
            </div>

         </div>
      </section>
    );

  }

}