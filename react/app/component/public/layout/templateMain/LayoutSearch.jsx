import React, {Component} from 'react';
import LayoutAdvanceSearch from "./LayoutAdvanceSearch.jsx";
import {FormattedMessage} from 'react-intl';
import {Link} from "react-router";

export default class LayoutSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyword: "",
      type: "",
      name: "",
      carsStatus: [],
      yearFrom: 1930,
      yearTo: 2020,
      priceFrom: 10000,
      priceTo: 60000,
      isShow: false,
      make: [],
      model: [],
      exteriorColour:[],
      zipCode: "",
      bodyStyle: [],
      price_range: "",
      yearDefaultDisabled: true,
      textValue: null,
      percent: null,
      value: 0,
    };
    this.onSlide = this.onSlide.bind(this);
    this.onSlideYear = this.onSlideYear.bind(this);
    this.triggerChildAlert = this.triggerChildAlert.bind(this);
    this.showAdvanced = this.showAdvanced.bind(this);
    this.hideAdvanced = this.hideAdvanced.bind(this);
    this.onChangeMakes = this.onChangeMakes.bind(this);
    this.onChangeCode = this.onChangeCode.bind(this);
    this.onChangeModels = this.onChangeModels.bind(this);
    this.onChangeBody = this.onChangeBody.bind(this);
    this.onChangeKeyword = this.onChangeKeyword.bind(this);
  }

  onSlide(render, handle, value, un, percent) {
    let value1 = Math.round(value[0]);
    let value2 = Math.round(value[1]);
    this.setState({priceFrom: value1, priceTo: value2})
  };

  onSlideYear(render, handle, value, un, percent) {
    let value_1 = Math.round(value[0]);
    let value_2 = Math.round(value[1]);
    this.setState({yearFrom: value_1, yearTo: value_2})
  };

  onChangeMakes(makeSelected) {
    let {make} = this.state;
    make[0] = makeSelected
    this.setState({make: make});
  }

  onChangeModels(event) {
    let {model} = this.state;
    model[0] = event.target.value;
    this.setState({model: model});
  }

  onChangeCode(event) {
    this.setState({zipCode: event.target.value});
  }

  onChangeCarsStatus(carsStatusValue) {
    let {carsStatus} = this.state;
    const res = carsStatus.filter(cs => {return cs === carsStatusValue});
    if(res.length > 0){
      carsStatus.splice(carsStatus.indexOf(carsStatusValue), 1);
    }else{
      carsStatus.push(carsStatusValue);
    }
    this.setState({carsStatus:carsStatus});
  }

  onChangeBody(event) {
    let {bodyStyle} = this.state;
    bodyStyle[0] = event.target.value;
    this.setState({bodyStyle: bodyStyle});
  }

  onChangeKeyword(event){
    this.setState({keyword: event.target.value});
  }

  showAdvanced() {
    this.setState({isShow: true});
    this.props.changeAdvancedSearchMode(true);
  }

  hideAdvanced() {
    this.setState({isShow: false});
    this.props.changeAdvancedSearchMode(false);
  }

  componentWillMount() {

  }

  triggerChildAlert(event) {
    //this.refs.child.showAlert();
    event.preventDefault();
  }


  render() {
    const {keyword, priceFrom, priceTo, yearFrom, yearTo, zipCode, bodyStyle, carsStatus, make, model,exteriorColour} = this.state;
    let newTo = {
      pathname: "/catalog/inventory",
      state: {keyword:keyword,makes:make,models:model,exteriorColour:exteriorColour,yearFrom:""+yearFrom,yearTo:""+yearTo,
        priceFrom:""+priceFrom,priceTo:""+priceTo,body:bodyStyle,status:carsStatus,offset:0,size:1000}
    };
    return (
      <div className=" search-content">
        <div className=" row ml-0 mr-0 justify-content-center search-container">
          <div className="no-overflow fit-box col-xl-6 col-lg-8 col-md-8 col-sm-8 col-10 text-center">
            <div className=" search-box">
              <div className="  white-box text-center p-3 pl-5 pr-5">
                <form onSubmit={this.triggerChildAlert}>
                  <div className="search-title">
                    <h5 className="text-dark"><FormattedMessage id="Search.title"/></h5>
                  </div>
                  <div className="mt-3">
                  <div className="btn-group" role="group" style={{"width":this.state.isShow ? "85%": "100%"}}>
                    <div className="row car-type-selector text-center">
                      <div className="col-md-4">
                        <button type="button"
                                className={"btn-car-type" + (carsStatus.find(cs => cs === "new") ? " selected" : "")}
                                onClick={() => this.onChangeCarsStatus("new")} value="NEW"><FormattedMessage
                          id="Search.newCarsButton"/></button>
                      </div>
                      <div className="col-md-4">
                        <button type="button"
                                className={"btn-car-type" + (carsStatus.find(cs => cs === "used") ? " selected" : "")}
                                onClick={() => this.onChangeCarsStatus("used")} value="USED"><FormattedMessage
                          id="Search.usedCarsButton"/></button>
                      </div>
                      <div className="col-md-4">
                        <button type="button"
                                className={"btn-car-type" + (carsStatus.find(cs => cs === "certified") ? " selected" : "")}
                                onClick={() => this.onChangeCarsStatus("certified")} value="CERTIFIEDS">
                          <FormattedMessage id="Search.certifiedCarsButton"/></button>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div className="form mt-3 ">
                    <div className="form-group">
                      <div className="input-group">
                        <FormattedMessage id="Search.inputPlaceholder">
                          {placeholder => <input id="email" type="text" className="form-control uppercase-input" onChange={this.onChangeKeyword}
                                                 required placeholder={placeholder}/>}
                        </FormattedMessage>
                        <i type="icon"></i>
                      </div>

                      {!this.state.isShow ?
                        <div id="advancedSearchOption">
                          <p className="btn-link" onClick={this.showAdvanced}><u><FormattedMessage
                            id="Search.advancedSearch"/></u></p>
                        </div> : null}

                      {this.state.isShow ?
                        <LayoutAdvanceSearch hideAdvanced={this.hideAdvanced} onChangeMakes={this.onChangeMakes}
                                             onChangeModels={this.onChangeModels} zipCode={zipCode}
                                             onChangeCode={this.onChangeCode} onSlide={this.onSlide}
                                             onSlideYear={this.onSlideYear}
                                             priceFrom={priceFrom} priceTo={priceTo} yearFrom={yearFrom} yearTo={yearTo}
                                             onChangeBody={this.onChangeBody}
                        /> : null}

                    </div>
                    <div className="row show-up btn-go-box">
                      <Link to={newTo}>
                        <button type="submit" className="btn-search"><FormattedMessage
                          id="Search.go"/></button>
                      </Link>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}