import React, {Component} from 'react'
import {pathServer} from "../../../../Constants";
import {FormattedMessage} from "react-intl";

export default class LayoutNew extends Component {
  constructor(props){
    super(props);
  }

  render(){

    return (
      <section id="news">
        <div className="container-fluid news-section">
          <div className="row ml-0 mr-0 justify-content-center welcome-content" >
            <div className="col-lg-11">
              <div className="panel bg-white p-3">
                <div className="title-news">
                  <h2 className="mb-4 mt-4 text-center"><FormattedMessage id="Home.newsTitle1"/> <b> <FormattedMessage id="Home.newsTitle2"/></b> </h2>
                </div>

                <div className="content-news mt-3">
                  <div className="row">

                    <div className="col-lg-3">
                      <img src={pathServer.PATH_IMG + "background.png"} className="img-fluid" alt=""/>
                    </div>
                    <div className="col-lg-9">
                      <div className="big-title float-left">
                        <h3><FormattedMessage id="Home.newsSubtitle1"/></h3>
                      </div>
                      <div className="search-title float-right">
                        <img className="arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"} alt=""/>
                      </div>
                      <div className="description-news d-inline-block">
                        <em> 2 months ago / By Person's name</em>
                        <p className="text-justify mt-2"><FormattedMessage id="Home.newsParagraph1"/></p>
                      </div>
                    </div>
                    <span className="divider-news w-100"></span>
                    <div className="col-lg-3 mt-2">
                      <img src={pathServer.PATH_IMG + "background.png"} className="img-fluid" alt=""/>
                    </div>
                    <div className="col-lg-9">
                      <div className="big-title float-left">
                        <h3><FormattedMessage id="Home.newsSubtitle2"/></h3>
                      </div>
                      <div className="search-title float-right">
                        <img className="arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"} alt=""/>
                      </div>
                      <div className="description-news d-inline-block">
                        <em> 2 months ago / By Person's name</em>
                        <p className="text-justify mt-2"><FormattedMessage id="Home.newsParagraph2"/></p>
                      </div>
                    </div>
                    <span className="divider-news w-100"></span>
                    <div className="col-lg-3 mt-2">
                      <img src={pathServer.PATH_IMG + "background.png"} className="img-fluid" alt=""/>
                    </div>
                    <div className="col-lg-9">
                      <div className="big-title float-left">
                        <h3><FormattedMessage id="Home.newsSubtitle3"/></h3>
                      </div>
                      <div className="search-title float-right">
                        <img className="arrow" width="25" src={pathServer.PATH_IMG + "icons/arrow.svg"} alt=""/>
                      </div>
                      <div className="description-news d-inline-block">
                        <em> 2 months ago / By Person's name</em>
                        <p className="text-justify mt-2"><FormattedMessage id="Home.newsParagraph3"/></p>
                      </div>
                    </div>

                  </div>
                </div>

                <div className="row justify-content-center">
                  <button type="button" className="btnMore"> More</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );

  }

}