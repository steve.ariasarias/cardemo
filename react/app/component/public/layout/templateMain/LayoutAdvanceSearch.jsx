import React,{Component} from 'react'
import {pathServer,CAR_YEAR_START, CURRENT_YEAR} from "../../../../Constants";
import {demoCars} from "../../../../data/cars"; /*This is Json demo cars, after that It's work with API*/
import Nouislider from 'nouislider-react';
import './nouislider.css';
import {FormattedMessage} from "react-intl";
import {makeCars} from "../../../../data/makes";
import {modelCars} from "../../../../data/model";
import {connect} from "react-redux";
import {loadMakeAndModel} from "../../../../action";

const optionsBody = [
  {
    label:"Sedan", value: "sedan"
  },
  {
    label:"SUV", value: "suv"
  },
  {
    label:"Coupe", value: "coupe"
  },
  {
    label:"Hatchback", value: "hatchback"
  },
  {
    label:"Wagon", value: "wagon"
  },
  {
    label:"Truck", value: "truck"
  },
  {
    label:"Pick-up Truck", value: "pickup-truck"
  },
  {
    label:"Minivan", value: "minivan"
  },
  {
    label:"Van ", value: "van"
  },
  {
    label:"Limousine", value: "limousine"
  },
  {
    label:"Roadster", value: "roadster"
  },
  {
    label:"Compact", value: "compact"
  },
  {
    label:"Convertible", value: "convertible"
  },
  {
    label:"Crossover", value: "crossover"
  }
];
class LayoutAdvanceSearch extends  Component {

  constructor(props) {
    super(props);
    this.state = {
      cars:[],
      price_range: "",
      yearDefaultDisabled : true,
      textValue: null,
      percent: null,
      value: 0,
      selectedView: '0',

    };
    this.onChangeMake = this.onChangeMake.bind(this);
  }

  componentWillMount() {
    let {makes, models} = this.props;
    if(!makes || !models){
      this.props.loadMakeAndModel()
    }

  }

  onChangeMake(e){
    this.setState({selectedView: e.target.value});
    this.props.onChangeMakes(e.target.value)
  }


  render() {

    const { priceFrom,priceTo,yearFrom,yearTo,zipCode,makes,models} = this.props;
    const { selectedView} = this.state;
    if(!makes || !models) return null;
    let modelItem = [];
    if(makes && models){
      modelItem = models.map(function(model,index) {
        if(model.makeId===selectedView){
          return <option  key={index} value={model.id}>{model.name}</option>;
        }
      });
    }


    return (
      <div id="search" className="text-center">
          <div className="form mt-3">
            <div className="row">

              <div className="col-md-4">
                <FormattedMessage id="Search.make">
                  {value => <select defaultValue="0" className="custom-select select1" onChange={this.onChangeMake}>
                    <option value="0">{value}</option>
                    {makes.map((option, index) =>{
                        return(
                          <option key={index} value={option.id}>{option.name}</option>
                        )
                      })
                    }
                    </select>}
                </FormattedMessage>
              </div>

              <div className="col-md-4">
                <FormattedMessage id="Search.model">
                  {value => <select  className="custom-select select1" onChange={this.props.onChangeModels} >
                    <option value="0">{value}</option>
                    {modelItem}
                  </select>}
                </FormattedMessage>
              </div>
              <div className="col-md-4">
                <FormattedMessage id="Search.bodyStyle">
                  {value => <select defaultValue="1"  className="custom-select select1" onChange={this.props.onChangeBody}>
                    <option value="1">{value}</option>
                    {
                      optionsBody.map((option, index) =>{
                        return(
                          <option key={index} value={option.name}>{option.label}</option>
                        )
                      })
                    }
                  </select>}
                </FormattedMessage>
              </div>
            </div>

            <div className="row pt-2 pb-2" id="rangeDivs">
              <div className="col-md-6">
                <div className="slider">
                  {priceFrom && priceTo
                  && (
                    <h6 className="pb-2"> <b className="float-left">${priceFrom}</b> <label><FormattedMessage
                      id="Search.price"/></label> <b className="float-right">${priceTo}</b> </h6>
                  )}
                  <Nouislider connect  range={{ min: 1000, max: 100000 }} step={1000} behaviour="tap" start={[10000, 60000]} onSlide={this.props.onSlide} />
                </div>
              </div>
              <div className="col-md-6">
                <div className="slider">
                  {yearFrom && yearTo
                  && (
                    <h6 className="pb-2"> <b className="float-left">{yearFrom}</b> <label><FormattedMessage
                      id="Search.year"/></label> <b className="float-right">{yearTo}</b> </h6>
                  )}
                  <Nouislider connect  range={{ min: CAR_YEAR_START, max: CURRENT_YEAR + 1 }} step={1} behaviour="tap" start={[1930, 2000]} onSlide={this.props.onSlideYear} />
                </div>
              </div>
            </div>
            <div id="advancedSearchOption">
              <p className="btn-link" onClick={this.props.hideAdvanced}><u><FormattedMessage
                id="Search.showLess"/></u></p>
            </div>
          </div>



      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    makes: state.makes,
    models:state.models
  }
};


export default connect(mapStateToProps, {loadMakeAndModel})(LayoutAdvanceSearch)