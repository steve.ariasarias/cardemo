import React, {Component} from 'react'
import {pathServer} from "../../../Constants";
import {LayoutCarTemplate} from "./LayoutCarTemplate.jsx";

export default class LayoutCarValue extends Component {

  constructor() {
    super();
    this.state = {
      classHeader: 'header-blue'
    }
  }

  render(){
    return(
      <LayoutCarTemplate colorNav={this.state.classHeader}>
      <div id="car-value">
        <div className="car-values-header"/>
        <div className="content-panel text-center">
          <div className="header">
            <div className="back float-left ml-2">
              <a href="catalog"><i className="back-icon"></i></a>
            </div>
            <div className="content-nav float-left ml-2">
              <a href="catalog" className="text nav-text">Home</a>
              <a className="text">></a>
              <a href="car-values" className="text nav-text">Car value</a>
            </div>
          </div>

          <p className="title">Welcome to Car Value page</p>
          <h1 className="subtitle">We have chosen two of the best options to get a car value for you.</h1>
          <p className="subtitle2">Make sure to have your VIN Number, Make, Model, Year, ZIP Code and Mileage before getting started.</p>
          <div className="row">
            <div className="car-value-option-panel right-bordered-panel">
              <img className="option-img" src={pathServer.PATH_IMG + "car-value-option-1.png"}/>
              <p className="left-option-text">Since 1918, when a young preacher's son named Les Kelley opened his used car dealership in Los Angeles and began creating a list of used cars he wanted to purchase from neighboring dealers, Kelley Blue Book® values have been recognized as the de facto standard in America. Today, no other pricing guide is as accepted and trusted by both consumers and the automotive industry for evaluating prices and determining values for new cars and used cars.</p>
            </div>
            <div className="car-value-option-panel">
              <img className="option-img" src={pathServer.PATH_IMG + "car-value-option-2.png"}/>
              <p className="right-option-text">Our retail value is what a person could reasonably pay for a vehicle at a dealer's lot. Our values are designed and intended to assist users in performing their own valuation of a particular used vehicle. ... NADA has been around for over 80 years and is the premier valuation guide in the used vehicle industry.</p>
            </div>
          </div>
          <div className="row">
            <div className="btn-option-panel right-bordered-panel"><a href="https://www.kbb.com/" target="_blank"><button className="btn btn-colored">GET STARTED</button></a></div>
            <div className="btn-option-panel"><a href="https://www.nadaguides.com/Cars" target="_blank"><button className="btn btn-colored">GET STARTED </button></a></div>
          </div>
        </div>
      </div>
      </LayoutCarTemplate>);
  }

}