import React, {Component} from 'react'
import {pathServer} from "../../../Constants";
import TextFieldGroup from '../../../TextFieldGroup';
import { validateInput} from '../../../authActions.js';
import {processLogin} from "../../../Api.js";
import {browserHistory} from 'react-router'

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modeLogIn: true,
      personSelected: true,
      token: 'token',
      email: '',
      password: '',
      errors: {},
      isLoading: false,
    };

    this.changePersonMode = this.changePersonMode.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  changeModeLogIn(isLogInActive) {
    this.setState({modeLogIn : isLogInActive, errors: {}});
    browserHistory.push("plan-selection")
  }


  changePersonMode(isPersonMode){
    this.setState({personSelected : isPersonMode});
  }

  isValid() {
    const { errors, isValid } = validateInput(this.state);
    if (!isValid) {
      this.setState({ errors });
    }
    return isValid;
  }

  onSubmit(e){
    e.preventDefault();
    const {email, password} = this.state;
    const user = {
      email: email,
      password: password
    };
    if (this.isValid()) {
      this.setState({ errors: {}, isLoading: true });
        processLogin(user).then((response) => {
          sessionStorage.setItem('email', response.dataAccount[0]);
          sessionStorage.setItem('firstName', response.dataAccount[1]);
          this.setState({isLoading: false })
          console.log("response",response)
          browserHistory.push('/admin/dashboard')
        }).catch((e) => {
          console.log("error",e);
          this.setState({errors: {form: 'Invalid Credentials', isLoading: false}})
        })
    }
    this.setState({isLoading: false })
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors, email, password, isLoading } = this.state;
    return (
      <div id="loginBox" className="blur-box center text-center p-3 pl-5 pr-5" style={{height: (errors!=undefined && errors.form)?"272px":"28%"}}>
        {(errors!=undefined && errors.form)?(errors.form && <div id="alert-authentication" className="alert alert-danger">{errors.form}</div>):null}
        <div className="btn-main-group" role="group"
             style={{top: (errors!=undefined && errors.form)?"-40px":"0",
               position: (errors!=undefined && errors.form)?"relative":""}}>
          <button type="button"
                  className={(this.state.modeLogIn ? "btn-secondary-selected" : "btn-secondary-no-selected")}
                  onClick={() => this.changeModeLogIn(true)}>LOG IN
          </button>
          <b>I</b>
          <button type="button"
                  className={(this.state.modeLogIn ? "btn-secondary-no-selected" : "btn-secondary-selected")}
                  onClick={() => this.changeModeLogIn(false)}>SIGN UP
          </button>
        </div>

        {!this.state.modeLogIn ?
          <div className="btn-group btn-group-options" role="group">
            <button type="button" className={"btn btn-first " + (this.state.personSelected ? " btn-selected" : "")}
                    onClick={() => this.changePersonMode(true)}>PERSON
            </button>
            <button type="button" className={this.state.personSelected ? "btn" : "btn btn-selected"}
                    onClick={() => this.changePersonMode(false)}>DEALER
            </button>
          </div> : null}



        <form onSubmit={this.onSubmit} style={{position:(errors!=undefined && errors.form)?"relative":null ,
                top: (errors!=undefined && errors.form)?"-40px":null}}>
          <div className="input-group">
            <div className="input-group-prepend">
              <img src={pathServer.PATH_IMG + "icons/user.svg"}/>
            </div>
            <TextFieldGroup
              field="email"
              placeholder="Email Address / Username"
              value={email}
              error={(errors)?errors.email:null}
              onChange={this.handleChange}
              type="text"
            />

          </div>
          <div className={"input-group " + (this.state.modeLogIn ? "last-item" : "")}>
            <div className="input-group-prepend">
              <img src={pathServer.PATH_IMG + "icons/lock.svg"}/>
            </div>
            <TextFieldGroup
              field="password"
              placeholder="Password"
              value={password}
              error={(errors)?errors.password:null}
              onChange={this.handleChange}
              type="password"
            />
          </div>

          {!this.state.modeLogIn ?
            <div>
              <div className="input-group">
                <div className="input-group-prepend">
                  <img src={pathServer.PATH_IMG + "icons/lock.svg"}/>
                </div>
                <input type="text" className="form-control" placeholder="Confirm Password" aria-label=""
                       aria-describedby="basic-addon1"/>
              </div>
              <div className="custom-control custom-checkbox last-item">
                <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                <label className="custom-control-label" htmlFor="customCheck1">By checking you accept <b>Terms and
                  Conditions</b></label>
              </div>
            </div> : null}
          <div className="form-group">
            <button className="btn-go" disabled={isLoading}>GO!</button>
          </div>
        </form>
      </div>
      )
  }
}