import React, {Component} from 'react';
import {pathServer} from "../../../Constants";
import {LayoutCarTemplate} from "./LayoutCarTemplate.jsx";
import Slider from 'react-slick';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

export  class LayoutAbout extends Component {

  constructor(props){
    super(props);
    this.state={
      classHeader:'header-blue',
      center: {
        lat: 40.3723571,
        lng: -111.9456269
      },
      zoom: 9
    };
   }
  render(){
    const settings = {
      dots: true,
      infinite: true,
      className: "center",
      centerMode: true,
      centerPadding: "30px",
      slidesToShow: 4,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 1000
    };

    const settings2 = {
      customPaging: function(i) {
        let baseUrl=pathServer.PATH_IMG;
        return (
          <a>
            <img className="img-reduced" src={`${baseUrl}/car/car${i}.png`} />
          </a>
        );
      },

      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
    };
    const settingsBrand = {
      dots: true,
      infinite: true,
      className: "center",
      centerMode: true,
      centerPadding: "30px",
      slidesToShow: 6,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 1000
    }
    const {classHeader}=this.state;
    const style = {
      width: '93%',
      height: '30%'
    }
    return (
      <LayoutCarTemplate colorNav={classHeader}>
        <section id="about-us">
            <div className="about-us-header"/>
            <div className="content-panel ">
              <div className="header">
                <div className="back float-left ml-2" >
                  <a href="catalog"><i className="back-icon"></i></a>
                </div>
                <div className="content-nav float-left ml-2">
                  <a href="catalog" className="text nav-text">Home</a>
                  <a className="text">&gt;</a><a href="" className="text nav-text">About Us</a></div>
              </div>
              <div className="info-car pl-4 pr-3">
                <div className="d-inline-flex mt-4">
                  <div className="description">
                    <h3>Who we are?</h3>
                    <p className="mt-4">ZoomAutos is Utah’s premier used auto magazine, offering affordable and effective advertising
                      for franchise and independent auto dealers.</p>
                    <p className="mt-2">
                      Our magazines are free to the public and can be picked
                      up weekly at 100s of locations including major grocery stores, convenience stores, restaurants and other
                      key high traffic areas along the Wasatch Front. To see if ZoomAutos Magazine is distributed in your area,
                      see the map below.
                    </p>
                    <p className="mt-2">
                      Thanks for visiting our site and supporting zoom.
                      We hope you enjoy reading our magazine as much as we enjoy publishing it!
                    </p>

                  </div>
                  <div className="ads">
                    <h3 className="text-center justify-content-center ">Ads</h3>
                  </div>
                </div>
              </div>
              <div className="description-car">
                <div className="location">
                  <h3>Our Dealer Partners</h3>
                  <div className="map-dealer">
                    <Map
                      google={this.props.google}
                      style={style}
                      initialCenter={{
                        lat: 40.4687908,
                        lng: -111.1896549
                      }}
                      zoom={8}>
                      <Marker
                        position={{lat: 40.899778, lng:  -111.995305}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }}/>
                      <Marker
                        position={{lat: 40.862588, lng: -111.939657}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }}/>

                      <Marker
                        position={{lat: 41.0769356, lng: -112.0024989}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.729992, lng: -111.895723}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.637321, lng:  -112.004951}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.555090, lng:   -111.836689}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.389034, lng: -111.675425}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.257262, lng: -111.694764}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.313887, lng:   -111.441771}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.077536, lng:   -111.751761}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 39.972265, lng:  -111.785713}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.214725, lng:   -110.372141}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />
                      <Marker
                        position={{lat: 40.413377, lng:  -109.535893}}
                        icon={{
                          url: pathServer.PATH_IMG + "circle.png",
                          anchor: new google.maps.Point(16,16),
                          scaledSize: new google.maps.Size(16,16)
                        }} />


                    </Map>
                  </div>
                </div>
                <div className="panel mr-5 ml-5 brands">
                  <Slider {...settingsBrand}>
                    <div>
                      <img  width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
                    </div>
                    <div>
                      <img   width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
                    </div>
                    <div>
                      <img  width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
                    </div>
                    <div>
                      <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
                    </div>
                    <div>
                      <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
                    </div>
                    <div>
                      <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
                    </div>
                    <div>
                      <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
                    </div>
                    <div>
                      <img width="85" src={pathServer.PATH_IMG + "leivalogo2.jpg"} />
                    </div>
                  </Slider>
                </div>
                <div className="text-center pb-5">
                  <h3>Meet our partners</h3>
                  <img className="logo-about mt-4" src={pathServer.PATH_IMG + "logo-about.png"} />
                </div>


              </div>
            </div>
        </section>
      </LayoutCarTemplate>
    );

  }

}

export default GoogleApiWrapper({
  apiKey: ("AIzaSyAJHDIApLbh7HHsYFK0j5U_z9qhVU4UyxY")
})(LayoutAbout)