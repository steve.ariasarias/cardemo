import React from 'react'
import {Route, IndexRoute} from 'react-router'
import LayoutMain from './LayoutMain.jsx'
import Login from './Login.jsx'
import LayoutCarDetail from './LayoutCarDetail.jsx'
import LayoutAbout from './LayoutAbout.jsx'
import LayoutInventory from './inventory/LayoutInventory.jsx'
import LayoutCarComparison from "./LayoutCarComparison.jsx";
import LayoutCarValue from "./LayoutCarValue.jsx";
import LayoutSellMyCar from "./LayoutSellMyCar.jsx";
import LayoutPlanSelection from "./LayoutPlanSelection.jsx";
import LayoutAccountInfo from "./LayoutAccountInfo.jsx";

import LayoutSavedSearches from "./../../admin/layout/search/LayoutSavedSearches.jsx";
import LayoutListing from "./../../admin/layout/listing/LayoutListing.jsx";
import LayoutCarSelected from "./../../admin/layout/listing/LayoutCarSelected.jsx";

import LayoutAccountDashboard from "./../../admin/layout/account/LayoutAccountDashboard.jsx";

import Dashboard from "./../../admin/Dashboard.jsx";
import LayoutListingsForm from "../../admin/layout/listing/LayoutListingsForm.jsx";
import Setting from "../Setting.jsx";

let Routes;
console.log("window.template",window.template)
switch (window.template) {
  case 'template1':
  default:
    require('./templateMain/templateMain.less');
    Routes = (
      <Route>
        <Route path="/catalog">
          <IndexRoute component={LayoutMain}/>
          <Route path="setting" component={Setting}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/account/dashboard" component={LayoutAccountDashboard}/>
          <Route exact path="/about" component={LayoutAbout}/>
          <Route exact path="/comparison/:vinsList" component={LayoutCarComparison}/>
          <Route exact path="/car-values" component={LayoutCarValue}/>
          <Route exact path="/sell" component={LayoutSellMyCar}/>
          <Route exact path="/plan-selection" component={LayoutPlanSelection}/>
          <Route exact path="/account-info/:planTypeId" component={LayoutAccountInfo}/>
          <Route path="inventory" component={LayoutInventory}>
            <Route path=":vin" component={LayoutCarDetail}/>
          </Route>
        </Route>
        <Route path="/admin">
          <IndexRoute component={Dashboard}/>
          <Route exact path="dashboard" component={Dashboard}> </Route>
          <Route exact path="account" component={LayoutAccountDashboard}> </Route>
          <Route exact path="saved" component={LayoutSavedSearches}> </Route>
          <Route exact path="listing" component={LayoutListing}> </Route>
          /*<Route exact path="/admin/listing" component={LayoutCarSelected}> </Route>*/
          <Route exact path="listing/form" component={LayoutListingsForm}> </Route>
        </Route>
      </Route>
    );
    break;
}

export default Routes

