import React, {Component} from 'react';
import {pathServer} from "../../../../Constants";
import {Link} from "react-router";
import {connect} from "react-redux";

const LoggedMenu = ({menuOptions, logout, refMethod}) => {
  return(
    <div id="logged-menu-options" ref={refMethod}>
      {
        menuOptions.map((menuOption, index)=>{
          return (
            <a href={menuOption.link}>
              <div key={index} className="logged-menu-option">
                {menuOption.displayName}
              </div>
            </a>
          );
        })
      }
      <div id="logout-menu-option" onClick={logout}>Log out</div>
    </div>
  )
};

class LayoutHeader extends Component {
  constructor(props) {
    super(props);
    this.state={
      showNavMenu:false,
      showMenuOptions:false,
      menuOptions:[
        {link:'/admin/dashboard', displayName:'Dashboard'},
        {link:'/admin/account', displayName:'Account'},
        {link:'/admin/listing', displayName:'My Listing'}],
    },
    this.showMenu=this.showMenu.bind(this);
    this.hideMenu=this.hideMenu.bind(this);
    this.openSessionMenu = this.openSessionMenu.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount(){
    window.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    window.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({showMenuOptions: false});
    }
  }

  openSessionMenu(){
    const {showMenuOptions} = this.state;
    this.setState({showMenuOptions:!showMenuOptions});
  }
  showMenu(){
    this.setState({showNavMenu: true});
    this.props.showMenuExpanded();
  }

  hideMenu(){
    this.setState({showNavMenu: false})
    this.props.showMenuExpanded();
  }

  logout(){
    sessionStorage.clear();
    location.href = "/logout";
  }

  render() {
    const {showMenuOptions, menuOptions} = this.state;
    let {email,firstName} = this.props;
    if(!email || email === "" && !firstName){
      email = sessionStorage.getItem("email")
      firstName = sessionStorage.getItem("firstName")
    }

    return (
      <div id="nav-car" className={this.props.colorNav}>
        <div className="menu-header navbar-fixed-top" ref="headerFixed">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-2 col-sm-3 text-center" id="menu-head-left">
                <div className="d-inline-flex dropdown">
                  {this.state.showNavMenu?
                    <i className = "fas fa-times close-icon mr-3 mt-2" onClick={this.hideMenu}></i>:
                    <i className="open-icon fas fa-bars mr-3 mt-2" onClick={this.showMenu}></i>}

                  <button className="btn dropdown-toggle" type="button" id="btnMenu1"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img className="idioma" src={pathServer.PATH_IMG + 'logo-en.png'} alt=""/>
                    <b>ENGLISH </b>
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <a className='dropdown-item '>ENGLISH</a>
                    <a className='dropdown-item '>ESPAÑOL</a>
                  </div>
                </div>

              </div>
              <div className="menu-center col-md-8 col-sm-6">
                <div className='dealer-logo text-center'>
                  <Link to="/catalog">
                    <img className="logo-header2" src={pathServer.PATH_IMG + "zoom-logo.png"} width="90"/>
                  </Link>
                </div>

              </div>
              {
                email?
                <div className="col-md-2 col-sm-3 text-left" id="menu-head-right">
                  <div id="logged-menu">
                    <a id="favorites-btn"><i className="far fa-star"/>Favorites</a>
                    <span id="logged-user">
                        <a className="user-greeting" onClick={()=> this.openSessionMenu()}>
                          Welcome, {firstName} <i className="fas fa-caret-down"/>
                        </a>
                      </span>
                  </div>
                  {showMenuOptions? <LoggedMenu menuOptions={menuOptions} logout={this.logout} refMethod={this.setWrapperRef}/> : null}
                </div>
                :
                <div className="col-md-2 col-sm-3 text-right" id="menu-head-right">
                  <Link to="/login">
                    <img src={pathServer.PATH_IMG + "icons/user.svg"}/>
                  </Link>
                </div>
              }
            </div>
            {this.state.showNavMenu?
              <div className="navbar-expanded mt-4">
                <nav aria-label="breadcrumb" className="row justify-content-center align-items-center" >
                  <ol className="breadcrumb text-center">
                    <li className="breadcrumb-item">
                      <Link activeClassName={location.pathname === '/catalog'? 'selected-breadcrumb-link': null} to="/catalog">HOME</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link activeClassName={location.pathname === '/catalog/inventory'? 'selected-breadcrumb-link': null} to="/catalog/inventory">INVENTORY</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link activeClassName={location.pathname === '/car-values'? 'selected-breadcrumb-link': null} to="/car-values">CAR VALUES</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link activeClassName={location.pathname === '/sell'? 'selected-breadcrumb-link': null} to="/sell">SELL MY CAR FOR FREE</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link activeClassName={location.pathname === '/about'? 'selected-breadcrumb-link': null} to="/about">US</Link>
                    </li>
                  </ol>
                </nav>
              </div>
              :null}
          </div>
        </div>
      </div>

    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    email: state.email,
    firstName:state.firstName
  }
};

export default connect(mapStateToProps)(LayoutHeader)