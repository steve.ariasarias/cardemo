import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import {LOCATION_ALL,CURRENT_LOCATION} from '../../../../Constants.js'

let map;
let marker;

const {
  Component
} = React;

export default class googleMap extends Component{

  constructor() {
    super();
    this.state = {
      locationsMap:null,
    }
  }

  componentWillReceiveProps(nextProps){
    this.renderMap(nextProps);
  }

  shouldComponentUpdate(){
    return false;
  }

  renderMap(props){
    map = new google.maps.Map(this.refs.map, {
      center:{lat :props.lat, lng:props.lng},
      mapTypeControl: false,
      styles: [{ stylers: [{ 'lightness': 4 }, { 'visibility': 'on' }] }],
      zoom:7,
      fullscreenControl: false,
      draggable: false,
      scrollwheel:  false,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      panControl: false,
      panControlOptions: {
        position: google.maps.ControlPosition.RIGHT_TOP
      },
      zoomControl: false,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.RIGHT_TOP
      },
      scaleControl: false,
      scaleControlOptions: {
        position: google.maps.ControlPosition.RIGHT_TOP
      },
      streetViewControl: false,
    });
    let{locations,locationId} = props;
    if(locationId === LOCATION_ALL || !locationId){
      locations.forEach(location =>{
        let showMarker = false;
        if(location.locationId === CURRENT_LOCATION)
          showMarker = true;
        this.geoCoder(location,locationId,showMarker);
      });
    }else{
      let locationFound = _.find(locations,location =>{return location.locationId === locationId});
      this.geoCoder(locationFound,locationId,true);
    }
  }

  componentDidMount(){
    this.renderMap(this.props);
  }

  geoCoder(location,locationId,showMarker){

    let that = this;
    let address = location.address + ', ' +location.city + ', '+ location.state + ' '+ location.zip;
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        let latitude = results[0].geometry.location.lat();
        let longitude = results[0].geometry.location.lng();
        if(locationId && locationId !== LOCATION_ALL){
          map.setCenter(new google.maps.LatLng(latitude, longitude));
        }
        marker = new google.maps.Marker({
          position: that.mapCenterLatLng(latitude, longitude),
          map: map
        });
        that.createMarker(map,marker,latitude,longitude);
        if(showMarker)
          google.maps.event.trigger(marker,'click');
      }
    });
  }

  createMarker(map,marker,lat,lng){
    let that = this;
    let infowindow = new google.maps.InfoWindow({
      content: 'div'
    });
    google.maps.event.addListener(marker,'click', function(){
      let div = document.createElement('div');
      ReactDOM.render(that._renderInfoWindow(lat,lng),div);
      infowindow.setContent(div);
      //infowindow.open(map,this);
    })
  }


  mapCenterLatLng(lat,lng) {
    return new google.maps.LatLng(lat, lng);
  }

  _renderInfoWindow(lat,lng){

  }

  render(){
    return(
      <div id="map" ref="map"/>
    )
  }

}