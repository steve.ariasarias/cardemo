import React, {Component} from 'react'
import {pathServer} from "../../../../Constants.js";
import FormUtils from "./../../../util/FormUtils";
import {Link} from "react-router";

export default class LayoutFooter extends Component {
  constructor(props){
    super(props);
    this.state = {
      btnFooter:false
    };
    this.onClick = this.onClick.bind(this);

  }
  componentDidMount() {
    window.addEventListener('scroll', () => {
      let y = FormUtils.getScrollTop();
      if (y < 200) {
        this.setState({btnFooter: false});

      }
      else {
        this.setState({btnFooter: true});
      }
    });
  }
  onClick(p, x) {
    let el = document.querySelector('back_to_top');
    let pointTo = FormUtils.getPosition(el);

    let y = FormUtils.getScrollTop();
    //window.scrollTo(point.x, point.y);
    let pointFrom = {
      x: 0,
      y: y //window.scrollY
    };
    scrollTo(pointFrom,pointTo,600);
  }
  render(){
    return (
      <div id="footer">
        <div className="container-fluid">
          <div className="row ml-0 mr-0 justify-content-center" >
            <div className="content-footer text-center">
              <h4> Email me price drops! </h4>
              <small className="mt-1"> Get discounts, sales and many others profits </small>

              <div className="row">
                <div className="col-sm-10">
                  <input type="text" className="form-control email-footer mt-4" placeholder="Type your email address"></input>
                </div>
                <div className="col-sm-1 mt-4 ml-2">
                  <a className="p-2 ml-2  text-dark aEmail" href="#" >
                    <i className="fas fa-check text-white"></i>
                  </a>
                </div>
              </div>


            </div>
          </div>
          <div className="nav-footer mt-5 pb-4">
            <div className="row  ml-0 mr-0 justify-content-center">
              <div className="col-lg-2 text-center">
                <img src={pathServer.PATH_IMG+"zoom-logo.png"} width="170"/>
              </div>
              <div className="col-lg-8">
                <nav aria-label="breadcrumb" className="row justify-content-center align-items-center">
                  <ol className="breadcrumb text-center">
                    <li className="breadcrumb-item">
                      <Link onClick={this.onClick} activeClassName={location.pathname === '/catalog'? 'selected-breadcrumb-link': null} to="/catalog">HOME</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link onClick={this.onClick} activeClassName={location.pathname === '/catalog/inventory'? 'selected-breadcrumb-link': null} to="/catalog/inventory">INVENTORY</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link onClick={this.onClick} activeClassName={location.pathname === '/car-values'? 'selected-breadcrumb-link': null} to="/car-values">CAR VALUES</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link onClick={this.onClick} activeClassName={location.pathname === '/sell'? 'selected-breadcrumb-link': null} to="/sell">SELL MY CAR FOR FREE</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link onClick={this.onClick} activeClassName={location.pathname === '/about'? 'selected-breadcrumb-link': null} to="/about">US</Link>
                    </li>
                  </ol>
                </nav>
              </div>
              <div className="col-lg-2">
                <a className="p-2" href="#"><i className="fab fa-facebook-f"></i></a>
                <a className="p-2" href="#"><i className="fab fa-instagram ig"></i></a>
                <a className="p-2" href="#"><i className="fab fa-twitter"></i></a>
                {/*<a id="back_to_top" className="p-2 ml-2 text-dark" href="#" onClick={this.onClick}>*/}
                {/*<i className="fas fa-arrow-up arrow"></i>*/}
                {/*</a>*/}
              </div>
            </div>
          </div>
        </div>

        <button onClick={this.onClick}  className="btn-back-top p-2 ml-2 text-dark" style={{display: this.state.btnFooter ? 'block' : 'none' }}>
          <i className="fas fa-arrow-up arrow"></i>
        </button>

      </div>
    );

  }

}