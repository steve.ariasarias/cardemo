import React, {Component} from 'react';

export default class Table extends Component {

  constructor() {
    super();
    this.renderRow = this.renderRow.bind(this);
  }

  renderRow(rowArray, rowIndex) {
    const isAdmin = this.props.admin;
    const equalSizes = this.props.equalSizes;
    return (<tr className={isAdmin && !this.props.emptyTitle && rowIndex==0 ? "first-white-row":null}>
      {rowArray.map(function (element, index) {
        return (<td className={isAdmin ? (index==rowArray.length-1 ? 'no-right-border light-padding':'light-padding'):null} width = {!equalSizes ? (index != 0 ? "22%" : "12%"):"50%"}>{element}</td>);;
      })}
    </tr>);
  }

  render() {
    return (<div>
      <table className={"table table-responsive-sm table-borderless" + (this.props.notStriped ? "":" table-striped")}>
        <thead className="thead-dark text-center">
        {this.props.title ?
          <tr>
            <th colSpan="20">{this.props.title}</th>
          </tr>:null}
        </thead>
        <tbody>
        {this.props.rows.map(function (row, index) {
          return this.renderRow(row, index);
        }.bind(this))}
        </tbody>
      </table>
    </div>);
  }
}