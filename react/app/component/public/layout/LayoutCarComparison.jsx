import React, {Component} from 'react';
import Table from "./common/Table.jsx";
import {pathServer} from "../../../Constants";
import Slider from "react-slick/lib";
import {LayoutCarTemplate} from "./LayoutCarTemplate.jsx";
import {getCarFromVin} from "../../../Api";
import {browserHistory} from "react-router";
import {connect} from "react-redux";
import {loadMakeAndModel} from "../../../action";

class LayoutCarComparison extends Component {

  constructor() {
    super();
    this.state = {
      classHeader: 'header-blue',
      vehicleCardWith: "90%",
      cars:[]
    },

    this.settings = {
      dots: false,
      infinite: true,
      className: "slider-panel center",
      centerMode: true,
      centerPadding: "30px",
      slidesToShow: 4,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 1000
    };
    this.renderVehicleCard = this.renderVehicleCard.bind(this);
    this.renderStars = this.renderStars.bind(this);
    this.renderCarsTable = this.renderCarsTable.bind(this);
    this.renderCarsCards = this.renderCarsCards.bind(this);
  }

  componentWillMount(){
    let {makes, models} = this.props;
    if(!makes || !models){
      this.props.loadMakeAndModel();
    }
  }

  componentDidMount(){
    let {vinsList} = this.props.params;
    let carVins = vinsList.split('-');
    let cars = [];
    carVins.forEach(vin => {
      getCarFromVin(vin).then((response) => {
        let foundCar = response.data.cars;
        if(foundCar.length > 0) {
          cars.push(foundCar[0]);
        }
        this.setState({cars: cars});
        console.log('cars:::',cars);
      }).catch((error) => {
        console.log(error)
      });
    })
  }

  renderVehicleCard(carData) {
    let {cars} = this.state;
    let cardHeight = 0;
    switch(cars.length){
      case 1:
        cardHeight = 250;
        break;
      case 2:
        cardHeight = 180;
        break;
      case 3:
        cardHeight = 160;
        break;
      case 4:
        cardHeight = 125;
    }
    return (
      <div>
        <div className="card" style={{height: cardHeight}}>
          <img className="card-img" src={carData.mainImageUrl || pathServer.PATH_IMG + "car.png"}/>
          <div className="card-details text-center">
            <div className="row text-center justify-content-center" style={{marginTop: cardHeight/2-25}}>
                <div className="btn-car-info" style={{marginRight: "10%"}} onClick={() => browserHistory.push('/catalog/inventory/'+carData.vin)}>DETAILS</div>
                <div className="btn-car-info">DELETE</div>
            </div>

          </div>

        </div>
        <div className="info-details">
          <p className="card-title text-center">{carData.make} {carData.model} {carData.year}</p>
          <div className="row icon-group text-center justify-content-center">
            <div className="info-icon">
              <img className="" src={pathServer.PATH_IMG + "icons/car.svg"}/>
              <p className="text-center card-details-title">{carData.body}</p></div>
            <div className="info-icon">
              <img className="" src={pathServer.PATH_IMG + "icons/doc.svg"}/>
              <p className="text-center card-details-title">{carData.title}</p></div>
            <div className="info-icon">
              <img className="" src={pathServer.PATH_IMG + "icons/setting.svg"}/>
              <p className="text-center card-details-title">{carData.transmission}</p></div>
            <div className="">
              <img className="" src={pathServer.PATH_IMG + "icons/gas-station.svg"}/>
              <p className="text-center card-details-title">{carData.cylinder}M/G</p></div>
          </div>
          <div className="row">
            <div className="col-sm-1">
              <span className="black-filled-star">☆</span>
            </div>
            <div className="col-sm-6">
              <p className="additional-info-card">34 favorites</p>
            </div>
            <div className="col-sm-1">
              <p className="price-info-card">${carData.price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
            </div>
          </div>

        </div>
      </div>
    );
  }

  renderStars(points) {
    return (<div>
      <span className="filled-star">☆</span>
      <span className="filled-star">☆</span>
      <span className="filled-star">☆</span>
      <span className="filled-star">☆</span>
      <span>☆</span>    {points}
    </div>);
  }

  isNotNull(value, sReplace){
    if(value && value!==null  && value!=="null" && value!=="")
      return value.substring(0,1).toUpperCase() + value.substring(1).toLowerCase()
    else
      return sReplace;
  }

  renderCarsTable(title){
    let {cars} = this.state;
    let transmissionRow = ["Transmission"];
    let driveRow = ["Drive"];
    let trimRow = ["Trim"];
    let bodyRow = ["Body"];
    let mileageRow = ["Mileage"];
    let titleRow = ["Title"];
    let cylinderRow = ["Cylinder"];
    let fuelRow = ["Fuel"];
    let intColorRow = ["Interior Color"];
    let extColorRow = ["Exterior Color"];
    let intConditionRow = ["Interior Condition"];
    let extConditionRow = ["Exterior Condition"];
    cars.forEach(car => {
      trimRow.push(this.isNotNull(car.trim,"-"));
      bodyRow.push(this.isNotNull(car.body,"-"));
      mileageRow.push(this.isNotNull(car.mileage,"-"));
      titleRow.push(this.isNotNull(car.title,"-"));
      transmissionRow.push(this.isNotNull(car.transmission,"-"));
      cylinderRow.push(this.isNotNull(car.cylinder,"-"));
      fuelRow.push(this.isNotNull(car.fuel,"-"));
      driveRow.push(this.isNotNull(car.drive,"-"));
      intColorRow.push(this.isNotNull(car.interiorColor,"-"));
      extColorRow.push(this.isNotNull(car.exteriorColor,"-"));
      intConditionRow.push(this.isNotNull(car.interiorCondition,"-"));
      extConditionRow.push(this.isNotNull(car.exteriorCondition,"-"));
    })
    return(<Table title={title}
                  rows={[trimRow, bodyRow, mileageRow,
                  titleRow, transmissionRow, cylinderRow, fuelRow,
                  driveRow, intColorRow, extColorRow, intConditionRow,
                  extConditionRow]}></Table>
    );
  }

  renderCarsCards(){
    let cardRow = ["General information"];
    let {cars} = this.state;
    let {makes,models} = this.props;
    console.log("makes",makes)
    console.log("models",models)
    cars.forEach(car => {
      const make = makes.find(x => x.id === car.makeId).name;
      const model = models.find(x => x.id === car.modelId).name;
      cardRow.push(this.renderVehicleCard({
        vin: car.vin,
        make: make,
        model: model,
        year: car.year,
        body: car.body,
        title: car.title,
        transmission: car.transmission,
        cylinder: car.cylinder,
        price: car.price,
        mainImageUrl: car.mainImageUrl
      }));
    })
    return(<Table notStriped={true}
                  rows={[cardRow]}></Table>);
  }

  render() {
    let {cars} = this.state;
    return (
      <LayoutCarTemplate colorNav={this.state.classHeader}>
      <div id="car-comparison">
          <div className="comparison-header"/>
          <div className="content-panel">
            <div className="header">
              <div className="back float-left ml-2">
                <a href="/catalog/inventory"><i className="back-icon"></i></a>
              </div>
              <div className="content-nav float-left ml-2">
                <a href="catalog/inventory" className="text nav-text">Inventory</a>
                <a className="text">></a>
                <a href="" className="text nav-text">Comparing</a>
              </div>
              <div className="options float-right">
                <div className="btn-group" role="group" aria-label="Basic example">
                  <a href={"mailto:?body="+window.location.href} className="p-2 email-option">
                    <i className="option-icon email-icon"/>
                  </a>
                  <a href="" className="p-2 sel">
                    <i className="option-icon printer-icon"/>
                  </a>
                </div>
              </div>
            </div>
            {this.renderCarsCards()}
            {this.renderCarsTable("Overview")}
            <div className="slider-panel bg-white " id="other-vehicles">
              <Slider {...this.settings}>
                <div>
                  <img width={this.state.vehicleCardWith} src={pathServer.PATH_IMG + "background.png"}/>
                  <div className="title-other">
                    <b className="float-left ">Make Model</b>
                    <b className="float-right text-success">$price</b>
                  </div>
                </div>
                <div>
                  <img width={this.state.vehicleCardWith} src={pathServer.PATH_IMG + "background.png"}/>
                  <div className="title-other">
                    <b className="float-left ">Make Model</b>
                    <b className="float-right text-success">$price</b>
                  </div>
                </div>
                <div>
                  <img width={this.state.vehicleCardWith} src={pathServer.PATH_IMG + "background.png"}/>
                  <div className="title-other">
                    <b className="float-left ">Make Model</b>
                    <b className="float-right text-success">$price</b>
                  </div>
                </div>
                <div>
                  <img width={this.state.vehicleCardWith} src={pathServer.PATH_IMG + "background.png"}/>
                  <div className="title-other">
                    <b className="float-left ">Make Model</b>
                    <b className="float-right text-success">$price</b>
                  </div>
                </div>
                <div>
                  <img width={this.state.vehicleCardWith} src={pathServer.PATH_IMG + "background.png"}/>
                  <div className="title-other">
                    <b className="float-left ">Make Model</b>
                    <b className="float-right text-success">$price</b>
                  </div>
                </div>

              </Slider>
            </div>
          </div>
      </div>
      </LayoutCarTemplate>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    email: state.email,
    makes: state.makes,
    models: state.models
  }
};

export default connect(mapStateToProps,{loadMakeAndModel})(LayoutCarComparison)
