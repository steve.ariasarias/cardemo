import React, {Component} from 'react'
import {pathServer, PERSON_MODE_CODE, DEALER_PRO_MODE_CODE,
  EMAIL_ALREADY_USED_MESSAGE, ERROR_DUPLICATE_RECORD_CODE} from "../../../Constants";
import {browserHistory} from "react-router";
import {saveAccount} from "../../../Api";
import {LayoutCarTemplate} from "./LayoutCarTemplate.jsx";

export default class LayoutAccountInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      classHeader: 'header-orange',
      personSelected: (this.props.params.planTypeId == PERSON_MODE_CODE),
      acceptedTerms: false,
      account:{},
      hidePassword: true,
      hideConfirmPassword: true,
      confirmPassword:'',
      emailTakenError: false,
      emailFormatError: false,
      passwdMatchError: false,
      phoneError: false,
      termsUnacceptedError: false,
      missingNameError: false,
      missingLastNameError: false,
      missingDealerNameError: false,
      missingDealerNumberError: false,
      missingPasswordError: false
    },
    this.changePersonMode = this.changePersonMode.bind(this);
    this.renderInput = this.renderInput.bind(this);
    this.renderForm = this.renderForm.bind(this);
    this.acceptTerms = this.acceptTerms.bind(this);
    this.saveAccount = this.saveAccount.bind(this);
    this.renderInput = this.renderInput.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.changePasswordHidden = this.changePasswordHidden.bind(this);
    this.changeConfirmPasswordHidden = this.changeConfirmPasswordHidden.bind(this);
    this.saltRounds = 10;
  }


  changePersonMode() {
    this.setState({personSelected: !this.state.personSelected});
  }

  acceptTerms() {
    this.setState({acceptedTerms: !this.state.acceptedTerms, termsUnacceptedError: false});
  }

  isValidEmail(value) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(value).toLowerCase());
  }

  getFormattedPhone(phoneNumber) {
    let formattedPhone = phoneNumber;
    if (phoneNumber.length >= 1)
      formattedPhone = '(' + phoneNumber.substring(0);
    if (phoneNumber.length > 3)
      formattedPhone = formattedPhone.substring(0, 4) + ') ' + formattedPhone.substring(4);
    if (phoneNumber.length > 6)
      formattedPhone = formattedPhone.substring(0, 9) + '-' + formattedPhone.substring(9);
    return formattedPhone;
  }

  saveAccount(){
    const planTypeId = this.props.params.planTypeId;
    let {confirmPassword, acceptedTerms, account} = this.state;
    this.setState({termsUnacceptedError: !acceptedTerms,
      emailFormatError: !this.isValidEmail(account.email),
      phoneError: !account.phone || account.phone.length < 14,
      passwdMatchError: account.password !== confirmPassword,
      missingNameError: !account.firstName || account.firstName.length === "",
      missingLastNameError: !account.lastName || account.lastName.length === "",
      missingDealerNameError: !account.dealerName || account.dealerName.length === "",
      missingDealerNumberError: !account.dealerNumber || account.dealerNumber.length === "",
      missingPasswordError: !account.password || account.password.length === ""}, function () {
      let {emailFormatError,
        passwdMatchError, phoneError, missingNameError, missingLastNameError,
        missingPasswordError, missingDealerNameError, missingDealerNumberError, termsUnacceptedError} = this.state;
      if(!passwdMatchError &&
        !emailFormatError && !phoneError && !termsUnacceptedError && !missingNameError && !missingLastNameError && !missingPasswordError
        && (planTypeId == PERSON_MODE_CODE || (!missingDealerNameError && !missingDealerNumberError)) && planTypeId >= PERSON_MODE_CODE && planTypeId <= DEALER_PRO_MODE_CODE){
        account = {...account, planTypeId: planTypeId, password: account.password, phone: account.phone.replace(/[^0-9]+/g, '')};
        console.log('account data: '+JSON.stringify(account));
        saveAccount(account).then((response) => {
          console.log('RESPONSE: '+JSON.stringify(response));
          console.log('SUCCESS - account saved ' + response.accountId + ':::' + JSON.stringify(account));
          sessionStorage.setItem('email', response.email);
          sessionStorage.setItem('firstName', response.firstName);
          browserHistory.push('/admin/listing/form');
        }).catch((error) => {
          console.log('ACCOUNT DATA ERROR: ' + JSON.stringify(error));
          if(error.code === ERROR_DUPLICATE_RECORD_CODE){
            this.setState({emailTakenError: true});
          }
        });
      }
    });
  }

  handleChange(e) {
    let {account, confirmPassword} = this.state;
    switch(e.target.name){
      case "email":
        this.setState({emailFormatError: !this.isValidEmail(e.target.value), emailTakenError: false});
        break;
      case "phone":
        this.setState({phoneError: e.target.value.length < 14});
        break;
      case "password":
        this.setState({passwdMatchError: e.target.value !== confirmPassword, missingPasswordError: e.target.value.length < 1});
        break;
      case "confirmPassword":
        this.setState({passwdMatchError: account.password !== e.target.value});
        break;
      case "name":
        this.setState({missingNameError: e.target.value.length < 1});
        break;
      case "lastName":
        this.setState({missingLastNameError: e.target.value.length < 1});
        break;
      case "dealerName":
        this.setState({missingDealerNameError: e.target.value.length < 1});
        break;
      case "dealerNumber":
        this.setState({missingDealerNumberError: e.target.value.length < 1});
    }
  }

  changePasswordHidden(){
    this.setState({hidePassword: !this.state.hidePassword});
  }

  changeConfirmPasswordHidden(){
    this.setState({hideConfirmPassword: !this.state.hideConfirmPassword});
  }

  renderInput(inputsData) {
    let that = this;
    return (
      <div className="row justify-content-center">
        {inputsData.map(function (inputData, index) {
          return (<div>
            {inputData.errorMessage && inputData.hasError ? <div className="message-error-box"><spa className="message-error">{inputData.errorMessage}</spa></div>:null}
            <div className={"input-group " + inputData.lengthClass}
                       style={inputsData.length > 1 && index == 0 ? {"marginRight": "14px"} : null}>
            {inputData.imagePath ?
              <div className="input-group-prepend">
                <img className={inputData.imageClass} src={pathServer.PATH_IMG + inputData.imagePath}/>
              </div> : null}
            <input maxlength={inputData.maxLength} type={(inputData.name==="password" && that.state.hidePassword || inputData.name==="confirmPassword" && that.state.hideConfirmPassword) ? "password" : "text"} className="form-control"
                   placeholder={inputData.placeholder} onChange={(event) => {inputData.onChange(event); that.handleChange(event)}} value={inputData.value} name={inputData.name}/>
            {inputData.rightImagePath ?
              <div className="right-image-input" onClick={inputData.name==="password" ? that.changePasswordHidden: that.changeConfirmPasswordHidden}>
                <img src={pathServer.PATH_IMG + inputData.rightImagePath} style={{opacity: inputData.rightImageOpacity}}/>
              </div> : null}
          </div>
            </div>);
        })}
      </div>);
  }

  renderForm(formData) {
    return (formData.map(function (inputData, index) {
      return (this.renderInput(inputData));
    }.bind(this)));
  }

  render() {
    let {emailTakenError, hidePassword, emailFormatError, phoneError,
      termsUnacceptedError, passwdMatchError, missingNameError, missingLastNameError,
      missingDealerNameError, missingDealerNumberError, missingPasswordError, hideConfirmPassword} = this.state;
    return (
      <LayoutCarTemplate colorNav={this.state.classHeader}>
        <div id="account-info">
          <div className="account-info-header"/>
          <div className="content-panel text-center">
            <div className="header">
              <div className="back float-left ml-2">
                <a href="../plan-selection"><i className="back-icon"></i></a>
              </div>
              <div className="content-nav float-left ml-2">
                <a href="../plan-selection" className="text nav-text">1. Select a Plan</a>
                <a className="text">></a>
                <a href="#" className="text nav-text selected-nav-text">2. Account info</a>
                <a className="text">></a>
                <a href="#" className="text nav-text">3. Listings</a>
              </div>
            </div>
            <img className={"user-mode-img" + (this.state.personSelected ? " person-img" : " dealer-img")}
                 src={pathServer.PATH_IMG + (this.state.personSelected ? "icons/user.svg" : "dealer-icon.png")}/>
            <div>
              <div className="btn-group btn-group-options" role="group">
                <button type="button" className="btn btn-selected">{this.state.personSelected ? "PERSON":"DEALER"}</button>
              </div>
              {this.state.personSelected ? null : this.renderInput([{
                placeholder: "Dealer name",
                name:"dealerName",
                imagePath: "store.png",
                lengthClass: "short-input",
                imageClass: "store-img",
                hasError: missingDealerNameError, errorMessage:"Please fill Dealer Name",
                onChange: (event) => this.setState({account: {...this.state.account, dealerName:event.target.value}})
              },
                {name:"dealerNumber", placeholder: "Dealer number", lengthClass: "short-input no-icon-input",
                  hasError: missingDealerNumberError, errorMessage:"Please fill Dealer Number", onChange: (event) => this.setState({account: {...this.state.account, dealerNumber:event.target.value}})}])}
              {this.renderForm([[{placeholder: "Name", name:"name", imagePath: "icons/user.svg", lengthClass: "short-input",
                hasError: missingNameError, errorMessage:"Please fill Name", onChange: (event) => this.setState({account: {...this.state.account, firstName:event.target.value}})},
                {name: "lastName", placeholder: "Last Name", lengthClass: "short-input no-icon-input",
                  hasError: missingLastNameError, errorMessage:"Please fill Last Name", onChange: (event) => this.setState({account: {...this.state.account, lastName:event.target.value}})}],
                [{name: "email", placeholder: "Email Address / Username", imagePath: "email.png",
                  lengthClass: "long-input", onChange: (event) => this.setState({account: {...this.state.account, email:event.target.value}}), errorMessage: "Email is not valid.",
                  hasError: emailFormatError}],
                [{name: "phone", placeholder: "Phone", imagePath: "icons/phoneNumber.svg", maxLength: "14",
                  lengthClass: "long-input",value: this.state.account.phone, onChange: (event) => this.setState({account: {...this.state.account, phone:this.getFormattedPhone(event.target.value.replace(/[^0-9]+/g, ''))}}),
                  errorMessage: "Phone number is incomplete.", hasError: phoneError}],
                [{name: "password", placeholder: "Password", imagePath: "icons/lock.svg", lengthClass: "long-input", isPassword: hidePassword, rightImagePath: "lockPassword.png",
                  rightImageOpacity: 1-0.6*hidePassword, hasError: missingPasswordError, errorMessage:"Please fill Password", onChange: (event) => this.setState({account: {...this.state.account, password: event.target.value}})}],
                [{name: "confirmPassword", placeholder: "Confirm Password", imagePath: "icons/lock.svg", lengthClass: "long-input", isPassword: hidePassword, onChange: (event) => this.setState({confirmPassword: event.target.value}),
                  errorMessage: "This password does not match the one entered above.", hasError: passwdMatchError, rightImagePath: "lockPassword.png", rightImageOpacity: 1-0.6*hideConfirmPassword}]])}
              <div className="row justify-content-center">
                <div className="custom-checkbox text-left">
                  <div className="square d-inline-flex" onClick={this.acceptTerms}
                       style={{"backgroundColor": (this.state.acceptedTerms ? "#17DC00" : "#FFFFFF")}}/>
                  <label className="d-inline-flex" htmlFor="customCheck1" onClick={this.acceptTerms}>By checking you
                    accept <b>Terms and
                      Conditions</b></label>
                  <div className="message-error-box" hidden={!termsUnacceptedError}><spa className="message-error">Review terms and conditions.</spa></div>
                </div>
              </div>
              <label className="error-warning" hidden={!emailTakenError} style={{marginBottom: "28px"}}>{EMAIL_ALREADY_USED_MESSAGE[0]}<br/>{EMAIL_ALREADY_USED_MESSAGE[1]}</label>
            </div>
            <div>
              <button className="btn btn-colored" onClick={this.saveAccount}>GO!</button>
            </div>
          </div>
        </div>
      </LayoutCarTemplate>
    );
  }


}