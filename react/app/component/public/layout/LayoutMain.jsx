import React, {Component} from 'react'
import LayoutHeader from './templateMain/LayoutHeader.jsx'
import {connect} from 'react-redux'
import {pathServer} from "../../../Constants";
import LayoutSearch from "./templateMain/LayoutSearch.jsx";
import LayoutWelcome from "./templateMain/LayoutWelcome.jsx";
import LayoutNew from "./templateMain/LayoutNew.jsx";
import LayoutMobile from "./templateMain/LayoutMobile.jsx";
import LayoutFooter from "./templateMain/LayoutFooter.jsx";
import {IntlProvider, addLocaleData} from "react-intl";
import en from "react-intl/locale-data/en";
import es from "react-intl/locale-data/es";
import FormUtils from "./../../util/FormUtils";

import localeData from "../../../../public/locales/data.json";

addLocaleData([...en, ...es]);

class LayoutMain extends Component {

  constructor() {
    super();
    this.onClick = this.onClick.bind(this);
    this.state = {
      language: null,
      messages: null,
      btnFooter: false,
      advancedSearchMode: false
    };
    this.setLanguage = this.setLanguage.bind(this);
    this.changeAdvancedSearchMode = this.changeAdvancedSearchMode.bind(this);
  }

  componentWillMount() {
    this.setLanguage(((navigator.languages && navigator.languages[0]) ||
      navigator.language).toLowerCase().split(/[_-]+/)[0]);
  }

  componentDidMount() {
    window.addEventListener('scroll', () => {
      let y = FormUtils.getScrollTop();
      if (y < 200) {
        this.setState({btnFooter: false});

      }
      else {
        this.setState({btnFooter: true});
      }
    });

  }

  setLanguage(language) {
    this.setState({
      language: language,
      messages: localeData[language] ||
        localeData.en
    });
  }

  onClick(p, x) {
    let el = document.querySelector('back_to_top');
    let pointTo = FormUtils.getPosition(el);

    let y = FormUtils.getScrollTop();
    //window.scrollTo(point.x, point.y);
    let pointFrom = {
      x: 0,
      y: y //window.scrollY
    };
    scrollTo(pointFrom, pointTo, 600);
  }

  changeAdvancedSearchMode(advancedSearchMode) {
    this.setState({advancedSearchMode: advancedSearchMode});
  }


  render() {
    return (
      <IntlProvider locale={this.state.language} messages={this.state.messages}>
        <div id="mainTemplate" className="layout-container-main">
          <div className="background">
          </div>
          <div id="headerTop" className="header-content">
            {/*<div className="background" style={{"height": this.state.backgroundHeight+"px"}}></div>*/}

            {/*<div className="banner-img-container" >*/}
            {/*<img className="car" src={pathServer.PATH_IMG + "BMW-X6.png"} />*/}
            {/*</div>*/}
            <LayoutHeader menuSelected="home" onChangeLanguage={this.setLanguage}
                          selectedLanguage={this.state.language}/>
            <div id="separatorHeader" className="separator-header"/>
          </div>
          <LayoutSearch changeAdvancedSearchMode={this.changeAdvancedSearchMode}/>
          <div className={"content1 " + (this.state.advancedSearchMode ? "bigger-content-margin": "smaller-content-margin")}>
            <LayoutWelcome/>
            <LayoutNew/>
          </div>
          <LayoutMobile/>
          <button onClick={this.onClick} className="btn-back-top p-2 ml-2 text-dark"
                  style={{display: this.state.btnFooter ? 'block' : 'none'}}>
            <i className="fas fa-arrow-up arrow"></i>
          </button>
          <LayoutFooter/>


        </div>
      </IntlProvider>

    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {}
};

export default connect(mapStateToProps)(LayoutMain)