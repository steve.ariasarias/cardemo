import React, {Component} from 'react';
import {pathServer} from "../../../Constants";
import Slider from 'react-slick';
import {getCarFromVin, setOrDeleteFavorite} from "../../../Api";
import {connect} from "react-redux";
import {browserHistory} from "react-router";
import {LayoutCarTemplate} from "./LayoutCarTemplate.jsx";
import LayoutLocationMap from "./common/googleMap.jsx"
import {loadMakeAndModel} from "../../../action";
import { componentWillAppendToBody } from "react-append-to-body";
import Popup from "reactjs-popup";
import {LayoutCarPrintPreview} from "./LayoutCarPrintPreview.jsx";
function MyComponent({ children }) {
  return <div className="map-google-seller-information">{children}</div>;
}

const AppendedMyComponent = componentWillAppendToBody(MyComponent);

class LayoutCarDetail extends Component {
  constructor(props){
    super(props);

    this.changeTab = this.changeTab.bind(this);
    this.isNotNull = this.isNotNull.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.state={
      classHeader:'header-blue',
      hidePrevArrow: true,
      hideNextArrow: false,
      imageSelectedIndex: 0,
      car:null,
      showDescription: true,
      showSellerInformation: false,
      showGetQuote: false,
      showReviews: false,
      showPaymentCalculator: false,
      showPreviewPage:false,
      locations: [
        {
          active: true,
          address: "115 East 900 South",
          address2: "",
          city: "Orem",
          configurations: {
            code: "facebook_link",
            createDate: 1550642139000,
            dealerId: "10000",
            locationId: "1",
            modifiedDate: 1550642139000,
            name: "facebook_link",
            value: "https://www.facebook.com/LeivaMotors/",
          },
          country: "US",
          dealerId: "10000",
          locationId: "1",
          name: "Provo",
          state: "UT",
          zip: "84097"
        }
      ]
    }
  }

  changeTab(e){
    let {showDescription, showSellerInformation, showGetQuote, showReviews, showPaymentCalculator} = this.state;
    if(e.target.name==="Description"){
      showDescription= true;
      showSellerInformation= false;
      showGetQuote= false;
      showReviews= false;
      showPaymentCalculator= false;
    } else if(e.target.name==="Seller Information"){
      showDescription= false;
      showSellerInformation= true;
      showGetQuote= false;
      showReviews= false;
      showPaymentCalculator= false;
    } else if(e.target.name==="Get a Quote"){
      showDescription= false;
      showSellerInformation= false;
      showGetQuote= true;
      showReviews= false;
      showPaymentCalculator= false;
    } else if(e.target.name==="Reviews"){
      showDescription= false;
      showSellerInformation= false;
      showGetQuote= false;
      showReviews= true;
      showPaymentCalculator= false;
    } else if(e.target.name==="Payment Calculator"){
      showDescription= false;
      showSellerInformation= false;
      showGetQuote= false;
      showReviews= false;
      showPaymentCalculator= true;
    }

    this.setState({showDescription: showDescription, showSellerInformation: showSellerInformation, showGetQuote: showGetQuote, showReviews: showReviews, showPaymentCalculator: showPaymentCalculator});
  }

  setOrRemoveFavorite(carId){
    let {car} = this.state;
    let {vin} = this.props.params;
    setOrDeleteFavorite(carId).then((response) => {
      console.log("update favorite");
      let foundIndex = car.favorites.indexOf(carId);
      if(foundIndex  > -1){
        car.favorites.splice(foundIndex,1);
      }else{
        car.favorites = [];
        car.favorites.push(carId)
      }
      this.setState({car:car});
      //this.getCar(vin)
    })
  }

  componentWillMount(){
    let {vin} = this.props.params;
    this.getCar(vin);
    let {makes, models} = this.props;
    if(!makes || !models){
      this.props.loadMakeAndModel()
    }
  }

  getCar(vin){
    getCarFromVin(vin).then((response) => {
      let foundCar = response.data.cars;
      if(foundCar.length > 0) {
        let car = foundCar[0];
        this.setState({car: car})
      }
    }).catch((error) => {
      console.log(error)
    });
  }

  isNotNull(value, sReplace){
    if(value!==null  && value!=="null" && value!=="")
      return value.substring(0,1).toUpperCase() + value.substring(1).toLowerCase()
    else
      return sReplace;
  }

  closeModal(){
    this.setState({showPreviewPage:false});
  }
  renderSlider(imagesUrls){
    const imagesPerSlide = 6;
    const lastSlideIndex = (Math.ceil((imagesUrls.length)/imagesPerSlide)-1)*imagesPerSlide;
    let {hidePrevArrow, hideNextArrow} = this.state;
    function CustomizedPrevArrow(props) {
      const {onClick} = props;
      return (
        <div className="slick-arrow fas fa-arrow-left arrow prev-arrow" onClick={onClick} hidden={hidePrevArrow} />
      );
    }
    function CustomizedNextArrow(props) {
      const {onClick} = props;
      return (
        <div className="slick-arrow fas fa-arrow-right arrow next-arrow" onClick={onClick} hidden={hideNextArrow} />
      );
    }
    const sliderSettings = {
      dots: false,
      infinite: false,
      slidesToShow: imagesPerSlide,
      slidesToScroll: imagesPerSlide,
      speed: 1000,
      arrows: true,
      prevArrow: <CustomizedPrevArrow/>,
      nextArrow: <CustomizedNextArrow/>,
      variableWidth: true,
      afterChange: (currentSlide) => {
        this.setState({
          hideNextArrow: (currentSlide === lastSlideIndex),
          hidePrevArrow: (currentSlide === 0)
        });
      },
      beforeChange: (prevSlide, currentSlide) => {
        this.setState({imageSelectedIndex: currentSlide});
      }
    };
    return(<div className="justify-content-center text-center">
      {
        imagesUrls ?
          <Slider {...sliderSettings}>
            {
              imagesUrls.map((imgUrl,index) => {
                return(
                  <div key={index}>
                    <img src={imgUrl} className="slider-image" onClick={() => this.setState({imageSelectedIndex: index})}/>
                  </div>
                )
              })
            }
          </Slider>:null
      }
    </div>);
  }

  handleClick(lat,lng,e){
    e.preventDefault();
    window.open('https://maps.google.com/maps?q=' + lat+ ',' + lng);
  }

  render(){
    const settings = {
      dots: true,
      infinite: true,
      className: "center",
      centerMode: true,
      centerPadding: "30px",
      slidesToShow: 4,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 1000
    };
    let isFavorite = false;
    let {showDescription, showSellerInformation, showGetQuote, showReviews, showPaymentCalculator, locations} = this.state;
    const {classHeader,car}=this.state;
    const {makes, models} = this.props;
    if(!car) return null;
    let {email} = this.props;
    if(!email || email === ""){
      email = sessionStorage.getItem("email");
      if(email && car && car.favorites && car.favorites.length > 0){
        isFavorite = car.favorites.indexOf(car.id) > -1;
      }
    }
    const make = (makes.find(make => make.id === car.makeId) || {}).name;
    const model = (models.find(model => model.id === car.modelId) || {}).name;
    let {imageSelectedIndex} = this.state;

    return (
      <LayoutCarTemplate colorNav={classHeader}>
        <section id="car-detail">
          <div className="headerCar"/>

          <div className="contentCar">
            <div className="header">
              <div className="back float-left ml-2" >
                <a style={{cursor:"pointer"}} onClick={browserHistory.goBack}><i className="back-icon "/></a>
              </div>
              <div className="content-nav float-left ml-2">
                <a style={{cursor:"pointer"}} onClick={browserHistory.goBack} className="text nav-text">Inventory</a>
                <a className="text">&gt;</a><a href="" className="text nav-text">{this.isNotNull(car.year,"")} {make} {model} {this.isNotNull(car.trim,"")}</a>
              </div>
              <div className="options float-right">
                <div className="btn-group" role="group" aria-label="Basic example">
                  <a href={"mailto:?body="+window.location.href} className="p-2 email-option">
                    <i className="option-icon email-icon"/>
                  </a>
                  <a href="#" className="p-2 sel" onClick={()=> this.setState({showPreviewPage:true})}><i className="ml-2 fas fa-print"/></a>
                  {
                    (email) ? <div className="d-inline-flex"><i className="star ml-2 fas fa-star" style={{color: (isFavorite)?'#00C2FF':'#d3d3d3'}} onClick={this.setOrRemoveFavorite.bind(this,car.id,email)}/>
                      <a href="" className="p-2 sel"><b>Compare </b><span className="badge">4</span></a></div> : null
                  }
                </div>
              </div>
            </div>
            <div className="info-car pl-3 pr-3">
              <div className="row justify-content-center mt-4">
                <div className="col-lg-6 left">
                  <span className="badge badge-dark h6">New</span><br/>
                  <h3>{this.isNotNull(car.year,"")} {make} {model} {this.isNotNull(car.trim,"")}</h3>
                  <h3>${this.isNotNull(car.price,"")}</h3>
                  <div className="border-orange"/>
                  <div className="row mt-4">
                    <div className="col-lg-6">
                      <h6 ><img src={pathServer.PATH_IMG + "icons/gas-station.svg"} width="20px" alt="" className="mr-2"/> {(car.cylinder!==null && car.cylinder!=="null" && car.cylinder!=="")?car.cylinder:"-"} M./Gln</h6>
                      <h6 ><img src={pathServer.PATH_IMG + "icons/data.svg"} width="20px" alt="" className="mr-2"/> Data</h6>
                      <h6 ><img src={pathServer.PATH_IMG + "icons/car.svg"} width="20px" alt="" className="mr-2"/> {(car.body!==null && car.body!=="null" && car.body!=="")?car.body.substring(0,1).toUpperCase() + car.body.substring(1).toLowerCase():"-"}</h6>
                      <h6 ><img src={pathServer.PATH_IMG + "icons/user-circle.svg"} width="20px" alt="" className="mr-2"/> By LeivaMotors</h6>
                    </div>
                    <div className="col-lg-6 ">
                      <h6 ><img src={pathServer.PATH_IMG + "icons/document.svg"} width="20px" alt="" className="mr-2"/>{(car.title!==null && car.title!=="null" && car.title!=="")?car.title.substring(0,1).toUpperCase() + car.title.substring(1).toLowerCase():"-"}</h6>
                      <h6 ><img src={pathServer.PATH_IMG + "icons/setting.svg"} width="20px" alt="" className="mr-2"/>{(car.transmission!==null  && car.transmission!=="null" && car.transmission!=="")?car.transmission.substring(0,1).toUpperCase() + car.transmission.substring(1).toLowerCase():"-"}</h6>
                      <h6 ><img src={pathServer.PATH_IMG + "icons/off.svg"} width="20px" alt="" className="mr-2"/> 10% OFF</h6>
                    </div>
                  </div>
                </div>
                <div className="justify-content-center" id="carousel-car">
                  <img className="image-selected" src={car.mainImageUrl ? (imageSelectedIndex === 0 ? car.mainImageUrl:car.imagesUrls[imageSelectedIndex - 1])
                    : pathServer.PATH_IMG + "car.png"} onClick={() => {if(imageSelectedIndex!=car.imagesUrls.length){this.setState({imageSelectedIndex: imageSelectedIndex+1})}}}
                       style={{cursor: "pointer"}}/>
                  {car.mainImageUrl ? this.renderSlider(car.imagesUrls ? [car.mainImageUrl].concat(car.imagesUrls) : [car.mainImageUrl]): null}
                </div>
              </div>
            </div>
            <div className="description-car mt-4">
              <ul className="nav nav-tabs pl-5 pr-3" id="myTab" role="tablist">
                <li className="nav-item">
                  <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                     aria-controls="home" aria-selected="true" name="Description" onClick={(e)=>this.changeTab(e)}>Description</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                     aria-controls="home" aria-selected="false" name="Seller Information" onClick={(e)=>this.changeTab(e)}>Seller Information</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                     aria-controls="profile" aria-selected="false" name="Get a Quote" onClick={(e)=>this.changeTab(e)}>Get a Quote</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                     aria-controls="contact" aria-selected="false" name="Reviews" onClick={(e)=>this.changeTab(e)}>Reviews</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                     aria-controls="contact" aria-selected="false" name="Payment Calculator" onClick={(e)=>this.changeTab(e)}>Payment Calculator</a>
                </li>
              </ul>
              <div className="tab-content" id="myTabContent">
                <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  {
                    (showDescription)?
                      <div className="row justify-content-center" id="principal">
                        <div id="id-content-tabs-detail-inventory" className="col-lg-6 p-5">

                          <p className="text-justify mb-3 mt-4">
                            {(car.description!==null  && car.description!=="null" && car.description!=="")?car.description:""}
                          </p>
                        </div>
                        <div className="col-lg-6 p-5 mb-5 content-description">
                          <div className="row">
                            <div className="col-lg-6">
                              <h6 className="bg-white text-dark mt-2 p-2">Year:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Make:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Model:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Trim:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Body:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Mileage:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">VIN:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Title Type:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Transmission:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Cylinders:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Fuel Type:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Drive Type:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Interior Color:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Exterior Color:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Interior Condition:</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">Exterior Condition:</h6>
                            </div>
                            <div className="col-lg-6 description-car">
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.year,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{make}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{model}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.trim,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.body,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.mileage,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{car.vin}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.title,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.transmission,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.cylinder,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.fuel,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.drive,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.interiorColor,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.exteriorColor,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.interiorCondition,"-")}</h6>
                              <h6 className="bg-white text-dark mt-2 p-2">{this.isNotNull(car.exteriorCondition,"-")}</h6>
                            </div>

                          </div>
                        </div>
                      </div>
                      : (showSellerInformation)?
                      <div className="row justify-content-center" id="principal">
                        <div id="id-col-lg-6-p-5" className="col-lg-6 p-5">
                          <div className="box-main-seller-information">
                            <img  className="img-seller-information" src={pathServer.PATH_IMG + "leivamotorslogo.jpg"} />
                            <div className="text-main-seller-information">
                              <p className="title-text-seller-information">Leiva Motors LLC.</p>
                              <p className="address-seller-information">386 E 892 S Provo, UT.</p>
                              <p className="phone-seller-information">(801)927 45577</p>
                              <p className="email-address-seller-information">provo@leivamotors.com</p>
                              <p className="web-seller-information"><u>www.leivamotors.com</u></p>
                            </div>
                            {car.isTextSeller == "true" ?
                              <div className="main-box-seller-information">
                                <p>Send a message from [User email]<span className="change-seller-information">Change</span></p>
                                <div className="box-seller-information">
                                  <input className="email-seller-information" placeholder="Hi, I would like to receive more information."/>
                                  <img className="arrow-box-seller-information"  src={pathServer.PATH_IMG + "arrow-right.jpg"} />
                                </div>
                              </div> : null }

                          </div>
                          <div className="map-seller-information">
                            <LayoutLocationMap lat={40.2256805} lng={-111.64971980000001} locations={locations} locationId={null}/>
                          </div>
                        </div>
                      </div>
                      :(showGetQuote)?
                        <div className="row justify-content-center" id="principal">
                          <div id="id-col-lg-6-p-5" className="col-lg-6 p-5">
                            <p className="text-justify mb-3 mt-4">
                              Get a Quote Info
                            </p>
                          </div>
                        </div>
                        : (showPaymentCalculator)?
                          <div className="row justify-content-center" id="principal">
                            <div id="id-col-lg-6-p-5" className="col-lg-6 p-5">
                              <p className="text-justify mb-3 mt-4">
                                Payment Calculator
                              </p>
                            </div>
                          </div>
                          : (showReviews)?
                            <div className="row justify-content-center" id="principal">
                              <div id="id-col-lg-6-p-5" className="col-lg-6 p-5">
                                <p className="text-justify mb-3 mt-4">
                                  show Reviews Info
                                </p>
                              </div>
                            </div>: null
                  }
                </div>
                {
                  (showSellerInformation)?
                    <AppendedMyComponent>
                      <div className="label-map-seller-information"
                           onClick={this.handleClick.bind(this, 40.2256805, -111.64971980000001)}
                           style={{display:""}}>DIRECTIONS</div>
                    </AppendedMyComponent>
                    : null
                }
                <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">Info2</div>
                <div className="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">Info3</div>
              </div>
              <div className="panel bg-white mb-4" id="other-vehicles">
                <Slider {...settings}>
                  <div>
                    <img  width="275" src={pathServer.PATH_IMG + "background.png"} />
                    <div className="title-other">
                      <b className="float-left ">Make Model</b>
                      <b className="float-right text-success">$price</b>
                    </div>
                  </div>
                  <div>
                    <img  width="275" src={pathServer.PATH_IMG + "background.png"} />
                    <div className="title-other">
                      <b className="float-left ">Make Model</b>
                      <b className="float-right text-success">$price</b>
                    </div>
                  </div>
                  <div>
                    <img  width="275" src={pathServer.PATH_IMG + "background.png"} />
                    <div className="title-other">
                      <b className="float-left ">Make Model</b>
                      <b className="float-right text-success">$price</b>
                    </div>
                  </div>
                  <div>
                    <img  width="275" src={pathServer.PATH_IMG + "background.png"} />
                    <div className="title-other">
                      <b className="float-left ">Make Model</b>
                      <b className="float-right text-success">$price</b>
                    </div>
                  </div>
                  <div>
                    <img  width="275" src={pathServer.PATH_IMG + "background.png"} />
                    <div className="title-other">
                      <b className="float-left ">Make Model</b>
                      <b className="float-right text-success">$price</b>
                    </div>
                  </div>

                </Slider>
              </div>
            </div>

          </div>

        </section>
        <Popup open={this.state.showPreviewPage}  onClose={this.closeModal}  modal  position="top center" contentStyle={{
         height: "80%", width: "85%" , overflowY:"scroll",
        }}>
          <div>
            <a className="close" onClick={this.closeModal}>
              &times;
            </a>
            <LayoutCarPrintPreview carSelected={car} makeName={make} modelName={model} closeModal={this.closeModal}></LayoutCarPrintPreview>
          </div>
        </Popup>
      </LayoutCarTemplate>
    );

  }

}
const mapStateToProps = (state, ownProps) => {
  return {
    email: state.email,
    makes: state.makes,
    models: state.models
  }
};

export default connect(mapStateToProps,{loadMakeAndModel})(LayoutCarDetail)