import React, {Component} from 'react'
import {pathServer} from "../../../Constants";
import {Link} from "react-router";
import {LayoutCarTemplate} from "./LayoutCarTemplate.jsx";

export default class LayoutSellMyCar extends Component {

  constructor() {
    super();
    this.state = {
      classHeader: 'header-orange'
    }
  }


  render(){
    return(
      <LayoutCarTemplate colorNav={this.state.classHeader}>
      <div id="sell-my-car">
        <div className="sell-my-car-header"/>
        <div className="content-panel text-center">
          <div className="header">
            <div className="back float-left ml-2">
              <a href="catalog"><i className="back-icon"></i></a>
            </div>
            <div className="content-nav float-left ml-2">
              <a href="catalog" className="text nav-text">Home</a>
              <a className="text">></a>
              <a href="sell" className="text nav-text">Sell My Car</a>
            </div>
          </div>

          <p className="title">Welcome to Sell My Car page</p>
          <p className="subtitle">Sell your car easily by following three steps</p>
          <div className="option-cards-panel  d-inline-flex justify-content-center">
            <div className="option-card">
              <img className="option-img" src={pathServer.PATH_IMG + "new-user-image.png"}/>
              <p className="subtitle">Create a free account</p>
            </div>
            <div className="option-card">
              <img className="option-img" src={pathServer.PATH_IMG + "list-image.png"}/>
              <p className="subtitle">List your car</p>
            </div>
            <div className="option-card">
              <img className="option-img" src={pathServer.PATH_IMG + "chart-image.png"}/>
              <p className="subtitle">Admin from your Zoom Dashboard</p>
            </div>
          </div>
          <div>
            <Link to="/plan-selection"><button className="btn btn-colored">Create Account</button></Link>
            <Link to="/login"><p className="sign-in-link">Sign in</p></Link>
          </div>
        </div>
      </div>
      </LayoutCarTemplate>
    );
  }
}