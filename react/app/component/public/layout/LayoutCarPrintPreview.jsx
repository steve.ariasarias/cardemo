import React, {Component} from 'react';
import {pathServer} from "../../../Constants";


export class LayoutCarPrintPreview extends Component {

  constructor(props){
    super(props);
    this.state={
      car :'',
    };
    this.isNotNull=this.isNotNull.bind(this);
  }

  componentDidMount(){
    let {carSelected}= this.props;

    this.setState({car:carSelected});
    console.log("carSelecred",carSelected);
  }

  isNotNull(value, sReplace){
    if(value!==null  && value!=="null" && value!=="")
      return value;
    else
      return sReplace;
  }


  render(){
    let {car} = this.state;
    const {makeName,modelName} = this.props;
    return (
        <section className="preview-print-car">

          <div className="content-body p-4">
            <div className="images-car w-100 d-inline-flex">
                <div className="col-r33 pr-2">
                  <img className="image-selected" width="300px" src={car.mainImageUrl} />
                </div>
            </div>
            <div className="description-car mt-2">
              <h3>{this.isNotNull(makeName)} {modelName} {car.year}</h3>
              <div className="border-orange"></div>
              <div className="list-data d-inline-flex mt-3 mb-3">
                  <h6 ><img src={pathServer.PATH_IMG + "icons/gas-station.svg"} width="20px" alt="" className="mr-3"/> {(car.cylinder!==null && car.cylinder!=="null" && car.cylinder!=="")?car.cylinder:"-"} M./Gln</h6>
                  <h6><svg className="mr-3"  width="20px" height="20px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.152 37.69">
                    <path id="ic_description_24px" d="M22.845,2H7.769a3.764,3.764,0,0,0-3.75,3.769L4,35.921A3.764,3.764,0,0,0,7.75,39.69H30.383a3.78,3.78,0,0,0,3.769-3.769V13.307Zm3.769,30.152H11.538V28.383H26.614Zm0-7.538H11.538V20.845H26.614ZM20.96,15.191V4.827L31.325,15.191Z" transform="translate(-4 -2)"/>
                    </svg>
                    {(car.title!==null && car.title!=="null" && car.title!=="")?car.title:"-"}
                  </h6>
                  <h6><img src={pathServer.PATH_IMG + "icons/car.svg"} width="20px" alt="" className="mr-3"/> {(car.body!==null && car.body!=="null" && car.body!=="")?car.body :"-"}</h6>
                  <h6><img src={pathServer.PATH_IMG + "icons/setting.svg"} width="20px" alt="" className="mr-3"/>{(car.transmission!==null  && car.transmission!=="null" && car.transmission!=="")?car.transmission:"-"}</h6>
                  <h6><img src={pathServer.PATH_IMG + "icons/data.svg"} width="20px" alt="" className="mr-3"/> Data</h6>
              </div>
              <div className="dealer-description mt-2 d-inline-flex w-100">
                <div className="col-r50">
                  <h4>Dealer Description</h4>
                  <p className="text mt-2 pr-4">
                    {this.isNotNull(car.description,"-")}
                  </p>
                </div>
                <div className="col-r50">
                  <h6 className="gray text-dark">Year: <b>{this.isNotNull(car.year,"-")}</b></h6>
                  <h6 className="white text-dark">Make: <b>{makeName}</b></h6>
                  <h6 className="gray text-dark">Model: <b>{modelName}</b></h6>
                  <h6 className="white text-dark">Trim: <b>{this.isNotNull(car.trim,"-")}</b> </h6>
                  <h6 className="gray text-dark">Body:  <b>{this.isNotNull(car.body,"-")}</b></h6>
                  <h6 className="white text-dark">Mileage: <b>{this.isNotNull(car.mileage,"-")}</b></h6>
                  <h6 className="gray text-dark">VIN:  <b>{car.vin}</b></h6>
                  <h6 className="white text-dark">Title Type: <b>{this.isNotNull(car.title,"-")}</b></h6>
                  <h6 className="gray text-dark">Transmission:  <b>{this.isNotNull(car.transmission,"-")}</b></h6>
                  <h6 className="white text-dark">Cylinders: <b>{this.isNotNull(car.cylinder,"-")}</b></h6>
                  <h6 className="gray text-dark">Fuel Type: <b>{this.isNotNull(car.fuel,"-")}</b></h6>
                  <h6 className="white text-dark">Drive Type: <b>{this.isNotNull(car.drive,"-")}</b></h6>
                  <h6 className="gray text-dark">Interior Color: <b>{this.isNotNull(car.interiorColor,"-")}</b></h6>
                  <h6 className="white text-dark">Exterior Color: <b>{this.isNotNull(car.exteriorColor,"-")}</b></h6>
                  <h6 className="gray text-dark">Interior Condition: <b>{this.isNotNull(car.interiorCondition,"-")}</b></h6>
                  <h6 className="white text-dark">Exterior Condition: <b>{this.isNotNull(car.exteriorCondition,"-")}</b></h6>
                </div>
              </div>
            </div>
          </div>
          <div className="content-footer d-inline-flex w-100">
            <div className="col-r25 pl-4">
              <h5 className="font-weight-bold">Leiva Motors LLC.</h5>
              <h6>386 E 892 S Provo, UT.</h6>
            </div>
            <div className="col-r50">
              <h6>(801)927 45577</h6>
              <h6>provo@leivamotors.com</h6>
            </div>
            <div className="col-r25">
              <h6 className="float-right mr-5">www.leivamotors.com</h6>
            </div>
          </div>
        </section>
    );
  }
}
