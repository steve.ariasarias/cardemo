import React, {Component} from 'react'
import LayoutInventoryListItem from "./LayoutInventoryListItem.jsx";

export default class LayoutInventoryList extends Component {
  constructor() {
    super();
  }

  render() {
    let {addToComparisonCue, onSearch,query} = this.props;
    return(
      <div id="list-inventory">
      {
        (this.props.cars.length > 0) ? this.props.cars.map(function(car,index){
          return <LayoutInventoryListItem key={index} car={car} addToComparisonCue={addToComparisonCue} onSearch={onSearch} />
        }):<div className="message-not-found">
          <h3>{`No listings found for your ${query.status.join(",").replace("certified","certify")} vehicle search`}</h3>
        </div>
      }
    </div>);
  }
}
