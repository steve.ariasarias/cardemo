import React, {Component} from 'react'
import {pathServer} from "../../../../Constants";
import {Link} from "react-router";
import {connect} from "react-redux";
import {findIndex} from "lodash/array";
import {setOrDeleteFavorite} from "../../../../Api";
import {loadMakeAndModel} from "../../../../action";

class LayoutInventoryMosaicItem extends Component {

  constructor(props) {
    super(props);
    this.renderCharacteristics = this.renderCharacteristics.bind(this);
  }

  componentWillMount(){
    let {makes, models} = this.props;
    if(!makes || !models){
      this.props.loadMakeAndModel()
    }
  }

  setOrRemoveFavorite(carId){
    setOrDeleteFavorite(carId).then((response) => {
      console.log("update favorite");
      this.props.onSearch(null)
    })
  }

  renderCharacteristics() {
    return (
      <div className="row icon-group mt-3 ml-xl-1 mr-xl-1">
        <div className="col-sm-3 text-center pr-0 pl-0"><img className="thumbnail-img img-2" src={pathServer.PATH_IMG + "icons/car.svg"}/>
          <p id="id-characteristics-text" className="characteristics-text ">Sedan</p></div>
        <div className="col-sm-3 text-center pr-0 pl-0"><img className="thumbnail-img img-4" src={pathServer.PATH_IMG + "icons/doc.svg"}/>
          <p className="characteristics-text ">Clean Title</p></div>
        <div className="col-sm-3 text-center pr-0 pl-0"><img className="thumbnail-img img-1" src={pathServer.PATH_IMG + "icons/setting.svg"}/>
          <p className="characteristics-text ">AWM</p></div>
        <div className="col-sm-3 text-center pr-0 pl-0"><img className="thumbnail-img img-3" src={pathServer.PATH_IMG + "icons/gas-station.svg"}/>
          <p className="characteristics-text">18M/G</p></div>

      </div>
    );
  }

  render() {
    let {car, email, makes, models} = this.props;
    let isFavorite = false;
    const make = (makes.find(make => make.id === car.makeId) || {}).name;
    const model = (models.find(model => model.id === car.modelId) || {}).name;
    const makeModel = make + " " +model;
    if(!car || !makes || !models) return null;
    if(!email || email === ""){
      email = sessionStorage.getItem("email");
      if(email && car.favorites && car.favorites.length > 0){
        isFavorite = findIndex(car.favorites,car.id)
      }
    }
    return (
      <div id="mosaic-inventory-item" className="card">
        <Link to={`/catalog/inventory/${car.vin}`} style={{height: "auto"}}>
          {
            (email) ? <i className="ml-2 fas fa-star star-mosaic" style={{color: (isFavorite)?'blue':'white'}} onClick={this.setOrRemoveFavorite.bind(this,car.id,email)}/> : null
          }
          <img id="id-card-img" className="card-img" src={car.mainImageUrl} style={{height: "12.7vw !important", width: "100% important"}}/>
        </Link>
        <div className="list-card-text">
          <label id="id-ml-2" className="ml-2">
            {car.year.substr(2) + " " + makeModel}</label>
          <br/>
          <b id="id-mb-2" className="ml-2">By. Leiva Motors</b>
          <h6 id="id-float-right" className="float-right">${car.price}</h6>
          {this.renderCharacteristics()}
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    email: state.email
  }
};

export default connect(mapStateToProps,{loadMakeAndModel})(LayoutInventoryMosaicItem)