import React, {Component} from 'react'
import LayoutInventoryMosaicItem from "./LayoutInventoryMosaicItem.jsx";

export default class LayoutInventoryMosaic extends Component {

  constructor() {
    super();
    this.renderCarsGrid = this.renderCarsGrid.bind(this);
    this.renderCarsRow = this.renderCarsRow.bind(this);
  }

  componentDidMount() {
    if(document.getElementsByClassName("container").item(0)){
      document.getElementsByClassName("container").item(0).style.padding = 0;
    }
  }

  renderCarsRow(cars) {
    let that = this;
    return (
      <div id="id-row" className="row">
        {cars.map(function (car, index) {
          return (<div key={index} id="id-col-sm-4" className="col col-sm-4">
                <LayoutInventoryMosaicItem car={car} onSearch={that.props.onSearch}/>
          </div>);
        })}
      </div>
    );
  }

  renderCarsGrid(cars, colsNumber) {
    return (
      <div className="container">
        {cars.map(function(car, index){
          if(index%colsNumber==0){
            return (<div key={index}>{this.renderCarsRow(cars.slice(index, index + colsNumber))}</div>);
          }
        }.bind(this))}
      </div>
    );
  }

  render() {
    let {query} = this.props;
    return (
      <div id="grid-inventory" className="class-grid-inventory">
        {
          (this.props.cars.length > 0)?this.renderCarsGrid(this.props.cars, 3):
            <div className="message-not-found">
              <h3>{`No listings found for your ${query.status.join(",").replace("certified","certify")} vehicle search`}</h3>
            </div>
        }
      </div>);
  }
}