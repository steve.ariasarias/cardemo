import React, {Component} from 'react'
import Nouislider from "nouislider-react";
import { optionBody, optionColor,CAR_YEAR_START,CURRENT_YEAR } from "../../../../Constants";
import {loadMakeAndModel} from "../../../../action";
import {connect} from "react-redux";

const RenderCheckBoxOptions = ({options, filterFunction, optionsSelected}) => {
  return(
    <div>
      {options.map((option, index) => {
          return (
            <div key={"make-"+index} className="custom-control custom-checkbox">
              <input type="checkbox" className="custom-control-input" id={option.id+option.name}
                     checked={(optionsSelected.filter(x=> x === option.id).length>0)}
                     onClick={()=>filterFunction(option.id)}/>
              <label key={option.id} className={"custom-control-label label-makes"} htmlFor={option.id+option.name}>{option.name}</label>
            </div>
          )})
      }
    </div>
  );
};

const RenderModelCheckBoxOptions = ({models, selectedView, filterFunction, optionsSelected, makes}) => {
  return(
    selectedView.map((makeSelected) => {
      let makeName = makes.find(make => make.id === makeSelected);
      return(
        <div>
          <label className="custom-control-label custom-label-title"> {makeName? makeName.name:""}</label>
          {models.map((model, index) => {
            if (model.makeId === makeSelected) {
              return (
                <div key={"model-"+index} className="custom-control custom-checkbox">
                  <input type="checkbox" className="custom-control-input" id={model.id+model.name}
                         checked={(optionsSelected.filter(x=> x === model.id).length>0)}
                         onClick={() => filterFunction(model.id)}/>
                  <label key={model.id} className={"custom-control-label"} htmlFor={model.id+model.name}>{model.name}</label>
                </div>
              )
            }
          })
          }
        </div>
      )
    })
  );
};


class LayoutInventorySearch extends Component {

  constructor() {
    super();
    this.state = {
      showMakes: true,
      countMakes :0,
      countModels:0,
      showModels: false,
      showYear: false,
      showMileage: false,
      showBodyStyle: false,
      showPrice: true,
      showColors: true,
      yearFrom: CAR_YEAR_START,
      yearTo: CURRENT_YEAR + 1,
      priceFrom: 1000,
      priceTo: 60000,
      selectedView: [],
      mileageFrom: 1000,
      mileageTo: 70000,
      filters:{
        'makes':[],
        'models':[],
        'yearFrom': null,
        'yearTo': null,
        'mileageFrom':null,
        'mileageTo':null,
        'body':[],
        'priceFrom':null,
        'priceTo':null,
        'exteriorColour':[],
      },
      makes:[],
      models:[],
    };
    this.onSelectMakes   = this.onSelectMakes.bind(this);
    this.onSelectModels  = this.onSelectModels.bind(this);
    this.onSelectMileage = this.onSelectMileage.bind(this);
    this.onSelectColors  = this.onSelectColors.bind(this);
    this.onSelectBody    = this.onSelectBody.bind(this);
    this.onSelectYear    = this.onSelectYear.bind(this);
    this.onSelectPrice   = this.onSelectPrice.bind(this);
    this.onSlidePrice    = this.onSlidePrice.bind(this);
    this.onSlideYear     = this.onSlideYear.bind(this);
    this.onSlideMileage  = this.onSlideMileage.bind(this);
    this.onCheckMakes    = this.onCheckMakes.bind(this);
    this.onCheckModels   = this.onCheckModels.bind(this);
    this.onCheckColour   = this.onCheckColour.bind(this);
    this.onCheckBody     = this.onCheckBody.bind(this);
  }

  componentWillReceiveProps(nextProps,nextContext){
    const {query} = nextProps;
    const {makeApi, modelApi} = this.props;
    if(!makeApi || !modelApi) return null;

    let {selectedView,filters} = this.state;
    if(query.makes && query.makes.length > 0) {
      selectedView[0] = query.makes[0];
      filters.makes[0] = query.makes[0];
    }
    if(query.models && query.models.length > 0) filters.models[0] = query.models[0];

    if(query.body && query.body.length > 0) filters.body[0] = query.body[0];

    if(query.makes && query.models){
      this.setState({
        countMakes:query.makes.length,
        countModels:query.models.length,
        selectedView: selectedView,
        filters:filters});
    }

    makeApi.map((make)=>{
      filters.makes.includes(make.id)? make['selected'] = true : make['selected'] = false;
    });
    modelApi.map((model)=>{
      filters.models.includes(model.id)? model['selected'] = true : model['selected'] = false;
    });

    const makes = [...makeApi];
    const models = [...modelApi];
    makes.sort((actual, next)=>{return(actual.selected === next.selected)? 0 : (actual.selected)? -1 : 1;});
    models.sort((actual, next)=>{return(actual.selected === next.selected)? 0 : (actual.selected)? -1 : 1;});

    this.setState({makes:makes,models:models});
  }

  componentWillMount(){
    const {makeApi, modelApi} = this.props;
    if(!makeApi || !modelApi){
      this.props.loadMakeAndModel()
    }
  }

  componentDidMount() {
    const {filters,yearFrom,yearTo} = this.state;
    filters.yearFrom = yearFrom;
    filters.yearTo = yearTo;
    this.setState({filters:filters});
  }

  onSlideYear(render, handle, value, un, percent) {
    let {query} = this.props;
    let value1 = Math.round(value[0]);
    let value2 = Math.round(value[1]);
    const {filters} = this.state;
    filters.yearFrom = value1;
    filters.yearTo = value2;
    this.setState({yearFrom: value1, yearTo: value2,filters:filters});
    query.yearFrom = ""+value1;
    query.yearTo = ""+value2;
    this.props.onSearch(query);
  };

  onSlidePrice(render, handle, value, un, percent) {
    let {query} = this.props;
    let value1 = Math.round(value[0]);
    let value2 = Math.round(value[1]);
    const {filters} = this.state;
    filters.priceFrom = value1;
    filters.priceTo = value2;
    this.setState({priceFrom: value1, priceTo: value2,filters:filters});
    query.priceFrom = value1;
    query.priceTo = value2;
    this.props.onSearch(query);
  };

  onSlideMileage(render, handle, value){
    let {query} = this.props;
    const {filters} = this.state;
    let value1 = Math.round(value[0]);
    let value2 = Math.round(value[1]);
    filters.mileageFrom = value1;
    filters.mileageTo = value2;
    this.setState({mileageFrom:value1, mileageTo:value2,filters:filters});
    query.mileageFrom = value1;
    query.mileageTo = value2;
    this.props.onSearch(query);
  }


  onSelectMakes() {
    this.setState({showMakes: !this.state.showMakes});
  }

  onSelectModels() {
    this.setState({showModels: !this.state.showModels});
  }

  onSelectColors() {
    this.setState({showColors: !this.state.showColors});
  }

  onSelectBody() {
    this.setState({
      showBodyStyle: !this.state.showBodyStyle
    });
  }


  onSelectYear(){
    this.setState({showYear: !this.state.showYear});
  }

  onSelectMileage(){
    this.setState({showMileage: !this.state.showMileage});
  }

  onSelectPrice(){
    this.setState({showPrice: !this.state.showPrice});
  }

  renderColoredCircle(color) {
    return (<span className="dot" style={{"backgroundColor": color}}/>);
  }

  renderSliderOption(minValue, maxValue, step, startLeftValue, startRightValue, slideMethod, unitChar) {
    return (<div className="slider text-center">
      {startLeftValue && startRightValue
      && (
        <div className="pb-2">
          <button className="btn-value ml-2">{unitChar+startLeftValue}</button>
          - <button type="button" className="btn-value ml-2">{unitChar+startRightValue}</button>
        </div>
      )}
      <Nouislider connect range={{min: minValue, max: maxValue}} step={step} behaviour="tap"
                  start={[startLeftValue, startRightValue]} onSlide={slideMethod}/>
    </div>);
  }

  renderOptions(optionList) {
    return (
      optionList.map(
        function (option, index) {
          return (
            <div key={index} className="custom-control custom-checkbox">
              <input type="checkbox" className="custom-control-input" id={option.id}/>
              <label key={option.id} className={"custom-control-label"} htmlFor={option.id}>{option.name}</label>
            </div>
          )})
    );
  }

  onCheckMakes(selectedMake){
    const {countMakes, filters,countModels, makes} = this.state;
    let {query,makeApi} = this.props;
    let {selectedView} = this.state;
    const res = filters.makes.filter(make => {return make === selectedMake});
    let newFilters = filters;
    if(res.length > 0){
      newFilters.makes.splice( newFilters.makes.indexOf(selectedMake), 1 );
      let modelsLength = newFilters.models.length;
      newFilters.models = [];
      selectedView = newFilters.makes;
      const sortedMakes = this.sortElements(makeApi,selectedView);
      this.setState({
        countMakes:countMakes-1,
        countModels:countModels - modelsLength,
        filters:newFilters,
        selectedView: selectedView,
        makes:sortedMakes
      });
    }else{
      newFilters.makes.push(selectedMake);
      selectedView = newFilters.makes;
      const sortedMakes = this.sortElements(makeApi,selectedView);
      this.setState({
        countMakes:countMakes+1,
        filters:newFilters,
        selectedView: selectedView,
        makes:sortedMakes});
    }
    query.makes = newFilters.makes;
    query.models = newFilters.models;
    this.props.onSearch(query);
  }

  onCheckModels(selectedModel){
    const {filters, countModels,models} = this.state;
    const res = filters.models.filter(model => {return model === selectedModel});
    let {query,modelApi} = this.props;
    let newFilters = filters;
    if(res.length > 0){
      newFilters.models.splice( newFilters.models.indexOf(selectedModel), 1 );
      const sortedModels = this.sortElements(modelApi,newFilters.models);
      this.setState({filters:newFilters,countModels:countModels-1,models:sortedModels});
    }else{
      newFilters.models.push(selectedModel);
      const sortedModels = this.sortElements(modelApi,newFilters.models);
      this.setState({filters:newFilters,countModels:countModels+1,models:sortedModels});
    }
    query.models = newFilters.models;
    this.props.onSearch(query);
  }

  onCheckColour(selectedColour){
    const {filters} = this.state;
    let {query} = this.props;
    const res = filters.exteriorColour.filter(colour => {return colour === selectedColour});
    let newFilters = filters;
    if(res.length > 0){
      newFilters.exteriorColour.splice( newFilters.exteriorColour.indexOf(selectedColour), 1 );
      this.setState({filters:newFilters});
    }else{
      newFilters.exteriorColour.push(selectedColour);
      this.setState({filters:newFilters});
    }
    query.exteriorColour = newFilters.exteriorColour;
    this.props.onSearch(query);
  }

  onCheckBody(selectedBody){
    const {filters} = this.state;
    let {query} = this.props;
    const res = filters.body.filter(body => {return body === selectedBody});
    let newFilters = filters;
    if(res.length > 0){
      newFilters.body.splice( newFilters.body.indexOf(selectedBody), 1 );
      this.setState({filters:newFilters});
    }else{
      newFilters.body.push(selectedBody);
      this.setState({filters:newFilters});
    }
    query.body = newFilters.body;
    this.props.onSearch(query);
  }

  sortElements(list,selectedList){
    const newList = [...list];

    if ('selected' in newList[0]){
      newList.map((element)=>{
        selectedList.includes(element.id)? element.selected = true : element.selected = false;
      });
    }else{
      newList.map((element)=>{
        selectedList.includes(element.id)? element['selected'] = true : element['selected'] = false;
      });
    }
    newList.sort((actual, next)=>{return(actual.selected === next.selected)? 0 : (actual.selected)? -1 : 1});
    return newList;
  }

  render() {
    let {countMakes,selectedView,countModels,makes,models} = this.state;
    let {query}  = this.props;

    return (
      <div id="inventory-search">
        <nav className="nav flex-column">
          <li className="nav-item" onClick={this.onSelectMakes}>
            {this.state.showMakes ? <i className = "fas fa-caret-up"/> : <i className="fas fa-caret-down"/>}
            <button type="button" className="btn btn-search-option" onClick={this.onSelectMakes}>Makes</button>
            <b className="count"> ({countMakes})</b>
          </li>
          <div className={this.state.showMakes ? "makes-inventory": "list-none"}>
            <RenderCheckBoxOptions options={makes} filterFunction={this.onCheckMakes} optionsSelected={query.makes}/>
          </div>

          <li className="nav-item" onClick={this.onSelectModels}>
            {this.state.showModels ? <i className = "fas fa-caret-up"/> : <i className="fas fa-caret-down"/>}
            <button type="button" className="btn btn-search-option" onClick={this.onSelectModels}>Model</button>
            <b className="count"> ({countModels})</b>
          </li>
          {this.state.showModels ?
            <div className="makes-inventory">
            <RenderModelCheckBoxOptions models={models} selectedView={selectedView} filterFunction={this.onCheckModels} optionsSelected={query.models} makes={makes}/>
          </div> : null}


          <li className="nav-item"  onClick={this.onSelectYear}>
            {this.state.showYear ? <i className = "fas fa-caret-up"/> : <i className="fas fa-caret-down"/>}
            <button type="button" className="btn btn-search-option" onClick={this.onSelectYear}>Year</button>
          </li>
          {this.state.showYear ? this.renderSliderOption(CAR_YEAR_START,(CURRENT_YEAR + 1), 1, this.state.yearFrom, this.state.yearTo, this.onSlideYear,'') : null}

          <li className="nav-item" onClick={this.onSelectMileage}>
            {this.state.showMileage ? <i className = "fas fa-caret-up"/> : <i className="fas fa-caret-down"/>}
            <button type="button" className="btn btn-search-option" onClick={this.onSelectMileage}>Mileage</button>
          </li>
          {this.state.showMileage ? this.renderSliderOption(1000,1000000, 1000, this.state.mileageFrom, this.state.mileageTo, this.onSlideMileage,'') : null}

          <li className="nav-item" onClick={this.onSelectBody}>
            {this.state.showBodyStyle ? <i className = "fas fa-caret-up"/> : <i className="fas fa-caret-down"/>}
            <button type="button" className="btn btn-search-option" onClick={this.onSelectBody}>Body Style</button>
          </li>
          {this.state.showBodyStyle ?  <RenderCheckBoxOptions options={optionBody} filterFunction={this.onCheckBody} optionsSelected={query.body} /> : null}
          <li className="nav-item" onClick={this.onSelectPrice}>
            {this.state.showPrice ? <i className = "fas fa-caret-up"/> : <i className="fas fa-caret-down"/>}
            <button type="button" className="btn btn-search-option" onClick={this.onSelectPrice}>Price</button>
          </li>
          {this.state.showPrice ? this.renderSliderOption(1000, 100000, 1000, this.state.priceFrom, this.state.priceTo, this.onSlidePrice,'$') : null}

          <li className="nav-item" onClick={this.onSelectColors}>
            {this.state.showColors ? <i className = "fas fa-caret-up"/> : <i className="fas fa-caret-down"/>}
            <button type="button" className="btn btn-search-option" onClick={this.onSelectColors}>Colors</button>
          </li>
          {this.state.showColors ?   <RenderCheckBoxOptions options={optionColor} filterFunction={this.onCheckColour} optionsSelected={query.exteriorColour}/> : null}
        </nav>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    makeApi: state.makes,
    modelApi:state.models
  }
};


export default connect(mapStateToProps, {loadMakeAndModel})(LayoutInventorySearch)