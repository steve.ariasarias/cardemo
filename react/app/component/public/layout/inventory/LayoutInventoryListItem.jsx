import React, {Component} from 'react'
import {pathServer} from "../../../../Constants";
import {browserHistory, Link} from "react-router";
import {setOrDeleteFavorite} from "../../../../Api";
import {findIndex} from "lodash/array";
import {connect} from "react-redux";
import {loadMakeAndModel} from "../../../../action";

class LayoutInventoryListItem extends Component {

  constructor(props) {
    super(props);
    this.renderCharacteristics = this.renderCharacteristics.bind(this);
  }

  componentWillMount(){
    let {makes, models} = this.props;
    if(!makes || !models){
      this.props.loadMakeAndModel()
    }
  }

  setOrRemoveFavorite(carId){
    setOrDeleteFavorite(carId).then((response) => {
      console.log("update favorite");
      this.props.onSearch(null)
    })
  }

  viewMore(vin){
    browserHistory.push('/catalog/inventory/'+vin)
  }

  renderCharacteristics() {
    let {car, addToComparisonCue, email} = this.props;
    let isFavorite = false;
    if(!email || email === ""){
      email = sessionStorage.getItem("email");
      if(email && car.favorites && car.favorites.length > 0){
        isFavorite = findIndex(car.favorites,car.id)
      }
    }
    return (
      <div className="d-inline-flex">
        <div className="d-inline-flex icon-group">
          <div className="characteristic-info"><img className="thumbnail-img" src={pathServer.PATH_IMG + "icons/car.svg"}/>
            <p className="characteristics-text text-center">{car.body}</p></div>
          <div className="characteristic-info"><img className="thumbnail-img" src={pathServer.PATH_IMG + "icons/doc.svg"}/>
            <p className="characteristics-text text-center">{car.title}</p></div>
          <div className="characteristic-info"><img className="thumbnail-img" src={pathServer.PATH_IMG + "icons/setting.svg"}/>
            <p className="characteristics-text text-center">{car.transmission}</p></div>
          <div className="characteristic-info"><img className="thumbnail-img" src={pathServer.PATH_IMG + "icons/gas-station.svg"}/>
            <p className="characteristics-text">{car.cylinder}M/G</p></div>
        </div>
        <div className="right-options">
          {email ? <div className="text-right">
            <button className="btn-less-opacity" onClick={()=>addToComparisonCue(car.vin)}>+ COMPARE</button>
          </div>:null}
          {email ? <div className="star-icon"  style={{marginLeft: "15px",color: (isFavorite)?"blue":"white"}} onClick={(email) ? this.setOrRemoveFavorite.bind(this,car.id,email):null}>
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                   viewBox="0 0 43.244 43.244" style={{enableBackground:"new 0 0 43.244 43.244"}} fill-rule="nonzero" fill="#D2D2D2">
                <path fill="#D2D2D2" d="M22.69,1.785l4.555,14.143l14.872-0.016c0.619,0,1.127,0.492,1.127,1.111
	c0,0.365-0.19,0.698-0.46,0.905l0,0l-12.047,8.714l4.619,14.142c0.19,0.587-0.127,1.206-0.714,1.397
	c-0.381,0.127-0.794,0.032-1.079-0.206L21.626,33.26L9.611,42.022c-0.508,0.365-1.206,0.254-1.555-0.238
	c-0.222-0.302-0.27-0.683-0.159-1H7.881l4.635-14.142L0.469,17.927c-0.508-0.365-0.619-1.063-0.254-1.556
	c0.238-0.333,0.603-0.492,0.984-0.46l14.793,0.016l4.571-14.143c0.19-0.587,0.809-0.905,1.397-0.73
	C22.325,1.182,22.578,1.451,22.69,1.785L22.69,1.785z M25.372,17.387L21.626,5.753l-3.762,11.635l0,0
	c-0.143,0.46-0.571,0.778-1.063,0.778L4.579,18.149l9.904,7.159l0,0c0.381,0.286,0.556,0.778,0.397,1.254l-3.793,11.619l9.857-7.19
	c0.381-0.286,0.921-0.302,1.333-0.016l9.872,7.206l-3.778-11.571c-0.175-0.46-0.032-1,0.381-1.302l9.92-7.159l-12.19,0.016
	C25.991,18.197,25.531,17.879,25.372,17.387L25.372,17.387z" stroke="#D2D2D2" stroke-width="1.5"/>
              </svg>
          </div>:null}
        </div></div>
    );
  }

  render() {
    const {car, makes, models} = this.props;
    if(!car || !makes || !models) return null;
    const make = (makes.find(make => make.id === car.makeId) || {}).name;
    const model = (models.find(model => model.id === car.modelId) || {}).name;
    const makeModel = make + " " +model;
    return (<div className="list-card">
        <div className="list-card-invisible-container" onClick={this.viewMore.bind(this,car.vin)}/>
        <img className="d-flex align-self-center mr-3 car-img" src={car.mainImageUrl || pathServer.PATH_IMG + "car.png"}
             alt="car image"/>
        <div className="list-card-text">
          <label className="title">{car.year.substr(2)} {makeModel}</label><b>By. Leiva Motors</b><h6 className="float-right">${car.price}</h6>
          <p className="description-text">{car.description}</p>
          {this.renderCharacteristics()}
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    email: state.email,
    makes: state.makes,
    models: state.models
  }
};

export default connect(mapStateToProps,{loadMakeAndModel})(LayoutInventoryListItem)
