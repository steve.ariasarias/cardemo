import React, {Component} from 'react';
import {pathServer} from "../../../../Constants.js";
import Slider from 'react-slick';
export default class LayoutInventorySlide extends Component {
  constructor() {
    super();
    this.settings = {
      dots: false,
      infinite: true,
      speed: 1000,
      slidesToShow: 2,

    };
    this.state = {
      carSliders:[
        {id:"1",status:"New", class:"model1 " ,label:"Make Model",number: "001",image:"BMW-X6.png"},
        {id:"2",status:"Certified", class:"model2 " ,label:"Make Model",number: "002",image:"BMW-X6.png"},
        {id:"3",status:"New", class:"model1 " ,label:"Make Model",number: "003",image:"BMW-X6.png"},
        {id:"4",status:"Certified", class:"model2 " ,label:"Make Model",number: "004",image:"BMW-X6.png"},
      ]
    }
  }
  render() {
    let {carSliders} = this.state;
    return (
      <div id="carousel-inventory">
        <div id="carousel-inventory">
          <div id="id-justify-content-center" className="justify-content-center">
            <Slider {...this.settings}>
              {carSliders.map(function (car,index) {

                return (
                  <div id="id-carousel-p-3" className={"carousel p-3 " + (car.class) + (index == 2 ? "borderLeftRed": null)}>
                    <div className="d-flex">
                      <div className="float-left p-2 pl-4">
                        <span className="badge badge-secondary">{car.status}</span>
                        <h4 className="mt-1 ">{car.label}</h4>
                        <h4>{car.number}</h4>
                      </div>
                      <div className="float-right p-2 pt-3 mx-auto ">
                        <img width="280" src={pathServer.PATH_IMG + car.image}/>
                      </div>
                    </div>
                    <div className="make-model-word">
                      <button className="btnMore ml-2">MORE</button>
                    </div>
                  </div>
                )
              })}
            </Slider>
          </div>
        </div>
      </div>

    );
  }
}