import React, {Component} from 'react'

import LayoutInventoryGrid from "./LayoutInventoryGrid.jsx";
import LayoutInventoryList from "./LayoutInventoryList.jsx";
import LayoutInventoryMosaic from "./LayoutInventoryMosaic.jsx";
import LayoutInventorySearch from "./LayoutInventorySearch.jsx";
import LayoutInventorySlide from "./LayoutInventorySlide.jsx";
import LayoutInventoryFilter from "./LayoutInventoryFilter.jsx";
import LayoutHeader from "../common/LayoutHeader.jsx";
import LayoutFooter from "../common/LayoutFooter.jsx";
import {searchInventoryPublicApi} from "../../../../Api";
import {LIST_INVENTORY_VIEW_MODE,CAR_YEAR_START, CURRENT_YEAR} from "../../../../Constants";

export default class LayoutInventory extends Component {
  constructor() {
    super();
    this.state = {
      cars: [],
      rowsNumber: 5,
      colsNumber: 3,
      pageNumber: 1,
      viewMode: 1,
      classHeader: 'header-orange',
      comparisonCue: [],
      orderList :[5, 10, 15, 20],
      showHeaderExpanded:false,
      query:{
        makes:[],
        models:[],
        body:[],
        yearFrom:CAR_YEAR_START.toString(),
        yearTo:CURRENT_YEAR.toString(),
        status:[],
        exteriorColour:[],
        offset:0,
        size:1000
      }
    };
    this.refHeader=React.createRef();
    this.refBody=React.createRef();
    this.prevPage = this.prevPage.bind(this);
    this.nextPage = this.nextPage.bind(this);
    this.changeView = this.changeView.bind(this);
    this.addToComparisonCue = this.addToComparisonCue.bind(this);
    this.getCars = this.getCars.bind(this);
    this.onChangeHeader=this.onChangeHeader.bind(this);
    this.orderNewOldest= this.orderNewOldest.bind(this);
    this.orderByPrice= this.orderByPrice.bind(this);

  }

  componentWillMount() {
    //this.getCars("");
  }

  componentDidMount(){
    let {state} = this.props.location;
    const {query} = this.state;

    if(!this.props.children){
      console.log("param1",state);
      if(state){
        this.setState({query:{...this.state.query, state}});
      }
      this.getCars(state? state:query);
    }
    let pageContent = this.refBody.current;
    if(pageContent){
      // let pagecopy = pageContent.cloneNode(true);
      let blurryContent = this.refHeader.current;
      // blurryContent.appendChild(pagecopy);
      window.onscroll = function() { blurryContent.scrollTop = window.pageYOffset;}
    }
  }

  getCars(query) {
    if(!query){
      query = this.state.query;
    }
    searchInventoryPublicApi(query).then((response) => {
      let dataCars= response.data.cars;
      dataCars.sort(function (a,b) {
        a=new Date(a.createdDate*1000);
        b=new Date(b.createdDate*1000);
        return b - a;
      });
      this.setState({cars: dataCars,query:query});
    }).catch((error) => {
      console.log(error)
    });
  }

  changeView(viewMode) {
    this.setState({viewMode: viewMode});
    if(viewMode === LIST_INVENTORY_VIEW_MODE){
      this.setState({colsNumber: 1});
    } else {
      this.setState({colsNumber: 3});
    }
  }
  orderNewOldest(type){
    const {cars}=this.state;
    let newCars=cars;
    if(type=="asc"){
      newCars.sort(function (a,b) {
        a=new Date(a.createdDate*1000);
        b=new Date(b.createdDate*1000);
        return a - b;
      });
    } else if(type == "desc"){
      newCars.sort(function (a,b) {
        a=new Date(a.createdDate*1000);
        b=new Date(b.createdDate*1000);
        return b - a;
      });
    }
    this.setState({cars: newCars});
  }

  orderByPrice(type){
    const {cars}=this.state;
    let newCars=cars;
    if(type=="asc"){
      newCars.sort(function (a,b) {
        return a.price - b.price;
      });
    } else if(type == "desc"){
      newCars.sort(function (a,b) {
        return b.price - a.price;
      });
    }
    this.setState({cars: newCars});
  }


  prevPage() {
    if (this.state.pageNumber !== 1) {
      this.setState({pageNumber: this.state.pageNumber - 1});
    }
  }

  nextPage() {
    if (this.state.pageNumber !== Math.ceil(this.state.cars.length / (this.state.rowsNumber * this.state.colsNumber))) {
      this.setState({pageNumber: this.state.pageNumber + 1});
    }
  }

  addToComparisonCue(carVin) {
    let cue = this.state.comparisonCue;
    this.state.comparisonCue.length === 4 ? alert('The maximum quantity of vehicles to compare is 4') :
      (cue.indexOf(carVin) < 0 ? cue.push(carVin) : null);
    this.setState({comparisonCue: cue});
  }
  onChangeHeader(){
    let {showHeaderExpanded} = this.state;
    this.setState({showHeaderExpanded: !showHeaderExpanded});
  }

  render() {
    const {classHeader,orderList, rowsNumber, colsNumber, pageNumber, query} = this.state;
    let {showHeaderExpanded,cars} = this.state;
    let firstIndex = (rowsNumber * colsNumber) * (pageNumber - 1);
    let lastIndex = firstIndex + (rowsNumber * colsNumber);

    return (
      (this.props.children) ?
        this.props.children :
        <div id="inventory">
          <div className={showHeaderExpanded ? " blurryscroll-expanded " :" blurryscroll "}  aria-hidden="true"  ref={this.refHeader} >
            <div className={"headerCar"}/>
            <div style={{height: "100rem", backgroundColor:"white"}}/>
          </div>
          <LayoutHeader  colorNav={classHeader} showMenuExpanded={this.onChangeHeader}/>
          <div  ref={this.refBody}>
            <div className="headerCar"/>
            <div id="main-inventory" className="main-inventory">
              {/*<b id="title-inventory" className="w-100 mb-3 d-flex justify-content-center">Inventory</b>*/}
              <div className="container-fluid" id="panel-inventory">
                <LayoutInventorySlide/>

                <div className="col-r100 d-inline-flex inventory-container">
                  <div className="col-r26">
                    <LayoutInventoryFilter changeView={this.changeView} comparisonCue={this.state.comparisonCue}
                                           onSearch={this.getCars} orderNewOldest={this.orderNewOldest} orderPrice={this.orderByPrice}/>
                    <LayoutInventorySearch query={query} onSearch={this.getCars}/>
                  </div>
                  <div className="col-r74">
                    {this.state.viewMode === 1 ?
                      <LayoutInventoryGrid cars={cars.slice(firstIndex, lastIndex)}
                                           query={query}
                                           addToComparisonCue={this.addToComparisonCue}
                                           onSearch={this.getCars} /> :
                      this.state.viewMode === 2 ?
                        <LayoutInventoryMosaic cars={cars.slice(firstIndex, lastIndex)}
                                               query={query}
                                               addToComparisonCue={this.addToComparisonCue}
                                               onSearch={this.getCars}/> :
                        this.state.viewMode === 3 ?
                          <LayoutInventoryList cars={cars.slice(firstIndex, lastIndex)}
                                               query={query}
                                               addToComparisonCue={this.addToComparisonCue}
                                               onSearch={this.getCars}/> : false}
                  </div>
                </div>
                <div className="filter-pagination d-inline-flex">
                  <div className="dropdown">
                    <button className="paginate-button btn dropdown-toggle" type="button" id="btnMenu"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i className="fas fa-list-ul mr-1"></i>
                      <b>{rowsNumber}</b>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                      {orderList.map((value)=>{
                        return(
                          <a className="dropdown-item" href="#!" onClick={() => this.setState({rowsNumber: value})}>{value}</a>
                        );
                      })}
                    </div>
                  </div>
                  <nav aria-label="Page navigation" className="float-right">
                    <ul className="pagination">
                      <li className="page-item">
                        <a className="page-link" href="#!" aria-label="Previous" onClick={this.prevPage}>
                          <svg className="arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.152 30.152">
                            <path id="ic_arrow_forward_24px" d="M15.076,0,12.419,2.657,22.934,13.191H0V16.96H22.934L12.419,27.495l2.657,2.657L30.152,15.076Z" transform="translate(30.152 30.152) rotate(-180)"/>
                          </svg>

                        </a>
                      </li>
                      <li className="page-text">
                        <a><b>{pageNumber}</b> OF {Math.ceil(cars.length / (rowsNumber * colsNumber))}</a>
                      </li>
                      <li className="page-item">
                        <a className="page-link" href="#!" aria-label="Next" onClick={this.nextPage}>
                          <svg className="arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.152 30.152">
                            <path id="ic_arrow_forward_24px" d="M19.076,4,16.419,6.657,26.934,17.191H4V20.96H26.934L16.419,31.495l2.657,2.657L34.152,19.076Z" transform="translate(-4 -4)"/>
                          </svg>

                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
          <LayoutFooter/>
        </div>
    );
  }
}