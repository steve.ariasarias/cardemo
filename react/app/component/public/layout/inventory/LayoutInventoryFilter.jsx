import React, {Component} from 'react'
import {pathServer} from "../../../../Constants";
import {Link} from "react-router";
import {searchInventoryApi} from '../../../../Api.js';
import {findIndex} from "lodash/array";

export default class LayoutInventoryFilter extends Component {

  constructor() {
    super();
    this.state={
      selectedView:1,
      orderByDate: 'asc',
      orderByPrice: 'asc',
    };
    this.changeView = this.changeView.bind(this);
    this.changeOrderByDate = this.changeOrderByDate.bind(this);
    this.changeOrderByPrice= this.changeOrderByPrice.bind(this);
    this.onChangeSearch = this.onChangeSearch.bind(this);

  }

  changeView(viewMode){
    this.props.changeView(viewMode);
    this.setState({selectedView:viewMode});
  }

  onChangeSearch(event){
    const query = event.target.value != '' ? {
      "query":{
        "bool":{
          "should":[
            {"match": {"model_id": {
                  "query":event.target.value.toLowerCase(),
                  "minimum_should_match": "90%"
                }}},
            {"match": {"make_id": {
                  "query":event.target.value.toLowerCase(),
                  "minimum_should_match": "90%"
                }}}
          ]
        }}
    } : {};
    this.props.onSearch(query);
  }

  changeOrderByDate() {
    const {orderByDate} = this.state;
    this.props.orderNewOldest(this.state.orderByDate);
    if(orderByDate == 'asc'){
      this.setState({orderByDate : 'desc'});
    } else {
      this.setState({orderByDate : 'asc'});
    }
  }
  changeOrderByPrice(){
    const {orderByPrice} = this.state;
    this.props.orderPrice(this.state.orderByPrice);
    if(orderByPrice == 'asc'){
      this.setState({orderByPrice : 'desc'});
    } else {
      this.setState({orderByPrice : 'asc'});
    }
  }


  render() {
    let {email} = this.props;
    if(!email || email === ""){
      email = sessionStorage.getItem("email");
    }
    return (
       <div id="filter-inventory">
         <div className="filter-1">
           <ul className="list-inline d-inline-flex col-r100 ">
             <div className="filter-view col-r49">
               <li className="list-inline-item" onClick={()=>this.changeView(1)}>
                 <svg fill={this.state.selectedView==1?'red':'#d2d2d2'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.921 33.921">
                   <path id="ic_insert_photo_24px" d="M36.921,33.152V6.769A3.78,3.78,0,0,0,33.152,3H6.769A3.78,3.78,0,0,0,3,6.769V33.152a3.78,3.78,0,0,0,3.769,3.769H33.152A3.78,3.78,0,0,0,36.921,33.152ZM13.365,22.787l4.711,5.672,6.6-8.5,8.48,11.307H6.769Z" transform="translate(-3 -3)"/>
                 </svg>
               </li>
               <li className="list-inline-item" onClick={()=>this.changeView(2)}><svg fill={this.state.selectedView==2?'red':'#d2d2d2'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.036 24.498">
                 <path id="ic_view_module_24px" d="M4,16.307h9.422V5H4ZM4,29.5h9.422V18.191H4Zm11.307,0h9.422V18.191H15.307Zm11.307,0h9.422V18.191H26.614ZM15.307,16.307h9.422V5H15.307ZM26.614,5V16.307h9.422V5Z" transform="translate(-4 -5)"/>
               </svg>
               </li>
               <li className="list-inline-item" onClick={()=>this.changeView(3)}>
                 <svg fill={this.state.selectedView==3?'red':'#d2d2d2'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.036 26.383">
                   <path id="ic_view_list_24px" d="M4,21.96h7.538V14.422H4Zm0,9.422h7.538V23.845H4ZM4,12.538h7.538V5H4Zm9.422,9.422H36.036V14.422H13.422Zm0,9.422H36.036V23.845H13.422ZM13.422,5v7.538H36.036V5Z" transform="translate(-4 -5)"/>
                 </svg>
               </li>
             </div>
             {email ? <Link  className="col-r51" to={{ pathname: (this.props.comparisonCue.length > 0 ? '/comparison/'+this.props.comparisonCue.join('-'):'')}}>
               <li className="list-inline-item compare col-r100"><b> Compare </b><span className="badge">{this.props.comparisonCue.length}</span></li>
             </Link>:null}
           </ul>
         </div>
         <div className="filter-2 col-r100 d-inline-flex">
            <div className="col-r49">
                <button onClick={this.changeOrderByDate} className="sort-new btn-filter col-r100" >
                  <svg xmlns="http://www.w3.org/2000/svg" width="42.475" height="52.158" viewBox="0 0 42.475 52.158">
                    <g id="Group_951" data-name="Group 951" transform="translate(-4680.525 104)">
                      <path id="Union_2" data-name="Union 2" d="M.045,29.682,5.7,24.026l4.818,4.818V0h8V37h-.09V48.158Z" transform="translate(4680.48 -100)"/>
                      <path id="Union_3" data-name="Union 3" d="M.045,29.682,5.7,24.026l4.818,4.818V0h8V37h-.09V48.158Z" transform="translate(4723.045 -55.842) rotate(180)"/>
                    </g>
                  </svg>
                  <span>{this.state.orderByDate == 'asc' ?  "Newest - Oldest" : "Oldest - Newest"}</span></button>
            </div>
            <div className="col-r51">
                <button  onClick={this.changeOrderByPrice} className="sort-hight btn-filter col-r100" >
                  <svg xmlns="http://www.w3.org/2000/svg" width="42.475" height="52.158" viewBox="0 0 42.475 52.158">
                    <g id="Group_951" data-name="Group 951" transform="translate(-4680.525 104)">
                      <path id="Union_2" data-name="Union 2" d="M.045,29.682,5.7,24.026l4.818,4.818V0h8V37h-.09V48.158Z" transform="translate(4680.48 -100)"/>
                      <path id="Union_3" data-name="Union 3" d="M.045,29.682,5.7,24.026l4.818,4.818V0h8V37h-.09V48.158Z" transform="translate(4723.045 -55.842) rotate(180)"/>
                    </g>
                  </svg>
                  <span>{this.state.orderByPrice == 'asc' ?  "Highest - Lowest" : "Lowest - Highest"} </span></button>
            </div>
         </div>
         <div className="filter-3 col-r100 d-inline-flex">
           <div className="col-r15">
             <svg className="btn-sort" xmlns="http://www.w3.org/2000/svg" width="301.238" height="200.825" viewBox="0 0 301.238 200.825">
               <path id="ic_sort_24px" d="M3,206.825H103.413V173.354H3ZM3,6V39.471H304.238V6ZM3,123.148H203.826V89.677H3Z" transform="translate(-3 -6)"/>
             </svg>
           </div>
           <div className="col-r85">
             <div className="input-group">
               <input type="text" placeholder="Quick Search"/>
               <div className="input-group-prepend">
                 <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.96 32.96">
                   <path id="ic_search_24px" d="M26.556,23.729H25.067l-.528-.509a12.268,12.268,0,1,0-1.319,1.319l.509.528v1.489l9.422,9.4,2.808-2.808Zm-11.307,0a8.48,8.48,0,1,1,8.48-8.48A8.469,8.469,0,0,1,15.249,23.729Z" transform="translate(-3 -3)"/>
                 </svg>
               </div>
             </div>
           </div>
         </div>
       </div>

    );
  }
}