import React, {Component} from 'react'
import LayoutInventoryGridItem from "./LayoutInventoryGridItem.jsx";
import {pathServer} from "../../../../Constants";

export default class LayoutInventoryGrid extends Component {

  constructor(props) {
    super(props);
    this.renderCarsGrid = this.renderCarsGrid.bind(this);
    this.renderCarsRow = this.renderCarsRow.bind(this);
  }

  renderCarsRow(cars) {
    let {onSearch, addToComparisonCue} = this.props;
    return (
      <div className="row">
        {cars.map(function (car, index) {
          return (<div key={index} className="col col-sm-4">
            <LayoutInventoryGridItem car={car} onSearch={onSearch} addToComparisonCue={addToComparisonCue}/>
          </div>);
        })}
      </div>
    );
  }

  renderCarsGrid(cars, colsNumber) {
    return (
      <div className="container-fluid" style={{paddingLeft:"10px"}}>
        {
          cars.length > 0 ? cars.map(function (car, index) {
            if (index % colsNumber == 0) {
              return (<div key={index}>
                {this.renderCarsRow(cars.slice(index, index + colsNumber))}
              </div>);
            }
          }, this) : <div className="message-not-found">
              <h3></h3>
          </div>
        }
      </div>
    );
  }

  render() {
    let {query} = this.props;
    return (
      <div id="grid-inventory">
        {
          (this.props.cars.length > 0) ? this.renderCarsGrid(this.props.cars, 3):
            <div className="message-not-found">
              <h3>{`No listings found for your ${query.status.join(",").replace("certified","certify")} vehicle search`}</h3>
            </div>
        }
      </div>);
  }
}