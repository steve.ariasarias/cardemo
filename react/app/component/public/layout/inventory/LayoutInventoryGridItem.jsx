import React, {Component} from 'react'
import {pathServer} from "../../../../Constants";
import {browserHistory, Link} from "react-router";
import {connect} from "react-redux";
import {setOrDeleteFavorite} from "../../../../Api";
import {findIndex} from 'lodash/array'
import {loadMakeAndModel} from "../../../../action";
class LayoutInventoryGridItem extends Component {

  constructor(props) {
    super(props);
    this.viewMore = this.viewMore.bind(this);
  }

  viewMore(vin){
    browserHistory.push('/catalog/inventory/'+vin)
  }

  setOrRemoveFavorite(carId){
    setOrDeleteFavorite(carId).then((response) => {
      console.log("update favorite");
      this.props.onSearch(null)
    })
  }

  componentWillMount(){
    let {makes, models} = this.props;
    if(!makes || !models){
      this.props.loadMakeAndModel()
    }
  }

  render() {
    let {car, email, makes, models} = this.props;
    let isFavorite = false;
    if(!car || !makes || !models) return null;
    if(!email || email === ""){
      email = sessionStorage.getItem("email");
      if(email && car.favorites && car.favorites.length > 0){
        isFavorite = findIndex(car.favorites,car.id)
      }
    }
    const make = (makes.find(make => make.id === car.makeId) || {}).name;
    const model = (models.find(model => model.id === car.modelId) || {}).name;
    const makeModel = make + " " +model;
    return (
      <div id="grid-inventory-item" className="card">

        <div className="card-img-overlay d-flex flex-column justify-content-end">
          <label className="card-text">{car.year? car.year.substr(2,3):null} {makeModel}<h6 className="float-right">${car.price}</h6></label>
        </div>
        <img className="card-img" src={car.mainImageUrl ? car.mainImageUrl : pathServer.PATH_IMG + "car.png"}/>
        <div className="card-details flex-column">
          <div className="row row-details">
            <div className="col-sm-10 text-left">
              <p className="card-details-title">{makeModel}<br/>{car.year}</p>
            </div>
            <div className="col-50-right">
              {
                (email) ? <i className="ml-2 fas fa-star" style={{color: (isFavorite)?'blue':'white'}} onClick={this.setOrRemoveFavorite.bind(this,car.id,email)}/> : null
              }
            </div>

          </div>

          <div className="row-details">
            <div className="row icon-group">
              <div className="col">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.921 30.152">
                  <path id="ic_directions_car_24px" d="M33,6.9A2.816,2.816,0,0,0,30.325,5H9.6A2.834,2.834,0,0,0,6.92,6.9L3,18.191V33.267a1.89,1.89,0,0,0,1.884,1.884H6.769a1.89,1.89,0,0,0,1.884-1.884V31.383H31.267v1.884a1.89,1.89,0,0,0,1.884,1.884h1.884a1.89,1.89,0,0,0,1.884-1.884V18.191ZM9.6,25.729A2.827,2.827,0,1,1,12.422,22.9,2.823,2.823,0,0,1,9.6,25.729Zm20.729,0A2.827,2.827,0,1,1,33.152,22.9,2.823,2.823,0,0,1,30.325,25.729ZM6.769,16.307,9.6,7.827H30.325l2.827,8.48Z" transform="translate(-3 -5)"/>
                </svg>
                <p className="text-center card-details-title">{car.body}</p></div>
              <div className="col">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.152 37.69">
                  <path id="ic_description_24px" d="M22.845,2H7.769a3.764,3.764,0,0,0-3.75,3.769L4,35.921A3.764,3.764,0,0,0,7.75,39.69H30.383a3.78,3.78,0,0,0,3.769-3.769V13.307Zm3.769,30.152H11.538V28.383H26.614Zm0-7.538H11.538V20.845H26.614ZM20.96,15.191V4.827L31.325,15.191Z" transform="translate(-4 -2)"/>
                </svg>
                <p className="text-center card-details-title">{car.title}</p></div>
              <div className="col">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36.661 37.69">
                  <path id="ic_settings_24px" d="M34.607,22.692a14.685,14.685,0,0,0,.132-1.847A14.687,14.687,0,0,0,34.607,19l3.976-3.109a.951.951,0,0,0,.226-1.206l-3.769-6.52a.947.947,0,0,0-1.15-.415L29.2,9.632a13.768,13.768,0,0,0-3.185-1.847L25.3,2.791A.919.919,0,0,0,24.374,2H16.837a.919.919,0,0,0-.923.791L15.2,7.785a14.478,14.478,0,0,0-3.185,1.847L7.32,7.748a.919.919,0,0,0-1.15.415L2.4,14.683a.929.929,0,0,0,.226,1.206L6.6,19a14.945,14.945,0,0,0-.132,1.847A14.945,14.945,0,0,0,6.6,22.692L2.627,25.8A.951.951,0,0,0,2.4,27.007l3.769,6.52a.947.947,0,0,0,1.15.415l4.692-1.884A13.768,13.768,0,0,0,15.2,33.9l.716,4.994a.919.919,0,0,0,.923.791h7.538A.919.919,0,0,0,25.3,38.9l.716-4.994A14.478,14.478,0,0,0,29.2,32.058l4.692,1.884a.919.919,0,0,0,1.15-.415l3.769-6.52a.951.951,0,0,0-.226-1.206Zm-14,4.749a6.6,6.6,0,1,1,6.6-6.6A6.6,6.6,0,0,1,20.606,27.441Z" transform="translate(-2.271 -2)"/>
                </svg>
                <p className="text-center card-details-title">{car.transmission}</p></div>
              <div className="col">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.094 33.921">
                  <path id="ic_local_gas_station_24px" d="M33.718,10.971l.019-.019-7.01-7.01-2,2,3.976,3.976a4.7,4.7,0,0,0,1.677,9.1,4.817,4.817,0,0,0,1.884-.4V32.21a1.884,1.884,0,0,1-3.769,0v-8.48a3.78,3.78,0,0,0-3.769-3.769H22.845V6.769A3.78,3.78,0,0,0,19.076,3H7.769A3.78,3.78,0,0,0,4,6.769V36.921H22.845V22.787h2.827V32.21a4.711,4.711,0,0,0,9.422,0v-17.9A4.715,4.715,0,0,0,33.718,10.971Zm-14.643,5.22H7.769V6.769H19.076Zm11.307,0a1.884,1.884,0,1,1,1.884-1.884A1.89,1.89,0,0,1,30.383,16.191Z" transform="translate(-4 -3)"/>
                </svg>
                <p className="text-center card-details-title">{car.cylinder}M/G</p></div>
            </div>
            {email ? <div className="btn-car-content float-left">
              <div className="btn-car-info" onClick={()=>this.props.addToComparisonCue(car.vin)}>+ COMPARE</div>
            </div>:null}
            <div className="btn-car-content float-right">
              <div className="btn-car-info" onClick={this.viewMore.bind(this,(car.vin)?car.vin:car.vinNumber)}>
                MORE
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    email: state.email,
    makes: state.makes,
    models: state.models
  }
};

export default connect(mapStateToProps,{loadMakeAndModel})(LayoutInventoryGridItem)