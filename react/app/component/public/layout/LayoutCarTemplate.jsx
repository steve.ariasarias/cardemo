import React, {Component} from 'react'
import LayoutFooter from "./common/LayoutFooter.jsx";
import LayoutHeader from "./common/LayoutHeader.jsx";



export class LayoutCarTemplate extends Component {

  constructor() {
    super();
    this.state={
      showHeaderExpanded:false,
    }

    this.refHeader=React.createRef();
    this.refBody=React.createRef();
    this.onChangeHeader=this.onChangeHeader.bind(this);
  }

  componentDidMount(){
    let pageContent = this.refBody.current;
    let pagecopy = pageContent.cloneNode(true);
    let blurryContent = this.refHeader.current;
    blurryContent.appendChild(pagecopy);
    window.onscroll = function() { blurryContent.scrollTop = window.pageYOffset;}
  }

  onChangeHeader(){
    let {showHeaderExpanded} = this.state;
    this.setState({showHeaderExpanded: !showHeaderExpanded});
  }

  render() {
    let {showHeaderExpanded} = this.state;
    return (
        <div className="car-template">
          <div className={showHeaderExpanded ? " blurryscroll-expanded " :" blurryscroll "}  aria-hidden="true"  ref={this.refHeader} />
          <LayoutHeader  colorNav={this.props.colorNav} showMenuExpanded={this.onChangeHeader}/>
          <div className="content-template" ref={this.refBody}>
            {this.props.children}
          </div>
          <LayoutFooter/>
        </div>

    )
  }
}



