import React, {Component} from 'react'
import {pathServer, PERSON_MODE, DEALER_MODE, PERSON_MODE_CODE} from "../../../Constants";
import {Link} from "react-router";
import {LayoutCarTemplate} from "./LayoutCarTemplate.jsx";

export default class LayoutPlanSelection extends Component {

  constructor() {
    super();
    this.state = {
      classHeader: 'header-orange',
      selectedOptionIndex: 0,
      hoveredOptionIndex: -1
    }

  }

  render() {
    return (
      <LayoutCarTemplate colorNav={this.state.classHeader}>
        <div id="plan-selection">
          <div className="plan-selection-header"/>
          <div className="content-panel text-center">
            <div className="header">
              <div className="back float-left ml-2">
                <a href="sell"><i className="back-icon"></i></a>
              </div>
              <div className="content-nav float-left ml-2">
                <a href="#" className="text nav-text selected-nav-text">1. Select a Plan</a>
                <a className="text">></a>
                <a href="sell" className="text nav-text">2. Account info</a>
                <a className="text">></a>
                <a href="sell" className="text nav-text">3. Listings</a>
              </div>
            </div>
            <p className="title">Select the plan that fits better to you</p>
            <div className="option-cards-panel  d-inline-flex justify-content-center">
              <div className={"bigger-option-card" + (this.state.selectedOptionIndex != 0 ? " hover-option-card" : "")}
                   onClick={() => this.setState({selectedOptionIndex: 0})}
                   onMouseOver={() => this.setState({hoveredOptionIndex: 0})}
                   onMouseLeave={() => this.setState({hoveredOptionIndex: -1})}>
                <div className="option-card">
                  {this.state.selectedOptionIndex == 0 ?
                    <div className="row justify-content-center">
                      <div className="selected-tag">SELECTED</div>
                    </div> : null}
                  <img
                    className={this.state.hoveredOptionIndex == 0 && this.state.selectedOptionIndex != 0 ? "hovered-img" : "option-img"}
                    src={pathServer.PATH_IMG + "owner-plan.png"}/>
                  <p className="title plan-card-title">Owner</p>
                  <p className="plan-info">2 free listings<br/>1 user<br/>Track your listings</p>
                  {this.state.hoveredOptionIndex == 0 && this.state.selectedOptionIndex != 0 ?
                    <p className="plan-info more-link">MORE</p> : null}
                  <p className="price"
                     style={this.state.hoveredOptionIndex > -1 && this.state.hoveredOptionIndex != this.state.selectedOptionIndex ? {
                       "fontStyle": "italic",
                       "marginBottom": "33px"
                     } : {"marginBottom": "33px"}}>FREE</p>
                </div>
                {this.state.hoveredOptionIndex == 0 && this.state.selectedOptionIndex != 0 ?
                  <button className="btn btn-colored">SELECT</button> : null}
              </div>
              <div className={"bigger-option-card" + (this.state.selectedOptionIndex != 1 ? " hover-option-card" : "")}
                   onClick={() => this.setState({selectedOptionIndex: 1})}
                   onMouseOver={() => this.setState({hoveredOptionIndex: 1})}
                   onMouseLeave={() => this.setState({hoveredOptionIndex: -1})}>
                <div className="option-card">
                  {this.state.selectedOptionIndex == 1 ?
                    <div className="row justify-content-center">
                      <div className="selected-tag">SELECTED</div>
                    </div> : null}
                  <img
                    className={this.state.hoveredOptionIndex == 1 && this.state.selectedOptionIndex != 1 ? "hovered-img" : "option-img"}
                    src={pathServer.PATH_IMG + "baby-dealer-plan.png"}/>
                  <p className="title plan-card-title">Baby Dealer</p>
                  <p className="plan-info">1-30 listings<br/>2 user<br/>Track your listings</p>
                  {this.state.hoveredOptionIndex == 1 && this.state.selectedOptionIndex != 1 ?
                    <p className="plan-info more-link">MORE</p> : null}
                  <p className="price"
                     style={this.state.hoveredOptionIndex > -1 && this.state.hoveredOptionIndex != this.state.selectedOptionIndex ? {"fontStyle": "italic"} : null}>$299</p>
                  <p className="payment-frequency">pay every 30 days</p>
                </div>
                {this.state.hoveredOptionIndex == 1 && this.state.selectedOptionIndex != 1 ? <button
                  className="btn btn-colored">{this.state.selectedOptionIndex < 1 ? "UPGRADE" : "SELECT"}</button> : null}
              </div>
              <div className={"bigger-option-card" + (this.state.selectedOptionIndex != 2 ? " hover-option-card" : "")}
                   onClick={() => this.setState({selectedOptionIndex: 2})}
                   onMouseOver={() => this.setState({hoveredOptionIndex: 2})}
                   onMouseLeave={() => this.setState({hoveredOptionIndex: -1})}>
                {this.state.hoveredOptionIndex == 2 && this.state.selectedOptionIndex != 2 ?
                  <div className="bigger-popular-tag"><span className="star">★</span> Popular</div> :
                  (this.state.selectedOptionIndex != 2 ?
                    <div className="popular-tag"><span className="star">★</span> Popular</div> : null)}

                <div className="option-card">
                  {this.state.selectedOptionIndex == 2 ?
                    <div>
                      <div className="row"><span className="star smaller-popular-tag">★</span></div>
                      <div className="row justify-content-center">
                        <div className="selected-tag">SELECTED</div>
                      </div>
                    </div> : null}
                  <img
                    className={this.state.hoveredOptionIndex == 2 && this.state.selectedOptionIndex != 2 ? "hovered-img" : "option-img"}
                    src={pathServer.PATH_IMG + "dealer-plan.png"}/>
                  <p className="title plan-card-title">Dealer</p>
                  <p className="plan-info">1-70 listings<br/>5 user<br/>Track your listings</p>
                  {this.state.hoveredOptionIndex == 2 && this.state.selectedOptionIndex != 2 ?
                    <p className="plan-info more-link">MORE</p> : null}
                  <p className="price"
                     style={this.state.hoveredOptionIndex > -1 && this.state.hoveredOptionIndex != this.state.selectedOptionIndex ? {"fontStyle": "italic"} : null}>$499</p>
                  <p className="payment-frequency">pay every 30 days</p>
                </div>
                {this.state.hoveredOptionIndex == 2 && this.state.selectedOptionIndex != 2 ? <button
                  className="btn btn-colored">{this.state.selectedOptionIndex < 2 ? "UPGRADE" : "SELECT"}</button> : null}
              </div>
              <div className={"bigger-option-card" + (this.state.selectedOptionIndex != 3 ? " hover-option-card" : "")}
                   onClick={() => this.setState({selectedOptionIndex: 3})}
                   onMouseOver={() => this.setState({hoveredOptionIndex: 3})}
                   onMouseLeave={() => this.setState({hoveredOptionIndex: -1})}>
                <div className="option-card">
                  {this.state.selectedOptionIndex == 3 ?
                    <div className="row justify-content-center">
                      <div className="selected-tag">SELECTED</div>
                    </div> : null}
                  <img
                    className={this.state.hoveredOptionIndex == 3 && this.state.selectedOptionIndex != 3 ? "hovered-img" : "option-img"}
                    src={pathServer.PATH_IMG + "dealer-pro-plan.png"}/>
                  <p className="title plan-card-title">Dealer PRO</p>
                  <p className="plan-info">Unlimited listings<br/>Unlimited user<br/>Track your listings</p>
                  {this.state.hoveredOptionIndex == 3 && this.state.selectedOptionIndex != 3 ?
                    <p className="plan-info more-link">MORE</p> : null}
                  <p className="price"
                     style={this.state.hoveredOptionIndex > -1 && this.state.hoveredOptionIndex != this.state.selectedOptionIndex ? {"fontStyle": "italic"} : null}>$799</p>
                  <p className="payment-frequency">pay every 30 days</p>
                </div>
                {this.state.hoveredOptionIndex == 3 && this.state.selectedOptionIndex != 3 ? <button
                  className="btn btn-colored">{this.state.selectedOptionIndex < 3 ? "UPGRADE" : "SELECT"}</button> : null}
              </div>
            </div>
            {this.state.hoveredOptionIndex < 0 || this.state.hoveredOptionIndex == this.state.selectedOptionIndex ?
              <div>
                <Link to={{pathname:"/account-info/"+(PERSON_MODE_CODE+this.state.selectedOptionIndex)}}>
                  <button className="btn btn-colored">CONTINUE</button>
                </Link>
              </div> : null}
          </div>
        </div>
      </LayoutCarTemplate>
    );
  }
}