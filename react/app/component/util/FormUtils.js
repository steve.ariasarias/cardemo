import _ from 'lodash'

var EMAIL_REGEX = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
var ONLY_DIGIT = /\d/g;
var numberRegex = /^(?:\(([0-9]{3})\)|([0-9]{3}))[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
var dateRegex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

var dateRegex2 = /([0-9]{2})[\/]?([0-9]{2})[\/]?([0-9]{4})$/;
var DEFAULT_IMAGE_URL = 'http://dummyimage.com/240X180/e4e6ed/7b7b80.jpg&text=No+Image+Found';



function addValidationToInput(input,validation){
  if(!input.validation)
    input.validation = [];

  input.validation.push(validation);
}

module.exports = {
  checkRequired(value) {
    if (_.isArray(value)) {
      return _.reduce(value, function (acc, n) {
        return acc || (n && n.length > 0);
      });
    } else if (_.isBoolean(value)) {
      return true;
    } else {
      if(!isNaN(parseFloat(value)) && isFinite(value)) {
        return parseFloat(value) > 0;
      } else {
        return value !== undefined && value !== null && value.length > 0;
      }
    }
  },

  checkValidVIN(value) {
    return (value.length == 17);
  },

  checkValidEmail(value) {
    return EMAIL_REGEX.test(value);
  },

  checkValidPhone(value){
    return numberRegex.test(value);
    /*console.log(value);
    if (!value) return false;
    if (ONLY_DIGIT.test(value))
      return value.match(/\d/g).length===10;
    else return false*/
  },

  checkValidDate(value) {
    return dateRegex2.test(value);
  },

  getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;

    while(element) {
      /*xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
       yPosition += (element.offsetTop - element.scrollTop + element.clientTop);*/
      xPosition += (element.offsetLeft  + element.clientLeft);
      yPosition += (element.offsetTop  + element.clientTop);
      element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
  },

  getMainImageURL(car){
    //var defaultImgId = car.images && car.images.length > 0 && car.images[0].id;
    //var img = _.find(car.images,{id: car.mainImage || defaultImgId});
    var img = car.mainImage;
    return img ? img : DEFAULT_IMAGE_URL;
  },

  getAllCarStore(){
    var carsInformation;
    //cars = CarStore.getAll();
    carsInformation = {
      cars : [
      { year:2010,
        make:"Sedan",
        model:"S1",
        vin:"VCL4958473904533",
        suggestedPrice:5000,
        mileage:10000,
        mainImage:"https://s3-us-west-2.amazonaws.com/dealer-pro/photo/10000/1/car1.jpeg",
        title:{attributeValue:"Clean"},
        trim:{attributeValue:"SE"},
        traction:{attributeValue:"AWD"},
        body:{attributeValue:"sedan"},
        transmission:{attributeValue:"Automatic"},
        drive:{attributeValue:"Drive"},
        passenger:{attributeValue:"4"},
        cylinder:{attributeValue:"6"},
        fuel:{attributeValue:"Fuel"},
        exteriorColor:{attributeValue:"White"},
        description:{attributeValue:"description"}
      },
      { year:2011,
        make:"Sedan",
        model:"S2",
        vin:"VCL4958473904534",
        suggestedPrice:6000,
        mileage:11000,
        mainImage:"https://s3-us-west-2.amazonaws.com/dealer-pro/photo/10000/1/car1.jpeg",
        title:{attributeValue:"Clean"},
        trim:{attributeValue:"SE"},
        traction:{attributeValue:"AWD"},
        body:{attributeValue:"sedan"},
        transmission:{attributeValue:"Automatic"},
        drive:{attributeValue:"Drive"},
        cylinder:{attributeValue:"6"},
        fuel:{attributeValue:"Fuel"},
        passenger:{attributeValue:"4"},
        exteriorColor:{attributeValue:"Black"},
        description:{attributeValue:"description"}
      },
      { year:2012,
        make:"Sedan",
        model:"S3",
        vin:"VCL4958473904535",
        suggestedPrice:7000,
        mileage:12000,
        mainImage:"https://s3-us-west-2.amazonaws.com/dealer-pro/photo/10000/1/car1.jpeg",
        title:{attributeValue:"Clean"},
        trim:{attributeValue:"SE"},
        traction:{attributeValue:"AWD"},
        body:{attributeValue:"sedan"},
        transmission:{attributeValue:"Automatic"},
        drive:{attributeValue:"Drive"},
        cylinder:{attributeValue:"6"},
        fuel:{attributeValue:"Fuel"},
        passenger:{attributeValue:"4"},
        exteriorColor:{attributeValue:"Black"},
        description:{attributeValue:"description"}
      },
      { year:2008,
        make:"Sedan",
        model:"S4",
        vin:"VCL4958473904536",
        suggestedPrice:4000,
        mileage:9000,
        mainImage:"https://s3-us-west-2.amazonaws.com/dealer-pro/photo/10000/1/car1.jpeg",
        title:{attributeValue:"Clean"},
        trim:{attributeValue:"SE"},
        traction:{attributeValue:"AWD"},
        body:{attributeValue:"sedan"},
        transmission:{attributeValue:"Automatic"},
        drive:{attributeValue:"Drive"},
        cylinder:{attributeValue:"6"},
        fuel:{attributeValue:"Fuel"},
        passenger:{attributeValue:"4"},
        exteriorColor:{attributeValue:"Black"},
        description:{attributeValue:"description"}
      },
      { year:2007,
        make:"Sedan",
        model:"S5",
        vin:"VCL4958473904537",
        suggestedPrice:3500,
        mileage:8000,
        mainImage:"https://s3-us-west-2.amazonaws.com/dealer-pro/photo/10000/1/car1.jpeg",
        title:{attributeValue:"Clean"},
        trim:{attributeValue:"SE"},
        traction:{attributeValue:"AWD"},
        body:{attributeValue:"sedan"},
        transmission:{attributeValue:"Automatic"},
        drive:{attributeValue:"Drive"},
        cylinder:{attributeValue:"6"},
        fuel:{attributeValue:"Fuel"},
        passenger:{attributeValue:"4"},
        exteriorColor:{attributeValue:"Black"},
        description:{attributeValue:"description"}
      },
      { year:2006,
        make:"Sedan",
        model:"S6",
        vin:"VCL4958473904538",
        suggestedPrice:3000,
        mileage:7000,
        mainImage:"https://s3-us-west-2.amazonaws.com/dealer-pro/photo/10000/1/car1.jpeg",
        title:{attributeValue:"Clean"},
        trim:{attributeValue:"SE"},
        traction:{attributeValue:"AWD"},
        body:{attributeValue:"sedan"},
        transmission:{attributeValue:"Automatic"},
        drive:{attributeValue:"Drive"},
        cylinder:{attributeValue:"6"},
        fuel:{attributeValue:"Fuel"},
        passenger:{attributeValue:"4"},
        exteriorColor:{attributeValue:"Black"},
        description:{attributeValue:"description"}
      }

    ]
   };
    
   return  carsInformation;
  },

  loadYears(lastYear,firstYear){
    var years = new Array();
    for (var i=lastYear; i>=firstYear; i--) {
      years.push({id:i,value:i.toString(),name:i.toString()});
    } return years;
  },

  loadPrices(lastPrice,firstPrice){
    var prices = new Array();
    for (var i=lastPrice; i>=firstPrice; i--) {
      prices.push({id:i,value:i.toString(),name:i.toString()});
    } return prices;
  },

  getScrollTop() {
    if (typeof pageYOffset !== 'undefined') {
      //most browsers except IE before #9
      return pageYOffset;
    }
    else {
      let B = document.body; //IE 'quirks'
      let D = document.documentElement; //IE with doctype
      D = (D.clientHeight) ? D : B;
      return D.scrollTop;
    }
  },

  scrollTo(pointFrom, pointTo, duration) {
    if (duration <= 0) return;
    let difference = pointTo.y - pointFrom.y;
    let perTick = Math.round(difference / duration * 10);
    setTimeout(function () {
      pointFrom.y = pointFrom.y + perTick;
      window.scrollTo(pointTo.x, pointFrom.y);
      if (pointTo.y === pointFrom.y){
        return;
      } else{
        let dur = duration - 10;
        scrollTo(pointFrom, pointTo, dur);
      }
    }, 10);
  }

};