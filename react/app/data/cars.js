/**
 * Created by johnny on 10/24/16.
 */


export const demoCars = [
  {
    "id" : "1516081860357001OrOV",
    "make_id" : "chevrolet",
    "model_id" : "suburban",
    "year" : 2007,
    "purchased_date" : "2018-04-23",
    "mileage" : 146600,
    "price" : 3000
  },
  {
    "id" : "1513446310609001LYoi",
    "make_id" : "nissan",
    "model_id" : "altima",
    "year" : 2018,
    "purchased_date" : "2018-04-18",
    "mileage" : 123658
  },
  {
    "id" : "1524083645216001gICq",
    "make_id" : "am-general",
    "model_id" : "hummer",
    "year" : 2017,
    "purchased_date" : "2018-04-18",
    "mileage" : 12
  },
  {
    "id" : "1513446310665002Lnhl",
    "make_id" : "jeep",
    "model_id" : "patriot",
    "year" : 2017,
    "purchased_date" : "2018-03-06",
    "mileage" : 31640
  },
  {
    "id" : "1520351567964001svBl",
    "make_id" : "chevrolet",
    "model_id" : "cruze",
    "year" : 2017,
    "purchased_date" : "2018-03-06",
    "mileage" : 14090
  },
  {
    "id" : "1520351567964002sqrG",
    "make_id" : "hyundai",
    "model_id" : "accent",
    "year" : 2017,
    "purchased_date" : "2018-03-06",
    "mileage" : 19775
  },
  {
    "id" : "1518534598448001ULCF",
    "make_id" : "acura",
    "model_id" : "ilx",
    "year" : 2018,
    "purchased_date" : "2018-02-13",
    "mileage" : 434
  },
  {
    "id" : "1518445782986001ThqA",
    "make_id" : "kia",
    "model_id" : "soul",
    "year" : 2018,
    "purchased_date" : "2018-02-12",
    "mileage" : 11440
  },
  {
    "id" : "1518018509281001jBJm",
    "make_id" : "ford",
    "model_id" : "mustang",
    "year" : 2012,
    "purchased_date" : "2018-02-07",
    "mileage" : 110550
  },
  {
    "id" : "1518016032084001jXSL",
    "make_id" : "infiniti",
    "model_id" : "g35",
    "year" : 2008,
    "purchased_date" : "2018-02-07",
    "mileage" : 134970
  },
  {
    "id" : "1518017513124001jSEZ",
    "make_id" : "mini",
    "model_id" : "hardtop-2-door",
    "year" : 2006,
    "purchased_date" : "2018-02-07",
    "mileage" : 70210
  },
  {
    "id" : "1518012204185001jLnl",
    "make_id" : "nissan",
    "model_id" : "350z",
    "year" : 2003,
    "purchased_date" : "2018-02-07",
    "mileage" : 185850
  },
  {
    "id" : "1513446310632002LxgT",
    "make_id" : "volkswagen",
    "model_id" : "jetta",
    "year" : 2006,
    "purchased_date" : "2018-02-07",
    "mileage" : 157500
  },
  {
    "id" : "1518020563212001jeRc",
    "make_id" : "ford",
    "model_id" : "f-150",
    "year" : 2009,
    "purchased_date" : "2018-02-07",
    "mileage" : 155455
  },
  {
    "id" : "1518013503573001jSnq",
    "make_id" : "chevrolet",
    "model_id" : "camaro",
    "year" : 2012,
    "purchased_date" : "2018-02-07",
    "mileage" : 75990
  },
  {
    "id" : "1518012452294001jwFQ",
    "make_id" : "bmw",
    "model_id" : "3-series",
    "year" : 2007,
    "purchased_date" : "2018-02-07",
    "mileage" : 102060
  },
  {
    "id" : "1518013995599001jUIy",
    "make_id" : "gmc",
    "model_id" : "sierra-1500hd",
    "year" : 2011,
    "purchased_date" : "2018-02-07",
    "mileage" : 130995
  },
  {
    "id" : "1518020218687001jVHd",
    "make_id" : "dodge",
    "model_id" : "ram-pickup-1500",
    "year" : 2005,
    "purchased_date" : "2018-02-07",
    "mileage" : 148200
  },
  {
    "id" : "1513446310618001LKQX",
    "make_id" : "dodge",
    "model_id" : "charger",
    "year" : 2006,
    "purchased_date" : "2018-02-07",
    "mileage" : 152255
  },
  {
    "id" : "1516808771505001zTjT",
    "make_id" : "am-general",
    "model_id" : "hummer",
    "year" : 2017,
    "purchased_date" : "2018-01-24",
    "mileage" : 1000
  }
];
