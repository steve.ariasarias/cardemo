import assign from 'lodash/assign'

export let PortalEndpoints = {
  SEARCH_COMMAND: '_search?size=1000',
  CARS_COLLECTION_PATH: '/zoom/cars',
  SEARCHES_COLLECTION_PATH: '/saved/searches',
  ACCOUNTS_COLLECTION_PATH:'/accounts/account-info',
  LOCATIONS_COLLECTION_PATH:'/locations/location-info',
  SAVE_LISTING: 'api/inventory/listing',
  SET_OR_DELETE_FAVORITE: 'api/inventory/car/favorite',
  GET_ACCOUNT_LOGGING:'api/user/accountInfo',
  SAVE_ACCOUNT: 'api/user/account',
  UPDATE_CAR: 'api/inventory/car',
  SEARCH_INVENTORY_ADMIN: 'api/listing/search',
  SEARCH_INVENTORY_PUBLIC: 'public/api/inventory/search',
  GET_INVENTORY_BY_VIN: 'public/api/inventory/',
  CAR_UPLOAD_IMAGE: 'api/inventory/car/uploadImage',
  PUBLIC_LOAD_MAKE_AND_MODEL:'public/api/inventory/loadMakeAndModel',
  UPLOAD_MAKE_AND_MODEL:'api/setting/saveMakesAndModels',
  UPDATE_COMMAND:'/_update',
  VEHICLES_DATA_URL:'https://vpic.nhtsa.dot.gov/api/vehicles',
  VIN_NUMBER_SEARCH_URL: '/DecodeVin/{vinNumber}?format=json',
  MAKES_LIST_URL: '/GetAllMakes?format=json',
  MODELS_LIST_URL: '/GetModelsForMakeId/{makeId}?format=json',
  PROCESS_LOGIN : 'processLogin',
  CHANGE_PASSWORD: 'api/user/account/change-password',
  SAVE_SEARCH: 'api/savesearch',
  GET_SAVE_SEARCHES: 'api/savesearch/load'
};

export function getServerUrl() {
  // return 'http://localhost:9000/';
  if (window.baseHref) {
    return window.baseHref;
  }
  return '';
}

function getElasticsearchUrl() {
  // return 'http://localhost:9000/';
  console.log("window.urlElasticsearch", window.initialState);
  if (window.initialState.urlElasticsearch !== "") {
    return window.initialState.urlElasticsearch;
  }
  return '';
}

function doGet(url, body, props) {
  props = props || {};
  return fetch(
    url,
    assign({
      method: 'get',
      body: body,
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }, props)
  )
    .then(checkStatus)
}

function doPost(url, body, props) {
  props = props || {};
  //body = body || '';
  return fetch(
    url,
    assign({
      method: 'post',
      body: body,
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }, props)
  )
    .then(checkStatus)
}

function doDelete(url, body, props) {
  props = props || {};
  return fetch(
    url,
    assign({
      method: 'delete',
      body: body,
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }, props)
  )
    .then(checkStatus)
}

export function getCarSpecsByVin(vin) {
  const url = `https://api.edmunds.com/api/vehicle/v2/vins/${vin}?fmt=json&api_key=qs4pu33cnafcwtamghqtpa5u`;
  return doGet(url)
}

function doPut(url, body, props) {
  props = props || {};
  //body = body || '';
  return fetch(
    url,
    assign({
      method: 'put',
      body: body,
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }, props)
  )
    .then(checkStatus)
}

function checkStatus(response) {
  //--console.log(response);
  //--console.log(response);
  return response.json().then((data) => {
    let newResponse = {...data, status: response.status, ok: response.ok};
    if (response.status >= 200 && response.status < 300) {
      return newResponse
    } else {
      throw newResponse
    }
  })
}

export function searchInventoryApi(query, collectionPath) {
  return doPost(getElasticsearchUrl() + collectionPath, JSON.stringify(query), {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function searchSavedSearchesApi(query, collectionPath){
  return doPost(getElasticsearchUrl() + collectionPath, JSON.stringify(query), {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function getSavedSearchesApi(){
  return doGet(getServerUrl() + PortalEndpoints.GET_SAVE_SEARCHES)
}

export function addSaveSearch(data){
  return doPost(getServerUrl() + PortalEndpoints.SAVE_SEARCH,JSON.stringify(data),{headers: {
      'Content-Type': 'application/json'
    }})
}

export function saveDocument(document, collectionPath) {
  return doPost(getElasticsearchUrl() + collectionPath, JSON.stringify(document), {
    headers: {
      'Content-Type': 'application/json'
    }
  });
}

export function saveListing(data){
  return doPost(getServerUrl() + PortalEndpoints.SAVE_LISTING,JSON.stringify(data),{headers: {
      'Content-Type': 'application/json'
    }})
}

export function updateCar(data,carId){
  return doPost(getServerUrl() + PortalEndpoints.UPDATE_CAR + '/' + carId,JSON.stringify(data),{headers: {
      'Content-Type': 'application/json'
    }})
}

export function searchInventoryAdminApi(query) {
  return doPost(getServerUrl() + PortalEndpoints.SEARCH_INVENTORY_ADMIN, JSON.stringify(query), {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function searchInventoryPublicApi(query) {
  return doPost(getServerUrl() + PortalEndpoints.SEARCH_INVENTORY_PUBLIC, JSON.stringify(query), {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function getCarFromVin(vin){
  return doGet(getServerUrl() + PortalEndpoints.GET_INVENTORY_BY_VIN + vin);
}



export function uploadCarImage(carId, files,indexMainImage) {
  let data = new FormData();
  let orders = [];
  if (files && files.length > 0) {
    let fileNames = [];
    files.forEach((file) => {
      fileNames.push(file.name);
    orders.push(file.order);
    data.append(file.name, file);
  });
    data.append('fileNames', fileNames);
    data.append('orders', orders);
    data.append('indexMainImage', indexMainImage);
  }
  return doPost(getServerUrl() + PortalEndpoints.CAR_UPLOAD_IMAGE + '/' + carId, data, {headers: {}})
}


export function saveAccount(data){
  return doPost(getServerUrl() + PortalEndpoints.SAVE_ACCOUNT,JSON.stringify(data),{headers: {
      'Content-Type': 'application/json'
    }})
}

export function updateAccount(data){
  return doPut(getServerUrl() + PortalEndpoints.SAVE_ACCOUNT,JSON.stringify(data),{headers: {
      'Content-Type': 'application/json'
    }})
}

export function getInfoAccountLogging(){
  return doGet(getServerUrl() + PortalEndpoints.GET_ACCOUNT_LOGGING);
}

export function changePassword(data){
  return doPost(getServerUrl() + PortalEndpoints.CHANGE_PASSWORD, JSON.stringify(data), {
    headers:{'Content-type': 'application/json'}
  })
}

export function loadMakeAndModelApi(){
  return doGet(`${getServerUrl()+PortalEndpoints.PUBLIC_LOAD_MAKE_AND_MODEL}`)
}

export function saveMakesAndModels(data){
  return doPost(getServerUrl() + PortalEndpoints.UPLOAD_MAKE_AND_MODEL,JSON.stringify(data),{headers: {
      'Content-Type': 'application/json'
    }})
}

export function getDataFromVinNumber(vinNumber){
  return doGet((PortalEndpoints.VEHICLES_DATA_URL+PortalEndpoints.VIN_NUMBER_SEARCH_URL).replace('{vinNumber}',vinNumber))
}

export function getMakesList(){
  return doGet((PortalEndpoints.VEHICLES_DATA_URL+PortalEndpoints.MAKES_LIST_URL))
}

export function getModelsList(makeId){
  return doGet((PortalEndpoints.VEHICLES_DATA_URL+PortalEndpoints.MODELS_LIST_URL).replace('{makeId}',makeId))
}

export function processLogin(data) {
  return doPost(getServerUrl() + PortalEndpoints.PROCESS_LOGIN,JSON.stringify(data),{headers: {
      'Content-Type': 'application/json'
    }})
}

export function setOrDeleteFavorite(carId){
  return doGet(`${getServerUrl()+PortalEndpoints.SET_OR_DELETE_FAVORITE+ "/" + carId}`)
}


