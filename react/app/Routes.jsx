import React from 'react'
import {Route, IndexRoute} from 'react-router'
import Home from './component/admin/Home.jsx'
import Dashboard from './component/admin/Dashboard.jsx'


const Routes = (
  <Route path="/main" component={Home}>
    <IndexRoute component={Dashboard}/>
  </Route>
);

export default Routes