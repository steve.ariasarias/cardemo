/**
 * Created by johnny on 18/08/16
 */
import {InputTypes,VALIDATION_REQUIRED} from "./Constants";
import _ from 'lodash'
import React from 'react'
import DatePicker from 'react-bootstrap-date-picker'
import InputElement from 'react-input-mask'
import DynamicNumber from 'react-dynamic-number';

var moment = require('moment');

export function defaultString(str){
  if(str === null || str === undefined)
    return '';
  return str;
}

export function formatDollar(num) {
  if(num){
    var p = num.toFixed(2).split(".");
    return "$" + p[0].split("").reverse().reduce(function(acc, num, i, orig) {
      return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
  }else{
    return "$ 0.00";
  }
}

export function formatDecimal(num) {
  if(num && !isNaN(num)){
    var p = num.toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
      return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
  }
}

export function addZeros(num) {
  var number = Number(num);
  // Cast as number
  // If not a number, return 0
  if (isNaN(number) || number == 0) {
    return '';
  }
  // If there is no decimal, or the decimal is less than 2 digits, toFixed
  if (String(number).split(".").length < 2 || String(number).split(".")[1].length<=2 ){
    number = number.toFixed(2);
  }
  // Return the number
  return number;
}

export function isNumeric(value) {
  return (value.match(/^[0-9]+$/) !== null);
}

export function isYear(value) {
  return (value.match(/^[0-9]{4}$/) !== null);
}

export function isFloat(value) {
  return (value.match(/^[0-9]*.?[0-9]+$/) !== null);
}

export function isValidPrice(price) {
  var priceWithoutCurrency = _.startsWith(price, '$') ? price.substring(1) : price;
  var priceWithoutCommas = priceWithoutCurrency.replace(',', '');
  return (priceWithoutCommas.match(/^\d+(\.\d+)?$/) !== null);
}

export function isValidVin(value) {
  return (value.match(/^[\w]{17}$/) !== null);
}

export function isNotTextAttribute(type){
  return InputTypes.SELECT === type || InputTypes.CHECKBOX || InputTypes.RADIO_BUTTON
}

export function getAttributesByAttributeIds(attributes,attributeIds){
  return _.filter(attributes,function (form) {
    return _.find(attributeIds,function(attributeId){ return form.id === attributeId})
  });
}

export function getAttributeJSON(attributes,values){
  let item = {};
  attributes.forEach((attribute)=>{
    if(attribute.id === attribute.name || (attribute.inputType !== InputTypes.SELECT && attribute.inputType !== InputTypes.RADIO_BUTTON)){
      if(values)
        item[attribute.id] = values[attribute.id] || "";
      else
        item[attribute.id] = "";
    }else{
      if(values)
        item[attribute.id] = values[attribute.id] || [];
      else
        item[attribute.id] = [];
    }
  });
  return item;
}

export function parseObjectForm(sections,values){
  let objectForm = {};
  sections.forEach((section)=>{
    let sectionCopy = JSON.parse(JSON.stringify(section));
    if(section.isArray){
      sectionCopy.sectionSubs = [];
      if(!values){
        //let initialSectionSub = _.isArray(section.sectionSubs) ? section.sectionSubs[0] : section.sectionSubs;
        if(_.isArray(section.sectionSubs)){
          section.sectionSubs.forEach(sectionSub => {
            if(!sectionSub.isHidden)
              sectionCopy.sectionSubs.push(sectionSub);
          })
        }else{
          sectionCopy.sectionSubs.push(section.sectionSubs)
        }
      }else{
        section.sectionSubs.forEach(sectionSub =>{
          let attributeId = sectionSub.attributeIds[0];
          let valueFound = values[attributeId];
          if(valueFound)
            sectionCopy.sectionSubs.push(sectionSub);
        });
      }
    }
    objectForm[section.name] = sectionCopy;
  });
  //console.log("ObjectForm:",objectForm);
  return objectForm;
}

export function parseValues(attributes,sections,valuesFromDB){
  let values = {};
  _.forEach(sections,function(section,index){
    if(section.isArray){
      values[section.name] = [];
      if(!valuesFromDB){
        //let attributesBySection = getAttributesByAttributeIds(attributes,section.sectionSubs[0].attributeIds);
        //values[section.name].push(getAttributeJSON(attributesBySection));
        let attributesBySection = [];
        section.sectionSubs.forEach(sectionSub => {
          if(!sectionSub.isHidden){
            let attributesBySectionSub = getAttributesByAttributeIds(attributes,sectionSub.attributeIds);
            values[section.name].push(getAttributeJSON(attributesBySectionSub));
          }
        });
      }else{
        section.sectionSubs.forEach(sectionSub =>{
          let attributesFound = getAttributesByAttributeIds(attributes,sectionSub.attributeIds);
          let attributeRequired = _.find(attributesFound,function (attribute) { return attribute.validation });
          let valueFound = valuesFromDB[attributeRequired.id];
          if(valueFound)
            values[section.name].push(getAttributeJSON(attributesFound,valuesFromDB));
        });
      }

    }else{
      let attributesBySection = getAttributesByAttributeIds(attributes,section.attributeIds);
      values[section.name] = getAttributeJSON(attributesBySection,valuesFromDB);
    }
  });
  //console.log("values:",values);
  return values;
}

export function setErrors(attributes,section,errors){
  for(let key in section){
    if(section[key] === "" || (_.isArray(section[key]) && section[key].length === 0)){
      let attributeFound = _.find(attributes,function (attribute) { return attribute.id === key});
      if(attributeFound.validation.length > 0){
        _.forEach(attributeFound.validation, function (validation) {
          if(validation.type === VALIDATION_REQUIRED){
            errors[key] = [];
            errors[key].push(validation.type);
          }
        });
      }
    }
  }
}

export function validate(errors){
  let hasError = false;
  for(let key in errors){
    let errorBySection = errors[key];
    for(let sectionKey in errorBySection){
      if(errorBySection[sectionKey])
        return true
    }
  }
  return hasError
}


export function cleanArray(array){
  while (array.length > 0) {
    array.pop();
  }
}

export function moveElementTest(name,index,maxIndex){
  let lst = [];
  let indexUp;
  let indexDown;
  if (index === 0){
    indexUp = index + 1;
    while(indexUp < maxIndex){
      lst.push(name+indexUp);
      indexUp = indexUp +1;
    }
  } else if (index !== maxIndex){
    indexDown = index - 1;
    while(indexDown >= 0){
      lst.push(name+indexDown);
      indexDown = indexDown -1;
    }

    indexUp = index + 1;
    while(indexUp < maxIndex){
      lst.push(name+indexUp);
      indexUp = indexUp +1;
    }

  } else {
    indexDown = index - 1;
    while(indexDown >= 0){
      lst.push(name+indexDown);
      indexDown = indexDown -1;
    }
  }

  return lst;
}

export function  getWidthColumn(column){
  let width = "";
  switch(column){
    case 1 :
      width = "col100";
      break;
    case 2 :
      width = "col50";
      break;
    case 3 :
      width = "col33";
      break;
    case 4 :
      width = "col25";
      break;
    case 5 :
      width = "col20";
      break;
  }

  return width
}

export function getStatus(elements){
  let lst = [];
  elements.forEach(element =>{
    lst.push(element.status);
  });

  return lst;
}

export function moveElement(statusList,status){
  let lst = [];
  statusList.forEach(el =>{
    if(el !== status)
      lst.push(el);
  });
  return lst;
}

export function generateUniqueID(){
  return (new Date()).getTime().toString();
}

export function getInputElement(attribute){
  switch(attribute.inputType){
    case InputTypes.TEXT :
      return (
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            <input className="form-control"
                   id={attribute.id}
                   value=""
                   placeholder=""/>
          </div>
        </div>
      );
    case InputTypes.SELECT :
    case InputTypes.STATE:
    case InputTypes.MULTI_SELECT:
      return (
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            <select className="form-control"
                    id={attribute.id}
                    readOnly={true}
                    value="">
            </select>
          </div>
        </div>
      );
      break;
    case InputTypes.RADIO :
      return (
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            {
              attribute.attributeValues.map((av, i) => {
                return(
                  <div key={i} className="col100">
                    <div style={{float:"left"}}>
                      <input type="radio"
                             readOnly={true}
                             value={av.id}/>
                    </div>
                    <label style={{paddingLeft:5}}>{av.displayValue}</label>
                  </div>
                )
              })
            }
          </div>
        </div>
      );
      break;
    case InputTypes.DATE_PICKER :
      const calendar = <i aria-hidden="true" className="fa fa-calendar"/>;
      return (
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            <DatePicker className="form-control"
                        value={new Date().toISOString()}
                        dateFormat="MM/DD/YYYY"
                        isClearable={false}
                        readOnly={true}
                        clearButtonElement={calendar}/>
          </div>
        </div>
      );
    case InputTypes.PHONE_MASK:
      return(
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            <InputElement type="text"
                          className="form-control"
                          mask="(999)999-9999"
                          readOnly={true}
                          value=""/>
          </div>
        </div>
      );
    case InputTypes.DYNAMIC_NUMBER:
      return(
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            <DynamicNumber className="form-control"
                           placeholder={attribute.withSymbol ? '0.00' : '0'}
                           positive={true}
                           negative={false}
                           thousand={true}
                           readOnly={true}
                           separator={'.'}
                           integer={8}
                           fraction={2}/>
          </div>
        </div>
      );
    case InputTypes.NUMBER:
      return(
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            <DynamicNumber className="form-control"
                           placeholder="0"
                           positive={true}
                           negative={false}
                           thousand={true}
                           separator={''}
                           readOnly={true}
                           integer={2}
                           fraction={0}/>
          </div>
        </div>
      );
    case InputTypes.RADIO_BUTTON:
      return(
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            {
              attribute.attributeValues.map((option,idx) => {
                //let col = parseInt(100/attributeValues.length);
                return (
                  <div key={idx} style={{float:"left",padding:"0 5px"}}>
                    <button type="button"
                            disabled={true}
                            className="btn btn-default col100" style={{opacity:0.8}}>
                      {option.displayValue}
                    </button>
                  </div>
                )
              })
            }
          </div>
        </div>
      );
    case InputTypes.TEXT_AREA :
      return (
        <div className="col85" style={{padding:10}}>
          {
            attribute.displayValue ?
              <div>
                <label>{attribute.displayValue}</label>
              </div>: null
          }
          <div>
            <textarea className="form-control"
                      id={attribute.id}
                      value=""
                      placeholder=""/>
          </div>
        </div>
      );
    default :
      break;
  }
}