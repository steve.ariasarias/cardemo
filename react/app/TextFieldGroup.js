import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const TextFieldGroup = ({ field, value, label, error, type, onChange, checkUserExists, placeholder }) => {
  return (
    <div className={classnames('form-group', { 'has-error': error })} style={{width: "100%",
      marginBottom: (error)?"-24px":"0"}}>
      <input
        onChange={onChange}
        onBlur={checkUserExists}
        value={value}
        type={type}
        name={field}
        className="form-control"
        placeholder={placeholder}
      />
    {error && <span className="help-block"
                    style={{position: "relative", top: "-29px", color: "red", fontWeight: "400", fontSize: "13px"}}
    >{error}</span>}
    </div>  );
}

TextFieldGroup.propTypes = {
  field: PropTypes.string.isRequired,
  value: PropTypes.string,
  error: PropTypes.string,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  checkUserExists: PropTypes.func,
  placeholder: PropTypes.string.isRequired
}

TextFieldGroup.defaultProps = {
  type: 'text'
}

export default TextFieldGroup;
