/**
 * Created by johnny on 18/08/16
 */

export const FormInputTypes = {
  TEXT: 'text',
  LABEL: 'label',
  SELECT: 'select',
  PASSWORD: 'password',
  TEXT_HIDDEN: 'hidden',
  TEXT_AREA: 'textArea',
  TEXT_AUTOCOMPLETE: 'textAutocomplete',
  DYNAMIC_NUMBER: 'dynamicNumber',
  DATE_PICKER: 'datePicker',
  DEFAULT_IMAGE_URL: 'http://dummyimage.com/240X180/e4e6ed/7b7b80.jpg&text=No+Image+Found'
};



export const InputTypes = {
  TEXT: 'TEXT',
  LABEL: 'LABEL',
  SELECT: 'SELECT',
  MULTI_SELECT: 'MULTI_SELECT',
  PASSWORD: 'PASSWORD',
  TEXT_HIDDEN: 'HIDDEN',
  TEXT_AREA: 'TEXTAREA',
  TEXT_AUTOCOMPLETE: 'textAutocomplete',
  DYNAMIC_NUMBER: 'DYNAMIC_NUMBER',
  NUMBER: 'NUMBER',
  DATE_PICKER: 'DATE_PICKER',
  PHONE_MASK: 'PHONE_MASK',
  IMAGE:'IMAGE',
  RADIO_BUTTON: 'RADIO_BUTTON',
  RADIO: 'RADIO',
  CHECKBOX: 'CHECKBOX',
  STATE: 'STATE',
  PARAGRAPH:'PARAGRAPH',
  DEFAULT_IMAGE_URL: 'http://dummyimage.com/240X180/e4e6ed/7b7b80.jpg&text=No+Image+Found'
};


export const StyleInputsSearch = {
  MAKE_SHOW: 'makeShow',
  MAKE_NO_SHOW: 'makeNoShow',
  MODEL_SHOW: 'modelShow',
  MODEL_NO_SHOW: 'modelNoShow',
  YEAR_SHOW: 'yearShow',
  YEAR_NO_SHOW: 'yearNoShow',
  PRICE_SHOW: 'priceShow',
  PRICE_NO_SHOW: 'priceNoShow',
  MILEAGE_SHOW: 'mileageShow',
  MILEAGE_NO_SHOW: 'mileageNoShow',
  TYPE_SHOW: 'typeShow',
  TYPE_NO_SHOW: 'typeNoShow',
  STATUS_SHOW: 'statusShow',
  STATUS_NO_SHOW: 'statusNoShow'
};

export const formCar = {
  stockId: {
    attributeCode: 'stockId',
    attributeValue: ''
  },
  show: {
    attributeCode: 'show',
    attributeValue: ''
  },
  status: {
    attributeCode: 'status',
    attributeValue: ''
  },
  trim: {
    attributeCode: 'trim',
    attributeValue: ''
  },
  body: {
    attributeCode: 'body',
    attributeValue: ''
  },
  title: {
    attributeCode: 'title',
    attributeValue: ''
  },
  interiorColor: {
    attributeCode: 'interiorColor',
    attributeValue: ''
  },
  interiorCondition: {
    attributeCode: 'interiorCondition',
    attributeValue: ''
  },
  exteriorColor: {
    attributeCode: 'exteriorColor',
    attributeValue: ''
  },
  exteriorCondition: {
    attributeCode: 'exteriorCondition',
    attributeValue: ''
  },
  numberDoor: {
    attributeCode: 'numberDoor',
    attributeValue: ''
  },
  cylinder: {
    attributeCode: 'cylinder',
    attributeValue: ''
  },
  liter: {
    attributeCode: 'liter',
    attributeValue: ''
  },
  transmission: {
    attributeCode: 'transmission',
    attributeValue: ''
  },
  fuel: {
    attributeCode: 'fuel',
    attributeValue: ''
  },
  drive: {
    attributeCode: 'drive',
    attributeValue: ''
  },
  traction: {
    attributeCode: 'traction',
    attributeValue: ''
  },
  typeCar: {
    attributeCode: 'typeCar',
    attributeValue: ''
  },
  licNro: {
    attributeCode: 'licNro',
    attributeValue: ''
  },
  insurance: {
    attributeCode: 'insurance',
    attributeValue: ''
  },
  policy: {
    attributeCode: 'policy',
    attributeValue: ''
  },
  certifyOdometer: {
    attributeCode: 'certifyOdometer',
    attributeValue: ''
  },
  licensePlateType: {
    attributeCode: 'licensePlateType',
    attributeValue: ''
  },
  licensePlateTransfer: {
    attributeCode: 'licensePlateTransfer',
    attributeValue: ''
  },
  licensePlateOther: {
    attributeCode: 'licensePlateOther',
    attributeValue: ''
  }
};

export const attributeInfo = {
  firstName: {
    attributeCode: 'firstName',
    attributeValue: ''
  },
  middleName: {
    attributeCode: 'middleName',
    attributeValue: ''
  },
  lastName: {
    attributeCode: 'lastName',
    attributeValue: ''
  },
  addressLine1: {
    attributeCode: 'addressLine1',
    attributeValue: ''
  },
  cellPhone: {
    attributeCode: 'cellPhone',
    attributeValue: ''
  },
  addressLine2: {
    attributeCode: 'addressLine2',
    attributeValue: ''
  },
  city: {
    attributeCode: 'city',
    attributeValue: ''
  },
  homeNumber: {
    attributeCode: 'homeNumber',
    attributeValue: ''
  },
  state: {
    attributeCode: 'state',
    attributeValue: ''
  },
  zipCode: {
    attributeCode: 'zipCode',
    attributeValue: ''
  },
  email: {
    attributeCode: 'email',
    attributeValue: ''
  },
  signature: {
    attributeCode: 'signature',
    attributeValue: ''
  },
  dateStatement: {
    attributeCode: 'dateStatement',
    attributeValue: ''
  },
  secondPhone: {
    attributeCode: 'secondPhone',
    attributeValue: ''
  },
  fullName: {
    attributeCode: 'fullName',
    attributeValue: ''
  },
  applicantType: {
    attributeCode: 'applicantType',
    attributeValue: ''
  },
  relation: {
    attributeCode: 'relation',
    attributeValue: ''
  },
  driverLicenseNumber: {
    attributeCode: 'driverLicenseNumber',
    attributeValue: ''
  },
  driverLicenseState: {
    attributeCode: 'driverLicenseState',
    attributeValue: ''
  },
  creditScore: {
    attributeCode: 'creditScore',
    attributeValue: ''
  },
  typeCredit: {
    attributeCode: 'typeCredit',
    attributeValue: ''
  },
  typeId: {
    attributeCode: 'typeId',
    attributeValue: ''
  },
  dobmdy: {
    attributeCode: 'dobmdy',
    attributeValue: ''
  }
};

export const formType = [
  {
    id: 'Sedan',
    name: 'Sedan',
    value: 'Sedan',
    selected: false
  },
  {
    id: 'SUV',
    name: 'SUV',
    value: 'SUV',
    selected: false
  },
  {
    id: 'Coupe',
    name: 'Coupe',
    value: 'Coupe',
    selected: false
  },
  {
    id: 'Hatchback',
    name: 'Hatchback',
    value: 'Hatchback',
    selected: false
  },
  {
    id: 'Wagon',
    name: 'Wagon',
    value: 'Wagon',
    selected: false
  },
  {
    id: 'Truck',
    name: 'Truck',
    value: 'Truck',
    selected: false
  },
  {
    id: 'Pick-up Truck',
    name: 'Pick-up Truck',
    value: 'Pick-up Truck',
    selected: false
  },
  {
    id: 'Minivan',
    name: 'Minivan',
    value: 'Minivan',
    selected: false
  },
  {
    id: 'Van',
    name: 'Van',
    value: 'Van',
    selected: false
  },
  {
    id: 'Limousine',
    name: 'Limousine',
    value: 'Limousine',
    selected: false
  },
  {
    id: 'Roadster',
    name: 'Roadster',
    value: 'Roadster',
    selected: false
  },
  {
    id: 'Compact',
    name: 'Compact',
    value: 'Compact',
    selected: false
  },
  {
    id: 'Convertible',
    name: 'Convertible',
    value: 'Convertible',
    selected: false
  },
  {
    id: 'Crossover',
    name: 'Crossover',
    value: 'Crossover',
    selected: false
  }
];

export const formStatus = [
  {
    id: 'Inspect/Repair',
    name: 'Inspect/Repair',
    value: 'Inspect/Repair',
    selected: false
  },
  {
    id: 'Hold',
    name: 'Hold',
    value: 'Hold',
    selected: false
  },
  {
    id: 'Front Line Ready',
    name: 'Front Line Ready',
    value: 'Front Line Ready',
    selected: false
  },
  {
    id: 'Sold',
    name: 'Sold',
    value: 'Sold',
    selected: false
  }
];

export const pathServer = {
  PATH_IMG: "/assets/images/"
};

export const filterType = {
  PARAMETERS_ECONOMIC: {
    rangePriceTo: 8000
  },
  PARAMETERS_BEST: {
    priceBest: 3000
  },
  PARAMETERS_NEWEST: {
    newEst: true
  }
};

export const moneySign = '$';

export const formState = [
  {value: 'AL', label: 'Alabama'},
  {value: 'AK', label: 'Alaska'},
  {value: 'AZ', label: 'Arizona'},
  {value: 'AR', label: 'Arkansas'},
  {value: 'CA', label: 'California'},
  {value: 'CO', label: 'Colorado'},
  {value: 'CT', label: 'Connecticut'},
  {value: 'DE', label: 'Delaware'},
  {value: 'DC', label: 'District Of Columbia'},
  {value: 'FL', label: 'Florida'},
  {value: 'GA', label: 'Georgia'},
  {value: 'HI', label: 'Hawaii'},
  {value: 'ID', label: 'Idaho'},
  {value: 'IL', label: 'Illinois'},
  {value: 'IN', label: 'Indiana'},
  {value: 'IA', label: 'Iowa'},
  {value: 'KS', label: 'Kansas'},
  {value: 'KY', label: 'Kentucky'},
  {value: 'LA', label: 'Louisiana'},
  {value: 'ME', label: 'Maine'},
  {value: 'MD', label: 'Maryland'},
  {value: 'MA', label: 'Massachusetts'},
  {value: 'MI', label: 'Michigan'},
  {value: 'MN', label: 'Minnesota'},
  {value: 'MS', label: 'Mississippi'},
  {value: 'MO', label: 'Missouri'},
  {value: 'MT', label: 'Montana'},
  {value: 'NE', label: 'Nebraska'},
  {value: 'NV', label: 'Nevada'},
  {value: 'NH', label: 'New Hampshire'},
  {value: 'NJ', label: 'New Jersey'},
  {value: 'NM', label: 'New Mexico'},
  {value: 'NY', label: 'New York'},
  {value: 'NC', label: 'North Carolina'},
  {value: 'ND', label: 'North Dakota'},
  {value: 'OH', label: 'Ohio'},
  {value: 'OK', label: 'Oklahoma'},
  {value: 'OR', label: 'Oregon'},
  {value: 'PA', label: 'Pennsylvania'},
  {value: 'RI', label: 'Rhode Island'},
  {value: 'SC', label: 'South Carolina'},
  {value: 'SD', label: 'South Dakota'},
  {value: 'TN', label: 'Tennessee'},
  {value: 'TX', label: 'Texas'},
  {value: 'UT', label: 'Utah'},
  {value: 'VT', label: 'Vermont'},
  {value: 'VA', label: 'Virginia'},
  {value: 'WA', label: 'Washington'},
  {value: 'WV', label: 'West Virginia'},
  {value: 'WI', label: 'Wisconsin'},
  {value: 'WY', label: 'Wyoming'}
];

export const formEmail = {
  CLIENT_SUBJECT_MESSAGE: 'Leiva Motors: Thanks for contacting us',
  ADMIN_SUBJECT_MESSAGE: 'Leiva Motors: New message',
  CLIENT_TEXT_MESSAGE: 'Your email has been received. ',
  ADMIN_TEXT_MESSAGE: 'There is a new message',
  ADMIN_EMAIL : 'Rafaelfmaldonado@gmail.com',
  //ADMIN_EMAIL: 'johnny.apolinariob@gmail.com',
  ADMIN_NAME: 'Rafael Maldonado',
  CLIENT_HTML_MESSAGE: "Your email has been received.  </br><blockquote><div>",
  ADMIN_HTML_MESSAGE: "New message created. </br><blockquote><div>",
  DEALER_EMAIL: "support@leivamotors.com",
  CLIENT_SUBJECT_APPLICANT: 'Leiva Motors: Thanks for register your application',
  ADMIN_SUBJECT_APPLICANT: 'Leiva Motors: There is a new Finance Request registered.',
  CLIENT_TEXT_APPLICANT: 'Your applicant has been received. ',
  ADMIN_TEXT_APPLICANT: 'There is a new Finance Request registered.',
  CLIENT_HTML_APPLICANT: "",
  ADMIN_HTML_APPLICANT: "There is a new Finance Request registered. </br><blockquote><div>"
};

export const optionBody = [
  {id: 'Sedan', name: 'Sedan', value: 'Sedan', selected: false},
  {id: 'SUV', name: 'SUV', value: 'SUV', selected: false},
  {id: 'Coupe', name: 'Coupe', value: 'Coupe', selected: false},
  {id: 'Hatchback', name: 'Hatchback', value: 'Hatchback', selected: false},
  {id: 'Wagon', name: 'Wagon', value: 'Wagon', selected: false},
  {id: 'Truck', name: 'Truck', value: 'Truck', selected: false},
  {id: 'Minivan', name: 'Minivan', value: 'Minivan', selected: false},
  {id: 'Van', name: 'Van', value: 'Van', selected: false},
  {id: 'Limousine', name: 'Limousine', value: 'Limousine', selected: false},
  {id: 'Roadster', name: 'Roadster', value: 'Roadster', selected: false},
  {id: 'Compact', name: 'Compact', value: 'Compact', selected: false},
  {id: 'Convertible', name: 'Convertible', value: 'Convertible', selected: false},
  {id: 'Crossover', name: 'Crossover', value: 'Crossover', selected: false}
];

export const optionFuel = [
  {id: 'Gasoline', name: 'Gasoline', value: 'Gasoline', selected: false},
  {id: 'Diesel', name: 'Diesel', value: 'Diesel', selected: false},
  {id: 'Bio-Diesel', name: 'Bio-Diesel', value: 'Bio-Diesel', selected: false},
  {id: 'Bio-Fuel', name: 'Bio-Fuel', value: 'Bio-Fuel', selected: false},
  {id: 'Electric', name: 'Electric', value: 'Electric', selected: false},
  {id: 'Hybrid', name: 'Hybrid', value: 'Hybrid', selected: false},
  {id: 'Compressed Natural Gas', name: 'C.Natural Gas', value: 'Compressed Natural Gas', selected: false},
  {id: 'Ethanol', name: 'Ethanol', value: 'Ethanol', selected: false},
  {id: 'Flex Fuel', name: 'Flex Fuel', value: 'Flex Fuel', selected: false},
  {id: 'Liquified Natural Gas', name: 'L.Natural Gas', value: 'Liquified Natural Gas', selected: false},
  {id: 'Liquified Petroleum', name: 'L.Petroleum', value: 'Liquified Petroleum', selected: false}
];

export const optionCylinder = [
  {id: 3, name: '3', value: '3', selected: false},
  {id: 4, name: '4', value: '4', selected: false},
  {id: 5, name: '5', value: '5', selected: false},
  {id: 6, name: '6', value: '6', selected: false},
  {id: 8, name: '8', value: '8', selected: false},
  {id: 10, name: '10', value: '10', selected: false},
  {id: 12, name: '12', value: '12', selected: false}
];


export const optionMilleage = [
  {name: 0 , value: 0},
  {name: 1000 , value: 1000},
  {name: 5000 , value: 5000},
  {name: 10000 , value: 10000},
  {name: 20000 , value: 20000},
  {name: 30000 , value: 30000},
  {name: 40000 , value: 40000},
  {name: 50000 , value: 50000},
  {name: 60000 , value: 60000},
  {name: 70000 , value: 70000},
  {name: 80000 , value: 80000},
  {name: 90000 , value: 90000},
  {name: 100000 , value: 100000},
  {name: 110000 , value: 110000},
  {name: 120000 , value: 120000},
  {name: 130000 , value: 130000},
  {name: 140000 , value: 140000},
  {name: 150000 , value: 150000},
  {name: 160000 , value: 160000},
  {name: 170000 , value: 170000},
  {name: 180000 , value: 180000},
  {name: 190000 , value: 190000},
  {name: 200000 , value: 200000},
  {name: 1000000 , value: 1000000},
];

export const optionColor = [
  {id: 'Black', name: 'Black', value: 'Black', selected: false},
  {id: 'Blue', name: 'Blue', value: 'Blue', selected: false},
  {id: 'Brown', name: 'Brown', value: 'Brown', selected: false},
  {id: 'Gold', name: 'Gold', value: 'Gold', selected: false},
  {id: 'Green', name: 'Green', value: 'Green', selected: false},
  {id: 'Grey', name: 'Grey', value: 'Grey', selected: false},
  {id: 'Orange', name: 'Orange', value: 'Orange', selected: false},
  {id: 'Pink', name: 'Pink', value: 'Pink', selected: false},
  {id: 'Purple', name: 'Purple', value: 'Purple', selected: false},
  {id: 'Red', name: 'Red', value: 'Red', selected: false},
  {id: 'Silver', name: 'Silver', value: 'Silver', selected: false},
  {id: 'White', name: 'White', value: 'White', selected: false},
  {id: 'Yellow', name: 'Yellow', value: 'Yellow', selected: false},
  {id: 'Tan', name: 'Tan', value: 'Tan', selected: false},
  {id: 'Maroon', name: 'Maroon', value: 'Maroon', selected: false}
];

export const optionSeller=[
  {id:"Owner" , name: 'For Sale By Owner', value: 'For Sale By Owner'},
  {id:"Dealer" , name:  'For Sale By Dealer', value: 'For Sale By Dealer'},
];

export const optionCarStatus = [
  {id: 'New', name: 'New', value: 'New', selected: false},
  {id: 'Used', name: 'Used', value: 'Used', selected: false},
  {id: 'Certified', name: 'Certified', value: 'Certified', selected: false}
];

export const optionTitle = [
  {id: 'Clean', name: 'Clean', value: 'Clean', selected: false},
  {id: 'Dismantled', name: 'Dismantled', value: 'Dismantled', selected: false},
  {id: 'Rebuilt/Reconstructed', name: 'Rebuilt', value: 'Rebuilt/Reconstructed', selected: false},
  {id: 'Salvage', name: 'Salvage', value: 'Salvage', selected: false}
];

export const optionCondition = [
  {id: 'Excellent', name: 'Excellent', value: 'Excellent', selected: false},
  {id: 'Very Good', name: 'Very Good', value: 'Very Good', selected: false},
  {id: 'Good', name: 'Good', value: 'Good', selected: false},
  {id: 'Fair', name: 'Fair', value: 'Fair', selected: false},
  {id: 'Poor', name: 'Poor', value: 'Poor', selected: false}
];

export const optionDoor = [
  {id: '2', name: '2', value: '2', selected: false},
  {id: '3', name: '3', value: '3', selected: false},
  {id: '4', name: '4', value: '4', selected: false},
  {id: '5', name: '5', value: '5', selected: false}
];

export const optionTransmission = [
  {id: 'automatic', name: 'Automatic', value: 'Automatic', selected: false},
  {id: 'manual', name: 'Manual', value: 'Manual', selected: false},
  {id: 'automanual', name: 'Automanual', value: 'Automanual', selected: false},
  {id: 'CVT', name: 'CVT', value: 'CVT', selected: false}
];

export const optionDrive = [
  {id: '2WD', name: '2-Wheel Drive', value: '2-Wheel Drive', selected: false},
  {id: '4WD', name: '4-Wheel Drive', value: '4-Wheel Drive', selected: false},
  {id: 'AWD', name: 'AWD', value: 'AWD', selected: false},
  {id: 'FWD', name: 'FWD', value: 'FWD', selected: false},
  {id: 'RWD', name: 'RWD', value: 'RWD', selected: false}
];

export const optionFarmUse = [
  {id: 'Yes', name: 'Yes', value: 'Yes', selected: false},
  {id: 'No', name: 'No', value: 'No', selected: false},
];

export const statusCar = [
  {
    id: '18',
    name: 'Inspect/Repair'
  },
  {
    id: '19',
    name: 'Hold'
  },
  {
    id: '20',
    name: 'Front Line Ready'
  },
  {
    id: '21',
    name: 'Sold'
  }
];

export const showCar = [
  {
    id: '45',
    name: 'Show'
  },
  {
    id: '46',
    name: 'Hide'
  }
];

export const optionMakes = [
  {value: '1', name: 'Acura'},
  {value: '2', name: 'Alfa Romero'}
];

export const optionModels = [
  {value: '1', name: 'ILX Sedan', makeId:'1'},
  {value: '2', name: 'MDX Hybrid', makeId:'1'},
  {value: '3', name: 'MDX SUV', makeId:'1'},
  {value: '4', name: 'NSX Coupe', makeId:'1'},
  {value: '5', name: 'RDX SUV', makeId:'1'},
  {value: '6', name: 'RLX Hybrid', makeId:'1'},
  {value: '7', name: 'RLX Sedan', makeId:'1'},
  {value: '8', name: 'TLX Sedan', makeId:'1'},
  {value: '9', name: '4C Convertible', makeId:'2'},
  {value: '10', name: '4C Coupe', makeId:'2'},
  {value: '11', name: 'Giulia Quadrifoglio', makeId:'2'},
  {value: '12', name: 'Giulia Sedan', makeId:'2'}
];

export const infoAttributes   = [
  {id:"car.stockId",value:""},
  {id:"car.show",value:"Visibility"},
  {id:"car.status",value:"Status"},
  {id:"car.trim",value:""},
  {id:"car.body",value:""},
  {id:"car.title",value:""},
  {id:"car.interiorColor",value:""},
  {id:"car.interiorCondition",value:""},
  {id:"car.exteriorColor",value:""},
  {id:"car.exteriorCondition",value:""},
  {id:"car.numberDoor",value:""},
  {id:"car.cylinder",value:""},
  {id:"car.liter",value:""},
  {id:"car.transmission",value:""},
  {id:"car.fuel",value:""},
  {id:"car.drive",value:""},
  {id:"car.traction",value:""}
];


export const LOCATION_ALL = '-1';
export const CURRENT_LOCATION = '1';

export const SORT_ICON_DEFAULT = "fa fa-sort";
export const SORT_ICON_ASC = "fa fa-sort-asc";
export const SORT_ICON_DESC = "fa fa-sort-desc";
export const DESC_SORT = "desc";
export const ASC_SORT  = "asc";

export const FORM_APPLICANT = 'formApplicant';
export const FORM_CUSTOMER = 'formCustomer';
export const FORM_CAR ='formCar';
export const FORM_CAR_NEW ='formCarNew';
export const FORM_USER ='formUser';
export const FORM_CHANGE_PASSWORD ='formChangePassword';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';

export const optionApplicantType = [
  {id: 'Individual Credit', name: 'Individual Credit', value: 'Individual Credit', selected: false},
  {id: 'Join Credit', name: 'Join Credit', value: 'Join Credit', selected: false},
];

export const optionYear = [
  {id: 1, name: 1, value: 1, selected: false},
  {id: 2, name: 2, value: 2, selected: false},
  {id: 3, name: 3, value: 3, selected: false},
  {id: 4, name: 4, value: 4, selected: false},
  {id: 5, name: 5, value: 5, selected: false},
  {id: 6, name: 6, value: 6, selected: false},
  {id: 7, name: 7, value: 7, selected: false},
  {id: 8, name: 8, value: 8, selected: false},
  {id: 9, name: 9, value: 9, selected: false},
  {id: 10, name: 10, value: 10, selected: false}
];

export const optionMonth = [
  {id: 1, name: 1, value: 1, selected: false},
  {id: 2, name: 2, value: 2, selected: false},
  {id: 3, name: 3, value: 3, selected: false},
  {id: 4, name: 4, value: 4, selected: false},
  {id: 5, name: 5, value: 5, selected: false},
  {id: 6, name: 6, value: 6, selected: false},
  {id: 7, name: 7, value: 7, selected: false},
  {id: 8, name: 8, value: 8, selected: false},
  {id: 9, name: 9, value: 9, selected: false},
  {id: 10, name: 10, value: 10, selected: false},
  {id: 11, name: 11, value: 11, selected: false}
];

export const FORM_CAR_INFORMATION = "CAR_INFORMATION";
export const FORM_CAR_HEADER = "CAR_HEADER";
export const FORM_CAR_MECHANICAL = "CAR_MECHANICAL";
export const FORM_LOCATION = "LOCATION";
export const FORM_REPAIR = "REPAIR";
export const FORM_USER_CHANGE_PASSWORD = "USER_CHANGE_PASSWORD";



export const SECTION_NAME_RESIDENCE = "applicant_residence";
export const SECTION_NAME_EMPLOYMENT = "applicant_employment";
export const SECTION_NAME_APPLICANT = "applicant";

export const ADD = "ADD";
export const REMOVE = "REMOVE";

export const SECTION_RESIDENCE_PREVIOUS = "previous";
export const SECTION_RESIDENCE_CURRENT = "current";
export const SECTION_RESIDENCE_ANOTHER = "another";

export const SECTION_EMPLOYMENT_PREVIOUS = "previous";
export const SECTION_EMPLOYMENT_CURRENT = "current";
export const SECTION_EMPLOYMENT_ANOTHER = "another";

export const ATTRIBUTE_APPLICANT_TYPE = "customer.applicantType";
export const ATTRIBUTE_APPLICANT_RELATION = "customer.relation";
export const ATTRIBUTE_RESIDENCE_YEAR = "customer.year";
export const ATTRIBUTE_RESIDENCE_MONTH = "customer.month";
export const ATTRIBUTE_EMPLOYER_YEAR = "customer.employerYear";
export const ATTRIBUTE_EMPLOYER_MONTH = "customer.employerMonth";
export const ATTRIBUTE_RESIDENCE_STATE = "customer.state";
export const ATTRIBUTE_EMPLOYER_STATE = "customer.employerState";

export const YEAR_OPTIONS = [1,2,3,4,5,6,7,8,9,10];

export const MONTH_OPTIONS = [1,2,3,4,5,6,7,8,9,10,11];

export const optionState = [
  {id:'AL',name: 'AL', displayValue: 'Alabama'},
  {id: 'AK',name: 'AK', displayValue: 'Alaska'},
  {id: 'AZ',name: 'AZ', displayValue: 'Arizona'},
  {id: 'AR',name: 'AR', displayValue: 'Arkansas'},
  {id: 'CA',name: 'CA', displayValue: 'California'},
  {id: 'CO',name: 'CO', displayValue: 'Colorado'},
  {id: 'CT',name: 'CT', displayValue: 'Connecticut'},
  {id: 'DE',name: 'DE', displayValue: 'Delaware'},
  {id: 'DC',name: 'DC', displayValue: 'District Of Columbia'},
  {id: 'FL',name: 'FL', displayValue: 'Florida'},
  {id: 'GA',name: 'GA', displayValue: 'Georgia'},
  {id: 'HI',name: 'HI', displayValue: 'Hawaii'},
  {id: 'ID',name: 'ID', displayValue: 'Idaho'},
  {id: 'IL',name: 'IL', displayValue: 'Illinois'},
  {id: 'IN',name: 'IN', displayValue: 'Indiana'},
  {id: 'IA',name: 'IA', displayValue: 'Iowa'},
  {id: 'KS',name: 'KS', displayValue: 'Kansas'},
  {id: 'KY',name: 'KY', displayValue: 'Kentucky'},
  {id: 'LA',name: 'LA', displayValue: 'Louisiana'},
  {id: 'ME',name: 'ME', displayValue: 'Maine'},
  {id: 'MD',name: 'MD', displayValue: 'Maryland'},
  {id: 'MA',name: 'MA', displayValue: 'Massachusetts'},
  {id: 'MI',name: 'MI', displayValue: 'Michigan'},
  {id: 'MN',name: 'MN', displayValue: 'Minnesota'},
  {id: 'MS',name: 'MS', displayValue: 'Mississippi'},
  {id: 'MO',name: 'MO', displayValue: 'Missouri'},
  {id: 'MT',name: 'MT', displayValue: 'Montana'},
  {id: 'NE',name: 'NE', displayValue: 'Nebraska'},
  {id: 'NV',name: 'NV', displayValue: 'Nevada'},
  {id: 'NH',name: 'NH', displayValue: 'New Hampshire'},
  {id: 'NJ',name: 'NJ', displayValue: 'New Jersey'},
  {id: 'NM',name: 'NM', displayValue: 'New Mexico'},
  {id: 'NY',name: 'NY', displayValue: 'New York'},
  {id: 'NC',name: 'NC', displayValue: 'North Carolina'},
  {id: 'ND',name: 'ND', displayValue: 'North Dakota'},
  {id: 'OH',name: 'OH', displayValue: 'Ohio'},
  {id: 'OK',name: 'OK', displayValue: 'Oklahoma'},
  {id: 'OR',name: 'OR', displayValue: 'Oregon'},
  {id: 'PA',name: 'PA', displayValue: 'Pennsylvania'},
  {id: 'RI',name: 'RI', displayValue: 'Rhode Island'},
  {id: 'SC',name: 'SC', displayValue: 'South Carolina'},
  {id: 'SD',name: 'SD', displayValue: 'South Dakota'},
  {id: 'TN',name: 'TN', displayValue: 'Tennessee'},
  {id: 'TX',name: 'TX', displayValue: 'Texas'},
  {id: 'UT',name: 'UT', displayValue: 'Utah'},
  {id: 'VT',name: 'VT', displayValue: 'Vermont'},
  {id: 'VA',name: 'VA', displayValue: 'Virginia'},
  {id: 'WA',name: 'WA', displayValue: 'Washington'},
  {id: 'WV',name: 'WV', displayValue: 'West Virginia'},
  {id: 'WI',name: 'WI', displayValue: 'Wisconsin'},
  {id: 'WY',name: 'WY', displayValue: 'Wyoming'}
];

export const ATTRIBUTE_APPLICANT_TYPE_CODE = "joinCredit";


export const VALIDATION_VIN       = "vin";
export const VALIDATION_EMAIL     = "email";
export const VALIDATION_PHONE     = "phone";
export const VALIDATION_REQUIRED  = "required";
export const INPUT_TYPES = ["TEXT","LABEL","SELECT","PASSWORD","HIDDEN","TEXTAREA","TEXTAUTOCOMPLETE","DYNAMIC_NUMBER","DATE_PICKER","RADIO", "CHECKBOX","MULTI_SELECT"];
export const INPUT_TYPES_IS_SELECT = ["SELECT","RADIO", "CHECKBOX","MULTI_SELECT"];
export const INPUT_TYPES_ISNOT_SELECT= ["TEXT","LABEL","PASSWORD","HIDDEN","TEXTAREA","TEXTAUTOCOMPLETE","DYNAMIC_NUMBER","DATE_PICKER"];

export const FORM_KEY_SECTION     = "sectionSubs";
export const FORM_KEY_ROW         = "rows";
export const FORM_KEY_COLUMN      = "columns";

export const SECTION_CODE         = "section";
export const FORM_KEY             = "formKey";
export const ELEMENT_TYPE         = "elementType";
export const ELEMENT_TYPE_SECTION     = "section";
export const ELEMENT_TYPE_SECTION_SUB = "sectionSub";
export const ELEMENT_TYPE_ROW         = "row";

export const ELEMENT_STATUS_SECTION     = "sec";
export const ELEMENT_STATUS_SECTION_SUB = "sub";
export const ELEMENT_STATUS_ROW         = "row";
export const ELEMENT_STATUS_COLUMN      = "column";

export const SELECT="SELECT";
export const ENTITY_TYPES = [
  {name:"CAR", value:"CAR"},
  {name:"CUSTOMER", value:"CUSTOMER"},
  {name:"USER", value:"USER"}
];
export const RENDER_TYPES = [
  {name:"SUBMIT", value:"SUBMIT"},
  {name:"IN LINE", value:"IN LINE"}
];

export const toolBarElements = [
  {name: 'paragraph', tooltip: 'Paragraph Text', type: 'PARAGRAPH', iconType: 'text'},
  {name: 'textfield', tooltip: 'Short Answer', type: 'TEXT', iconType: 'menu-short'},
  {name: 'textarea', tooltip: 'Long Answer', type: 'TEXT', iconType: 'forms'},
  {name: 'select', tooltip: 'Select List', type: 'SELECT', iconType: 'select'},
  //{name: 'multiselect', tooltip: 'Multiple select', type: 'Multiple', iconType: 'select'},
  {name: 'radio', tooltip: 'Radio List', type: 'RADIO', iconType: 'radio-checked'},
  {name: 'checkbox', tooltip: 'Checkbox List', type: 'CHECKBOX', iconType: 'checkbox-checked'},
  {name: 'date', tooltip: 'Date', type: 'DATE_PICKER', iconType: 'calendar-event'}
];

export const textOptions = [
  {name: 'text', value: 'Text', type: 'TEXT'},
  {name: 'textArea', value: 'Area', type: 'TEXTAREA'},
  {name: 'date', value: 'Date', type: 'DATE_PICKER'},
  {name: 'paragraph', value: 'Paragraph', type: 'PARAGRAPH'},
  {name: 'number', value: 'Number', type: 'NUMBER'},
  {name: 'decimal', value: 'Decimal', type: 'DYNAMIC_NUMBER'},
  {name: 'cellPhone', value: 'CellPhone', type: 'PHONE_MASK'}
];

export const selectOptions = [
  {name: 'select', value: 'Select', type: 'SELECT'},
  {name: 'radio', value: 'Radio', type: 'RADIO'},
  {name: 'checkBox', value: 'CheckBox', type: 'CHECKBOX'},
  {name: 'multiSelect', value: 'Multi Select', type: 'MULTI_SELECT'}
];

export const validationOptions = [
  {name: 'required', value: 'Required', type: 'SELECT'},
  {name: 'email', value: 'Email', type: 'RADIO'},
  {name: 'vin', value: 'VIN', type: 'CHECKBOX'},
  {name: 'phone', value: 'Phone', type: 'MULTI_SELECT'}
];


export const FORMS_TABLE_ORDER = [
  {
    header: "Actions",
    body: "",
    width: "15%"
  },
  {
    header: "Name",
    body: "name",
    width: "40%"
  },
  {
    header: "Form Code",
    body: "code",
    width: "25%"
  },
  {
    header: "Entity Type",
    body: "entityType",
    width: "20%"
  }
];

export const ROLES_TABLE_ORDER = [
  {
    header: "Actions",
    body: "",
    width: "20%"
  },
  {
    header: "Name",
    body: "name",
    width: "35%"
  },
  {
    header: "Description",
    body: "description",
    width: "35%"
  }
];



export const ATTRIBUTES_TABLE_ORDER = [
  {
    header: "Actions",
    body: "",
    width: "15%"
  },
  {
    header: "Name",
    body: "name",
    width: "40%"
  },
  {
    header: "Display Value",
    body: "displayValue",
    width: "25%"
  },
  {
    header: "Response Type",
    body: "typeAttribute",
    width: "20%"
  }
];

export const FORMS_SEARCH_OPTIONS= [
  {
    id: "0",
    name: "name",
    placeHolder: "search name",
    inputType: "TEXT",
    attributeValues: [],
    validation: []
  }
];

export const ROLES_SEARCH_OPTIONS= [
  {
    id: "0",
    name: "name",
    placeHolder: "search name",
    inputType: "TEXT",
    attributeValues: [],
    validation: []
  }
];

export const ATTRIBUTES_SEARCH_OPTIONS= [
  {
    id: "0",
    name: "name",
    placeHolder: "search name, display value",
    inputType: "TEXT",
    validation: []
  },
  {
    id: "1",
    name: "entityType",
    inputType: "SELECT",
    defaultSelect: "entity type",
    selectValues: [
      {
        id: "CAR",
        value: "CAR",
        selected: false
      },
      {
        id: "CUSTOMER",
        value: "CUSTOMER",
        selected: false
      },
      {
        id: "USER",
        value: "USER",
        selected: false
      },
    ],
    validation: []
  }
];


export const NUMBER_PAGES_SHOWN     = 10;
export const NUMBER_PAGES_SHOWN_CAR = 5;

export const HEIGHT_CAR_LIST = 200;

export const ATTRIBUTE_ID_ROLES = "roles";
export const ATTRIBUTE_ID_CONFIRM_PASSWORD = "confirmPassword";
export const ATTRIBUTE_ID_PASSWORD = "password";
export const CURRENT_YEAR =(new Date).getFullYear();

export const TEMPLATES_SEARCH_OPTIONS= [
  {
    id: "0",
    name: "name",
    placeHolder: "search name",
    inputType: "TEXT",
    attributeValues: [],
    validation: []
  }
];

export const TEMPLATES_TABLE_ORDER = [
  {
    header: "Actions",
    body: "",
    width: "15%"
  },
  {
    header: "Name",
    body: "name",
    width: "40%"
  },
  {
    header: "Description",
    body: "description",
    width: "25%"
  }
];
export const STATUS_SOLD = "SOLD";

export const LABEL_SOLD = "SOLD";

export const VISIBILITY_HIDE = "Hide";

export const VISIBILITY_SHOW = "Show";

export const FORM_APPLICANT_ID_MAIN = '1536249883804001UBpj';

export const INTERIOR_COLOR_CODE = "car.interiorColor";
export const EXTERIOR_COLOR_CODE = "car.exteriorColor";

export const imageExpand = pathServer.PATH_IMG + "ic_expand_more.png";
export const imageMainBoard = pathServer.PATH_IMG + "ic_mainboard_check.png";
export const imageSquare = pathServer.PATH_IMG + "ic_check_icon.png";
export const imageList = pathServer.PATH_IMG + "ic_list.png";
export const imageParagraph = pathServer.PATH_IMG + "ic_format_textdirection_l_to_r.png";
export const imageDate = pathServer.PATH_IMG + "ic_today.png";
export const imageCheckBoxList = pathServer.PATH_IMG + "ic_radio_button_checked.png";
export const imageSingleLine =pathServer.PATH_IMG + "ic_text_fields.png";

export const STATUS_DELETE = "delete";
export const PERSON_MODE_CODE = 101;
export const DEALER_PRO_MODE_CODE = 104;
export const EMAIL_ALREADY_USED_MESSAGE = ['There is already an account created with this email address,',
  'please use a different email address or contact Zoom Autos for help.'];
export const ERROR_DUPLICATE_RECORD_CODE = 1062;
export const LIST_INVENTORY_VIEW_MODE = 3;
export const EMPTY_FIELD_ERROR_MESSAGE = "Please fill the field(s)";
export const BAD_REQUEST_ERROR = 400;
export const CAR_YEAR_START = 1900;