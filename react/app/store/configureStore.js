import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducer/index.js'

export default function configureStore(initialState,history){

  const middleware = routerMiddleware(history);

  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(middleware,thunk)
  )
}