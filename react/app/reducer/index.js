import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import {LOAD_MAKES_SUCCESS} from '../action/index.js'

const urlElasticsearch = (state = null) => {
  return state;
};

let email = (state=null) => {
  return state;
};

let accountId = (state=null) => {
  return state;
};

let firstName = (state=null) => {
  return state;
};

let urlImage = (state=null) => {
  return state;
};

const makes = (state=null,action) => {
  let{payload,type} = action;
  switch(type){
    case LOAD_MAKES_SUCCESS:
      if(payload)
        return payload.make;
      break;
    default:
      return state;
  }
};

const models = (state=null,action) => {
  let{payload,type} = action;
  switch(type){
    case LOAD_MAKES_SUCCESS:
      if(payload)
        return payload.model;
      break;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  email,
  accountId,
  firstName,
  urlImage,
  makes,
  models,
  routing: routerReducer
});

export default rootReducer;