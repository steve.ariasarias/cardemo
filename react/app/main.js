import React from 'react'
import {render} from 'react-dom'
import {browserHistory,Router} from 'react-router'
import Routes from './Routes.jsx';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

require('../styles/app.less');

const store = configureStore(window.initialState,browserHistory);
const history = syncHistoryWithStore(browserHistory, store);

render((
  <Provider store={store} >
    <MuiThemeProvider>
      <Router history={history}>
        {Routes}
      </Router>
    </MuiThemeProvider>
  </Provider>
), document.getElementById('app-container'));

