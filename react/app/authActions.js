import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
  let errors = {};

  if (Validator.isEmpty(data.email)) {
    errors.email = 'The field Email/UserName is required';
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = 'This field Password is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
}
