/**
 * Created by javierl on 3/10/16
 */

import React from 'react'
import {render} from 'react-dom'
import {browserHistory,Router} from 'react-router'
import configureStore from './store/configureStore';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Routes from './component/public/layout/LayoutRenderer.jsx'

const store = configureStore(window.initialState,browserHistory);
const history = syncHistoryWithStore(browserHistory, store);

require('../styles/catalog.less');

render((
  <Provider store={store} >
    <MuiThemeProvider>
      <Router history={history}>
        {Routes}
      </Router>
    </MuiThemeProvider>
  </Provider>
), document.getElementById('app-container'));

