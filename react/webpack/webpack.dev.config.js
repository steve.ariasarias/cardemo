var _ = require('lodash');
var scripts = require('./scripts')
var baseConfig = require('./base.config')
var path = require('path');
var webpack = require('webpack');



//var entries = _.merge({
//      bundle: './app/main.js'
//  },
//  scripts.chunks);

var config = _.merge(baseConfig, {
    entry: {
      bundle: './app/main.js',
      catalog: './app/catalog.js'
    },
    output: {
        path: path.resolve(__dirname, '../../public/javascripts'),
        publicPath: 'assets/',
        filename: '[name].js',
        chunkFilename: 'chunk.[id].js',
        pathinfo: true,
    },
    devServer: {
        contentBase: 'web',
        devtool: 'eval',
        port: 14602,
        hot: true,
        inline: true
    },
    devtool: 'eval'

});

module.exports = config ;