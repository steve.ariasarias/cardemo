#!/usr/bin/env bash

sbt docker:stage

#mkdir target/build

#cp -R target/docker/stage/* target/build
#cp Dockerfile target/build

pushd target/docker/stage

zip  -r zoom-1.0-SNAPSHOT.zip .

popd

mv target/docker/stage/zoom-1.0-SNAPSHOT.zip .