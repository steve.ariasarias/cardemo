package controllers

import com.zoom.services.{AccountService, UserService}
import com.zoom.utils.ResponseUtils.InitialState
import javax.inject._
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc._

import scala.async.Async._
import scala.concurrent.ExecutionContext.Implicits._


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               //override val userService: UserService,
                               loggingAction: LoggingAction,
                               accountService: AccountService,
                               configuration: Configuration) extends AbstractController(cc) {


  def goPublic(index :String) = Action { implicit request =>
    //val elasticSearch= configuration.underlying.getString("urlElasticsearch")
    //val data = Json.stringify(Json.toJson(InitialState(elasticSearch)))
    Ok(views.html.main("","catalog"))
  }

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index(index :String) = loggingAction{ implicit request =>
    val user = request.user
    implicit val accountId = request.user.accountId
    val data = Json.stringify(Json.toJson(InitialState(user.email,user.accountId,user.firstName,user.image)))
    Ok(views.html.main(data,"catalog"))
  }

  def main() = Action { implicit request =>
    Redirect(routes.HomeController.goPublic(""))
  }


}
