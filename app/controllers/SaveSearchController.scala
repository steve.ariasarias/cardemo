package controllers

import com.zoom.models._
import com.zoom.services._
import com.zoom.utils.Constants._
import com.zoom.utils.RequestUtils._
import com.zoom.utils.ResponseUtils._
import com.zoom.utils.ResponseMessageUtil.{MESSAGE, STATUS, _}
import javax.inject.{Inject, Singleton}
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{ControllerComponents, PlayBodyParsers, _}
import scala.async.Async.{async, await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class SaveSearchController @Inject()(cc: ControllerComponents,
                                     parsers: PlayBodyParsers,
                                     saveSearchService: SaveSearchService,
                                     userService: UserService, loggingAction: LoggingAction

                                 ) extends AbstractController(cc) {

  val logger = Logger(this.getClass)

  def save = loggingAction.async(parsers.json) { implicit request =>
    request.body.validate[ProcessSaveSearch].fold(
      errors => {
        logger.error(s"there was an error with your request -> $errors")
        Future.successful(BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "There was an error with your request. Please complete required fields")))
      },
      requestSaveSearch => {
        async {
          implicit val accountId = request.user.accountId
          val saveSearch = SaveSearch(requestSaveSearch.id, accountId, requestSaveSearch.name, requestSaveSearch.filters, requestSaveSearch.coincidence, requestSaveSearch.isAlertActive)
          val (responseCode, responseMessages,saveSearchId) = await(saveSearchService.save(saveSearch))
          if (responseCode == RESPONSE_CODE_SUCCESS) {
            logger.info(s"Save search [$saveSearchId] saved successfully in DB")
            Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Successfully save","saveSearchId"->saveSearchId))
          } else {
            logger.error(s"Error save search in DB $responseMessages")
            BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> responseMessages))
          }
        }
      }
    )
  }


  def get = loggingAction.async { implicit request =>
    async {
      implicit val accountId= request.user.accountId
      val data = await(saveSearchService.all()).toList
      Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", DATA -> Json.obj("saveSearches" -> Json.toJson(data))))
    }
  }
}
