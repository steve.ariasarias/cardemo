package controllers

import javax.inject.{Inject, Singleton}
import play.api.mvc.{ControllerComponents, PlayBodyParsers}
import play.api.mvc._
import com.zoom.utils.RequestUtils._
import com.zoom.utils.ResponseMessageUtil.{MESSAGE, STATUS}
import play.api.Logger
import play.api.libs.json.Json
import com.zoom.utils.ResponseMessageUtil._
import com.zoom.utils.Constants._

import scala.async.Async.{async, await}
import scala.concurrent.Future
import java.util.Calendar
import java.sql.Timestamp

import com.zoom.models._
import com.zoom.services._
import com.zoom.utils.ResponseUtils.ResponseAccount
import org.mindrot.jbcrypt.BCrypt

import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class AccountController @Inject()(cc: ControllerComponents,
                                  parsers: PlayBodyParsers,
                                  accountService: AccountService,
                                  userService: UserService, loggingAction: LoggingAction

                                 ) extends AbstractController(cc) {

  val logger = Logger(this.getClass)

  def save = Action.async(parsers.json) { implicit request =>
    request.body.validate[ProcessAccount].fold(
      errors => {
        logger.error(s"there was an error with your request -> $errors")
        Future.successful(BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "There was an error with your request. Please complete required fields")))
      },
      requestAccount => {
        async {
          val account = Account(requestAccount.accountId, requestAccount.email, requestAccount.firstName, requestAccount.lastName, requestAccount.password, requestAccount.dealerName, requestAccount.dealerNumber, requestAccount.phone, requestAccount.planTypeId, true, null, null)
          val (responseCode, responseMessages,accountId) = await(accountService.save(account))
          val user = User(null, accountId, requestAccount.email, requestAccount.firstName, requestAccount.lastName, requestAccount.password, true, requestAccount.phone, requestAccount.address)
          val (userResponseCode, userResponseMessages, userId) = await(userService.save(user))
          if (responseCode == RESPONSE_CODE_SUCCESS && userResponseCode == RESPONSE_CODE_SUCCESS) {
            logger.info(s"Account $accountId saved successfully in DB")
            Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", "accountId" -> accountId, "email"-> requestAccount.email, "firstName" -> requestAccount.firstName)).withSession(request.session + ("email" -> requestAccount.email) + ("accountId" -> user.accountId)  )
          } else {
            if(responseCode != RESPONSE_CODE_SUCCESS) {
              logger.error("Error save account in DB")
              if(responseCode == ERROR_DUPLICATE_RECORD.toString()){
                BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> responseMessages, CODE -> ERROR_DUPLICATE_RECORD))
              } else {
                BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> responseMessages))
              }
            } else {
              logger.error("Error save user in DB")
              BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> userResponseMessages))
            }
          }
        }
      }
    )
  }

  def update = loggingAction.async(parsers.json) { implicit request =>
    request.body.validate[ ProcessUser ].fold(
      errors => {
        logger.error(s"there was an error with your request -> $errors")
        Future.successful(BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "There was an error with your request. Please complete required fields")))
      },
      requestUser => {
        async {
          implicit val accountId = request.user.accountId
          val user = await(userService.findById(requestUser.id))
          if(user.isDefined){
            val userUpdate=user.get.copy(email=requestUser.email, firstName = requestUser.firstName, lastName = requestUser.lastName, phone = Some(requestUser.phone) , address = Some(requestUser.address))
            val (userResponseCode, userResponseMessages, userId) = await(userService.update(userUpdate))
            if (userResponseCode == RESPONSE_CODE_SUCCESS && userResponseCode == RESPONSE_CODE_SUCCESS) {
              logger.info(s"Account $accountId saved successfully in DB")
              Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", "accountId" -> Json.toJson(accountId))).withSession(request.session + ("email" -> requestUser.email))
            } else {
              if(userResponseCode != RESPONSE_CODE_SUCCESS) {
                logger.error("Error save account in DB")
                if(userResponseCode == ERROR_DUPLICATE_RECORD.toString()){
                  BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> userResponseMessages, CODE -> ERROR_DUPLICATE_RECORD))
                } else {
                  BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> userResponseMessages))
                }
              } else {
                logger.error("Error save user in DB")
                BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> userResponseMessages))
              }
            }
          }else{
            logger.error("Error user don't find in DB")
            BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "Not found"))
          }

        }
      }
    )
  }

  def changePassword = loggingAction.async(parsers.json) { implicit request =>
    request.body.validate[ChangePassword].fold(
      errors => {
        logger.error(s"there was an error with your request -> $errors")
        Future.successful(BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "There was an error with your request. Please complete required fields")))
      }, requestChangePassword => {
        async {
          implicit val accountId = request.user.accountId
          val user= await(userService.findByEmail(requestChangePassword.email))
          if(user.isDefined){
            if(BCrypt.checkpw(requestChangePassword.oldPassword,user.get.password)){
              val (responseCode, responseMessages) = await(userService.changePassword(requestChangePassword.email, requestChangePassword.newPassword))
              if (responseCode == RESPONSE_CODE_SUCCESS) {
                logger.info(s"password updated successfully in DB")
                Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Password changed successfully"))
              } else {
                logger.error("Error changing password in the db user in DB")
                BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> responseMessages))
              }
            } else {
              logger.error("Error Wrong password provided")
              BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "Error Wrong password provided"))
            }

          } else {
            logger.error("Error user not found")
            BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "Error user not found"))
          }
        }
      }
    )
  }


  def getAccount = loggingAction.async { implicit request =>
    async {
      implicit val accountId= request.user.accountId
      val account = await (accountService.findById(accountId))
      if(account.isEmpty){
        logger.error(s"Error account using this id [$accountId]")
        BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "Error account using this id"))
      } else {
        logger.info(s"Successful get account ")
        val user = await(userService.findByEmail(request.user.email))
        val responseAccount = ResponseAccount(user.get.id.getOrElse(""),user.get.accountId , user.get.firstName,user.get.lastName,user.get.address.getOrElse(""), user.get.email, user.get.phone.getOrElse(""), account.get.dealerName, account.get.dealerNumber)
        Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", DATA -> Json.obj("account" -> Json.toJson(responseAccount))))
      }
    }
  }
}
