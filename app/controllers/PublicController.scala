package controllers

import com.zoom.repositories.{MakeRepository, ModelRepository}
import com.zoom.services._
import com.zoom.utils.ResponseUtils._
import com.zoom.utils.RequestUtils._
import com.zoom.utils.Util._
import com.zoom.utils.ResponseMessageUtil._
import com.zoom.utils.Constants._
import javax.inject.{Inject, Singleton}
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._
import scala.async.Async.{async, await}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits._

@Singleton
class PublicController @Inject()(cc: ControllerComponents,
                                 parsers: PlayBodyParsers,
                                 elasticSearchService: ElasticSearchService,
                                 attributeValueService: AttributeValueService,
                                 carService: CarService,
                                 carAttributeValueService: CarAttributeValueService,
                                 carImageService: CarImageService,
                                 makeRepository: MakeRepository,
                                 modelRepository: ModelRepository
                               ) extends AbstractController(cc){

  val logger = Logger(this.getClass)

  def loadMakeAndModel = Action.async {
    async{
      var makeSelect = List[OptionMake]()
      var modelSelect = List[OptionModel]()
      val makes = await(makeRepository.all)
      val models = await(modelRepository.all)
      makes.foreach(make => {makeSelect = OptionMake(make.id.get,make.name,make.name):: makeSelect})
      models.foreach(model => {modelSelect = OptionModel(model.id.get,model.name,model.name,model.makeId):: modelSelect})
      //logger.info(s"make ${make} loaded.")
      //logger.info(s"model ${model} loaded.")
      Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Make and model load.", DATA -> Json.obj("make" -> Json.toJson(makeSelect.sortBy(_.name)), "model" -> Json.toJson(modelSelect.sortBy(_.name)))))
    }
  }

  def getCar(vin: String) = Action.async { implicit request =>
    async {
      val responseSearch = elasticSearchService.searchByTermQuery(CAR_VIN,vin)
      if(responseSearch.isError){
        logger.error(s"Error get car vin[$vin] from elasticsearch ${responseSearch.error.reason}")
        BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "Error get car"))
      } else {
        logger.info(s"Car get successfully from elasticsearch status[${responseSearch.status}]")
        logger.info(s"Car get successfully from elasticsearch[${responseSearch}]")
        val result: List[Map[String, Any]] = responseSearch.result.hits.hits.map(x => {
          x.sourceAsMap
        }).toList
        var responseCar = List[Map[String,Any]]()
        if(result.nonEmpty){
          val viewNow = result.head.getOrElse(CAR_VIEW,0).toString.toInt
          carService.updateView(result.head.getOrElse(CAR_ID,"").toString,viewNow + 1)
          elasticSearchService.updateDocument(responseSearch.result.hits.hits.head.id,Map(CAR_VIEW -> (viewNow + 1).toString))
          responseCar = result.head + (CAR_VIEW->(viewNow+1).toString) :: responseCar
        }
        Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", DATA -> Json.obj("cars" -> Json.toJson(responseCar))))
      }
    }
  }

  def searchCar = Action.async(parsers.json) { implicit request =>
    request.body.validate[FilterCarPublic].fold(
      errors => {
        logger.error(s"there was an error with your request -> $errors")
        Future.successful(BadRequest(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "there was an error with your request")))
      },
      filter => {
        async {
          val keyword = filter.keyword
          val makes = filter.makes
          val models = filter.models
          val yearFrom: Option[String] = filter.yearFrom
          val yearTo: Option[String] = filter.yearTo
          val priceMinimum: Option[BigDecimal] = filter.priceFrom
          val priceMaximum: Option[BigDecimal] = filter.priceTo
          val mileageMinimum: Option[BigDecimal] = filter.mileageFrom
          val mileageMaximum: Option[BigDecimal] = filter.mileageTo
          val body = filter.body
          val priceBest = filter.priceBest
          val newEst = filter.newEst
          val status = filter.status
          val exteriorColour = filter.exteriorColour
          val interiorColor = filter.interiorColor
          val cylinder = filter.body
          val trim = filter.trim
          logger.info(s"search car with filter keyword=[$keyword] make=[$makes] model=[$models] year range=[$yearFrom] " +
            s"to [$yearTo] price range [$priceMinimum] to [$priceMaximum] mileage range [$mileageMinimum] to [$mileageMaximum] " +
            s"priceBest [$priceBest] newEst [$newEst] body [$body] status [$status] trim [$trim]")
          /*val cars = await(carService.searchPublic(keyword, makes, models, yearFrom.map(_.toInt), yearTo.map(_.toInt), priceMinimum, priceMaximum, mileageMinimum,
            mileageMaximum, body, priceBest,newEst,status,exteriorColor,interiorColor,cylinder,trim))*/

          val responseSearch = elasticSearchService.searchDocument(filter)
          if(responseSearch.isError){
            logger.error(s"Error get cars from elasticsearch ${responseSearch.error.reason}")
            BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "Error get cars"))
          } else {
            logger.info(s"Car get successfully from elasticsearch status[${responseSearch.status}]")
            val result: List[Map[String, Any]] = responseSearch.result.hits.hits.map(x => {
              x.sourceAsMap
            }).toList
            Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", DATA -> Json.obj("cars" -> Json.toJson(result))))
          }
        }
      }
    )
  }

}
