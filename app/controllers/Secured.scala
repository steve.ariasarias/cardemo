package controllers

import com.zoom.models.User
import com.zoom.services.UserService
import com.zoom.utils.ResponseMessageUtil.MESSAGE
import javax.inject.Inject
import play.api.libs.json.Json
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}

class UserRequest[A](val user: User, request: Request[A]) extends WrappedRequest[A](request)

/*trait Secured{

  val userService: UserService

  //class UserRequest[A](val user: User, request: Request[A]) extends WrappedRequest[A](request)

  object LoggingAction extends ActionBuilder[UserRequest,AnyContent] with ActionTransformer[Request, UserRequest]{

     def invokeBlock[A](request: Request[A], block: Request[A] => Future[Result]): Future[Result] = {
      val email: String = request.session.get("email").getOrElse("")
      implicit val accountId: String = request.session.get("id").getOrElse("")

      val userOption = (
        for {
          Some(user) <- userService.findByEmail(email)
        } yield {
          Some(user)
        }
        ) recover { case _ => None }

      userOption.flatMap({
        case Some(user) => block(new UserRequest(user, request))
        case None => Future.successful(Results.Unauthorized(Json.obj(MESSAGE -> "Not Authorized")))
      })
    }
  }

}*/
class LoggingAction @Inject()(userService: UserService)(parserDefault: BodyParsers.Default)(implicit ec: ExecutionContext)
  extends ActionBuilder[UserRequest, AnyContent] {

  override def invokeBlock[A](request: Request[A], block: UserRequest[A] => Future[Result]): Future[Result] = {
    val email: String = request.session.get("email").getOrElse("")
    implicit val accountId: String = request.session.get("accountId").getOrElse("")

    val userOption = (
      for {
        Some(user) <- userService.findByEmail(email)
      } yield {
        Some(user)
      }
      ) recover { case _ => None }

    userOption.flatMap({
      case Some(user) => block(new UserRequest(user, request))
      case None => Future.successful(Results.Unauthorized(Json.obj(MESSAGE -> "Not Authorized")))
    })
  }

  override def executionContext: ExecutionContext = ec

  override def parser: BodyParser[AnyContent] = parserDefault

}