package controllers

import javax.inject.{Inject, Singleton}
import play.api.mvc.{Action, _}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future
import com.zoom.utils.RequestUtils._
import play.api.Logger
import play.api.libs.json.Json
import play.api.i18n.I18nSupport
import com.zoom.services.{AccountService, UserService}
import com.zoom.utils.RequestUtils.LoginRequest
import com.zoom.utils.ResponseMessageUtil._

@Singleton
class Login @Inject()(cc: ControllerComponents,
                      userService: UserService,
                      parsers: PlayBodyParsers
                      ) extends AbstractController(cc) with I18nSupport {

  val logger = Logger(this.getClass)

  def processLogin = Action.async(parsers.json) { implicit request =>
    request.body.validate[LoginRequest].fold(
      loginWithErrors => {
        logger.error(s"Error in login request -> $loginWithErrors")
        Future.successful(BadRequest("login.validation.errors"))
      },
      data => {
        println("data",data)
        userService.authenticate(data.email, data.password).flatMap({
          case Some(user) => {
            val dataUser = (user.email, user.firstName)
            logger.info(s"user [${data.email}] login to Zoom success..")
            //Redirect(routes.HomeController.index("")).withSession(request.session + ("email" -> user.email) + ("accountId" -> user.accountId))
            Future(Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Login Success", "dataAccount" -> dataUser)).withSession(request.session + ("email" -> user.email) + ("accountId" -> user.accountId)))
          }
          case _ => {
            logger.warn(s"User [${data.email}] not found with credentials.")
            Future.successful(BadRequest("login.authentication.error"))
          }
        })
      }
    )
  }

  def logout = Action {
    logger.info(s"Logout user")
    Redirect(routes.HomeController.goPublic("")).withNewSession
  }
}


