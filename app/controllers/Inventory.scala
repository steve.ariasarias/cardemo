package controllers


import javax.inject.{Inject, Singleton}
import com.zoom.services._
import com.zoom.utils.RequestUtils.ProcessListing
import com.zoom.utils.RequestUtils._
import com.zoom.models._
import com.zoom.utils.ResponseUtils._
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._

import scala.async.Async.{async, await}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import java.sql.{Date, Timestamp}
import java.util.Calendar

import com.zoom.utils.Constants._
import com.zoom.services.AmazonS3Service
import com.zoom.utils.Util
import com.zoom.utils.UniqueId

import scala.util.Try
import com.zoom.utils.Constants._
import com.zoom.utils.ResponseMessageUtil._

@Singleton
class Inventory @Inject()(cc: ControllerComponents,
                          parsers: PlayBodyParsers,
                          carService: CarService,
                          attributeService: AttributeService,
                          attributeValueService: AttributeValueService,
                          carAttributeValueService: CarAttributeValueService,
                          carImageService: CarImageService,
                          amazonS3Service: AmazonS3Service,
                          elasticSearchService: ElasticSearchService,
                          contactInfoService: ContactInfoService,
                          sellerLocationListingService: SellerLocationListingService,
                          loggingAction: LoggingAction
                          ) extends AbstractController(cc) {

  val logger = Logger(this.getClass)

  def uploadCarImage(carId: String) = loggingAction.async(parse.multipartFormData)  { implicit request =>
    val photo = "photo"
    implicit val accountId = request.user.accountId
    val path = s"$photo/$accountId/$carId"
    logger.info("step 1")
    async {
      val ordersDataPart = request.body.dataParts.get("orders")
      val orders = ordersDataPart.get
      val imageOrders = orders.apply(0).split(",")

      val indexMainImage = request.body.dataParts.get("indexMainImage")
      val indexList = indexMainImage.get
      logger.info("step 2")
      val files = await{Util.optionToFuture(Some(request.body.files.map(file => {
        (file.filename,file.ref.file)
      })), "Files Not Found")}
      logger.info("step 3")
      val fileIterator = files.toIterator
      var id=""
      var mainImage:Try[String]= null
      var order:Int=0
      var paths = Array[String]()
      while(fileIterator.hasNext){
        logger.info("step 4")
        val (fileName,file) = fileIterator.next()
        logger.info(s"indexMainImage: ${indexList.head}")
        if(order.toString == indexList.head){
          logger.info("step 5")
          mainImage = amazonS3Service.uploadFile(path, file,fileName)
        }
        val url: Try[String] = amazonS3Service.uploadFile(path, file,fileName)
        paths :+= url.get.toString
        id = UniqueId.generateId
        await{carImageService.save(CarImage(Some(id),accountId,url.get,Some(imageOrders(order).toInt),Some(carId)))}
        logger.info(s"New carImage id: $url")
        order += 1
      }
      val searchResponse = elasticSearchService.searchByTermQuery("id",carId)
      val response = elasticSearchService.updateDocument(searchResponse.result.hits.hits.head.id,Map("imagesUrls"->paths.slice(1,paths.length).asInstanceOf[Any],"mainImageUrl"->paths.head.asInstanceOf[Any]))
      val (carUpdateResponseCode, carUpdateResponseMessages) = await(carService.updateMainImage(carId,paths.head))
      if(response.isError){
        logger.error("Error update elasticsearch")
        BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "Error update elasticsearch"))
      }else if(carUpdateResponseCode == RESPONSE_CODE_ERROR){
        logger.error("Error updating main image url of car")
        BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> carUpdateResponseMessages))
      }else{
        logger.info("Data updated in elasticsearch and database")
        Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Save CarImage", "paths"->  Json.toJson(paths)))
      }
    }
  }

  def setAsMainImage(carId: String, imageId: String) = loggingAction.async { implicit request =>
    async {
      implicit val accountId = request.user.accountId
      val image = await(carImageService.findById(imageId))
      var url = ""
      if (image.isDefined) {
        url = image.get.url
        val car = await(carService.findById(carId))
        if (car.isDefined) {
          carService.updateMainImage(carId, url)
          logger.info(s"set main image ok in database")
          val searchResponse = elasticSearchService.searchByTermQuery(CAR_ID,carId)
          logger.info(s"search elasticsearch by id $carId ok")
          val response = elasticSearchService.updateDocument(searchResponse.result.hits.hits.head.id,Map("mainImageUrl"->url.asInstanceOf[Any]))
          if(response.isError){
            logger.error("Error update elasticsearch")
            BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "Error update elasticsearch main image"))
          }else{
            logger.info("Data updated in elasticsearch")
            Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Main image updated.","mainImage" ->url))
          }
        } else {
          logger.error(s"Not found car with id :-> $carId")
          BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "not found car"))
        }
      } else {
        logger.error(s"Not found image with id :-> $imageId")
        BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "not found image"))
      }
    }
  }

  def save = loggingAction.async(parsers.json) { implicit request =>
    request.body.validate[ProcessListing].fold(
      errors => {
        logger.error(s"there was an error with your request -> $errors")
        Future.successful(BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "There was an error with your request. Please complete required fields")))
      },
      requestListing => {
        async {
          implicit val accountId = request.user.accountId
          val now: Calendar = Calendar.getInstance()
          val requestCar = requestListing.car
          val car = Car(requestCar.carId,accountId,requestCar.year.toInt,requestCar.makeId,requestCar.modelId,requestCar.vin,BigDecimal(requestCar.price),0,requestCar.mainImageUrl,new Date(now.getTimeInMillis),requestCar.description,requestCar.mileage.toInt,0,true,0)
          val attributes = await(attributeService.loadByEntityType(ENTITY_TYPE_CAR))
          val attributeValuesDB = await(attributeValueService.all).toList
          val objectValues = carService.setObjectValue(attributes,requestCar.values,attributeValuesDB)
          val (responseCode, responseMessages, objectId) = await(carService.save(car,objectValues))
          if (responseCode == RESPONSE_CODE_ERROR) {
            logger.error("Error save car in DB")
            BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> responseMessages))
          } else {
            logger.info("Car saved successfully in DB")
            if(requestListing.contactInfo.isDefined){
              val requestContactInfo = requestListing.contactInfo.get
              val contactInfo = ContactInfo(requestContactInfo.id,accountId,objectId,requestContactInfo.typeSeller,requestContactInfo.dealerName.getOrElse(""),requestContactInfo.dealerNumber.getOrElse(""),
                requestContactInfo.firstName,requestContactInfo.lastName,requestContactInfo.phone,requestContactInfo.email,requestContactInfo.isTextSeller,requestContactInfo.isUseZoomInfo)
              val (responseCodeContact, responseMessageContact, contactId) = await(contactInfoService.save(contactInfo))
              if(responseCodeContact == RESPONSE_CODE_ERROR){
                logger.error("Error save contact Info in DB")
                BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> responseMessageContact))
              }else{
                logger.info("Contact Info saved successfully in DB")
                if(requestListing.sellerLocation.isDefined){
                  val requestSellerLocation = requestListing.sellerLocation.get
                  val sellerLocation = SellerLocationListing(requestSellerLocation.id,accountId,objectId,requestSellerLocation.address1,requestSellerLocation.address2,requestSellerLocation.city,
                    requestSellerLocation.state,requestSellerLocation.zipCode,requestSellerLocation.isUseSellerInfo)
                  val (responseCodeSellerLocation, responseMessageSellerLocation, sellerLocationId) = await(sellerLocationListingService.save(sellerLocation))
                  if(responseCodeSellerLocation == RESPONSE_CODE_ERROR){
                    logger.error("Error save seller location in DB")
                    BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> responseMessageSellerLocation))
                  } else {
                    saveListingElasticSearch(requestCar,objectId,now)
                  }
                }else{
                  saveListingElasticSearch(requestCar,objectId,now)
                }
              }
            }else{
              saveListingElasticSearch(requestCar,objectId,now)
            }
          }
        }
      }
    )
  }

  def saveListingElasticSearch(requestCar:ProcessCar, objectId:String, now:Calendar) = {
    var carInformation = Map[String, Any]()
    carInformation = requestCar.values ++ carInformation
    carInformation += ("vin" -> requestCar.vin)
    carInformation += ("year" -> requestCar.year.toInt)
    carInformation += ("price" -> requestCar.price.toInt)
    carInformation += ("modelId" -> requestCar.modelId)
    carInformation += ("modelName" -> requestCar.modelName)
    carInformation += ("mileage" -> requestCar.mileage.toInt)
    carInformation += ("makeId" -> requestCar.makeId)
    carInformation += ("makeName" -> requestCar.makeName)
    carInformation += ("mainImageUrl" -> requestCar.mainImageUrl)
    carInformation += ("description" -> requestCar.description)
    carInformation += ("view" -> 0)
    val carId = requestCar.carId
    if (carId.isDefined) {
      carInformation = requestCar.values ++ carInformation
      val result = elasticSearchService.searchById(carId.getOrElse("")).result
      if (result.isEmpty || result.hits.hits.length == 0) {
        BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> s"Not Found carId[$carId] in elasticsearch"))
      } else {
        elasticSearchService.updateDocument(result.hits.hits.head.id, carInformation)
        logger.info(s"Car $carId updated successfully in elasticsearch")
        Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", "carId" -> Json.toJson(objectId)))
      }
    } else {
      carInformation += ("id" -> objectId)
      carInformation += ("createdDate" -> now.getTimeInMillis)
      elasticSearchService.saveDocument(carInformation)
      logger.info(s"Car $objectId saved successfully in elasticsearch")
      Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", "carId" -> Json.toJson(objectId)))
    }
  }

  def searchListing = loggingAction.async(parsers.json) { implicit request =>
    request.body.validate[FilterCar].fold(
      errors => {
        logger.error(s"there was an error with your request -> $errors")
        Future.successful(BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> "there was an error with your request")))
      },
      filter => {
        async {
          implicit val accountId = request.user.accountId
          val keyword = filter.keyword
          val from = filter.from
          val size = filter.size
          val active = filter.active
          logger.info(s"search car with filter keyword=[$keyword] active [$active]")
          val cars = await(carService.search(keyword,from,size,active, accountId))
          val carAttributesValues = await(carAttributeValueService.findCarAttributeValues(accountId))
          val attributes = await(attributeService.all).toList
          val favoritesCars = await(carService.getFavorites)
          val imageCars = await(carService.getImageCars)
          //val attributeCarBody = attributes.filter(_.code == "body").head
          //val attributesValuesAll = await(attributeValueService.all)
          //val attributeValuesCarBody = attributesValuesAll.filter(_.attributeId == attributeCarBody.id.get)
          val carsIterator = cars.toIterator
          var carJsonList = List[Map[String,Any]]()
          while (carsIterator.hasNext) {
            var dataCar = Map[String,Any]()
            val car = carsIterator.next()
            dataCar += ("id" -> car.id.getOrElse(""))
            dataCar += ("createdDate" -> car.createDate.get)
            dataCar += ("vin" -> car.vin)
            dataCar += ("year" -> car.year)
            dataCar += ("price" -> car.suggestedPrice)
            dataCar += ("modelId" -> car.modelId)
            dataCar += ("mileage" -> car.mileage)
            dataCar += ("makeId" -> car.makeId)
            dataCar += ("description" -> car.description)
            dataCar += ("view" -> car.view)
            val carImagesById: List[String] = imageCars.filter(_.carId.contains(car.id.getOrElse(""))).map(_.url.toString).toList
            if(carImagesById.length == 1){
              dataCar += ("mainImageUrl" -> carImagesById.head)
            }else if(carImagesById.length > 1){
              dataCar += ("mainImageUrl" -> car.mainImage)
              dataCar += ("imagesUrls" -> carImagesById.filter(x => x != car.mainImage))
            }else{
              dataCar += ("mainImageUrl" -> car.mainImage)
              dataCar += ("imagesUrls" -> carImagesById)
            }
            val favoriteById = favoritesCars.filter(_.carId == car.id.getOrElse(""))
            dataCar += ("stars" -> favoriteById.length)
            val carAttributeValuesIterator = carAttributesValues.filter(_.objectId == car.id.get).toIterator
            val carInfo = getDataCarAttributeValues(carAttributeValuesIterator, attributes)
            dataCar = dataCar ++ carInfo
            carJsonList = dataCar :: carJsonList
          }
          logger.info(s"found cars ${cars.length}")
          Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "search Ok", DATA -> Json.obj("cars" -> carJsonList.sortBy(x=> x("createdDate").asInstanceOf[Timestamp]))))
        }
      }
    )
  }

  def getDataCarAttributeValues(carAttributeValuesIterator: Iterator[ObjectAttributeValue], attributes: List[Attribute]): Map[String, Any] = {
    var carInfo = Map[String,Any]()
    while (carAttributeValuesIterator.hasNext) {
      val carAttributeValue = carAttributeValuesIterator.next()
      val attributeCar = attributes.filter(_.id.contains(carAttributeValue.attributeId))
      if(attributeCar.nonEmpty){
        carInfo += (attributeCar.head.code -> carAttributeValue.value)
      }else{
        logger.warn(s"Not found car attribute with id[${carAttributeValue.attributeId}]")
      }
      /*if (attributeCar.head.typeAttribute.get != "TEXT" && attributeCar.head.typeAttribute.get != "TEXTAREA") {
        val attributeValue = attributesValuesOfAttributes.filter(_.id.contains(carAttributeValue.attributeValueId))
        if (attributeValue.nonEmpty) {
          infoCarList = InfoAttributeValue(attributeCar.head.id.get, attributeCar.head.code, attributeValue.head.displayValue) :: infoCarList
        } else if (carAttributeValue.attributeValueId.isEmpty) {
          infoCarList = InfoAttributeValue(attributeCar.head.id.get, attributeCar.head.code, carAttributeValue.value) :: infoCarList
        }
      } else {
        infoCarList = InfoAttributeValue(attributeCar.head.id.get, attributeCar.head.code, carAttributeValue.value) :: infoCarList
      }*/
    }
    carInfo
  }

  def setOrRemoveFavoriteCar(carId:String) = loggingAction.async { implicit request =>
    async {
      implicit val accountId = request.user.accountId
      val userId = request.user.id.getOrElse("")
      val car = await { carService.findOnlyById(carId)}
      if (car.isDefined) {
        val (responseCode, responseMessages, objectId) = await(carService.setOrRemoveFavorite(carId,userId))
        if (responseCode == RESPONSE_CODE_SUCCESS) {
          val result = elasticSearchService.searchByTermQuery(CAR_ID,carId).result
          if(result.isEmpty || result.hits.hits.length == 0){
            BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> s"Not Found carId[$carId] in elasticsearch"))
          }else{
            val favoriteCar = await(carService.getFavoritesByUser(userId))
            elasticSearchService.updateDocument(result.hits.hits.head.id,Map("favorites" -> favoriteCar.map(_.carId)))
            logger.info(s"Car $carId updated successfully in elasticsearch")
            Ok(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "Success", "carId" -> Json.toJson(objectId)))
          }
        } else {
          logger.error(s"Error saved or removed car with id [$carId] as favorite")
          BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> responseMessages))
        }
      } else {
        BadRequest(Json.obj(STATUS -> STATUS_ERROR, MESSAGE -> s"car with id [$carId] not found"))
      }
    }
  }

}
