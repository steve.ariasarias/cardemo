package controllers

import com.zoom.models.{Make, Model}
import com.zoom.repositories.MakeRepository
import com.zoom.utils.RequestUtils.MakeAndModelRequest
import com.zoom.utils.ResponseMessageUtil._
import javax.inject.{Inject, Singleton}
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._
import scala.async.Async._
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future

@Singleton
class AdministrativeController @Inject()(cc: ControllerComponents,
                                         parsers: PlayBodyParsers,
                                         makeRepository: MakeRepository) extends AbstractController(cc) {

  val logger = Logger(this.getClass)

  def saveMakesAndModels = Action.async(parsers.json) { implicit request =>
    request.body.validate[MakeAndModelRequest].fold(
      errors => {
        logger.error(s"there was an error with your request -> $errors")
        Future.successful(BadRequest(Json.obj(STATUS -> STATUS_OK, MESSAGE -> "there was an error with your request")))
      }, makeRequest => {
        async {
          var makes = List[Make]()
          var models = List[Model]()
          makeRequest.makes.foreach(make => {
            makes = Make(Some(make.id), make.name) :: makes
          })
          makeRequest.models.foreach(model => {
            models = Model(Some(model.id), model.makeId, model.name) :: models
          })
          val result = await(makeRepository.saveMakesAndModels(makes, models))
          if (result.isSuccess) {
            Ok(Json.obj(MESSAGE -> "upload makes and models"))
          } else {
            logger.error("error insert:", result.failed.get)
            BadRequest(Json.obj(STATUS -> STATUS_OK, MESSAGE -> result.failed.get.getMessage))
          }
        }
      }
    )
  }


}
