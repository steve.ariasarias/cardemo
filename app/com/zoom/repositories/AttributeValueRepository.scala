package com.zoom.repositories

import com.zoom.models.{AttributeValue, AttributeValuesTable}
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.MySQLProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.Future

@Singleton
class AttributeValueRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends AccountBaseRepository[AttributeValuesTable, AttributeValue](dbConfigProvider,TableQuery[AttributeValuesTable])
{

  def getByAttributeQuery(attributeId: String)(implicit accountId:String): Query[AttributeValuesTable, AttributeValue, Seq] = query.filter(table => table.attributeId === attributeId && table.accountId === accountId)

  def loadByAttributeId(attributeId: String)(implicit dealer:String): Future[List[AttributeValue]] = {
    val action = getByAttributeQuery(attributeId).to[List].result
    //println(action.statements.head)
    db.run(action)
  }

  def getAll = {
    db.run(query.result)
  }

  def findByCode(code: String)(implicit accountId: String): Future[Option[AttributeValue]] = db.run(query.filter(x => x.code === code && x.accountId === accountId).result.headOption)
}