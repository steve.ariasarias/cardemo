package com.zoom.repositories

import com.zoom.models.{ContactInfo, ContactInfoTable}
import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.lifted.TableQuery

class ContactInfoRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)extends AccountBaseRepository[ContactInfoTable, ContactInfo](dbConfigProvider,TableQuery[ContactInfoTable]){

}

