package com.zoom.repositories

import com.zoom.models._
import com.zoom.utils.Constants._
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.MySQLProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.Future
import scala.util.Try
import scala.concurrent.ExecutionContext.Implicits._

@Singleton
class CarRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider, carAttributeValueRepository: CarAttributeValueRepository) extends AccountBaseRepository[CarTable, Car](dbConfigProvider,TableQuery[CarTable])
{

  val Makes: TableQuery[MakesTable] = TableQuery[MakesTable]
  val Models: TableQuery[ModelsTable] = TableQuery[ModelsTable]
  val CarAttributeValues :TableQuery[CarAttributeValueTable] = TableQuery[CarAttributeValueTable]

  def create(car: Car, carAttributeValueList: List[ObjectAttributeValue])(implicit accountId:String): Future[Try[Unit]] = {
    db.run(
      DBIO.seq(
        saveQuery(car),
        carAttributeValueRepository.saveListQuery(carAttributeValueList)
      ).transactionally.asTry
    )
  }

  def updateCarDescription(id:String, description: String)(implicit accountId:String): Future[Try[Int]] = {
    getById(id).flatMap( {
      case Some(car) => updateById(car.id.get, car.copy(description = description))
      case _ => Future.successful(Try(0))
    })
  }

  def updateCarDescription2(id:String, description: String)(implicit accountId:String): Future[Unit] = {
    val a = (for {
      car <- getByIdQuery(id).result.head
      _ <- updateByIdQuery(car.id.get, car.copy(description = description))
    } yield ()).transactionally
    db.run(a)
  }

  def saveList(cars: List[Car], carAttributeValues: List[ObjectAttributeValue],carsUpdate:List[Car], carAttributeValuesUpdate: List[ObjectAttributeValue])(implicit accountId:String)= {
    val updateCars = carsUpdate.map(updateCar => {
      query.filter(x => x.id === updateCar.id && x.accountId ===accountId).update(updateCar)
    })
    db.run(DBIO.seq(
      query ++= cars,
      CarAttributeValues ++= carAttributeValues,
      DBIO.sequence(updateCars),
      carAttributeValueRepository.updateBatch(carAttributeValuesUpdate)
    ).transactionally.asTry)
  }

  def insertList(cars: List[Car], carAttributeValues: List[ObjectAttributeValue]) = {
    db.run(DBIO.seq(
      query ++= cars,
      CarAttributeValues ++= carAttributeValues
    ).transactionally.asTry)
  }

  def findByVin(vin: String)(implicit accountId: String): Future[Option[Car]] =
    db.run(query.filter((x) => x.vin === vin && x.accountId === accountId).result.headOption)

  def findById(carId: String): Future[Option[Car]] =
    db.run(query.filter(_.id === carId).result.headOption)

  def search(keyword:Option[String],from:Option[Int],size:Option[Int],active:Option[Boolean], accountId:String) = {
    /*db.run(Cars.filter(x => (x.customerId === customerId) &&  ((x.model like "%"+model.get+"%"))).to[List].result)*/

    val allCars = searchQuery(keyword,active,accountId)

    db.run(allCars.sortBy(_.order).drop(from.getOrElse(0)).take(size.getOrElse(CARS_BY_PAGE_DEFAULT)).to[List].result)
  }

  def quantityCars(keyword: Option[List[String]], makes: Option[List[String]], models: Option[List[String]],
                   yearFrom: Option[Int], yearTo: Option[Int], priceFrom: Option[BigDecimal], priceTo: Option[BigDecimal],
                   mileageFrom: Option[Int], mileageTo: Option[Int], body: Option[String], status:Option[List[String]],active:Option[Boolean],accountId: String): Future[Int] = {

    ///val allCars = searchQuery(keyword,makes,models,yearFrom,yearTo,priceFrom,priceTo,mileageFrom,mileageTo,body,status,active,accountId)
    db.run(query.length.result)
  }

  def getLastOrder() = {
    db.run(query.length.result)
  }

  def searchQuery(keyword:Option[String],active:Option[Boolean], accountId:String) = {

    val filterListing = query.filter { car =>
      var repString: List[Option[Rep[Boolean]]] = List(keyword.map(x => (car.vin === x) || (car.description like "%" + x + "%") || (car.makeId === x) || (car.modelId === x)))
      if(Try(keyword.getOrElse("").toInt).isSuccess){
        repString = keyword.map(x =>  car.year === x.toInt || car.mileage === x.toInt) :: repString
      }else if(Try(BigDecimal(keyword.getOrElse(""))).isSuccess){
        repString = keyword.map(x =>  car.suggestedPrice === BigDecimal(x)) :: repString
      }
      repString.collect({ case Some(criteria) => criteria }).reduceLeftOption(_ || _).getOrElse(true: Rep[Boolean])
    }.filter(x => x.accountId === accountId && x.active === active.getOrElse(true))
    filterListing.result.statements.foreach(println)
    /*val queryCarAttributesValue = CarAttributeValues.filter { carAttributeValue =>
      List(
        body.map(carAttributeValue.attributeValueId === _ ),
        status.map(carAttributeValue.value inSet _ )
      ).collect({ case Some(criteria) => criteria }).reduceLeftOption(_ || _).getOrElse(true: Rep[Boolean])
    }.map(_.carId)*/
    filterListing
  }

  def updateMainImage(carId: String, mainImage: String)(implicit accountId: String): Future[Try[Unit]] = {
    db.run(
      DBIO.seq(
        query.filter(x => x.id === carId && x.accountId === accountId).map(x => x.mainImage).update(mainImage)
      ).transactionally.asTry
    )
  }

  def updateView(carId: String, view: Int): Future[Int] = {
    db.run(query.filter(x => x.id === carId).map(x => x.view).update(view))
  }


  def updateCar(car: Car)(implicit accountId: String) = {
    db.run(query.filter(x => x.id === car.id.get && x.accountId === accountId).update(car))
  }

  def updateOrderCar(cars:List[Car])(implicit accountId:String) = {
    val updateCars = cars.map(car => {
      query.filter(x => x.id === car.id && x.accountId === car.accountId && x.accountId === accountId).update(car)
    })
    db.run(DBIO.sequence(updateCars))
  }

  def update(car: Car, carAttributeValueList: List[ObjectAttributeValue])(implicit accountId:String): Future[Try[Unit]] = {
    db.run(
      DBIO.seq(
        query.filter(_.id === car.id.get).update(car),
        CarAttributeValues.filter(_.carId === car.id.get).delete,
        CarAttributeValues ++= carAttributeValueList
      ).transactionally.asTry
    )
  }

  def searchPublic(keyword: Option[List[String]], makes: Option[List[String]], models: Option[List[String]],
                   yearFrom: Option[Int], yearTo: Option[Int], priceFrom: Option[BigDecimal], priceTo: Option[BigDecimal],
                   mileageFrom: Option[Int], mileageTo: Option[Int], body: Option[List[String]], priceBest: Option[BigDecimal],
                   newEst: Option[Boolean],title:Option[List[String]],exteriorColor:Option[List[String]],interiorColor:Option[List[String]],
                   cylinder:Option[List[String]],trim:Option[String]): Future[List[Car]] = {

    val queryMake = Makes.filter { makeItem =>
      List(
        makes.map(makeItem.name inSet  _ )
      ).collect({ case Some(criteria) => criteria }).reduceLeftOption(_ || _).getOrElse(true: Rep[Boolean])
    }.map(_.id)

    val queryModel = Models.filter { modelItem =>
      List(
        models.map(modelItem.name inSet  _ )
      ).collect({ case Some(criteria) => criteria }).reduceLeftOption(_ || _).getOrElse(true: Rep[Boolean])
    }.map(_.id)

    var yearList = List[Int]()

    if(keyword.nonEmpty){
      keyword.get.foreach(x => {
        try {
          yearList = x.toInt :: yearList
        } catch {
          case e: Exception => None
        }
      })
    }

    val filterYear = query.filter { car =>
      car.year inSet yearList
    }.map(_.id)

    var queryModelList = List[Rep[Boolean]]()
    val filterMake = Makes.filter { make =>
      keyword.getOrElse(List[String]()).map(x => {
        make.name like "%" + x + "%"
      }).collect({ case criteria => criteria }).reduceLeftOption(_ || _).getOrElse(true: Rep[Boolean])
    }.map(_.id)

    val filterModel = Models.filter { model =>
      keyword.foreach(x => {
        queryModelList = x.map(model.name like "%" + _ + "%")
      })
      queryModelList.collect({ case criteria => criteria }).reduceLeftOption(_ || _).getOrElse(true: Rep[Boolean])
    }.map(_.id)

    val filterDes = query.filter { car =>
      keyword.getOrElse(List[String]()).map(x => {
        car.description like "%" + x + "%"
      }).collect({ case criteria => criteria }).reduceLeftOption(_ || _).getOrElse(true: Rep[Boolean])
    }.map(_.id)


    val queryCar = query.filter { car =>
      List(
        yearFrom.map(car.year >= _),
        yearTo.map(car.year <= _),
        priceFrom.map(car.suggestedPrice >= _),
        priceTo.map(car.suggestedPrice <= _),
        mileageFrom.map(car.mileage >= _),
        mileageTo.map(car.mileage <= _),
      ).collect({ case Some(criteria) => criteria }).reduceLeftOption(_ && _).getOrElse(true: Rep[Boolean])
    }.filter(x => x.active === true)

    val queryCarAttributesValue = CarAttributeValues.filter { carAttributeValue =>
      List(
        body.map(carAttributeValue.value inSet _),
        title.map(carAttributeValue.value inSet _),
        exteriorColor.map(carAttributeValue.value inSet _),
        interiorColor.map(carAttributeValue.value inSet _),
        cylinder.map(carAttributeValue.value inSet _),
        trim.map(carAttributeValue.value like "%" + _ + "%")
      ).collect({ case Some(criteria) => criteria }).reduceLeftOption(_ && _).getOrElse(true: Rep[Boolean])
    }.filter(x => x.value === CARS_STATUS_FRONT_LINE_READY).map(_.carId)

    db.run(queryCar.filter(_.id in queryCarAttributesValue).filter(x => (x.makeId in filterMake) || (x.modelId in filterModel) || (x.id in filterDes) || (x.id in filterYear)).
      filter(x => (x.makeId in queryMake) && (x.modelId in queryModel)).to[List].result)
  }


}






