package com.zoom.repositories

import com.zoom.models.{Make,Model, MakesTable, ModelsTable}
import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import slick.jdbc.MySQLProfile.api._
import slick.jdbc.JdbcBackend
import slick.lifted.TableQuery

import scala.concurrent.Future


class MakeRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider){

  val query: TableQuery[MakesTable] = TableQuery[MakesTable]
  val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]
  val db: JdbcBackend#DatabaseDef = dbConfig.db
  val Models: TableQuery[ModelsTable] = TableQuery[ModelsTable]

  def all :Future[List[Make]] = db.run(query.to[List].result)

  def saveMakesAndModels(makes:List[Make],models:List[Model]) = {
    db.run(DBIO.seq(
      query ++= makes,
      Models ++= models
    ).transactionally.asTry)
  }

}

