package com.zoom.repositories

import com.zoom.models.{AttributeEntityTypes, AttributeEntityTypesTable, AttributesTable}
import javax.inject.{Inject, Singleton}
import slick.jdbc.MySQLProfile.api._
import play.api.db.slick.DatabaseConfigProvider
import slick.lifted.TableQuery

@Singleton
class AttributeEntityTypesRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends AccountBaseRepository[AttributeEntityTypesTable, AttributeEntityTypes](dbConfigProvider,TableQuery[AttributeEntityTypesTable])  {

  val Attributes: TableQuery[AttributesTable] = TableQuery[AttributesTable]

  def loadByEntityType(entityType:String) = {
    val attributes= for{
      attributeEntityType <-  query.filter(x=>x.entityType === entityType)
      attributes <-  attributeEntityType.attribute
    } yield attributes
    val action = attributes.to[List].result
    db.run(action)
  }


  def getByAttributeQuery(attributeId: String)(implicit accountId:String) = query.filter(table => table.attributeId === attributeId && table.accountId === accountId)

  def loadByAttributeId(attributeId: String)(implicit dealer:String) = {
    val action = getByAttributeQuery(attributeId).to[List].result
    //println(action.statements.head)
    db.run(action)
  }

  def loadByAttributeIds(attributeIds: List[String])(implicit accountId: String) = {
    db.run(query.filter(x=>  x.accountId === accountId && (x.attributeId inSet attributeIds)).to[List].result)
  }


}
