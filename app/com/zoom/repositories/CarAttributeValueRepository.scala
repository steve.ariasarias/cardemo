package com.zoom.repositories

import com.zoom.models.{CarAttributeValueTable, ObjectAttributeValue}
import com.zoom.utils.RequestUtils.InfoAttribute
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.MySQLProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.Future

@Singleton
class CarAttributeValueRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends LinkTableBaseRepository[CarAttributeValueTable, ObjectAttributeValue](dbConfigProvider,TableQuery[CarAttributeValueTable])
{

  def createList(carAttributeValues: List[ObjectAttributeValue]) = {
    query ++= carAttributeValues
  }

  def update(carAttributeValue: ObjectAttributeValue)(implicit accountId:String): Future[Int] = {
    db.run(query.filter(x => x.carId === carAttributeValue.objectId && x.accountId === carAttributeValue.accountId && x.attributeId === carAttributeValue.attributeId && x.accountId ===accountId).update(carAttributeValue))
  }

  def findCarAttributeValues(implicit accountId: String): Future[List[ObjectAttributeValue]] = {
    db.run(query.filter(_.accountId === accountId).to[List].result)
  }

  def findAttributesByCarId(carId: String)(implicit accountId: String): Future[List[ObjectAttributeValue]] = {
    db.run(query.filter(x => x.carId === carId && x.accountId === accountId).to[List].result)
  }

  def updateBatch(carAttributeValues: List[ObjectAttributeValue])(implicit accountId:String) = {
    val updateCarAttributeValues = carAttributeValues.map(carAttributeValue => {
      query.filter(x => x.carId === carAttributeValue.objectId &&  x.attributeId === carAttributeValue.attributeId && x.accountId === accountId).update(carAttributeValue)
    })
    DBIO.sequence(updateCarAttributeValues)
  }

  def updateAttributeValueIdAndValue(carId: String, attributeId:String,infoAttribute: InfoAttribute)(implicit accountId: String) = {
    db.run(query.filter(x => x.carId === carId && x.accountId === accountId && x.attributeId === attributeId).map(x => (x.attributeValueId, x.value)).update(infoAttribute.attributeCode, infoAttribute.attributeValue))
  }

}
