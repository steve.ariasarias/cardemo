package com.zoom.repositories

import com.zoom.models.{Model,ModelsTable}
import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import slick.jdbc.MySQLProfile.api._
import slick.jdbc.JdbcBackend
import slick.lifted.TableQuery

import scala.concurrent.Future


class ModelRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider){

  val query: TableQuery[ModelsTable] = TableQuery[ModelsTable]
  val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]
  val db: JdbcBackend#DatabaseDef = dbConfig.db

  def all :Future[List[Model]] = db.run(query.to[List].result)
}

