package com.zoom.repositories

import java.sql.Timestamp
import com.zoom.models.LinkTableBaseTable
import play.api.db.slick.DatabaseConfigProvider
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import scala.reflect.ClassTag
import slick.jdbc.MySQLProfile.api._
import slick.jdbc.JdbcBackend
import slick.lifted.CanBeQueryCondition

import scala.concurrent.Future


trait LinkTableBaseEntity{
  val accountId: String
  val createDate: Option[Timestamp]
  val modifiedDate: Option[Timestamp]
}

trait LinkTableBaseRepositoryComponent[T <: LinkTableBaseTable[E], E <: LinkTableBaseEntity] {

  def getAll(implicit accountId:String): Future[Seq[E]]

  def filter[C <: Rep[_]](expr: T => C)(implicit accountId:String,wt: CanBeQueryCondition[C]): Future[Seq[E]]

//  todo: findout what to return from save
  def save(row: E)(implicit accountId:String): Future[_]

}


trait LinkTableBaseRepositoryQuery[T <: LinkTableBaseTable[E], E <: LinkTableBaseEntity] {
  val query: TableQuery[T]

  def getAllQuery(implicit accountId: String): Query[T, E, Seq] = query.filter(_.accountId === accountId)

  def filterQuery[C <: Rep[_]](expr: T => C)(implicit accountId:String ,wt: CanBeQueryCondition[C]): Query[T, E, Seq] = query.filter(_.accountId === accountId).filter(expr)

  def saveQuery(row: E)(implicit accountId: String) = query += row

  def saveListQuery(rows: List[E])(implicit accountId: String) = query ++= rows

}


abstract class LinkTableBaseRepository[T <: LinkTableBaseTable[E], E <: LinkTableBaseEntity : ClassTag](dbConfigProvider: DatabaseConfigProvider, clazz: TableQuery[T]) extends LinkTableBaseRepositoryQuery[T, E] with LinkTableBaseRepositoryComponent[T, E] {

  //    lazy val clazzEntity = ClassTag[E].asInstanceOf[Class[E]]
  val query: TableQuery[T] = clazz
  val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]
  val db: JdbcBackend#DatabaseDef = dbConfig.db

  def getAll(implicit accountId: String): Future[Seq[E]] = {
    val action = getAllQuery.result
    println(action.statements.head)
    db.run(action)
  }

  def filter[C <: Rep[_]](expr: T => C)(implicit accountId: String,wt: CanBeQueryCondition[C]) = {
    db.run(filterQuery(expr).result)
  }

  def save(row: E)(implicit accountId: String) = {
    db.run(saveQuery(row))
  }

}


