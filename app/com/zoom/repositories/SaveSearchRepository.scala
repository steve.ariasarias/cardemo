package com.zoom.repositories

import com.zoom.models.{SaveSearch, SaveSearchesTable}
import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.lifted.TableQuery

class SaveSearchRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)extends AccountBaseRepository[SaveSearchesTable, SaveSearch](dbConfigProvider,TableQuery[SaveSearchesTable]){


}

