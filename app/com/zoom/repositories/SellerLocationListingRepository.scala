package com.zoom.repositories

import com.zoom.models.{SellerLocationListing, SellerLocationListingTable}
import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.lifted.TableQuery

class SellerLocationListingRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)extends AccountBaseRepository[SellerLocationListingTable, SellerLocationListing](dbConfigProvider,TableQuery[SellerLocationListingTable]){

}

