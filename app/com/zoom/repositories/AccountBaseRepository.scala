package com.zoom.repositories

import java.sql.Timestamp
import com.zoom.models.AccountBaseTable
import play.api.db.slick.DatabaseConfigProvider
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import scala.reflect.ClassTag
import slick.jdbc.MySQLProfile.api._
import slick.jdbc.JdbcBackend
import slick.lifted.CanBeQueryCondition
import scala.concurrent.Future
import scala.util.Try

trait AccountBaseEntity{
  val id: Option[String]
  val accountId: String
  val createDate: Option[Timestamp]
  val modifiedDate: Option[Timestamp]
}

trait AccountBaseRepositoryComponent[T <: AccountBaseTable[E], E <: AccountBaseEntity] {
  def getById(id: String)(implicit accountId:String): Future[Option[E]]

  def getAll(implicit accountId:String): Future[Seq[E]]

  def filter[C <: Rep[_]](expr: T => C)(implicit accountId:String,wt: CanBeQueryCondition[C]): Future[Seq[E]]

//  todo: findout what to return from save
  def save(row: E)(implicit accountId:String): Future[_]

  def deleteById(id: String)(implicit accountId: String): Future[Int]

  def updateById(id: String, row: E)(implicit accountId: String): Future[_]
}


trait AccountBaseRepositoryQuery[T <: AccountBaseTable[E], E <: AccountBaseEntity] {
  val query: TableQuery[T]

  def getByIdQuery(id: String)(implicit accountId:String): Query[T, E, Seq] = query.filter(table => table.id === id && table.accountId === accountId)

  def getAllQuery(implicit accountId: String): Query[T, E, Seq] = query.filter(_.accountId === accountId)

  def filterQuery[C <: Rep[_]](expr: T => C)(implicit accountId:String ,wt: CanBeQueryCondition[C]): Query[T, E, Seq] = query.filter(_.accountId === accountId).filter(expr)

  def saveQuery(row: E)(implicit accountId: String) = query += row

  def saveListQuery(rows: List[E])(implicit accountId: String) = query ++= rows

  def deleteByIdQuery(id: String)(implicit accountId: String) = query.filter(t => t.accountId === accountId && t.id === id).delete

  def updateByIdQuery(id: String, row: E)(implicit accountId: String) = query.filter(t => t.accountId === accountId && t.id === id).update(row)
}


abstract class AccountBaseRepository[T <: AccountBaseTable[E], E <: AccountBaseEntity : ClassTag](dbConfigProvider: DatabaseConfigProvider, clazz: TableQuery[T]) extends AccountBaseRepositoryQuery[T, E] with AccountBaseRepositoryComponent[T, E] {

  //    lazy val clazzEntity = ClassTag[E].asInstanceOf[Class[E]]
  val query: TableQuery[T] = clazz
  val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]
  val db: JdbcBackend#DatabaseDef = dbConfig.db

  def getAll(implicit accountId: String): Future[Seq[E]] = {
    val action = getAllQuery.sortBy(x => x.createDate.desc).result
    //println(action.statements.head)
    db.run(action)
  }

  def getById(id: String)(implicit accountId:String): Future[Option[E]] = {
    val action = getByIdQuery(id).result.headOption
    println(action.statements.head)
    db.run(action)
  }

  def filter[C <: Rep[_]](expr: T => C)(implicit accountId: String,wt: CanBeQueryCondition[C]) = {
    db.run(filterQuery(expr).result)
  }

  def save(row: E)(implicit accountId: String): Future[Try[Int]] = {
    db.run(saveQuery(row).asTry)
  }

  def updateById(id: String, row: E)(implicit accountId: String): Future[Try[Int]] = {
    val action = updateByIdQuery(id, row)
    println(action.statements.head)
    db.run(action.asTry)
  }

  def deleteById(id: String)(implicit accountId: String) = {
    db.run(deleteByIdQuery(id))
  }
}


