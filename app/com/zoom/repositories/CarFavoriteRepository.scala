package com.zoom.repositories

import com.zoom.models._
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.MySQLProfile.api._
import slick.lifted.TableQuery

@Singleton
class CarFavoriteRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends LinkTableBaseRepository[CarFavoritesTable, CarFavorite](dbConfigProvider,TableQuery[CarFavoritesTable])
{

  def create(carFavorite: CarFavorite)(implicit accountId:String) = {
    db.run(saveQuery(carFavorite).asTry)
  }

  def delete(carId: String, userId:String)(implicit accountId:String) = {
    db.run(query.filter(x => x.carId === carId && x.userId === userId && x.accountId === accountId).delete.asTry)
  }

  def all()(implicit accountId:String) = {
    db.run(query.result)
  }

  def getByCarIdAndUserId(carId:String, userId:String)(implicit accountId:String) = {
    db.run(query.filter(x => x.carId === carId && x.userId === userId && x.accountId === accountId).result.headOption)
  }

  def getByUserId(userId:String) = {
    db.run(query.filter(x => x.userId === userId).to[List].result)
  }

}






