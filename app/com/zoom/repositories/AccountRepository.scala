package com.zoom.repositories


import com.zoom.models.{Account, AccountTable}
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.basic.DatabaseConfig
import slick.jdbc.{JdbcBackend, JdbcProfile}

import scala.concurrent.Future
import scala.util.Try
import slick.jdbc.MySQLProfile.api._
import slick.lifted.TableQuery

@Singleton
class AccountRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) {

  val query: TableQuery[AccountTable] = TableQuery[AccountTable]
  val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]
  val db: JdbcBackend#DatabaseDef = dbConfig.db

  def findById(id: String): Future[Option[Account]] = db.run(query.filter(_.id === id).result.headOption)

  def findByEmail(email: String): Future[Option[Account]] = {
    println(email)
    db.run(query.filter(x => x.email === email).result.headOption)
  }

  def create(account: Account): Future[Try[Unit]] = {
    db.run(
      DBIO.seq(
        query += account
      ).transactionally.asTry
    )
  }
}
