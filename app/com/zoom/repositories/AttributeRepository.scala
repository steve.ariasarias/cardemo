package com.zoom.repositories

import com.zoom.models._
import com.zoom.utils.Constants._
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.MySQLProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.Future
import scala.util.Try

@Singleton
class AttributeRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                                    val entityTypesRepository: AttributeEntityTypesRepository,
                                    attributeValueRepository:AttributeValueRepository, attributeEntityTypesRepository:AttributeEntityTypesRepository
                                   ) extends AccountBaseRepository[AttributesTable, Attribute](dbConfigProvider,TableQuery[AttributesTable])
{
  val attributeEntityTypesTable: TableQuery[AttributeEntityTypesTable] = TableQuery[AttributeEntityTypesTable]
  val attributeValuesTable:TableQuery[AttributeValuesTable] = TableQuery[AttributeValuesTable]

  def loadByCode(code:String)(implicit accountId:String) = {
    db.run(query.filter(x=> x.code === code && x.accountId === accountId).result.headOption)
  }

  def getAll = db.run(query.result)

  def loadByEntityType(entityType:String): Future[List[Attribute]] = {
    entityTypesRepository.loadByEntityType(entityType)
  }

  def search(search:Option[String], entityType:Option[String])(implicit accountId:String) = {

    val querySearch = query.filter { item =>
      List(
        search.map(item.name like "%" + _ + "%"),
        search.map(item.displayValue like "%" + _ + "%")
      ).collect({ case Some(criteria) => criteria }).reduceLeftOption(_ || _).getOrElse(true: Rep[Boolean])
    }.map(_.id)

    if(entityType.isEmpty)
      db.run(query.filter(x => x.status =!= STATUS_DELETE && (x.id in querySearch)).to[List].result)
    else {
      val queryEntityType = attributeEntityTypesTable.filter(x => x.entityType === entityType.getOrElse("") && x.accountId === accountId).map(_.attributeId)
      db.run(query.filter(x => (x.id in querySearch) && (x.id in queryEntityType) && x.status =!= STATUS_DELETE).to[List].result)
    }
  }

  def create(attribute: Attribute, attributeValues:List[AttributeValue],attributeEntityTypesList:List[AttributeEntityTypes])(implicit accountId:String): Future[Try[Unit]] = {
    db.run(
      DBIO.seq(
        saveQuery(attribute),
        attributeValueRepository.saveListQuery(attributeValues),
        attributeEntityTypesRepository.saveListQuery(attributeEntityTypesList)
      ).transactionally.asTry
    )
  }

  def createList(attributes: List[Attribute], attributeValues:List[AttributeValue],attributeEntityTypesList:List[AttributeEntityTypes])(implicit accountId:String): DBIOAction[Try[Unit], NoStream, Effect.Write with Effect.Transactional] = {
    DBIO.seq(
        saveListQuery(attributes),
        attributeValueRepository.saveListQuery(attributeValues),
        attributeEntityTypesRepository.saveListQuery(attributeEntityTypesList)
    ).transactionally.asTry
  }

  def update(attribute: Attribute, attributeValues:List[AttributeValue],attributeEntityTypesList:List[AttributeEntityTypes])(implicit accountId:String): Future[Try[Unit]] = {
    db.run(
      DBIO.seq(
        updateByIdQuery(attribute.id.get,attribute),
        //--attributeValuesTable.filter(t => t.accountId === accountId && t.attributeId === attribute.id.get).delete,
        attributeEntityTypesTable.filter(t => t.accountId === accountId && t.attributeId === attribute.id.get).delete,
        attributeValueRepository.saveListQuery(attributeValues),
        attributeEntityTypesRepository.saveListQuery(attributeEntityTypesList)
      ).transactionally.asTry
    )
  }

  def updateStatus(attributeId: String, newStatus:String)(implicit accountId:String) = {
    db.run(query.filter(x => x.id === attributeId && x.accountId === accountId).map(x => x.status).update(newStatus))
  }

  def delete(attributeId: String)(implicit accountId:String): Future[Try[Unit]] = {
    db.run(
      DBIO.seq(
        attributeEntityTypesTable.filter(t => t.accountId === accountId && t.attributeId === attributeId).delete,
        attributeValuesTable.filter(t => t.accountId === accountId && t.attributeId === attributeId).delete,
        deleteByIdQuery(attributeId)
      ).transactionally.asTry
    )
  }
}