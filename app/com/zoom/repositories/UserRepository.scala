package com.zoom.repositories

import javax.inject.Inject
import com.zoom.models._
import org.mindrot.jbcrypt.BCrypt
import play.api.db.slick.DatabaseConfigProvider
import slick.lifted.TableQuery
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future
import scala.util.Try

class UserRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider
                              ) extends AccountBaseRepository[UserTable, User](dbConfigProvider, TableQuery[UserTable]) {

  def create(user: User): Future[Try[Unit]] = {
    db.run(DBIO.seq(query += user).transactionally.asTry)
  }

  def findByEmail(email: String): Future[Option[User]] = {
    db.run(query.filter(x => x.email === email).result.headOption)
  }

  def findById(idUser: String): Future[Option[User]] = {
    db.run(query.filter(x => x.id === idUser).result.headOption)
  }

  def update(user: User )(implicit accountId:String): Future[Try[Int]] = {
    db.run(query.filter(_.id === user.id.get).update(user).asTry)
  }


  def changePassword(email:String, newPassword: String) = {
    db.run(query.filter(x => x.email === email).map(_.password).update(BCrypt.hashpw(newPassword, BCrypt.gensalt())).asTry)
  }
}

