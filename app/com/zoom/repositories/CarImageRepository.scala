package com.zoom.repositories

import com.zoom.models.{CarImage, CarImageTable}
import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.MySQLProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.Future

class CarImageRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends AccountBaseRepository[CarImageTable, CarImage](dbConfigProvider,TableQuery[CarImageTable])
{


  def updateCarImageBatch(carImages:List[CarImage])(implicit accountId:String): Future[Unit] = {
    val updateCarImages = carImages.map(carImage =>{
      query.filter(x=> x.id === carImage.id && x.accountId === accountId).update(carImage)
    })
    val sequenceUpdate = DBIO.sequence(updateCarImages)
    db.run(DBIO.seq(
      sequenceUpdate
    ).transactionally)
  }

  def findByCarId (carId:String)(implicit accountId:String) :Future[List[CarImage]] = db.run(query.filter(x => x.carId === carId && x.accountId === accountId).to[List].result)

  def delete (id:String ,carId:String)(implicit accountId:String) : Future[Int] = db.run(query.filter(x => x.id === id && x.carId === carId && x.accountId === accountId).delete)

  def deleteCarImageBatch (carImageIds:List[String])(implicit accountId:String): Future[Unit] = {
    val deleteCarImages = carImageIds.map(carImage =>{
      query.filter(x => x.id === carImage && x.accountId === accountId).delete
    })
    val sequenceDelete = DBIO.sequence(deleteCarImages)
    db.run(DBIO.seq(
      sequenceDelete
    ).transactionally)
  }

  override def getAll(implicit accountId: String): Future[Seq[CarImage]] = {
    val action = getAllQuery.sortBy(x => x.imageOrder.asc).result
    db.run(action)
  }

}