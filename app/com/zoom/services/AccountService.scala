package com.zoom.services

import javax.inject.{Inject, Singleton}

import scala.async.Async.{async, await}
import play.api.Logger
import com.zoom.models.Account
import com.zoom.repositories.AccountRepository
import com.zoom.utils.UniqueId
import com.zoom.utils.Util.handleError
import org.mindrot.jbcrypt.BCrypt

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class AccountService @Inject()(accountRepository: AccountRepository){


  val logger = Logger(this.getClass)

  def findById(id: String) = accountRepository.findById(id)

  def findByEmail(email: String) = accountRepository.findByEmail(email)

  def save(account: Account): Future[(String, String, String)] = {
    async{
      val id = UniqueId.generateId
      val accountToBeInserted = account.copy(password = BCrypt.hashpw(account.password,BCrypt.gensalt()))
      val insertAccount = await {accountRepository.create(accountToBeInserted.copy(id=Some(id)))}
      val (responseCode, responseMessage) = handleError(insertAccount)
      (responseCode, responseMessage, id)
    }
  }

}
