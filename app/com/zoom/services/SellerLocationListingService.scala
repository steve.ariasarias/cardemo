package com.zoom.services

import com.zoom.models.SellerLocationListing
import com.zoom.repositories.{SellerLocationListingRepository}
import com.zoom.utils.UniqueId
import com.zoom.utils.Util.handleError
import javax.inject.{Inject, Singleton}
import play.api.Logger

import scala.async.Async.{async, await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class SellerLocationListingService @Inject()(sellerLocationListingRepository: SellerLocationListingRepository){

  val logger = Logger(this.getClass)

  def all()(implicit accountId:String) = sellerLocationListingRepository.getAll

  def save(sellerLocationListing: SellerLocationListing)(implicit accountId:String): Future[(String, String, String)] = {
    async{
      if(sellerLocationListing.id.isDefined){
        val update = await(sellerLocationListingRepository.updateById(sellerLocationListing.id.get,sellerLocationListing))
        val (responseCode,responseMessage) = handleError(update)
        (responseCode, responseMessage, sellerLocationListing.id.get)
      }else{
        val id = UniqueId.generateId
        val saveSearchToBeInserted = sellerLocationListing.copy(id = Some(id))
        val insert = await(sellerLocationListingRepository.save(saveSearchToBeInserted))
        val (responseCode,responseMessage) = handleError(insert)
        (responseCode, responseMessage, id)
      }
    }
  }
}