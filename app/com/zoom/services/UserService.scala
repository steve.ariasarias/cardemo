package com.zoom.services

import javax.inject.{Inject, Singleton}

import scala.async.Async.{async, await}
import play.api.Logger
import com.zoom.models.User
import com.zoom.repositories.UserRepository
import com.zoom.utils.Constants.RESPONSE_CODE_ERROR
import com.zoom.utils.UniqueId
import com.zoom.utils.Util.handleError
import org.mindrot.jbcrypt.BCrypt

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class UserService @Inject()(userRepository: UserRepository){


  val logger = Logger(this.getClass)

  def save(user: User): Future[(String, String, String)] = {
    async{
        val userId = UniqueId.generateId
        val userToBeInserted = user.copy(password = BCrypt.hashpw(user.password,BCrypt.gensalt()))
        val insertUser = await {userRepository.create(userToBeInserted.copy(id=Some(userId)))}
        val (responseCode, responseMessage) = handleError(insertUser)
        (responseCode, responseMessage, userId)
    }
  }

  def update(user: User): Future[(String, String, String)] = {
    async{
        implicit val accountId:String = user.accountId
        val updateUser = await {userRepository.update(user)}
        val (responseCode,responseMessage) = handleError(updateUser)
        (responseCode, responseMessage, user.id.get)
    }
  }


  def findByEmail(email: String)(implicit accountId: String) = userRepository.findByEmail(email)

  def findById(id: String)(implicit accountId: String) = userRepository.findById(id)


  def authenticate(email: String, password: String): Future[Option[User]] = {
    (for {
      Some(user) <- userRepository.findByEmail(email) if BCrypt.checkpw(password, user.password)
    } yield Some(user)
      ) recover {
      case e: Throwable =>
        val errorMsg = e.getMessage
        logger.error(s"Error authenticate $errorMsg",e)
        None
    }
  }

  def changePassword(email:String, newPassword: String) = {
    async{
      val res = await (userRepository.changePassword(email, newPassword))
      val (responseCode, responseMessage) = handleError(res)
      if(res.isSuccess && res.get > 0){
        (responseCode, responseMessage)
      }else{
        (RESPONSE_CODE_ERROR,"Wrong password provided")
      }
    }
  }

}
