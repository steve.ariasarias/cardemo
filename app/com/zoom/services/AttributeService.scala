package com.zoom.services

import java.sql.Timestamp
import java.util.Calendar
import com.zoom.models.{Attribute, AttributeEntityTypes, AttributeValue}
import com.zoom.utils.Constants._
import com.zoom.utils.Util._
import com.zoom.repositories.AttributeRepository
import com.zoom.utils.RequestUtils.OptionsRequest
import com.zoom.utils.ResponseUtils.{ResponseAttributes, ResponseAttributesEntity}
import com.zoom.utils.UniqueId
import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext.Implicits._
import scala.async.Async.{async, await}
import scala.concurrent.Future

@Singleton
class AttributeService @Inject()( attributeEntityTypesService: AttributeEntityTypesService,
                                  attributeRepository: AttributeRepository ) {

  
  def getSettingAttribute(attribute: Attribute, entityTypes:List[String], options:List[OptionsRequest])={

    var attributeValuesList= List[AttributeValue]()
    var attributeEntityTypesList = List[AttributeEntityTypes]()
    if (options.nonEmpty){
      options.foreach(option =>{
        val id = UniqueId.generateId
        val attributeValues =AttributeValue(Some(id),attribute.accountId,attribute.id.get,option.value,option.value,option.value,option.order) //option change
        attributeValuesList=attributeValues::attributeValuesList

      })
    }
    if (entityTypes.nonEmpty){
      entityTypes.foreach(entityType=>{
        val id = UniqueId.generateId
        val attributeEntityTypes = AttributeEntityTypes(Some(id),attribute.accountId,attribute.id.get,entityType)
        attributeEntityTypesList=attributeEntityTypes::attributeEntityTypesList
      })
    }

    (attributeValuesList,attributeEntityTypesList)
  }

  def save(attribute: Attribute, entityTypes:List[String], options:List[OptionsRequest])(implicit accountId:String) ={
    val (attributeValuesList,attributeEntityTypesList) = getSettingAttribute(attribute,entityTypes,options)
    attributeRepository.create(attribute,attributeValuesList,attributeEntityTypesList)
  }


  def update(attribute: Attribute, entityTypes:List[String], options:List[OptionsRequest])(implicit accountId:String) ={
    val now = Calendar.getInstance()
    val modifiedDate = new Timestamp(now.getTimeInMillis)
    val attributeToUpdate= attribute.copy(modifiedDate= Some(modifiedDate))
    val (attributeValuesList,attributeEntityTypesList) = getSettingAttribute(attribute,entityTypes,options)
    attributeRepository.update(attributeToUpdate,attributeValuesList,attributeEntityTypesList)
  }


  def all: Future[Seq[Attribute]] = attributeRepository.getAll

  def findById(id:String)(implicit accountId:String): Future[Option[Attribute]] = attributeRepository.getById(id)

  def loadByCode(code:String)(implicit accountId:String): Future[Option[Attribute]] = attributeRepository.loadByCode(code)

  def loadByEntityType(entityType:String): Future[List[Attribute]] = attributeRepository.loadByEntityType(entityType)

  def getResponseAttributesEntity(attributes:List[Attribute], attributeValues:Seq[AttributeValue]) = {
    val responseAttributes: List[ResponseAttributesEntity] = attributes.map(atr => {
      if(isInputTypeList(atr.typeAttribute.getOrElse(""))){
        val attributeValuesByAttributeId = attributeValues.filter(_.attributeId == atr.id.get)
        ResponseAttributesEntity(atr,Some(attributeValuesByAttributeId))
      }else{
        ResponseAttributesEntity(atr,None)
      }
    })
    responseAttributes
  }

  def getAttributesFieldsEntity(entityType:String)(implicit accountId:String) = {
    var attributes = List[Attribute]()
    entityType match {
      case ENTITY_TYPE_CUSTOMER =>{
        attributes = Attribute(Some("id"),accountId,"id","id","id",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("imageUrl"),accountId,"imageUrl","imageUrl","Image Url",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("firstName"),accountId,"firstName","firstName","First Name",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("lastName"),accountId,"lastName","lastName","Last Name",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("workNumber"),accountId,"workNumber","workNumber","Work Number",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("comments"),accountId,"comments","comments","Comments",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("active"),accountId,"active","active","Active",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("dateStatement"),accountId,"dateStatement","dateStatement","Date Statement",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("dobmdy"),accountId,"dobmdy","dobmdy","Dobmdy",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("password"),accountId,"password","password","Password",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("confirmPassword"),accountId,"confirmPassword","confirmPassword","Confirm Password",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
      }
      case ENTITY_TYPE_CAR =>{
        attributes = Attribute(Some("id"),accountId,"id","id","id",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("locationId"),accountId,"locationId","locationId","Location",Some(INPUT_SELECT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("year"),accountId,"year","year","Year",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("makeId"),accountId,"makeId","makeId","Make",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("modelId"),accountId,"modelId","modelId","Model",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("vin"),accountId,"vin","vin","VIN",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("purchaseDate"),accountId,"purchaseDate","purchaseDate","Purchase Date",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("retailPrice"),accountId,"retailPrice","retailPrice","Retail Price",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("suggestedPrice"),accountId,"suggestedPrice","suggestedPrice","Suggested Price",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("commission"),accountId,"commission","commission","Commission",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("mainImage"),accountId,"mainImage","mainImage","Main Image",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("soldDate"),accountId,"soldDate","soldDate","Sold Date",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("purchasedPrice"),accountId,"purchasedPrice","purchasedPrice","Purchased Price",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("description"),accountId,"description","description","Description",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("mileage"),accountId,"mileage","mileage","Mileage",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("order"),accountId,"order","order","Order",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
      }
      case ENTITY_TYPE_USER =>{
        attributes = Attribute(Some("id"),accountId,"id","id","id",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("email"),accountId,"email","email","Email",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("firstName"),accountId,"firstName","firstName","First Name",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("lastName"),accountId,"lastName","lastName","Last Name",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("image"),accountId,"image","image","Image",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("roles"),accountId,"roles","roles","Roles",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("password"),accountId,"password","password","Password",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes
        attributes = Attribute(Some("confirmPassword"),accountId,"confirmPassword","confirmPassword","Confirm Password",Some(INPUT_TEXT),Some(STATUS_AVAILABLE)) :: attributes

      }
    }
    attributes
  }

  def search(search: Option[String], entityType: Option[String])(implicit accountId:String): Future[List[ResponseAttributes]] = {
    async{
      var searchFilter: Option[String] = None
      var entityTypeFilter: Option[String] = None
      if(search.isDefined && !search.get.equals("") ){
        searchFilter = search
      }
      if(entityType.isDefined && !entityType.get.equals("")){
        entityTypeFilter = entityType
      }
      val attributes = await(attributeRepository.search(searchFilter, entityTypeFilter))
      val attributeEntityTypes = await(attributeEntityTypesService.loadByAttributeIds(attributes.map(_.id.get)))
      var responseAttributes = List[ResponseAttributes]()
      attributes.foreach(at => {
        val entityTypes = attributeEntityTypes.filter(_.attributeId == at.id.get).map(_.entityType)
        responseAttributes = ResponseAttributes(at,entityTypes) :: responseAttributes
      })
      responseAttributes
    }
  }

  def delete(id:String)(implicit accountId:String)=attributeRepository.delete(id)

  def updateStatus(attributeId:String, newStatus:String)(implicit accountId:String): Future[Int] = {
    attributeRepository.updateStatus(attributeId,newStatus)
  }
}
