package com.zoom.services

import com.sksamuel.elastic4s.http.ElasticDsl.termsQuery
import com.sksamuel.elastic4s.{ElasticsearchClientUri, RefreshPolicy}
import com.sksamuel.elastic4s.http.{ElasticClient, ElasticProperties}
import com.sksamuel.elastic4s.searches.queries.matches.MatchAllQuery
import com.sksamuel.elastic4s.searches.queries.{BoolQuery, QueryStringQuery}
import com.sksamuel.elastic4s.searches.queries.matches.MultiMatchQueryBuilderType.CROSS_FIELDS
import com.sksamuel.elastic4s.searches.queries.term.TermsQuery
import com.zoom.utils.RequestUtils.FilterCarPublic
import org.elasticsearch.index.query.{BoolQueryBuilder, QueryBuilders}
import org.elasticsearch.script.Script
import play.api.Logger
//import org.elasticsearch.action.support.WriteRequest.RefreshPolicy
import javax.inject.{Inject, Singleton}
import com.zoom.utils.Constants._
import scala.util.Try
import com.sksamuel.elastic4s.http.ElasticDsl

@Singleton
class ElasticSearchService @Inject()(){

  val logger = Logger(this.getClass)

  // you must import the DSL to use the syntax helpers
  import com.sksamuel.elastic4s.http.ElasticDsl._

  implicit val client = ElasticClient(ElasticProperties(ElasticSearchProperties.url))
  //implicit val client = HttpClient(ElasticsearchClientUri("abhisheks-mini", 9200))
  /*client.execute {
    indexInto("zoom" / "cars").fields("id" -> "Testing",
      "make_id" -> "make-testing",
      "model_id" -> "model-testing",
      "year" -> "1900".toLong,
      "vin" -> "vin-testing",
      "mileage" -> "0".toLong,
      "price" -> "0".toLong,
      "createdDate" -> "createdDate-testing",
      "renewedDate" -> "renewedDate-testing",
      "expirationDate" -> "expirationDate-testing",
      "description" -> "description-testing",
      "interiorColor" -> "interiorColor-testing",
      "exteriorColor" -> "exteriorColor-testing",
      "bodyType" -> "bodyType-testing",
      "title" -> "title-testing",
      "fuel" -> "fuel-testing",
      "cylinders" -> "cylinders-testing",
      "drive" -> "drive-testing",
      "transmission" -> "transmission-testing",
      "photos" -> "photos-testing",
      "imagesUrls" -> "imagesUrls-testing",
      "duration" -> "duration-testing",
      "stars" -> "stars-testing",
      "view" -> "view-testing",
      "mainImageUrl" -> "mainImageUrl-testing"
    )
    //bulk(

      //indexInto("zoom" / "cars").fields("country" -> "Namibia", "capital" -> "Windhoek")
    //).refresh(RefreshPolicy.WaitFor)
  }.await

  val response: Response[SearchResponse] = client.execute {
    search("zoom").matchQuery("make_id", "make-testing")
  }.await

  // prints out the original json
  val responseSearch: SearchResponse = response.result
  //println("####" + responseSearch.isEmpty)
  if(!responseSearch.isEmpty){
    println(response.result.hits.hits.head.sourceAsString)
  }*/


  def saveDocument(data:Map[String,Any]) ={
    val result = Try(client.execute {
      indexInto("zoom" / "cars").fields(data).refresh(RefreshPolicy.IMMEDIATE)
    }.await)
    if(result.isFailure){
      logger.error(s"Error save car in ElasticSearch ${result.get.error.reason}")
      logger.error(s"error ${result.get.error.`type`}")
      logger.error(s"error ${result.get.error.causedBy}")
    }else{
      logger.info(s"id ${result.get.result.id}")
      logger.info(s"Car saved successfully in ElasticSearch -> ${result.get.status}")
    }
    //client.close()
  }

  def searchDocument(filterCar:FilterCarPublic) = {
    /*var filtersCar = List[TermsQuery[String]]()
    if(filterCar.makes.getOrElse(List()).nonEmpty){
      val tQuery = termsQuery("makeId",filterCar.makes.get)
      filtersCar = tQuery :: filtersCar
    }*/
    var bqForStatus = BoolQuery()
    if(filterCar.status.getOrElse(List()).nonEmpty){
      bqForStatus = boolQuery().filter(termsQuery("status",filterCar.status.get.map(_.toLowerCase)))
    }
    var bqForMake = BoolQuery()
    if(filterCar.makes.getOrElse(List()).nonEmpty){
      bqForMake = boolQuery().filter(termsQuery("makeId",filterCar.makes.get.map(_.toLowerCase)))
    }
    var bqForColour = BoolQuery()
    if(filterCar.exteriorColour.getOrElse(List()).nonEmpty){
      bqForColour = boolQuery().filter(termsQuery("exteriorColor",filterCar.exteriorColour.get.map(_.toLowerCase)))
    }
    var bqForModel = BoolQuery()
    if(filterCar.models.getOrElse(List()).nonEmpty){
      bqForModel = bqForModel.should(termsQuery("modelId",filterCar.models.get.map(_.toLowerCase)))
    }
    var bqForBody = BoolQuery()
    if(filterCar.body.getOrElse(List()).nonEmpty){
      bqForBody = bqForBody.should(termsQuery("body",filterCar.body.get.map(_.toLowerCase)))
    }
    var bqForKeyword = BoolQuery()
    if(!filterCar.keyword.getOrElse("").equalsIgnoreCase("")){
      bqForKeyword = bqForKeyword.should(queryStringQuery(filterCar.keyword.get.toLowerCase)
        .field("vin")
        .field("makeName")
        .field("modelName")
        .defaultOperator("OR")
        .matchType(CROSS_FIELDS))
    }
    client.execute {
      /*multi(
        search("zoom").query({termQuery("vin", "georgia")}),
        search("zoom").query({termQuery("state", "georgia")}).from(offset).size(pageSize)
      )*/
      search("zoom")
        .query(
          //matchAllQuery()
          boolQuery().filter(
            //termsQuery("makeId",Set[String](""),"0"),
            //termsQuery("modelId",filter.models.getOrElse(List())),
            //termsQuery("body",filter.body.getOrElse(List())),
            bqForKeyword,
            rangeQuery("year").gte(filterCar.yearFrom.getOrElse(YEAR_FROM)),
            rangeQuery("year").lte(filterCar.yearTo.getOrElse(YEAR_TO)),
            rangeQuery("price").gte(filterCar.priceFrom.getOrElse(BigDecimal(PRICE_FROM)).toString()),
            rangeQuery("price").lte(filterCar.priceTo.getOrElse(BigDecimal(PRICE_TO)).toString()),
            rangeQuery("mileage").gte(filterCar.mileageFrom.getOrElse(BigDecimal(MILEAGE_FROM)).toString()),
            rangeQuery("mileage").lte(filterCar.mileageTo.getOrElse(BigDecimal(MILEAGE_TO)).toString()),
            bqForMake,
            bqForModel,
            bqForBody,
            bqForColour,
            bqForStatus
          )
        ).from(filterCar.offset).size(filterCar.size)
    }.await
  }

  def updateDocument(id:String,data:Map[String,Any]) = {
    client.execute {
      update(id).in("zoom" / "cars").doc(data).refresh(RefreshPolicy.Immediate)
    }.await
  }

  def searchById(id:String) = {
    client.execute {
      search("zoom")
        .query({matchQuery("id", id)})
    }.await
  }

  def getByIndexId(indexById:String)= {
    client.execute {
      get(indexById).from("zoom" / "cars")
    }.await
  }

  def searchByTermQuery(key:String, value: String) = {
    client.execute{
      search("zoom") query matchQuery(key, value)
    }.await
  }

}


object ElasticSearchProperties {
  val url = scala.util.Properties.envOrElse("ELASTICSEARCH_URL","http://localhost:9200")
}