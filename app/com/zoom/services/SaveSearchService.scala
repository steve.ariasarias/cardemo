package com.zoom.services

import com.zoom.models.{Account, SaveSearch}
import com.zoom.repositories.SaveSearchRepository
import com.zoom.utils.UniqueId
import com.zoom.utils.Util.handleError
import javax.inject.{Inject, Singleton}
import play.api.Logger
import scala.async.Async.{async, await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class SaveSearchService @Inject()(saveSearchRepository: SaveSearchRepository){


  val logger = Logger(this.getClass)

  def all()(implicit accountId:String) = saveSearchRepository.getAll

  def save(saveSearch: SaveSearch)(implicit accountId:String): Future[(String, String, String)] = {
    async{
      if(saveSearch.id.isDefined){
        val update = await(saveSearchRepository.updateById(saveSearch.id.get,saveSearch))
        val (responseCode,responseMessage) = handleError(update)
        (responseCode, responseMessage, saveSearch.id.get)
      }else{
        val id = UniqueId.generateId
        val saveSearchToBeInserted = saveSearch.copy(id = Some(id))
        val insert = await(saveSearchRepository.save(saveSearchToBeInserted))
        val (responseCode,responseMessage) = handleError(insert)
        (responseCode, responseMessage, id)
      }
    }
  }

}
