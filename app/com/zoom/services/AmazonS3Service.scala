package com.zoom.services

import java.io._
import javax.inject.Singleton

import com.amazonaws.auth.{AWSCredentials, BasicAWSCredentials}
import com.amazonaws.services.s3._
import com.amazonaws.services.s3.model._
import com.amazonaws.{AmazonClientException, AmazonServiceException}
import play.api.Logger

import scala.util.Try

/**
  * Created by johnny on 15/08/16.
  */
@Singleton
class AmazonS3Service {

  val logger = Logger(this.getClass)
  val credentials: AWSCredentials = new BasicAWSCredentials(AmazonS3Service.keyId,AmazonS3Service.secretId)
  val clientS3: AmazonS3Client  = new AmazonS3Client(credentials)

  def uploadFile(path: String, file: File, fileName:String): Try[String] ={
    val keyName = s"$path/${fileName}"
    val putObjectRequest = new PutObjectRequest(AmazonS3Service.bucketName,keyName,file)
    putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead)
    val t: Try[String] =Try(clientS3.putObject(putObjectRequest)) map (_ => fileName)
    if (t.isFailure) t.failed.foreach(_ => log(_)) //only for loggin purposes
    val url: Try[String] = Try(clientS3.getResourceUrl(AmazonS3Service.bucketName, keyName))
    if (url.isFailure) url.failed.foreach(_ => log(_)) //only for loggin purposes
    url
  }

  def uploadFilePublic(path: String, file: File, fileName:String): Try[String] ={
    val keyName = s"$path/${fileName}"
    val putObjectRequest = new PutObjectRequest(AmazonS3Service.bucketName,keyName,file)
    putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead)
    val t: Try[String] =Try(clientS3.putObject(putObjectRequest)) map (_ => fileName)
    if (t.isFailure) t.failed.foreach(_ => log(_)) //only for loggin purposes
    val url: Try[String] = Try(clientS3.getResourceUrl(AmazonS3Service.bucketName, keyName))
    if (url.isFailure) url.failed.foreach(_ => log(_)) //only for loggin purposes
    url
  }

  def removeFile(keyName:String): Try[Unit] = {
    val t: Try[Unit] = Try(clientS3.deleteObject(AmazonS3Service.bucketName,keyName)) //map ( x => throw new AmazonClientException("what"))
    if (t.isFailure) t.failed.foreach(_ => log(_)) //only for loggin purposes
    t
  }

  def downloadFileStream(path: String): Try[InputStream] = {
    val request = new GetObjectRequest(AmazonS3Service.bucketName, path.substring(1))
    val t: Try[InputStream] = Try(clientS3.getObject(request)) map (_.getObjectContent)
    if (t.isFailure) t.failed.foreach(_ => log(_)) //only for loggin purposes
    t
  }

  private def log(e:Throwable) = e match {
    case e: AmazonServiceException => {
      logger.error(s"error Message -> ${e.getMessage}")
      logger.error(s"HTTP status code -> ${e.getStatusCode}")
      logger.error(s"AWS error code -> ${e.getErrorCode}")
      logger.error(s"Error Type -> ${e.getErrorType}")
      logger.error(s"Request-ID -> ${e.getRequestId}")
    }
    case e: AmazonClientException => {
      logger.error(s"Error Message -> ${e.getMessage}")
    }
  }
}

object AmazonS3Service {
  val bucketName = "dealer-pro"
  val keyId = "AKIAIQKLKNJP6P3WYI7A"
  val secretId = "hf9QITfF6V3lvCtv2nYMFqmzlaVSQnonPpFPRWZl"
}
