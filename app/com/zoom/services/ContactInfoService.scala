package com.zoom.services

import com.zoom.models.ContactInfo
import com.zoom.repositories.ContactInfoRepository
import com.zoom.utils.UniqueId
import com.zoom.utils.Util.handleError
import javax.inject.{Inject, Singleton}
import play.api.Logger
import scala.async.Async.{async, await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class ContactInfoService @Inject()(contactInfoRepository: ContactInfoRepository){


  val logger = Logger(this.getClass)

  def all()(implicit accountId:String) = contactInfoRepository.getAll

  def save(contactInfo: ContactInfo)(implicit accountId:String): Future[(String, String, String)] = {
    async{
      if(contactInfo.id.isDefined){
        val update = await(contactInfoRepository.updateById(contactInfo.id.get,contactInfo))
        val (responseCode,responseMessage) = handleError(update)
        (responseCode, responseMessage, contactInfo.id.get)
      }else{
        val id = UniqueId.generateId
        val saveSearchToBeInserted = contactInfo.copy(id = Some(id))
        val insert = await(contactInfoRepository.save(saveSearchToBeInserted))
        val (responseCode,responseMessage) = handleError(insert)
        (responseCode, responseMessage, id)
      }
    }
  }

}
