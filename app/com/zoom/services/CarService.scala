package com.zoom.services

import com.zoom.models._
import com.zoom.repositories._
import com.zoom.utils.UniqueId
import com.zoom.utils.Util._
import javax.inject.{Inject, Singleton}
import play.api.Logger

import scala.concurrent.ExecutionContext.Implicits._
import scala.async.Async.{async, await}
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.util.Try

@Singleton
class CarService @Inject()(carRepository: CarRepository,
                           carFavoriteRepository: CarFavoriteRepository,
                           carImageRepository: CarImageRepository,
                           makeRepository: MakeRepository,
                           modelRepository: ModelRepository){

  val logger = Logger(this.getClass)

  def save(car: Car, carAttributeValueList: List[ObjectAttributeValue])(implicit accountId:String): Future[(String, String, String)] = {
    async {
      if(car.id.isDefined){
        val attributes = carAttributeValueList.map(x =>{x.copy(objectId = car.id.get)})
        val updateCar = await {carRepository.update(car, attributes)}
        val (responseCode,responseMessage) = handleError(updateCar)
        (responseCode, responseMessage, car.id.get)
      } else {
        val id = UniqueId.generateId
        val order = await {carRepository.getLastOrder()}
        val carToBeInserted = car.copy(id = Some(id), order = order + 1)
        val attributes = carAttributeValueList.map(x =>{x.copy(objectId = id)})
        val insertCar = await {carRepository.create(carToBeInserted, attributes)}
        val (responseCode,responseMessage) = handleError(insertCar)
        (responseCode, responseMessage, id)
      }
    }
  }

  def saveList(cars:List[Car], carAttributeValues: List[ObjectAttributeValue],carsUpdate:List[Car], carAttributeValuesUpdate: List[ObjectAttributeValue])
              (implicit accountId:String): Future[Try[Unit]] = {
    carRepository.saveList(cars,carAttributeValues,carsUpdate,carAttributeValuesUpdate)
  }

  def insertList(cars:List[Car], carAttributeValues: List[ObjectAttributeValue]) = {
    carRepository.insertList(cars,carAttributeValues)
  }

  def all(implicit accountId:String): Future[Seq[Car]] = carRepository.getAll

  def findById(id:String)(implicit accountId:String): Future[Option[Car]] = carRepository.getById(id)

  def findOnlyById(id:String): Future[Option[Car]] = carRepository.findById(id)

  def findByVin(vin:String)(implicit customerId:String) = carRepository.findByVin(vin)

  def search(keyword:Option[String],from:Option[Int],size:Option[Int],active:Option[Boolean], accountId:String) = {
    carRepository.search(keyword,from,size,active,accountId)
  }

  def updateMainImage(carId:String, mainImage:String)(implicit accountId:String)  = {
    async {
      val updateMainImage = await {carRepository.updateMainImage(carId,mainImage)}
      val (responseCode,responseMessage) = handleError(updateMainImage)
      (responseCode, responseMessage)
    }
  }

  def updateView(carId:String, view:Int)  = {
    carRepository.updateView(carId,view)
  }

  def inactiveCar(car: Car)(implicit accountId:String) = {
    carRepository.updateCar(car)
  }

  def updateOrderCars(cars:List[Car])(implicit accountId:String) = {
    carRepository.updateOrderCar(cars)
  }

  def searchPublic(keyword:Option[String],makes:Option[List[String]],models:Option[List[String]],
                    yearFrom:Option[Int],yearTo:Option[Int],priceFrom:Option[BigDecimal], priceTo:Option[BigDecimal],
                    mileageFrom:Option[Int], mileageTo:Option[Int],body:Option[List[String]],priceBest:Option[BigDecimal],newEst:Option[Boolean],
                   title:Option[List[String]],exteriorColor:Option[List[String]],interiorColor:Option[List[String]],cylinder:Option[List[String]],
                   trim:Option[String]) = {

    var keywordList :Option[List[String]] = None
    if(keyword.nonEmpty){
      keywordList = Some(keyword.get.split(" ").to[List])
    }
    carRepository.searchPublic(keywordList,makes,models,yearFrom,yearTo,priceFrom,priceTo,mileageFrom,mileageTo,body,priceBest,
      newEst,title,exteriorColor,interiorColor,cylinder,trim)
  }

  def allMakes = {makeRepository.all}

  def allModels = {modelRepository.all}

  def quantityCars(keyword:Option[String],makes:Option[List[String]],models:Option[List[String]],
             yearFrom:Option[Int],yearTo:Option[Int],
             priceFrom:Option[BigDecimal], priceTo:Option[BigDecimal],
             mileageFrom:Option[Int], mileageTo:Option[Int],body:Option[String],status:Option[List[String]],
                   active:Option[Boolean],customerId:String) = {
    var keywordList :Option[List[String]] = None
    if(keyword.nonEmpty){
      keywordList = Some(keyword.get.split(" ").to[List])
    }
    carRepository.quantityCars(keywordList,makes,models,yearFrom,yearTo,priceFrom,priceTo,mileageFrom,mileageTo,body,status,active,customerId)
  }

  def setObjectValue(attributes:List[Attribute],values:Map[String,Any],attributeValuesDB:List[AttributeValue])(implicit accountId:String): List[ObjectAttributeValue] = {
    var objectValue = List[ObjectAttributeValue]()
    attributes.foreach(attribute =>{
      val id = attribute.id.get
      val code = attribute.code
      //val inputType = attribute.typeAttribute
      if(values.exists(_._1.equalsIgnoreCase(code))){
        val attributeValueFound = attributeValuesDB.find(x => x.attributeId == id && (x.code.equalsIgnoreCase(code) || x.displayValue.equalsIgnoreCase(values(code).toString)))
        if(attributeValueFound.isDefined){
          objectValue = ObjectAttributeValue("",id,accountId,attributeValueFound.get.id.get,values(code).toString) :: objectValue
        }
      }
    })
    objectValue
  }

  def setOrRemoveFavorite(carId:String, userId:String)(implicit accountId:String) = {
    async{
      val existFavoriteCar = await(carFavoriteRepository.getByCarIdAndUserId(carId,userId))
      if(existFavoriteCar.isDefined){
        val removeFavorite = await(carFavoriteRepository.delete(carId, userId))
        val (responseCode,responseMessage) = handleError(removeFavorite)
        logger.info(s"car with id $carId was removed to favorite for user $userId")
        (responseCode, responseMessage, carId)
      }else{
        val addFavorite = await(carFavoriteRepository.create(CarFavorite(carId,userId,accountId)))
        val (responseCode,responseMessage) = handleError(addFavorite)
        logger.info(s"car with id $carId was added to favorite for user $userId")
        (responseCode, responseMessage, carId)
      }
    }
  }

  def getFavorites()(implicit accountId:String)= {
    carFavoriteRepository.all
  }

  def getFavoritesByUser(userId: String)= {
    carFavoriteRepository.getByUserId(userId)
  }

  def getImageCars()(implicit accountId:String) = {
    carImageRepository.getAll
  }

}
