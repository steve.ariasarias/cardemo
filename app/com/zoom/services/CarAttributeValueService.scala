package com.zoom.services

import com.zoom.models.ObjectAttributeValue
import com.zoom.repositories.CarAttributeValueRepository
import com.zoom.utils.RequestUtils.InfoAttribute
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class CarAttributeValueService @Inject()(carAttributeValueRepository: CarAttributeValueRepository){

  def create(carAttributeValue: ObjectAttributeValue)(implicit accountId:String)  = {
    carAttributeValueRepository.save(carAttributeValue)
  }

  def update(carAttributeValue: ObjectAttributeValue)(implicit accountId:String)  = {
    carAttributeValueRepository.update(carAttributeValue)
  }

  def findCarAttributeValues(implicit accountId:String): Future[List[ObjectAttributeValue]] = {
    carAttributeValueRepository.findCarAttributeValues
  }

  def findAttributesByCarId(carId:String)(implicit accountId:String) = {
    carAttributeValueRepository.findAttributesByCarId(carId)
  }

  def updateBatch(carAttributeValues :List[ObjectAttributeValue])(implicit accountId:String) = {
    carAttributeValueRepository.updateBatch(carAttributeValues)
  }

  def createList(carAttributeValues: List[ObjectAttributeValue]) = {
    carAttributeValueRepository.createList(carAttributeValues)
  }

  def updateAttributeValueIdAndValue(carId:String, attributeId:String,status:InfoAttribute)(implicit accountId:String) = {
    carAttributeValueRepository.updateAttributeValueIdAndValue(carId, attributeId, status)
  }


}
