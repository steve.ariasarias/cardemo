package com.zoom.services

import com.zoom.models.AttributeValue
import com.zoom.repositories.AttributeValueRepository
import com.zoom.utils.UniqueId
import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import scala.util.Try


@Singleton
class AttributeValueService @Inject()( attributeValueRepository: AttributeValueRepository) {
  def save(attribute: AttributeValue)(implicit accountId:String): Future[Try[Int]] = {
    if(attribute.id.isDefined){
      attributeValueRepository.updateById(attribute.id.get,attribute)
    }else{
      val id = UniqueId.generateId
      val carToBeInserted = attribute.copy(
        id = Some(id)
      )
      attributeValueRepository.save(carToBeInserted)
    }
  }

  def all: Future[Seq[AttributeValue]] = attributeValueRepository.getAll

  def allByAttributeId = all.map(values => values groupBy(_.attributeId))

  def findById(id:String)(implicit accountId:String ): Future[Option[AttributeValue]] = attributeValueRepository.getById(id)

  def loadByAttributeId(attributeId:String)(implicit accountId:String): Future[List[AttributeValue]] = attributeValueRepository.loadByAttributeId(attributeId)

  def findByCode(code:String)(implicit accountId:String ): Future[Option[AttributeValue]] = attributeValueRepository.findByCode(code)
}
