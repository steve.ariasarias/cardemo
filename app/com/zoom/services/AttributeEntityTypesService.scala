package com.zoom.services

import com.zoom.repositories.AttributeEntityTypesRepository
import javax.inject.{Inject, Singleton}

@Singleton
class AttributeEntityTypesService @Inject()(attributeEntityTypesRepository: AttributeEntityTypesRepository ) {


  def loadByAttributeId(attributeId: String)(implicit dealerId: String) = attributeEntityTypesRepository.loadByAttributeId(attributeId)

  def loadByAttributeIds(attributeIds: List[String])(implicit dealerId: String) = attributeEntityTypesRepository.loadByAttributeIds(attributeIds)

}
