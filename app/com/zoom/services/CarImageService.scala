package com.zoom.services

import com.zoom.models.CarImage
import com.zoom.repositories.CarImageRepository
import javax.inject.{Inject, Singleton}


@Singleton
class CarImageService @Inject()(carImageRepository:CarImageRepository){

  def save (carImage: CarImage)(implicit accountId : String) = carImageRepository.save(carImage)

  def updateCarImageBatch(carImages: List[CarImage])(implicit accountId : String) = carImageRepository.updateCarImageBatch(carImages)

  def findByCarId(carId:String) (implicit accountId : String) = carImageRepository.findByCarId(carId)

  def findById(id:String)(implicit accountId : String) = carImageRepository.getById(id)

  def delete(id:String, carId:String)(implicit accountId:String) = carImageRepository.delete(id,carId)

  def deleteBatch(carImageIds:List[String])( implicit accountId:String) = carImageRepository.deleteCarImageBatch(carImageIds)

}
