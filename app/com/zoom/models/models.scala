package com.zoom.models

import java.sql.{Date, Timestamp}

import com.zoom.repositories.{AccountBaseEntity, LinkTableBaseEntity}

case class PlanType(id: Option[String] = None,
                    name: String,
                    listingNumber:Int,
                    userNumber:Int,
                    price:BigDecimal,
                    createDate: Option[Timestamp] = None,
                    modifiedDate: Option[Timestamp] = None
                   )

case class Car(id: Option[String] = None,
               accountId: String,
               year: Int,
               makeId: String,
               modelId: String,
               vin: String,
               suggestedPrice: BigDecimal,
               commission: BigDecimal,
               mainImage: String,
               soldDate: Date,
               //passenger: Int, //attribute
               //addInfo: String, //attribute
               description: String,
               mileage: Int,
               order: Int,
               active: Boolean,
               view:Int,
               createDate: Option[Timestamp] = None,
               modifiedDate: Option[Timestamp] = None
              ) extends AccountBaseEntity

case class Attribute(id: Option[String] = None,
                     accountId: String,
                     code: String,
                     name: String,
                     displayValue: String,
                     typeAttribute: Option[String],
                     status: Option[String],
                     createDate: Option[Timestamp] = None,
                     modifiedDate: Option[Timestamp] = None
                    ) extends AccountBaseEntity

case class AttributeValue(id: Option[String] = None,
                          accountId: String,
                          attributeId: String,
                          code: String,
                          name: String,
                          displayValue: String,
                          order:Int,
                          createDate: Option[Timestamp] = None,
                          modifiedDate: Option[Timestamp] = None
                         ) extends AccountBaseEntity

trait AttributeValueBase{
  val attributeId:String
  val accountId: String
  val attributeValueId:String
  val value:String
  val createDate: Option[Timestamp]
  val modifiedDate: Option[Timestamp]
}

case class ObjectAttributeValue(objectId: String,
                                attributeId: String,
                                accountId: String,
                                attributeValueId: String,
                                value: String,
                                createDate: Option[Timestamp] = None,
                                modifiedDate: Option[Timestamp] = None) extends LinkTableBaseEntity

case class CarAttributeValue(carId: String,
                             attributeId: String,
                             accountId: String,
                             attributeValueId: String,
                             value: String,
                             createDate: Option[Timestamp] = None,
                             modifiedDate: Option[Timestamp] = None
                            ) extends LinkTableBaseEntity with AttributeValueBase


case class CarImage(id: Option[String] = None,
                    accountId: String,
                    url: String,
                    imageOrder: Option[Int],
                    carId: Option[String],
                    createDate: Option[Timestamp] = None,
                    modifiedDate: Option[Timestamp] = None
                   )extends AccountBaseEntity

case class Customer(id: Option[String] = None,
                    accountId: String,
                    imageUrl: Option[String],
                    firstName: String,
                    lastName: String,
                    workNumber: Option[String],
                    comments: Option[String],
                    active: Boolean,
                    dateStatement:Option[Date],
                    dobmdy:Option[Date],
                    createDate: Option[Timestamp] = None,
                    modifiedDate: Option[Timestamp] = None)extends AccountBaseEntity

case class CustomerAttributeValue(customerId: String,
                                  attributeId: String,
                                  attributeValueId: String,
                                  accountId: String,
                                  value: String,
                                  createDate: Option[Timestamp] = None,
                                  modifiedDate: Option[Timestamp] = None)extends LinkTableBaseEntity with AttributeValueBase

case class UserAttributeValue(userId: String,
                              attributeId: String,
                              attributeValueId: String,
                              accountId: String,
                              value: String,
                              createDate: Option[Timestamp] = None,
                              modifiedDate: Option[Timestamp] = None)extends LinkTableBaseEntity with AttributeValueBase

case class CustomerCar(saleId: String,
                       carId: String,
                       customerId: String,
                       accountId: String,
                       createDate: Option [Timestamp] = None,
                       modifiedDate: Option [Timestamp] = None
                       )extends LinkTableBaseEntity

case class User(id: Option[String] = None,
                accountId: String,
                email: String,
                firstName: String,
                lastName: String,
                password: String,
                active: Boolean,
                phone: Option[String] = None,
                address : Option[String] = None,
                image: Option[String] = None,
                createDate: Option[Timestamp] = None,
                modifiedDate: Option[Timestamp] = None
               ) extends AccountBaseEntity

case class VerificationToken(id: Option[String] = None,
                             userId: String,
                             token: String,
                             isValid: Boolean,
                             createDate: Option[Timestamp] = None,
                             modifiedDate: Option[Timestamp] = None
                            )

case class Account(id: Option[String] = None,
                   email:String,
                   firstName:String,
                   lastName:String,
                   password:String,
                   dealerName:Option[String],
                   dealerNumber:Option[String],
                   phone: Option[String],
                   planTypeId: String,
                   active:Boolean,
                   createDate: Option[Timestamp] = None,
                   modifiedDate: Option[Timestamp] = None
                  )

case class Location(id: Option[String] = None,
                    accountId: String,
                    name: String,
                    address: String,
                    address2: String,
                    city: String,
                    state: String,
                    zip: String,
                    country: String,
                    active: Boolean,
                    createDate: Option[Timestamp] = None,
                    modifiedDate: Option[Timestamp] = None
                   ) extends AccountBaseEntity

case class SaveSearch(id:Option[String] = None,
                      accountId: String,
                      name: String,
                      filters: String,
                      coincidence: Int,
                      isAlertActive: Boolean,
                      createDate: Option[Timestamp] = None,
                      modifiedDate: Option[Timestamp] = None
                      ) extends AccountBaseEntity

case class ChangPasswordForm(password: String,
                             confirmPassword: String
                            )

case class Role(id: Option[String] = None,
                name: String,
                description: String,
                createDate: Option[Timestamp] = None,
                modifiedDate: Option[Timestamp] = None
               )

case class UserRole(userId: String,
                    roleId: String,
                    accountId: String,
                    createDate: Option[Timestamp] = None,
                    modifiedDate: Option[Timestamp] = None
                   )extends LinkTableBaseEntity

case class Permission(id: Option[String] = None,
                      accountId: String,
                      name: String,
                      createDate: Option[Timestamp] = None,
                      modifiedDate: Option[Timestamp] = None
                     )

case class RolePermission(roleId: String,
                          permissionId: String,
                          accountId: String,
                          createDate: Option[Timestamp] = None,
                          modifiedDate: Option[Timestamp] = None
                         )

case class Make(id: Option[String] = None,
                 name: String,
                 createDate: Option[Timestamp] = None,
                 modifiedDate: Option[Timestamp] = None
               )

case class Model( id: Option[String] = None,
                  makeId:String,
                  name: String,
                  createDate: Option[Timestamp] = None,
                  modifiedDate: Option[Timestamp] = None
                )

case class AttributeEntityTypes(id:Option[String]=None,
                accountId: String,
                attributeId:String,
                entityType:String,
                createDate: Option[Timestamp] = None,
                modifiedDate: Option[Timestamp] = None
               ) extends AccountBaseEntity

case class CarFavorite(carId: String,
                       userId: String,
                       accountId: String,
                       createDate: Option[Timestamp] = None,
                       modifiedDate: Option[Timestamp] = None
                      ) extends LinkTableBaseEntity

case class ContactInfo(id: Option[String] = None,
                       accountId: String,
                       carId: String,
                       typeSeller: String,
                       dealerName: String,
                       dealerNumber: String,
                       firstName: String,
                       lastName: String,
                       phoneNumber: String,
                       email: String,
                       isTextSeller: Boolean,
                       isUseZoomInfo: Boolean,
                       createDate: Option[Timestamp] = None,
                       modifiedDate: Option[Timestamp] = None) extends AccountBaseEntity

case class SellerLocationListing(
                                  id: Option[String] = None,
                                  accountId: String,
                                  carId: String,
                                  address1: String,
                                  address2: Option[String] = None,
                                  city: String,
                                  state: String,
                                  zip: String,
                                  isUseSellerInfo: Boolean,
                                  createDate: Option [Timestamp] = None,
                                  modifiedDate: Option [Timestamp] = None
                                ) extends AccountBaseEntity