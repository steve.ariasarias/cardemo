package com.zoom.models

import java.sql.{Date, Timestamp}

import com.sksamuel.elastic4s.searches.queries.SimpleQueryStringFlag.NOT

import scala.reflect.ClassTag
import slick.jdbc.MySQLProfile.api._

abstract class NotAccountBaseTable[T](tag: Tag, tableName: String) extends Table[T](tag, tableName) {
  def createDate = column[Timestamp]("CREATE_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP"))
  def modifiedDate = column[Timestamp]("MODIFIED_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))
}

abstract class AccountBaseTable[E: ClassTag](tag: Tag, tableName: String) extends Table[E](tag, tableName) {
  //   val classOfEntity: Class[E] = ClassTag[E].asInstanceOf[Class[E]]
  val id: Rep[String] = column[String]("ID", O.PrimaryKey)
  val accountId = column[String]("ACCOUNT_ID")
  def createDate = column[Timestamp]("CREATE_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP"))
  def modifiedDate = column[Timestamp]("MODIFIED_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))
}

abstract class LinkTableBaseTable[E: ClassTag](tag: Tag, tableName: String) extends Table[E](tag, tableName) {
  val accountId = column[String]("ACCOUNT_ID",O.PrimaryKey)
  def createDate = column[Timestamp]("CREATE_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP"))
  def modifiedDate = column[Timestamp]("MODIFIED_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))
}

class PlanTypesTable(tag: Tag) extends NotAccountBaseTable[PlanType](tag,"PLAN_TYPES"){
  val id: Rep[String] = column[String]("ID", O.PrimaryKey)
  def name = column[String]("NAME")
  def listingNumber = column[Int]("LISTING_NUMBER")
  def userNumber = column[Int]("USER_NUMBER")
  def price = column[BigDecimal]("PRICE")
  def * = (id.?,name,listingNumber,userNumber,price,createDate.?,modifiedDate.?) <> (PlanType.tupled, PlanType.unapply)
}


class AccountTable(tag: Tag) extends Table[Account](tag, "ACCOUNTS"){
  val id: Rep[String] = column[String]("ID", O.PrimaryKey)
  def email = column[String]("EMAIL")
  def firstName = column[String]("FIRST_NAME")
  def lastName = column[String]("LAST_NAME")
  def password = column[String]("PASSWORD")
  def dealerName = column[String]("DEALER_NAME")
  def dealerNumber = column[String]("DEALER_NUMBER")
  def phone = column[String]("PHONE")
  def planTypeId = column[String]("PLAN_TYPE_ID")
  def active = column[Boolean]("ACTIVE")
  def createDate = column[Timestamp]("CREATE_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP"))
  def modifiedDate = column[Timestamp]("MODIFIED_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))
  def * = (id.?,email, firstName, lastName, password, dealerName.?, dealerNumber.?, phone.?, planTypeId, active,createDate.?,modifiedDate.?)<>(Account.tupled, Account.unapply)
}

class CarTable(tag: Tag) extends AccountBaseTable[Car](tag, "CARS") {
  def year = column[Int]("YEAR")
  def makeId = column[String]("MAKE_ID")
  def modelId = column[String]("MODEL_ID")
  def vin = column[String]("VIN")
  /*def minimumPrice = column[BigDecimal]("MINIMUM_PRICE")
  def soldPrice = column[BigDecimal]("SOLD_PRICE")*/
  def suggestedPrice = column[BigDecimal]("SUGGESTED_PRICE")
  def commission = column[BigDecimal]("COMMISSION")
  def mainImage = column[String]("MAIN_IMAGE")
  def soldDate = column[Date]("SOLD_DATE")
  /*def amountSold = column[BigDecimal]("AMOUNT_SOLD")
  def amountCollected = column[BigDecimal]("AMOUNT_COLLECTED")
  def adminNotes = column[String]("ADMIN_NOTES")*/
  def description = column[String]("DESCRIPTION")
  def mileage = column[Int]("MILEAGE")
  def order = column[Int]("CAR_ORDER")
  def active = column[Boolean]("ACTIVE")
  def view = column[Int]("VIEW")
  def * = (id.?,accountId,year,makeId, modelId, vin,suggestedPrice, commission, mainImage, soldDate,description,mileage,order,active,view,createDate.?,modifiedDate.?) <> (Car.tupled, Car.unapply)
}

class CarAttributeValueTable(tag:Tag) extends LinkTableBaseTable[ObjectAttributeValue](tag,"CAR_ATTRIBUTE_VALUES"){
  def carId = column[String]("CAR_ID",O.PrimaryKey)
  def attributeId = column[String]("ATTRIBUTE_ID",O.PrimaryKey)
  def attributeValueId = column[String]("ATTRIBUTE_VALUE_ID")
  def value = column[String]("VALUE")
  def * = (carId,attributeId,accountId,attributeValueId,value,createDate.?,modifiedDate.?) <> (ObjectAttributeValue.tupled,ObjectAttributeValue.unapply)
  def car = foreignKey("fk_cav_car_id",carId,TableQuery[CarTable])(_.id)
  def attribute = foreignKey("fk_cav_attribute_id",attributeId,TableQuery[AttributesTable])(_.id)
  def account = foreignKey("fk_cav_account_id",accountId,TableQuery[AccountTable])(_.id)
  def pk = primaryKey("pk_car_attribute_values", (carId,attributeId,accountId))
}

class AttributesTable(tag: Tag) extends AccountBaseTable[Attribute](tag,"ATTRIBUTES"){
  def code = column[String]("CODE")
  def name = column[String]("NAME")
  def displayValue = column[String]("DISPLAY_VALUE")
  def typeAttribute = column[String]("TYPE")
  def status = column[String]("STATUS")
  def * = (id.?,accountId,code,name,displayValue,typeAttribute.?,status.?,createDate.?,modifiedDate.?) <> (Attribute.tupled, Attribute.unapply)
}

class AttributeValuesTable(tag:Tag) extends AccountBaseTable[AttributeValue](tag, "ATTRIBUTE_VALUES"){
  def attributeId = column[String]("ATTRIBUTE_ID")
  def code = column[String]("CODE")
  def name = column[String]("NAME")
  def displayValue = column[String]("DISPLAY_VALUE")
  def order = column[Int]("POSITION")
  def * = (id.?, accountId,attributeId, code, name, displayValue,order,createDate.?, modifiedDate.?) <> (AttributeValue.tupled, AttributeValue.unapply)
}

class CarImageTable(tag:Tag) extends AccountBaseTable[CarImage](tag, "CAR_IMAGES"){
  def url = column[String]("URL")
  def imageOrder = column[Int]("IMAGE_ORDER")
  def carId = column[String]("CAR_ID")
  def * = (id.?, accountId, url, imageOrder?,carId?, createDate.?, modifiedDate.?) <> (CarImage.tupled, CarImage.unapply)
}

class CarFavoritesTable(tag:Tag) extends LinkTableBaseTable[CarFavorite](tag, "CAR_FAVORITES"){
  val accounts = TableQuery[AccountTable]
  val cars = TableQuery[CarTable]
  val users = TableQuery[UserTable]
  def carId = column[String]("CAR_ID")
  def userId = column[String]("USER_ID")
  def * = (carId, userId, accountId, createDate.?, modifiedDate.?) <> (CarFavorite.tupled, CarFavorite.unapply)
  def account = foreignKey("fk_car_favorites_account_id",accountId,accounts)(_.id)
  def car = foreignKey("fk_car_favorites_car_id",carId,cars)(_.id)
  def user = foreignKey("fk_car_favorites_user_id",userId,users)(_.id)
}

class CustomerTable(tag:Tag) extends AccountBaseTable[Customer](tag, "CUSTOMERS"){
  def imageUrl = column[String]("IMAGE_URL")
  def firstName = column[String]("FIRST_NAME")
  def lastName = column[String]("LAST_NAME")
  def workNumber = column[String]("WORK_NUMBER")
  def comments = column[String]("COMMENTS")
  def active = column[Boolean]("ACTIVE")
  def dateStatement = column[Date]("DATE_STATEMENT")
  def dobmdy = column[Date]("DOBMDY")
  def * = (id.?,accountId,imageUrl.?,firstName,lastName,workNumber.?,comments.?,active,dateStatement.?,dobmdy.?,createDate.?, modifiedDate.?) <> (Customer.tupled, Customer.unapply)
}

class UserTable(tag:Tag) extends AccountBaseTable[User](tag, "USERS"){
  def email = column[String]("EMAIL")
  def firstName = column[String]("FIRST_NAME")
  def lastName = column[String]("LAST_NAME")
  def password = column[String]("PASSWORD")
  def active = column[Boolean]("ACTIVE")
  def phone = column[String]("PHONE")
  def address = column[String]("ADDRESS")
  def image = column[String]("IMAGE")
  def * = (id.?,accountId,email,firstName,lastName,password,active,phone.?, address.?,image.?,createDate.?, modifiedDate.?) <> (User.tupled, User.unapply)
}

class CustomerAttributeValueTable(tag:Tag) extends LinkTableBaseTable[ObjectAttributeValue](tag,"CUSTOMER_ATTRIBUTE_VALUES"){
  def customerId = column[String]("CUSTOMER_ID")
  def attributeId = column[String]("ATTRIBUTE_ID")
  def attributeValueId = column[String]("ATTRIBUTE_VALUE_ID")
  def value = column[String]("VALUE")
  def * = (customerId,attributeId,accountId,attributeValueId,value,createDate.?,modifiedDate.?) <> (ObjectAttributeValue.tupled,ObjectAttributeValue.unapply)
}

class UserAttributeValueTable(tag:Tag) extends LinkTableBaseTable[ObjectAttributeValue](tag,"USER_ATTRIBUTE_VALUES"){
  def userId = column[String]("USER_ID")
  def attributeId = column[String]("ATTRIBUTE_ID")
  def attributeValueId = column[String]("ATTRIBUTE_VALUE_ID")
  def value = column[String]("VALUE")
  def * = (userId,attributeId,accountId,attributeValueId,value,createDate.?,modifiedDate.?) <> (ObjectAttributeValue.tupled,ObjectAttributeValue.unapply)
}

class VerificationTokenTable(tag:Tag) extends LinkTableBaseTable[VerificationToken](tag,"VERIFICATION_TOKENS") {
  def id =  column [String] ("ID", O.PrimaryKey)
  def userId = column[String] ("USER_ID")
  def token = column[String] ("TOKEN")
  def isValid =  column[Boolean]("IS_VALID")
  def * = (id.?, userId, token, isValid, createDate.?, modifiedDate.?)<>(VerificationToken.tupled, VerificationToken.unapply)
}

class MakesTable(tag: Tag) extends Table[Make](tag,"MAKES"){
  def id = column[String]("ID", O.PrimaryKey)
  def name = column[String]("NAME")
  def createDate = column[Timestamp]("CREATE_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP"))
  def modifiedDate = column[Timestamp]("MODIFIED_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))
  def * = (id.?,name,createDate.?,modifiedDate.?) <> (Make.tupled, Make.unapply)
}

class ModelsTable(tag: Tag) extends Table[Model](tag,"MODELS"){
  def id = column[String]("ID", O.PrimaryKey)
  def makeId = column[String]("MAKE_ID", O.PrimaryKey)
  def name = column[String]("NAME")
  def createDate = column[Timestamp]("CREATE_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP"))
  def modifiedDate = column[Timestamp]("MODIFIED_DATE",O.SqlType("timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"))
  def * = (id.?,makeId,name,createDate.?,modifiedDate.?) <> (Model.tupled, Model.unapply)
  def make = foreignKey("fk_models_make_id",makeId,TableQuery[MakesTable])(_.id)
  def pk = primaryKey("pk_model", (id,makeId))
}

class RolesTable(tag:Tag) extends NotAccountBaseTable[Role](tag, "ROLES"){
  def id = column[String]("ID",O.PrimaryKey)
  def name = column[String]("NAME")
  def description = column[String]("DESCRIPTION")
  def * = (id.?, name, description, createDate.?, modifiedDate.?) <> (Role.tupled, Role.unapply)
}

class UserRolesTable(tag:Tag) extends LinkTableBaseTable[UserRole](tag, "USERS_ROLES"){
  val users = TableQuery[UserTable]
  val roles = TableQuery[RolesTable]
  def userId = column[String]("USER_ID")
  def roleId = column[String]("ROLE_ID")
  def * = (userId, roleId, accountId, createDate.?, modifiedDate.?) <> (UserRole.tupled, UserRole.unapply)
  def user = foreignKey("fk_users_roles_user_id",userId,users)(_.id)
  def role = foreignKey("fk_users_roles_role_id",roleId,roles)(_.id)
  def pk = primaryKey("pk_users_roles", (userId,roleId))
}

class LocationsTable(tag:Tag) extends AccountBaseTable[Location](tag,"LOCATIONS"){
  def name = column[String]("NAME")
  def address = column[String]("ADDRESS")
  def address2 = column[String]("ADDRESS2")
  def city = column[String]("CITY")
  def state = column[String]("STATE")
  def zip = column[String]("zip")
  def country = column[String]("COUNTRY")
  def active = column[Boolean]("ACTIVE")
  def * = (id.?,accountId,name,address,address2,city,state,zip,country,active,createDate.?,modifiedDate.?)<>(Location.tupled, Location.unapply)
}

class AttributeEntityTypesTable(tag:Tag) extends AccountBaseTable[AttributeEntityTypes](tag,"ATTRIBUTE_ENTITY_TYPES"){
  val attributes = TableQuery[AttributesTable]
  val accounts = TableQuery[AccountTable]
  def attributeId = column[String]("ATTRIBUTE_ID")
  def entityType = column[String]("ENTITY_TYPE")
  def formAttributes = column[String]("FORM_ATTRIBUTES")
  def grouping = column[String]("GROUPING")
  def status = column[String]("STATUS")
  def renderType = column[String]("RENDER_TYPE")
  def * = (id.?,accountId,attributeId,entityType,createDate.?,modifiedDate.?)<>(AttributeEntityTypes.tupled,AttributeEntityTypes.unapply)
  def account = foreignKey("atr_entity_types_account",accountId,accounts)(_.id)
  def attribute = foreignKey("atr_entity_types_attribute",attributeId,attributes)(_.id)
}

class SaveSearchesTable(tag: Tag) extends AccountBaseTable[SaveSearch](tag,"SAVE_SEARCHES"){
  def name = column[String]("NAME")
  def filter = column[String]("FILTERS")
  def coincidence = column[Int]("TOTAL_COINCIDENCE")
  def alertIsActive = column[Boolean]("ALERT_ACTIVE")
  def * = (id.?,accountId,name,filter,coincidence,alertIsActive,createDate.?,modifiedDate.?) <> (SaveSearch.tupled, SaveSearch.unapply)
}

class ContactInfoTable(tag:Tag) extends AccountBaseTable[ContactInfo](tag,"CONTACT_INFO_LISTING"){
  def carId = column[String]("CAR_ID")
  def typeSeller = column[String]("TYPE_SELLER")
  def dealerName = column[String]("DEALER_NAME")
  def dealerNumber = column[String]("DEALER_NUMBER")
  def firstName = column[String]("FIRST_NAME")
  def lastName = column[String]("LAST_NAME")
  def phoneNumber = column[String]("PHONE_NUMBER")
  def email = column[String]("EMAIL")
  def isTextSeller = column[Boolean]("IS_TEXT_SELLER")
  def isUseZoomInfo = column[Boolean]("IS_USE_ZOOM_INFO")
  def * = (id.?,accountId,carId,typeSeller,dealerName,dealerNumber,firstName,lastName,phoneNumber,email,isTextSeller,isUseZoomInfo,createDate.?,modifiedDate.?)<>(ContactInfo.tupled, ContactInfo.unapply)
}

class SellerLocationListingTable(tag: Tag) extends AccountBaseTable[SellerLocationListing](tag, "SELLER_LOCATION_LISTING"){
  def carId = column[String]("CAR_ID")
  def address1 = column[String]("ADDRESS_1")
  def address2 = column[String]("ADDRESS_2")
  def city = column[String]("CITY")
  def state = column[String]("STATE")
  def zip = column[String]("ZIP")
  def isUseSellerInfo = column[Boolean]("is_use_seller_info")
  def * = (id.?,accountId, carId, address1, address2.?, city, state, zip, isUseSellerInfo, createDate.?,modifiedDate.?)<>(SellerLocationListing.tupled, SellerLocationListing.unapply)
}
