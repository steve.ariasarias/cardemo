package com.zoom.utils

import java.io.File
import java.sql.SQLException
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation._
import play.api.{Environment, Logger}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import com.zoom.utils.Constants._

object Util {

  val logger = Logger(this.getClass)

  val allNumbers = """\d+""".r
  val allLetters = """[A-Za-z]+""".r
  val passwordCheckConstraint: Constraint[String] = Constraint("constraints.passwordcheck")({
    plainText =>
      val errors = plainText match {
        case allNumbers() => Seq(ValidationError("Password is all numbers"))
        case allLetters() => Seq(ValidationError("Password is all letters"))
        case _ => Nil
      }
      if (errors.isEmpty) {
        Valid
      } else {
        Invalid(errors)
      }
  })

  val loginForm = Form(
    tuple(
      "email" -> nonEmptyText,
      "password" -> nonEmptyText
    )
  )

  def tryToFuture[A](t: => Try[A])(implicit executor: ExecutionContext): Future[A] = {
    Future {
      t
    }.flatMap {
      case Success(s) => Future.successful(s)
      case Failure(fail) => Future.failed(fail)
    }
  }

  def optionToFuture[A](t: Option[A], failureMessage: String = "Element Not Found") = {
    t.map(x => Future.successful(x)).getOrElse(Future.failed(new NoSuchElementException(failureMessage)))
  }

  def tempFile(path: String, environment: Environment) = {
    val result = path.split("/")
    val fileName: String = result.toList.last
    val tempFolder = environment.getFile("tmp/")
    if (!tempFolder.exists())
      tempFolder.mkdir()
    val pathToTemp = tempFolder.getPath
    new File(s"$pathToTemp/$fileName")
  }


 /* def handleError(result:Try[Unit]) = {
    var responseCode = RESPONSE_CODE_SUCCESS
    var responseMessage = RESPONSE_MESSAGE_SUCCESS

    if (result.isFailure) {
      responseCode = RESPONSE_CODE_ERROR
      responseMessage = result.failed.get.getMessage
      logger.error("There is error",result.failed.get)
    }
    (responseCode, responseMessage)
  }*/


  def handleError(result:Try[Any]) = {
    var responseCode = RESPONSE_CODE_SUCCESS
    var responseMessage = RESPONSE_MESSAGE_SUCCESS
    result match {
      case Success(_) => logger.info("Execute sql success")
      case Failure(t: SQLException) =>
        logger.error("SQLException => There is error",result.failed.get)
        responseCode = t.getErrorCode.toString
        responseMessage =  getMessageError(t.getErrorCode)
      case Failure(_) =>
        responseCode = RESPONSE_CODE_ERROR
        responseMessage = result.failed.get.getMessage
        logger.error("There is error",result.failed.get)
    }
    /*if (result.isFailure) {
      responseCode = RESPONSE_CODE_ERROR
      responseMessage = result.failed.get.getMessage
      logger.error("There is error",result.failed.get)
    }*/
    (responseCode, responseMessage)
  }

  def handleErrorToSave(result:Try[Int]) = {
    var responseCode = RESPONSE_CODE_SUCCESS
    var responseMessage = RESPONSE_MESSAGE_SUCCESS
    result match {
      case Success(_) => logger.info("Execute sql success")
      case Failure(t: SQLException) =>

        logger.error("There is error",result.failed.get)
        responseCode = t.getErrorCode.toString
        responseMessage =  getMessageError(t.getErrorCode)
      case Failure(_) =>
        responseCode = RESPONSE_CODE_ERROR
        responseMessage = result.failed.get.getMessage
        logger.error("There is error",result.failed.get)
    }
    /*if (result.isFailure) {
      responseCode = RESPONSE_CODE_ERROR
      responseMessage = result.failed.get.getMessage
      logger.error("There is error",result.failed.get)
    }*/
    (responseCode, responseMessage)
  }

  def getMessageError(errorCode:Int) ={
    var messageError = ""
    errorCode match {
      case ERROR_DUPLICATE_RECORD =>
        messageError = "Record already exist."
      case ERROR_TABLE_NOT_EXIST =>
        messageError = "The table doesn't exist."
      case _ => messageError = "Error generic."
    }
    messageError
  }

  def isInputTypeList(inputType:String) = {
    inputType.equalsIgnoreCase(INPUT_SELECT) || inputType.equalsIgnoreCase(RADIO_BUTTON) || inputType.equalsIgnoreCase(MULTI_SELECT)
  }

}
