package com.zoom.utils

object Constants {

  object Roles extends Enumeration {
    val ADMIN = Value("admin")
  }

  val ENTITY_TYPE_CUSTOMER = "CUSTOMER"
  val ENTITY_TYPE_CAR = "CAR"
  val ENTITY_TYPE_USER = "USER"
  val ENTITY_TYPE_APPLICANT = "APPLICANT"


  val INPUT_TEXT    = "TEXT"
  val INPUT_SELECT  = "SELECT"
  val MULTI_SELECT  = "MULTI_SELECT"
  val RADIO_BUTTON  = "RADIO_BUTTON"
  val STATE         = "STATE"
  val PASSWORD      = "PASSWORD"

  val APPLICANT_TYPE = "customer.applicantType"
  val CUSTOMER_EMAIL = "customer.email"

  val RESPONSE_CODE_SUCCESS = "00"
  val RESPONSE_CODE_ERROR = "99"
  val RESPONSE_MESSAGE_SUCCESS = "SUCCESS"


  val CUSTOMER_USER_ID  = "CUSTOMER_USER_ID"
  val CUSTOMER_ID       = "CUSTOMER_ID"
  val GENERAL           = "GENERAL"
  val PERSON_USER       = "USER"
  val PERSON_CUSTOMER   = "CUSTOMER"


  val DEFAULT_ACCOUNT_ID = "10000"
  val DEFAULT_LOCATION_ID = "1"

  val DEFAULT_NAME_SELECT = "DEFAULT"

  val ATTRIBUTE_CUSTOMER_EMAIL_CODE = "customer.email"
  val ATTRIBUTE_CUSTOMER_PHONE_CODE = "customer.cellPhone"

  val DEFAULT_TEMPLATE_ID   = "template1"
  val DEFAULT_TEMPLATE_MAIN = "templateMain"
  val SITE_DEFAULT          = "Zoom"
  val MODERN_VIEW_TEMPLATE  = "template5"

  val ATTRIBUTE_ID_PASSWORD      = "password"
  val ATTRIBUTE_ID_ROLES         = "roles"


  val ERROR_DUPLICATE_RECORD=1062
  val ERROR_TABLE_NOT_EXIST= 1146

  val STATUS_AVAILABLE = "available"
  val STATUS_DELETE    = "delete"
  val CARS_BY_PAGE_DEFAULT    = 16
  val CARS_STATUS_FRONT_LINE_READY   = "Front Line Ready"

  val CAR_ID = "id"
  val CAR_VIN = "vin"
  val CAR_VIEW = "view"

  val YEAR_FROM = "1930"
  val YEAR_TO = "2020"
  val PRICE_FROM = 10000
  val PRICE_TO = 60000
  val MILEAGE_FROM = 1000
  val MILEAGE_TO = 1000000


}
