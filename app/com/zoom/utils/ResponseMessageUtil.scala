package com.zoom.utils

object ResponseMessageUtil {

  val STATUS = "status"
  val STATUS_OK = "Ok"
  val STATUS_ERROR = "Error"
  val MESSAGE = "message"
  val CODE = "code"
  val DATA = "data"

}
