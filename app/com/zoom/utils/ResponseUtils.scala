package com.zoom.utils

import com.zoom.models._
import play.api.libs.json.{JsObject, JsValue, Json, Writes}

object ResponseUtils {

  case class InitialState(email:String ,accountId: String, firstName:String, urlImage: Option[String])
  case class OptionMake(id:String, name:String, displayValue:String)
  case class OptionModel(id:String, name:String, displayValue:String,makeId:String)

  case class ResponseAttributes(attribute: Attribute, entityTypes: List[String])
  case class ResponseAccount(id:String, accountId: String, firstName: String, lastName: String , address: String, email: String, phone: String, dealerName:Option[String], dealerNumber:Option[String])
  implicit val responseAttributesWrites = new Writes[ResponseAttributes] {
    def writes(responseAttributes: ResponseAttributes): JsValue = {
      Json.obj("id" -> responseAttributes.attribute.id) ++
        Json.obj("accountId" -> responseAttributes.attribute.accountId) ++
        Json.obj("code" -> responseAttributes.attribute.code) ++
        Json.obj("name" -> responseAttributes.attribute.name) ++
        Json.obj("displayValue" -> responseAttributes.attribute.displayValue) ++
        Json.obj("typeAttribute" -> responseAttributes.attribute.typeAttribute) ++
        Json.obj("entityTypes" -> responseAttributes.entityTypes)
    }
  }

  case class ResponseAttributesEntity(attribute: Attribute, attributeValues: Option[Seq[AttributeValue]])

  implicit val attributeWrite           = Json.writes[Attribute]
  implicit val attributeValuesWrite     = Json.writes[AttributeValue]
  implicit val carWrite                 = Json.writes[Car]
  implicit val carImageWrite            = Json.writes[CarImage]

  implicit val responseAttributesEntityWrites = new Writes[ResponseAttributesEntity] {
    def writes(responseAttributesEntity: ResponseAttributesEntity): JsValue = {
      Json.toJson(responseAttributesEntity.attribute).as[JsObject] ++
        Json.obj("attributeValues"->Json.toJson(responseAttributesEntity.attributeValues))
    }
  }

  case class InfoAttributeValue(attributeId:String, attributeCode:String, attributeValue: String)
  case class ResponseInfoCarAttribute(car:Car,infoCarAttribute: List[InfoAttributeValue])
  implicit val implicitResponseInfoCarAttributeWrite = new Writes[ResponseInfoCarAttribute] {
    def writes(responseInfoCarAttribute: ResponseInfoCarAttribute): JsValue = {
      val attribute = responseInfoCarAttribute.infoCarAttribute.map(t => (t.attributeCode,Json.obj("attributeCode" -> t.attributeCode,"attributeValue" -> t.attributeValue))).toMap
      Json.toJson(responseInfoCarAttribute.car).as[JsObject] ++ Json.toJson(attribute).as[JsObject]
    }
  }

  case class ResponseCarImage(carImage :List[CarImage])

  implicit val initialStateWrite    = Json.writes[InitialState]
  implicit val optionMakeWrites     = Json.writes[OptionMake]
  implicit val optionModelWrites    = Json.writes[OptionModel]
  implicit val responseAccount      = Json.writes[ResponseAccount]
  implicit val saveSearches         = Json.writes[SaveSearch]
}
