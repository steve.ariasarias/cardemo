package com.zoom.utils

import java.sql.Timestamp
import play.api.libs.json.Json.JsValueWrapper
import play.api.libs.json._

object RequestUtils {

  import play.api.libs.json._
  case class FilterCarPublic(keyword:Option[String], makes:Option[List[String]], models:Option[List[String]],
                             yearFrom:Option[String], yearTo:Option[String],
                             priceFrom:Option[BigDecimal], priceTo:Option[BigDecimal],
                             mileageFrom:Option[BigDecimal], mileageTo:Option[BigDecimal], body:Option[List[String]],
                             priceBest:Option[BigDecimal], newEst:Option[Boolean], status:Option[List[String]], exteriorColour:Option[List[String]],
                             interiorColor:Option[List[String]], trim:Option[String], offset:Int, size:Int)

  case class FilterCar(keyword:Option[String],from:Option[Int],size:Option[Int],active:Option[Boolean])

  case class InfoAttribute(attributeCode: String, attributeValue: String)

  case class ProcessCar(carId:Option[String], vin: String, makeId: String, makeName:String, modelId:String, modelName:String, year:String, price:String, mainImageUrl:String, description:String, mileage:String, values:Map[String,Any])
  case class ProcessContactInfo(id:Option[String], typeSeller: String, dealerName: Option[String], dealerNumber: Option[String],
                                firstName: String, lastName: String, phone: String, email: String, isTextSeller: Boolean, isUseZoomInfo:Boolean)
  case class ProcessSellerLocationListing(id:Option[String], address1: String, address2: Option[String], city: String, state: String,
                                          zipCode: String, isUseSellerInfo: Boolean)

  case class ProcessListing(car:ProcessCar,contactInfo:Option[ProcessContactInfo],sellerLocation:Option[ProcessSellerLocationListing])
  case class LoginRequest(email: String, password: String)

  case class ProcessAccount(accountId: Option[String], email: String, firstName:String, lastName:String, password: String, dealerName:Option[String], dealerNumber:Option[String], phone:Option[String], address: Option[String], planTypeId: String)
  case class ProcessUser(id: String ,email: String, firstName:String, lastName:String,  phone:String , address: String)

  case class SearchInventoryAdmin(query:String,offset:Int,size:Int)

  case class ProcessSaveSearch(id:Option[String], name:String, filters:String, coincidence:Int, isAlertActive:Boolean)

  case class ChangePassword(email:String, oldPassword:String, newPassword:String)

  case class OptionsRequest(id:String, name:String,value:String, order:Int)

  implicit val filterCarPublicFormat = Json.format[FilterCarPublic]
  implicit val infoAttribute = Json.format[InfoAttribute]
  implicit val optionRequestAttribute = Json.format[OptionsRequest]

  case class MakeRequest(id:String,name:String)

  case class ModelRequest(id:String,makeId:String,name:String)

  case class MakeAndModelRequest(makes:List[MakeRequest],models:List[ModelRequest])

  implicit val makeFormat  = Json.format[MakeRequest]
  implicit val modelFormat = Json.format[ModelRequest]
  implicit val makeRequestFormat = Json.format[MakeAndModelRequest]
  implicit val objectMapFormat = new Format[Map[String, Any]] {

    def writes(map: Map[String, Any]): JsValue = {
      Json.obj(map.map {
        case (s, a) => {
          val ret: (String, JsValueWrapper) = a match {
            case _: String => s -> JsString(a.asInstanceOf[String])
            //case _: java.util.Date => s -> JsString(a.asInstanceOf[String])
            case _: Integer => s -> JsString(a.toString)
            case _: java.lang.Long => s -> JsString(a.toString)
            case _: java.lang.Double => s -> JsString(a.toString)
            case _: scala.math.BigDecimal => s ->JsString(a.toString)
            case None => s -> JsNull
            case Some(a) => s -> JsString(a.toString)
            case JsArray(elements) => s -> JsArray(elements)
            case t:List[String] => s -> t
            case _ => s -> {
              if(a != null) JsString(a.toString) else JsString("")
            }
          }
          ret
        }
      }.toSeq: _*)

    }

    def reads(json: JsValue): JsResult[Map[String, Any]] = JsSuccess {
      json.as[JsObject].value.map {
        case (k, v) => {
          val ret: (String, Any) = v match {
            case JsBoolean(b) => k -> b.asInstanceOf[Boolean]
            case JsNumber(n) => k -> n.asInstanceOf[Number]
            case JsString(s) => k -> s.asInstanceOf[String]
            case JsNull => k -> v
            case JsArray(elements) => k -> elements.map(convert)
            case _ => k -> JsArray(v.asInstanceOf[List[Int]].map(JsNumber(_)))
          }
          ret
        }
      }.toMap
    }

    def convert(m: JsValue): Any = {
      m match {
        case JsString(s) => s
        case JsNumber(n) => n
        case JsBoolean(b) => b
      }
    }
  }

  implicit def ordered: Ordering[Timestamp] = new Ordering[Timestamp] {
    def compare(x: Timestamp, y: Timestamp): Int = x compareTo y
  }

  implicit val carRequestFormat = Json.format[ProcessCar]
  implicit val accountRequestFormat = Json.format[ProcessAccount]
  implicit val userRequestFormat = Json.format[ProcessUser]
  implicit val loginRequestFormat = Json.format[LoginRequest]
  implicit val searchInventoryFormat = Json.format[SearchInventoryAdmin]
  implicit val filterCarFormat = Json.format[FilterCar]
  implicit val changePasswordFormat = Json.format[ChangePassword]
  implicit val saveSearchFormat = Json.format[ProcessSaveSearch]
  implicit val processContactInfoFormat = Json.format[ProcessContactInfo]
  implicit val processSellerLocationListingFormat = Json.format[ProcessSellerLocationListing]
  implicit val processListingRequestFormat = Json.format[ProcessListing]

}