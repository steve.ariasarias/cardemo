
name := """zoom"""
organization := "com.zoom"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers += Resolver.sonatypeRepo("snapshots")

scalaVersion := "2.12.7"

crossScalaVersions := Seq("2.11.12", "2.12.7")

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
resolvers += "Sonatype snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies += guice

val elastic4sVersion = "6.5.1"
libraryDependencies ++= Seq(
  "org.elasticsearch" % "elasticsearch" % "2.4.0",
  "com.sksamuel.elastic4s" %% "elastic4s-core" % elastic4sVersion,

  // for the http client
  "com.sksamuel.elastic4s" %% "elastic4s-http" % elastic4sVersion,

  // if you want to use reactive streams
  "com.sksamuel.elastic4s" %% "elastic4s-http-streams" % elastic4sVersion,

  // testing
  "com.sksamuel.elastic4s" %% "elastic4s-testkit" % elastic4sVersion % "test",
  "com.sksamuel.elastic4s" %% "elastic4s-embedded" % elastic4sVersion % "test"
)



libraryDependencies ++= Seq(
    //jdbc,
    ws,
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  //"com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",
  //"com.typesafe.play" % "play-slick-evolutions_2.11" % "2.0.2",
  "org.flywaydb" %% "flyway-play" % "5.1.0",
  "com.github.stephenc.eaio-uuid" % "uuid" % "3.2.0",
  "org.mindrot" % "jbcrypt" % "0.3m",
  //  "com.h2database" % "h2" % "1.4.187",
  "org.scala-lang.modules" %% "scala-async" % "0.9.7",
  "mysql" % "mysql-connector-java" % "5.1.38",
  "commons-lang" % "commons-lang" % "2.6",
  "com.amazonaws" % "aws-java-sdk-bom" % "1.10.43",
  "com.amazonaws" % "aws-java-sdk-s3" % "1.10.43"
)


fork in run := true
parallelExecution in Test := true
//javaOptions ++= Seq("-Dconfig.file=conf/testing/application.conf")
//javaOptions in Test += "-Dconfig.file=conf/testing/application.conf"

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.zoom.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.zoom.binders._"

import com.typesafe.sbt.packager.docker._


enablePlugins(DockerPlugin)

dockerCommands := Seq(
  Cmd("FROM", "anapsix/alpine-java:8_server-jre"),
  Cmd("MAINTAINER", "Javier Leiva <javier@leivatech.com>"),
  Cmd("WORKDIR", "/opt/docker"),
  Cmd("ADD","opt /opt"),
  Cmd("RUN","""["chown", "-R", "daemon:daemon", "."]"""),
  Cmd("EXPOSE","9000"),
  Cmd("USER", "daemon"),     
  Cmd("ENTRYPOINT", """["bin/zoom"]"""),
  Cmd("CMD", """[]""")
)