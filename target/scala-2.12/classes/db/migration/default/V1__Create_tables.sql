-- -----------------------------------------------------
-- Table PLAN_TYPES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS PLAN_TYPES (
  id                VARCHAR(40) NOT NULL,
  name              VARCHAR(128) NOT NULL,
  listing_number    INT(4) NOT NULL,
  users_number      INT(4) NOT NULL,
  price             DOUBLE NULL DEFAULT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table ROLES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS ROLES (
  id            VARCHAR(40) NOT NULL,
  name          VARCHAR(255) NOT NULL,
  description   VARCHAR(500) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE INDEX name (name ASC)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table ACCOUNTS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS ACCOUNTS (
  id            VARCHAR(40) NOT NULL,
  email         VARCHAR(255) NOT NULL,
  first_name    VARCHAR(255) NOT NULL,
  last_name     VARCHAR(255) NOT NULL,
  password      VARCHAR(255) NOT NULL,
  sellerType    VARCHAR(255) NOT NULL,
  dealer_name   VARCHAR(255) NULL,
  dealer_number VARCHAR(255) NULL,
  phone         VARCHAR(255) NULL DEFAULT NULL,
  image         VARCHAR(255) NULL DEFAULT NULL,
  plan_type_id  VARCHAR(40) NULL REFERENCES PLAN_TYPES,
  role_id       VARCHAR(40) NOT NULL,
  active        BOOLEAN NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE INDEX email (email ASC),
  UNIQUE INDEX account_id (id ASC),
  CONSTRAINT fk_account_role_id FOREIGN KEY (role_id) REFERENCES ROLES (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table USERS
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS USERS (
  id            VARCHAR(40) NOT NULL,
  account_id    VARCHAR(255) NOT NULL,
  email         VARCHAR(255) NOT NULL,
  first_name    VARCHAR(255) NOT NULL,
  last_name     VARCHAR(255) NOT NULL,
  password      VARCHAR(255) NOT NULL,
  active        TINYINT(1) NOT NULL,
  image         VARCHAR(255) NULL DEFAULT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id),
  UNIQUE INDEX email (email ASC),
  UNIQUE INDEX user_id_account (id ASC, account_id ASC)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table ATTRIBUTES
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS ATTRIBUTES (
  id              VARCHAR(40) NOT NULL,
  account_id      VARCHAR(40) NOT NULL,
  code            VARCHAR(40) NOT NULL,
  name            VARCHAR(255) NOT NULL,
  display_value   VARCHAR(255) NULL DEFAULT NULL,
  type            VARCHAR(255) NOT NULL,
  status          VARCHAR(45) NULL DEFAULT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id, account_id),
  CONSTRAINT fk_attribute_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id),
  UNIQUE INDEX code (code ASC, account_id ASC),
  UNIQUE INDEX name (name ASC, account_id ASC)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table ATTRIBUTE_ENTITY_TYPES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS ATTRIBUTE_ENTITY_TYPES (
  id              VARCHAR(40) NOT NULL,
  account_id      VARCHAR(40) NOT NULL,
  attribute_id    VARCHAR(40) NOT NULL,
  entity_type     VARCHAR(255) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  INDEX fk_attribute_entity_types_attribute_id (attribute_id ASC),
  CONSTRAINT atr_entity_types_attribute FOREIGN KEY (attribute_id) REFERENCES ATTRIBUTES (id),
  CONSTRAINT atr_entity_types_account FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table ATTRIBUTE_VALUES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS ATTRIBUTE_VALUES (
  id                  VARCHAR(40) NOT NULL,
  account_id          VARCHAR(40) NOT NULL,
  attribute_id        VARCHAR(40) NOT NULL,
  code                VARCHAR(40) NOT NULL,
  name                VARCHAR(255) NOT NULL,
  display_value       VARCHAR(255) NOT NULL,
  position            INT(4) NOT NULL DEFAULT '0',
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE INDEX code (code ASC, attribute_id ASC, account_id ASC),
  UNIQUE INDEX name (name ASC, attribute_id ASC, account_id ASC),
  INDEX idx_attribute_id (attribute_id ASC),
  CONSTRAINT fk_atr_values_attribute_id FOREIGN KEY (attribute_id) REFERENCES ATTRIBUTES (id),
  CONSTRAINT fk_atr_values_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table MAKES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS MAKES (
  id            VARCHAR(40) NOT NULL,
  name          VARCHAR(128) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table MODELS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS MODELS (
  id                VARCHAR(40) NOT NULL,
  make_id           VARCHAR(40) NOT NULL,
  name              VARCHAR(128) NOT NULL,
  create_date       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date     TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id, make_id),
  INDEX idx_make_id (make_id ASC),
  CONSTRAINT fk_models_make_id FOREIGN KEY (make_id) REFERENCES MAKES (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table CARS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS CARS (
  id              VARCHAR(40) NOT NULL,
  account_id      VARCHAR(40) NOT NULL,
  make_id         VARCHAR(40) NOT NULL,
  model_id        VARCHAR(40) NOT NULL,
  year            INT(4) NULL DEFAULT NULL,
  vin             CHAR(17) NOT NULL,
  purchased_date  DATE NULL DEFAULT NULL,
  retail_price    DOUBLE NULL DEFAULT NULL,
  suggested_price DOUBLE NULL DEFAULT NULL,
  commission      DOUBLE NULL DEFAULT NULL,
  main_image      VARCHAR(255) NULL DEFAULT NULL,
  sold_date       DATE NULL DEFAULT NULL,
  purchased_price DOUBLE NULL DEFAULT NULL,
  description     VARCHAR(2500) NOT NULL DEFAULT '',
  mileage         INT(10) NULL DEFAULT NULL,
  car_order       INT(11) NULL DEFAULT NULL,
  active          TINYINT(1) NOT NULL,
  history_report  VARCHAR(255) NULL DEFAULT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE INDEX vin (vin ASC),
  INDEX idx_car_make_id (make_id ASC),
  INDEX idx_car_model_id (model_id ASC),
  CONSTRAINT fk_cars_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id),
  CONSTRAINT fk_cars_make_id FOREIGN KEY (make_id) REFERENCES MAKES (id),
  CONSTRAINT fk_cars_model_id FOREIGN KEY (model_id) REFERENCES MODELS (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table CAR_ATTRIBUTE_VALUES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS CAR_ATTRIBUTE_VALUES (
  account_id          VARCHAR(40) NOT NULL,
  car_id              VARCHAR(40) NOT NULL,
  attribute_id        VARCHAR(40) NOT NULL,
  attribute_value_id  VARCHAR(40) NOT NULL,
  value               VARCHAR(500) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (car_id, attribute_id, account_id),
  INDEX idx_cav_attribute_id (attribute_id ASC),
  INDEX idx_cav_av_id (attribute_value_id ASC),
  CONSTRAINT fk_cav_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id),
  CONSTRAINT fk_cav_car_id FOREIGN KEY (car_id) REFERENCES CARS (id),
  CONSTRAINT fk_cav_attribute_id FOREIGN KEY (attribute_id) REFERENCES ATTRIBUTES (id),
  CONSTRAINT fk_cav_attribute_value_id FOREIGN KEY (attribute_value_id) REFERENCES ATTRIBUTE_VALUES (id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table CAR_IMAGES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS CAR_IMAGES (
  id              VARCHAR(40) NOT NULL,
  account_id      VARCHAR(40) NOT NULL,
  url             VARCHAR(255) NOT NULL,
  image_order     INT(11) NULL DEFAULT NULL,
  car_id          VARCHAR(40) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  INDEX idx_car_images_car_id (car_id ASC),
  CONSTRAINT fk_car_images_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id),
  CONSTRAINT fk_car_images_car_id FOREIGN KEY (car_id) REFERENCES CARS (id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table PERMISSIONS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS PERMISSIONS (
  id            VARCHAR(40) NOT NULL,
  account_id     VARCHAR(40) NOT NULL,
  name          VARCHAR(255) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table ROLE_PERMISSIONS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS ROLE_PERMISSIONS (
  role_id           VARCHAR(40) NOT NULL,
  permission_id     VARCHAR(40) NOT NULL,
  account_id         VARCHAR(40) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (role_id, permission_id),
  INDEX idx_rp_permission_id (permission_id ASC),
  CONSTRAINT fk_role_permissions_role_id FOREIGN KEY (role_id) REFERENCES ROLES (id),
  CONSTRAINT fk_role_permissions_permission_id FOREIGN KEY (permission_id) REFERENCES PERMISSIONS (id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table USERS_ROLES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS USERS_ROLES (
  user_id         VARCHAR(40) NOT NULL,
  role_id         VARCHAR(40) NOT NULL,
  account_id       VARCHAR(40) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id, role_id),
  INDEX idx_ur_role_id (role_id ASC),
  CONSTRAINT fk_users_roles_user_id FOREIGN KEY (user_id) REFERENCES USERS (id),
  CONSTRAINT fk_users_roles_role_id FOREIGN KEY (role_id) REFERENCES ROLES (id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table USER_ATTRIBUTE_VALUES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS USER_ATTRIBUTE_VALUES (
  user_id             VARCHAR(40) NOT NULL,
  attribute_id        VARCHAR(40) NOT NULL,
  attribute_value_id  VARCHAR(40) NOT NULL,
  account_id          VARCHAR(40) NOT NULL,
  value               VARCHAR(500) NOT NULL,
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id, attribute_id, account_id),
  INDEX idx_attribute_id (attribute_id ASC),
  INDEX idx_attribute_value_id (attribute_value_id ASC),
  CONSTRAINT fk_user_atr_values_account FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id),
  CONSTRAINT fk_user_atr_values_user FOREIGN KEY (user_id) REFERENCES USERS (id),
  CONSTRAINT fk_user_atr_values_attribute FOREIGN KEY (attribute_id) REFERENCES ATTRIBUTES (id),
  CONSTRAINT fk_user_atr_values_attribute_value FOREIGN KEY (attribute_value_id) REFERENCES ATTRIBUTE_VALUES (id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table VERIFICATION_TOKENS
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS VERIFICATION_TOKENS (
  id              VARCHAR(40) NOT NULL,
  user_id         VARCHAR(40) NOT NULL,
  token           VARCHAR(255) NOT NULL,
  is_valid        TINYINT(1) NULL DEFAULT '1',
  create_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  INDEX idx_user_id (user_id ASC),
  CONSTRAINT fk_verification_tokens_user FOREIGN KEY (user_id) REFERENCES USERS (id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

