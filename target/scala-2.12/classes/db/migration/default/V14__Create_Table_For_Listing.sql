-- -----------------------------------------------------
-- Table SAVE SEARCHES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS CONTACT_INFO_LISTING (
  id                VARCHAR(40) NOT NULL,
  account_id        VARCHAR(40) NOT NULL,
  car_id            VARCHAR(40) NOT NULL,
  type_seller       VARCHAR(255) NOT NULL,
  dealer_name       VARCHAR(255) NOT NULL DEFAULT '',
  dealer_number     VARCHAR(255) NOT NULL DEFAULT '',
  first_name        VARCHAR(255) NOT NULL DEFAULT '',
  last_name         VARCHAR(255) NOT NULL DEFAULT '',
  phone_number      VARCHAR(255) NOT NULL DEFAULT '',
  email             VARCHAR(255) NOT NULL DEFAULT '',
  is_text_seller    BOOLEAN NOT NULL DEFAULT false,
  is_use_zoom_info  BOOLEAN NOT NULL DEFAULT false,
  create_date       TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_date     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id, account_id, car_id),
  CONSTRAINT fk_contact_info_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id),
  CONSTRAINT fk_contact_info_car_id FOREIGN KEY (car_id) REFERENCES CARS (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table seller_location_listing
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS SELLER_LOCATION_LISTING (
   id                 VARCHAR(40) NOT NULL,
   account_id         VARCHAR(40) NOT NULL,
   car_id             VARCHAR(40) NOT NULL,
   address_1          VARCHAR(255) NOT NULL,
   address_2          VARCHAR(255) NULL,
   city               VARCHAR(255) NOT NULL,
   state              VARCHAR(255) NOT NULL,
   zip                VARCHAR(255) NOT NULL,
   is_use_seller_info BOOLEAN NOT NULL DEFAULT false,
   create_date        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   modified_date      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (id),
   CONSTRAINT fk_seller_location_listing_car_id FOREIGN KEY (car_id) REFERENCES CARS (id),
   CONSTRAINT fk_seller_location_listing_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;