-- -----------------------------------------------------
-- Table SAVE SEARCHES
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS SAVE_SEARCHES (
  id                VARCHAR(40) NOT NULL,
  account_id        VARCHAR(40) NOT NULL,
  name              VARCHAR(255) NOT NULL,
  filters           MEDIUMTEXT NOT NULL,
  total_coincidence INT NOT NULL,
  alert_active      BOOLEAN NOT NULL,
  create_date       TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_date     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id, account_id),
  UNIQUE INDEX name_save_search (account_id ASC, name ASC),
  CONSTRAINT save_search_account FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;