// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/stevejamesfoncy/IdeaProjects/zoom-web/conf/routes
// @DATE:Sun May 19 00:40:39 PET 2019

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  HomeController_0: controllers.HomeController,
  // @LINE:10
  PublicController_2: controllers.PublicController,
  // @LINE:13
  AdministrativeController_5: controllers.AdministrativeController,
  // @LINE:21
  Login_4: controllers.Login,
  // @LINE:27
  Assets_6: controllers.Assets,
  // @LINE:30
  Inventory_1: controllers.Inventory,
  // @LINE:34
  AccountController_7: controllers.AccountController,
  // @LINE:38
  SaveSearchController_3: controllers.SaveSearchController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    HomeController_0: controllers.HomeController,
    // @LINE:10
    PublicController_2: controllers.PublicController,
    // @LINE:13
    AdministrativeController_5: controllers.AdministrativeController,
    // @LINE:21
    Login_4: controllers.Login,
    // @LINE:27
    Assets_6: controllers.Assets,
    // @LINE:30
    Inventory_1: controllers.Inventory,
    // @LINE:34
    AccountController_7: controllers.AccountController,
    // @LINE:38
    SaveSearchController_3: controllers.SaveSearchController
  ) = this(errorHandler, HomeController_0, PublicController_2, AdministrativeController_5, Login_4, Assets_6, Inventory_1, AccountController_7, SaveSearchController_3, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_0, PublicController_2, AdministrativeController_5, Login_4, Assets_6, Inventory_1, AccountController_7, SaveSearchController_3, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.main"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """catalog""", """controllers.HomeController.goPublic(path:String = "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """catalog""" + "$" + """path<.+>""", """controllers.HomeController.goPublic(path:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """public/api/inventory/loadMakeAndModel""", """controllers.PublicController.loadMakeAndModel"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """public/api/inventory/search""", """controllers.PublicController.searchCar"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """public/api/inventory/""" + "$" + """vin<[^/]+>""", """controllers.PublicController.getCar(vin:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/setting/saveMakesAndModels""", """controllers.AdministrativeController.saveMakesAndModels"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """car""", """controllers.HomeController.goPublic(path:String = "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """comparison/""" + "$" + """path<.+>""", """controllers.HomeController.goPublic(path:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """car-values""", """controllers.HomeController.goPublic(path:String = "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """sell""", """controllers.HomeController.goPublic(path:String = "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """plan-selection""", """controllers.HomeController.goPublic(path:String = "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """account-info/""" + "$" + """path<.+>""", """controllers.HomeController.goPublic(path:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.HomeController.goPublic(path:String = "")"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """processLogin""", """controllers.Login.processLogin"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logout""", """controllers.Login.logout"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """about""", """controllers.HomeController.goPublic(path:String = "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """account/dashboard""", """controllers.HomeController.goPublic(path:String = "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin""", """controllers.HomeController.index(path:String = "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin""" + "$" + """path<.+>""", """controllers.HomeController.index(path:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/inventory/listing""", """controllers.Inventory.save"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/inventory/car/uploadImage/""" + "$" + """carId<[^/]+>""", """controllers.Inventory.uploadCarImage(carId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/inventory/car/favorite/""" + "$" + """carId<[^/]+>""", """controllers.Inventory.setOrRemoveFavoriteCar(carId:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/listing/search""", """controllers.Inventory.searchListing"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/user/account""", """controllers.AccountController.save"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/user/account/change-password""", """controllers.AccountController.changePassword"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/user/account""", """controllers.AccountController.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/user/accountInfo""", """controllers.AccountController.getAccount"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/savesearch/load""", """controllers.SaveSearchController.get"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/savesearch""", """controllers.SaveSearchController.save"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_HomeController_main0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_main0_invoker = createInvoker(
    HomeController_0.main,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "main",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_HomeController_goPublic1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("catalog")))
  )
  private[this] lazy val controllers_HomeController_goPublic1_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """catalog""",
      """""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_HomeController_goPublic2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("catalog"), DynamicPart("path", """.+""",false)))
  )
  private[this] lazy val controllers_HomeController_goPublic2_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """catalog""" + "$" + """path<.+>""",
      """""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_PublicController_loadMakeAndModel3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("public/api/inventory/loadMakeAndModel")))
  )
  private[this] lazy val controllers_PublicController_loadMakeAndModel3_invoker = createInvoker(
    PublicController_2.loadMakeAndModel,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PublicController",
      "loadMakeAndModel",
      Nil,
      "GET",
      this.prefix + """public/api/inventory/loadMakeAndModel""",
      """""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_PublicController_searchCar4_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("public/api/inventory/search")))
  )
  private[this] lazy val controllers_PublicController_searchCar4_invoker = createInvoker(
    PublicController_2.searchCar,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PublicController",
      "searchCar",
      Nil,
      "POST",
      this.prefix + """public/api/inventory/search""",
      """""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_PublicController_getCar5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("public/api/inventory/"), DynamicPart("vin", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PublicController_getCar5_invoker = createInvoker(
    PublicController_2.getCar(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PublicController",
      "getCar",
      Seq(classOf[String]),
      "GET",
      this.prefix + """public/api/inventory/""" + "$" + """vin<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_AdministrativeController_saveMakesAndModels6_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/setting/saveMakesAndModels")))
  )
  private[this] lazy val controllers_AdministrativeController_saveMakesAndModels6_invoker = createInvoker(
    AdministrativeController_5.saveMakesAndModels,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdministrativeController",
      "saveMakesAndModels",
      Nil,
      "POST",
      this.prefix + """api/setting/saveMakesAndModels""",
      """""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_HomeController_goPublic7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("car")))
  )
  private[this] lazy val controllers_HomeController_goPublic7_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """car""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_HomeController_goPublic8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("comparison/"), DynamicPart("path", """.+""",false)))
  )
  private[this] lazy val controllers_HomeController_goPublic8_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """comparison/""" + "$" + """path<.+>""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_HomeController_goPublic9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("car-values")))
  )
  private[this] lazy val controllers_HomeController_goPublic9_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """car-values""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_HomeController_goPublic10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("sell")))
  )
  private[this] lazy val controllers_HomeController_goPublic10_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """sell""",
      """""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_HomeController_goPublic11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("plan-selection")))
  )
  private[this] lazy val controllers_HomeController_goPublic11_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """plan-selection""",
      """""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_HomeController_goPublic12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("account-info/"), DynamicPart("path", """.+""",false)))
  )
  private[this] lazy val controllers_HomeController_goPublic12_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """account-info/""" + "$" + """path<.+>""",
      """""",
      Seq()
    )
  )

  // @LINE:20
  private[this] lazy val controllers_HomeController_goPublic13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_HomeController_goPublic13_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """login""",
      """""",
      Seq()
    )
  )

  // @LINE:21
  private[this] lazy val controllers_Login_processLogin14_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("processLogin")))
  )
  private[this] lazy val controllers_Login_processLogin14_invoker = createInvoker(
    Login_4.processLogin,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Login",
      "processLogin",
      Nil,
      "POST",
      this.prefix + """processLogin""",
      """""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_Login_logout15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logout")))
  )
  private[this] lazy val controllers_Login_logout15_invoker = createInvoker(
    Login_4.logout,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Login",
      "logout",
      Nil,
      "GET",
      this.prefix + """logout""",
      """""",
      Seq()
    )
  )

  // @LINE:23
  private[this] lazy val controllers_HomeController_goPublic16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("about")))
  )
  private[this] lazy val controllers_HomeController_goPublic16_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """about""",
      """""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_HomeController_goPublic17_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("account/dashboard")))
  )
  private[this] lazy val controllers_HomeController_goPublic17_invoker = createInvoker(
    HomeController_0.goPublic(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "goPublic",
      Seq(classOf[String]),
      "GET",
      this.prefix + """account/dashboard""",
      """""",
      Seq()
    )
  )

  // @LINE:27
  private[this] lazy val controllers_Assets_versioned18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned18_invoker = createInvoker(
    Assets_6.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_HomeController_index19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin")))
  )
  private[this] lazy val controllers_HomeController_index19_invoker = createInvoker(
    HomeController_0.index(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Seq(classOf[String]),
      "GET",
      this.prefix + """admin""",
      """""",
      Seq()
    )
  )

  // @LINE:29
  private[this] lazy val controllers_HomeController_index20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin"), DynamicPart("path", """.+""",false)))
  )
  private[this] lazy val controllers_HomeController_index20_invoker = createInvoker(
    HomeController_0.index(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Seq(classOf[String]),
      "GET",
      this.prefix + """admin""" + "$" + """path<.+>""",
      """""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_Inventory_save21_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/inventory/listing")))
  )
  private[this] lazy val controllers_Inventory_save21_invoker = createInvoker(
    Inventory_1.save,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Inventory",
      "save",
      Nil,
      "POST",
      this.prefix + """api/inventory/listing""",
      """""",
      Seq()
    )
  )

  // @LINE:31
  private[this] lazy val controllers_Inventory_uploadCarImage22_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/inventory/car/uploadImage/"), DynamicPart("carId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Inventory_uploadCarImage22_invoker = createInvoker(
    Inventory_1.uploadCarImage(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Inventory",
      "uploadCarImage",
      Seq(classOf[String]),
      "POST",
      this.prefix + """api/inventory/car/uploadImage/""" + "$" + """carId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:32
  private[this] lazy val controllers_Inventory_setOrRemoveFavoriteCar23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/inventory/car/favorite/"), DynamicPart("carId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Inventory_setOrRemoveFavoriteCar23_invoker = createInvoker(
    Inventory_1.setOrRemoveFavoriteCar(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Inventory",
      "setOrRemoveFavoriteCar",
      Seq(classOf[String]),
      "GET",
      this.prefix + """api/inventory/car/favorite/""" + "$" + """carId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:33
  private[this] lazy val controllers_Inventory_searchListing24_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/listing/search")))
  )
  private[this] lazy val controllers_Inventory_searchListing24_invoker = createInvoker(
    Inventory_1.searchListing,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Inventory",
      "searchListing",
      Nil,
      "POST",
      this.prefix + """api/listing/search""",
      """""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_AccountController_save25_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/user/account")))
  )
  private[this] lazy val controllers_AccountController_save25_invoker = createInvoker(
    AccountController_7.save,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AccountController",
      "save",
      Nil,
      "POST",
      this.prefix + """api/user/account""",
      """""",
      Seq()
    )
  )

  // @LINE:35
  private[this] lazy val controllers_AccountController_changePassword26_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/user/account/change-password")))
  )
  private[this] lazy val controllers_AccountController_changePassword26_invoker = createInvoker(
    AccountController_7.changePassword,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AccountController",
      "changePassword",
      Nil,
      "POST",
      this.prefix + """api/user/account/change-password""",
      """""",
      Seq()
    )
  )

  // @LINE:36
  private[this] lazy val controllers_AccountController_update27_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/user/account")))
  )
  private[this] lazy val controllers_AccountController_update27_invoker = createInvoker(
    AccountController_7.update,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AccountController",
      "update",
      Nil,
      "PUT",
      this.prefix + """api/user/account""",
      """""",
      Seq()
    )
  )

  // @LINE:37
  private[this] lazy val controllers_AccountController_getAccount28_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/user/accountInfo")))
  )
  private[this] lazy val controllers_AccountController_getAccount28_invoker = createInvoker(
    AccountController_7.getAccount,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AccountController",
      "getAccount",
      Nil,
      "GET",
      this.prefix + """api/user/accountInfo""",
      """""",
      Seq()
    )
  )

  // @LINE:38
  private[this] lazy val controllers_SaveSearchController_get29_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/savesearch/load")))
  )
  private[this] lazy val controllers_SaveSearchController_get29_invoker = createInvoker(
    SaveSearchController_3.get,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SaveSearchController",
      "get",
      Nil,
      "GET",
      this.prefix + """api/savesearch/load""",
      """""",
      Seq()
    )
  )

  // @LINE:39
  private[this] lazy val controllers_SaveSearchController_save30_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/savesearch")))
  )
  private[this] lazy val controllers_SaveSearchController_save30_invoker = createInvoker(
    SaveSearchController_3.save,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SaveSearchController",
      "save",
      Nil,
      "POST",
      this.prefix + """api/savesearch""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_HomeController_main0_route(params@_) =>
      call { 
        controllers_HomeController_main0_invoker.call(HomeController_0.main)
      }
  
    // @LINE:8
    case controllers_HomeController_goPublic1_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_goPublic1_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:9
    case controllers_HomeController_goPublic2_route(params@_) =>
      call(params.fromPath[String]("path", None)) { (path) =>
        controllers_HomeController_goPublic2_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:10
    case controllers_PublicController_loadMakeAndModel3_route(params@_) =>
      call { 
        controllers_PublicController_loadMakeAndModel3_invoker.call(PublicController_2.loadMakeAndModel)
      }
  
    // @LINE:11
    case controllers_PublicController_searchCar4_route(params@_) =>
      call { 
        controllers_PublicController_searchCar4_invoker.call(PublicController_2.searchCar)
      }
  
    // @LINE:12
    case controllers_PublicController_getCar5_route(params@_) =>
      call(params.fromPath[String]("vin", None)) { (vin) =>
        controllers_PublicController_getCar5_invoker.call(PublicController_2.getCar(vin))
      }
  
    // @LINE:13
    case controllers_AdministrativeController_saveMakesAndModels6_route(params@_) =>
      call { 
        controllers_AdministrativeController_saveMakesAndModels6_invoker.call(AdministrativeController_5.saveMakesAndModels)
      }
  
    // @LINE:14
    case controllers_HomeController_goPublic7_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_goPublic7_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:15
    case controllers_HomeController_goPublic8_route(params@_) =>
      call(params.fromPath[String]("path", None)) { (path) =>
        controllers_HomeController_goPublic8_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:16
    case controllers_HomeController_goPublic9_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_goPublic9_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:17
    case controllers_HomeController_goPublic10_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_goPublic10_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:18
    case controllers_HomeController_goPublic11_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_goPublic11_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:19
    case controllers_HomeController_goPublic12_route(params@_) =>
      call(params.fromPath[String]("path", None)) { (path) =>
        controllers_HomeController_goPublic12_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:20
    case controllers_HomeController_goPublic13_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_goPublic13_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:21
    case controllers_Login_processLogin14_route(params@_) =>
      call { 
        controllers_Login_processLogin14_invoker.call(Login_4.processLogin)
      }
  
    // @LINE:22
    case controllers_Login_logout15_route(params@_) =>
      call { 
        controllers_Login_logout15_invoker.call(Login_4.logout)
      }
  
    // @LINE:23
    case controllers_HomeController_goPublic16_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_goPublic16_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:24
    case controllers_HomeController_goPublic17_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_goPublic17_invoker.call(HomeController_0.goPublic(path))
      }
  
    // @LINE:27
    case controllers_Assets_versioned18_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned18_invoker.call(Assets_6.versioned(path, file))
      }
  
    // @LINE:28
    case controllers_HomeController_index19_route(params@_) =>
      call(Param[String]("path", Right(""))) { (path) =>
        controllers_HomeController_index19_invoker.call(HomeController_0.index(path))
      }
  
    // @LINE:29
    case controllers_HomeController_index20_route(params@_) =>
      call(params.fromPath[String]("path", None)) { (path) =>
        controllers_HomeController_index20_invoker.call(HomeController_0.index(path))
      }
  
    // @LINE:30
    case controllers_Inventory_save21_route(params@_) =>
      call { 
        controllers_Inventory_save21_invoker.call(Inventory_1.save)
      }
  
    // @LINE:31
    case controllers_Inventory_uploadCarImage22_route(params@_) =>
      call(params.fromPath[String]("carId", None)) { (carId) =>
        controllers_Inventory_uploadCarImage22_invoker.call(Inventory_1.uploadCarImage(carId))
      }
  
    // @LINE:32
    case controllers_Inventory_setOrRemoveFavoriteCar23_route(params@_) =>
      call(params.fromPath[String]("carId", None)) { (carId) =>
        controllers_Inventory_setOrRemoveFavoriteCar23_invoker.call(Inventory_1.setOrRemoveFavoriteCar(carId))
      }
  
    // @LINE:33
    case controllers_Inventory_searchListing24_route(params@_) =>
      call { 
        controllers_Inventory_searchListing24_invoker.call(Inventory_1.searchListing)
      }
  
    // @LINE:34
    case controllers_AccountController_save25_route(params@_) =>
      call { 
        controllers_AccountController_save25_invoker.call(AccountController_7.save)
      }
  
    // @LINE:35
    case controllers_AccountController_changePassword26_route(params@_) =>
      call { 
        controllers_AccountController_changePassword26_invoker.call(AccountController_7.changePassword)
      }
  
    // @LINE:36
    case controllers_AccountController_update27_route(params@_) =>
      call { 
        controllers_AccountController_update27_invoker.call(AccountController_7.update)
      }
  
    // @LINE:37
    case controllers_AccountController_getAccount28_route(params@_) =>
      call { 
        controllers_AccountController_getAccount28_invoker.call(AccountController_7.getAccount)
      }
  
    // @LINE:38
    case controllers_SaveSearchController_get29_route(params@_) =>
      call { 
        controllers_SaveSearchController_get29_invoker.call(SaveSearchController_3.get)
      }
  
    // @LINE:39
    case controllers_SaveSearchController_save30_route(params@_) =>
      call { 
        controllers_SaveSearchController_save30_invoker.call(SaveSearchController_3.save)
      }
  }
}
