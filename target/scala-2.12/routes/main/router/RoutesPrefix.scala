// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/stevejamesfoncy/IdeaProjects/zoom-web/conf/routes
// @DATE:Sun May 19 00:40:39 PET 2019


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
