// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/stevejamesfoncy/IdeaProjects/zoom-web/conf/routes
// @DATE:Sun May 19 00:40:39 PET 2019

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers {

  // @LINE:27
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:27
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:38
  class ReverseSaveSearchController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:39
    def save(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/savesearch")
    }
  
    // @LINE:38
    def get(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/savesearch/load")
    }
  
  }

  // @LINE:21
  class ReverseLogin(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def logout(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "logout")
    }
  
    // @LINE:21
    def processLogin(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "processLogin")
    }
  
  }

  // @LINE:30
  class ReverseInventory(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:32
    def setOrRemoveFavoriteCar(carId:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/inventory/car/favorite/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("carId", carId)))
    }
  
    // @LINE:30
    def save(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/inventory/listing")
    }
  
    // @LINE:33
    def searchListing(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/listing/search")
    }
  
    // @LINE:31
    def uploadCarImage(carId:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/inventory/car/uploadImage/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("carId", carId)))
    }
  
  }

  // @LINE:7
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def index(path:String): Call = {
    
      (path: @unchecked) match {
      
        // @LINE:28
        case (path) if path == "" =>
          implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", ""))); _rrc
          Call("GET", _prefix + { _defaultPrefix } + "admin")
      
        // @LINE:29
        case (path)  =>
          
          Call("GET", _prefix + { _defaultPrefix } + "admin" + implicitly[play.api.mvc.PathBindable[String]].unbind("path", path))
      
      }
    
    }
  
    // @LINE:7
    def main(): Call = {
      
      Call("GET", _prefix)
    }
  
    // @LINE:8
    def goPublic(path:String): Call = {
    
      (path: @unchecked) match {
      
        // @LINE:8
        case (path) if path == "" =>
          implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", ""))); _rrc
          Call("GET", _prefix + { _defaultPrefix } + "catalog")
      
        // @LINE:9
        case (path)  =>
          
          Call("GET", _prefix + { _defaultPrefix } + "catalog" + implicitly[play.api.mvc.PathBindable[String]].unbind("path", path))
      
      }
    
    }
  
  }

  // @LINE:10
  class ReversePublicController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def searchCar(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "public/api/inventory/search")
    }
  
    // @LINE:10
    def loadMakeAndModel(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "public/api/inventory/loadMakeAndModel")
    }
  
    // @LINE:12
    def getCar(vin:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "public/api/inventory/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("vin", vin)))
    }
  
  }

  // @LINE:34
  class ReverseAccountController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:36
    def update(): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/user/account")
    }
  
    // @LINE:34
    def save(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/user/account")
    }
  
    // @LINE:35
    def changePassword(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/user/account/change-password")
    }
  
    // @LINE:37
    def getAccount(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/user/accountInfo")
    }
  
  }

  // @LINE:13
  class ReverseAdministrativeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:13
    def saveMakesAndModels(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/setting/saveMakesAndModels")
    }
  
  }


}
