// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/stevejamesfoncy/IdeaProjects/zoom-web/conf/routes
// @DATE:Sun May 19 00:40:39 PET 2019

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers.javascript {

  // @LINE:27
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:27
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:38
  class ReverseSaveSearchController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:39
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SaveSearchController.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/savesearch"})
        }
      """
    )
  
    // @LINE:38
    def get: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SaveSearchController.get",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/savesearch/load"})
        }
      """
    )
  
  }

  // @LINE:21
  class ReverseLogin(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def logout: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Login.logout",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
        }
      """
    )
  
    // @LINE:21
    def processLogin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Login.processLogin",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "processLogin"})
        }
      """
    )
  
  }

  // @LINE:30
  class ReverseInventory(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:32
    def setOrRemoveFavoriteCar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Inventory.setOrRemoveFavoriteCar",
      """
        function(carId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/inventory/car/favorite/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("carId", carId0))})
        }
      """
    )
  
    // @LINE:30
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Inventory.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/inventory/listing"})
        }
      """
    )
  
    // @LINE:33
    def searchListing: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Inventory.searchListing",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/listing/search"})
        }
      """
    )
  
    // @LINE:31
    def uploadCarImage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Inventory.uploadCarImage",
      """
        function(carId0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/inventory/car/uploadImage/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("carId", carId0))})
        }
      """
    )
  
  }

  // @LINE:7
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function(path0) {
        
          if (path0 == """ + implicitly[play.api.mvc.JavascriptLiteral[String]].to("") + """) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin"})
          }
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("path", path0)})
          }
        
        }
      """
    )
  
    // @LINE:7
    def main: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.main",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
    // @LINE:8
    def goPublic: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.goPublic",
      """
        function(path0) {
        
          if (path0 == """ + implicitly[play.api.mvc.JavascriptLiteral[String]].to("") + """) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "catalog"})
          }
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "catalog" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("path", path0)})
          }
        
        }
      """
    )
  
  }

  // @LINE:10
  class ReversePublicController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def searchCar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PublicController.searchCar",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "public/api/inventory/search"})
        }
      """
    )
  
    // @LINE:10
    def loadMakeAndModel: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PublicController.loadMakeAndModel",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "public/api/inventory/loadMakeAndModel"})
        }
      """
    )
  
    // @LINE:12
    def getCar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PublicController.getCar",
      """
        function(vin0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "public/api/inventory/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("vin", vin0))})
        }
      """
    )
  
  }

  // @LINE:34
  class ReverseAccountController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:36
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AccountController.update",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/user/account"})
        }
      """
    )
  
    // @LINE:34
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AccountController.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/user/account"})
        }
      """
    )
  
    // @LINE:35
    def changePassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AccountController.changePassword",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/user/account/change-password"})
        }
      """
    )
  
    // @LINE:37
    def getAccount: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AccountController.getAccount",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/user/accountInfo"})
        }
      """
    )
  
  }

  // @LINE:13
  class ReverseAdministrativeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:13
    def saveMakesAndModels: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdministrativeController.saveMakesAndModels",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/setting/saveMakesAndModels"})
        }
      """
    )
  
  }


}
