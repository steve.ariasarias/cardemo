// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/stevejamesfoncy/IdeaProjects/zoom-web/conf/routes
// @DATE:Sun May 19 00:40:39 PET 2019

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSaveSearchController SaveSearchController = new controllers.ReverseSaveSearchController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLogin Login = new controllers.ReverseLogin(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseInventory Inventory = new controllers.ReverseInventory(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePublicController PublicController = new controllers.ReversePublicController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAccountController AccountController = new controllers.ReverseAccountController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAdministrativeController AdministrativeController = new controllers.ReverseAdministrativeController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSaveSearchController SaveSearchController = new controllers.javascript.ReverseSaveSearchController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLogin Login = new controllers.javascript.ReverseLogin(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseInventory Inventory = new controllers.javascript.ReverseInventory(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePublicController PublicController = new controllers.javascript.ReversePublicController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAccountController AccountController = new controllers.javascript.ReverseAccountController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAdministrativeController AdministrativeController = new controllers.javascript.ReverseAdministrativeController(RoutesPrefix.byNamePrefix());
  }

}
