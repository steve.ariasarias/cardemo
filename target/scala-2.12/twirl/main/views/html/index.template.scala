
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[String,RequestHeader,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(data: String)(implicit request: RequestHeader):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),_display_(/*3.2*/main(data)),format.raw/*3.12*/("""
"""))
      }
    }
  }

  def render(data:String,request:RequestHeader): play.twirl.api.HtmlFormat.Appendable = apply(data)(request)

  def f:((String) => (RequestHeader) => play.twirl.api.HtmlFormat.Appendable) = (data) => (request) => apply(data)(request)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Tue Oct 23 13:23:28 PET 2018
                  SOURCE: /Users/stevejamesfoncy/IdeaProjects/zoom-web/app/views/index.scala.html
                  HASH: b8293ffd1ac947a71e857c579e68df67bdbacc82
                  MATRIX: 743->1|884->49|911->51|941->61
                  LINES: 21->1|26->2|27->3|27->3
                  -- GENERATED --
              */
          