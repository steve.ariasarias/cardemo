
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object main extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[String,String,RequestHeader,play.twirl.api.HtmlFormat.Appendable] {

  /*
* This template is called from the `index` template. This template
* handles the rendering of the page header and body tags. It takes
* two arguments, a `String` for the title of the page and an `Html`
* object to insert into the body of the page.
*/
  def apply/*7.2*/(data: String, bundle: String = "bundle")(implicit request: RequestHeader):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*7.76*/("""

"""),format.raw/*9.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Zoom</title>

        <link rel="shortcut icon" type="image/png" href="img/favicon.ico" />
        """),format.raw/*18.114*/("""
            """),format.raw/*19.13*/("""<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css"/> -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        """),format.raw/*22.112*/("""
        """),format.raw/*23.9*/("""<link rel="stylesheet" href=""""),_display_(/*23.39*/routes/*23.45*/.Assets.versioned(s"stylesheets/$bundle.css").absoluteURL()),format.raw/*23.104*/("""">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Work+Sans:200,300,400" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
        <script src="https://npmcdn.com/react-bootstrap-table/dist/react-bootstrap-table.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJHDIApLbh7HHsYFK0j5U_z9qhVU4UyxY"></script>
        <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        <script src="js/load-image.all.min.js"></script>


    </head>
    <body>
        <script type="application/javascript">
                var initialState = """),_display_(/*39.37*/Html(data)),format.raw/*39.47*/(""";
        </script>

        <div id="app-container"></div>
        """),_display_(/*43.10*/globals()),format.raw/*43.19*/("""
        """),format.raw/*44.9*/("""<script src=""""),_display_(/*44.23*/routes/*44.29*/.Assets.versioned("javascripts/fetch.js")),format.raw/*44.70*/(""""></script> """),format.raw/*44.151*/("""
        """),format.raw/*45.9*/("""<script src=""""),_display_(/*45.23*/routes/*45.29*/.Assets.versioned(s"javascripts/$bundle.js")),format.raw/*45.73*/(""""></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        """),format.raw/*48.98*/("""

    """),format.raw/*50.5*/("""</body>
</html>"""))
      }
    }
  }

  def render(data:String,bundle:String,request:RequestHeader): play.twirl.api.HtmlFormat.Appendable = apply(data,bundle)(request)

  def f:((String,String) => (RequestHeader) => play.twirl.api.HtmlFormat.Appendable) = (data,bundle) => (request) => apply(data,bundle)(request)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Apr 04 20:18:41 PET 2019
                  SOURCE: /Users/stevejamesfoncy/IdeaProjects/zoom-web/app/views/main.scala.html
                  HASH: 0252330d01b33b15287306d47adddc6ec58635db
                  MATRIX: 1003->261|1172->335|1202->339|1509->722|1551->736|2353->1612|2390->1622|2447->1652|2462->1658|2543->1717|3766->2913|3797->2923|3897->2996|3927->3005|3964->3015|4005->3029|4020->3035|4082->3076|4123->3157|4160->3167|4201->3181|4216->3187|4281->3231|4757->3768|4792->3776
                  LINES: 26->7|31->7|33->9|42->18|43->19|46->22|47->23|47->23|47->23|47->23|63->39|63->39|67->43|67->43|68->44|68->44|68->44|68->44|68->44|69->45|69->45|69->45|69->45|72->48|74->50
                  -- GENERATED --
              */
          