CREATE TABLE IF NOT EXISTS CAR_FAVORITES (
  car_id          VARCHAR(40) NOT NULL,
  user_id         VARCHAR(40) NOT NULL,
  account_id      VARCHAR(40) NOT NULL,
  create_date     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_date   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (car_id,user_id,account_id),
  INDEX idx_cf_car_id (car_id ASC),
  CONSTRAINT fk_car_favorites_account_id FOREIGN KEY (account_id) REFERENCES ACCOUNTS (id),
  CONSTRAINT fk_car_favorites_car_id FOREIGN KEY (car_id) REFERENCES CARS (id),
  CONSTRAINT fk_car_favorites_user_id FOREIGN KEY (user_id) REFERENCES USERS (id)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

ALTER TABLE CARS
DROP COLUMN purchased_date,
DROP COLUMN retail_price,
DROP COLUMN purchased_price;