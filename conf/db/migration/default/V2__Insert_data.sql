-- Default Roles

INSERT INTO ROLES(id,name,description,create_date,modified_date)
VALUES('principal','Principal','Access to all Permissions',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ROLES(id,name,description,create_date,modified_date)
VALUES('superAdmin','SuperAdmin','Access to all Permissions',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO ACCOUNTS (id, email, first_name, dealer_name, dealer_number, last_name, password, sellerType, phone, image, plan_type_id, role_id, active, create_date, modified_date)
VALUES('10000', 'system@zoom.com','zoom','zoom','','zoom','','Sale By Dealer','','',null,'principal',1,CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- Default Attribute values

INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('1', '10000', 'car.trim', 'Trim', 'Trim','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('2', '10000', 'car.body', 'Body', 'Body','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('3', '10000', 'car.interiorColor', 'Interior Color', 'Interior Color','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('4', '10000', 'car.title', 'Title', 'Title','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('5', '10000', 'car.status', 'Status', 'Status','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('6', '10000', 'car.exteriorColor', 'Exterior Color', 'Exterior Color','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('9', '10000', 'car.minimumPrice', 'Minimum Price', 'Minimum Price','DECIMAL', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('13', '10000', 'car.transmission', 'Transmission', 'Transmission','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('14', '10000', 'car.traction', 'Traction', 'Traction','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('15', '10000', 'car.fuel', 'Fuel', 'Fuel','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('17', '10000', 'car.cylinder', 'Cylinder', 'Cylinder','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('20', '10000', 'car.interiorCondition', 'Interior Condition', 'Interior Condition','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('21', '10000', 'car.exteriorCondition', 'Exterior Condition', 'Exterior Condition','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('22', '10000', 'car.stockNumber', 'Stock Number', 'STOCK NUMBER','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('23', '10000', 'car.numberDoor', 'Number Door', 'Number Door','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('24', '10000', 'car.liter', 'Liter', 'Liter','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('26', '10000', 'car.drive', 'Drive', 'Drive','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('27', '10000', 'car.show', 'Show', 'Show','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('60', '10000', 'car.kslPrice', 'KSL Price', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('61', '10000', 'customer.firstName', 'First Name', 'First Name','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('62', '10000', 'customer.middleName', 'Middle Name', 'Middle Name','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('63', '10000', 'customer.lastName', 'Last Name', 'Last Name','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('65', '10000', 'customer.cellPhone', 'Customer Cell Phone', 'Cell Phone','PHONE_MASK', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('66', '10000', 'customer.addressLine2', 'Customer Address Line2', 'Address Line2','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('39', '10000', 'customer.addressLine1', 'Customer Address Line1', 'Address Line1','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('68', '10000', 'customer.homeNumber', 'Home Number', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('71', '10000', 'customer.email', 'Customer Email', 'Email','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('72', '10000', 'customer.secondPhone', 'Second Phone', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('73', '10000', 'customer.applicantType', 'Applicant Type', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('74', '10000', 'customer.relation', 'Relation', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('75', '10000', 'customer.driverLicenseNumber', 'Driver License Number', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('76', '10000', 'customer.driverLicenseState', 'Driver License State', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('77', '10000', 'customer.dobmdy', 'DOB', '','DATE', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('78', '10000', 'customer.creditScore', 'Credit Score', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('79', '10000', 'customer.typeCredit', 'Type Credit', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('80', '10000', 'customer.typeId', 'Type Id', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('97', '10000', 'customer.addressPrevious', 'Customer Address Previous', 'Address Previous','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('98', '10000', 'customer.aptPrevious', 'APT Previous', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('99', '10000', 'customer.cityPrevious', 'Customer CityPrevious', 'City Previous','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('100', '10000', 'customer.statePrevious', 'Customer State Previous', 'State Previous','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('101', '10000', 'customer.zipCodePrevious', 'Zip Code Previous', 'Zip Code Previous','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('102', '10000', 'customer.housingStatusPrevious', 'Housing Status Previous', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('103', '10000', 'customer.yearPrevious', 'Year Previous', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('104', '10000', 'customer.monthPrevious', 'Month Previous', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('105', '10000', 'customer.monthlyRentPrevious', 'Monthly Rent Previous', '','DYNAMIC_NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('64', '10000', 'customer.address', 'Customer Address', 'Address','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('81', '10000', 'customer.apt', 'APT', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('67', '10000', 'customer.city', 'Customer City', 'City','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('69', '10000', 'customer.state', 'Customer State', 'State','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('70', '10000', 'customer.zipCode', 'Zip Code', 'Zip Code','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('82', '10000', 'customer.housingStatus', 'Housing Status', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('83', '10000', 'customer.year', 'Year', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('84', '10000', 'customer.month', 'Month', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('85', '10000', 'customer.monthlyRent', 'Monthly Rent', '','DYNAMIC_NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('106', '10000', 'customer.addressAnother', 'Customer Address Another', 'Address','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('107', '10000', 'customer.aptAnother', 'APT Another', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('108', '10000', 'customer.cityAnother', 'Customer City Another', 'City Another','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('109', '10000', 'customer.stateAnother', 'Customer State Another', 'State Another','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('110', '10000', 'customer.zipCodeAnother', 'Zip Code Another', 'Zip Code Another','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('111', '10000', 'customer.housingStatusAnother', 'Housing Status Another', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('112', '10000', 'customer.yearAnother', 'Year Another', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('113', '10000', 'customer.monthAnother', 'Month Another', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('114', '10000', 'customer.monthlyRentAnother', 'Monthly Rent Another', '','DYNAMIC_NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('115', '10000', 'customer.employerNamePrevious', 'Employer Name Previous', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('116', '10000', 'customer.jobTitlePrevious', 'Job Title Previous', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('117', '10000', 'customer.employerCellPhonePrevious', 'Employer Cell Phone Previous', '','PHONE_MASK', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('118', '10000', 'customer.employerAddressPrevious', 'Employer Address Previous', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('119', '10000', 'customer.employerCityPrevious', 'Employer City Previous', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('120', '10000', 'customer.employerStatePrevious', 'Employer State Previous', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('121', '10000', 'customer.employerZipCodePrevious', 'Employer Zip code Previous', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('122', '10000', 'customer.employmentStatusPrevious', 'Employment Status Previous', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('123', '10000', 'customer.employerYearPrevious', 'Employer Year Previous', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('124', '10000', 'customer.employerMonthPrevious', 'Employer Month Previous', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('125', '10000', 'customer.monthlyInComePrevious', 'Monthly in come Previous', '','DYNAMIC_NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('86', '10000', 'customer.employerName', 'Employer Name', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('87', '10000', 'customer.jobTitle', 'Job Title', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('88', '10000', 'customer.employerCellPhone', 'Employer Cell Phone', '','PHONE_MASK', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('89', '10000', 'customer.employerAddress', 'Employer Address', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('90', '10000', 'customer.employerCity', 'Employer City', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('91', '10000', 'customer.employerState', 'Employer State', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('92', '10000', 'customer.employerZipCode', 'Employer Zip code', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('93', '10000', 'customer.employmentStatus', 'Employment Status', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('94', '10000', 'customer.employerYear', 'Employer Year', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('95', '10000', 'customer.employerMonth', 'Employer Month', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('96', '10000', 'customer.monthlyInCome', 'Monthly in come', '','DYNAMIC_NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('126', '10000', 'customer.employerNameAnother', 'Employer Name Another', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('127', '10000', 'customer.jobTitleAnother', 'Job Title Another', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('128', '10000', 'customer.employerCellPhoneAnother', 'Employer Cell Phone Another', '','PHONE_MASK', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('129', '10000', 'customer.employerAddressAnother', 'Employer Address Another', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('130', '10000', 'customer.employerCityAnother', 'Employer City Another', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('131', '10000', 'customer.employerStateAnother', 'Employer State Another', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('132', '10000', 'customer.employerZipCodeAnother', 'Employer Zip code Another', '','TEXT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('133', '10000', 'customer.employmentStatusAnother', 'Employment Status Another', '','SELECT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('134', '10000', 'customer.employerYearAnother ', 'Employer Year Another', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('135', '10000', 'customer.employerMonthAnother', 'Employer Month Another', '','NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTES (id, account_id, code, name, display_value,type, create_date, modified_date) VALUES ('136', '10000', 'customer.monthlyInComeAnother ', 'Monthly in come Another', '','DYNAMIC_NUMBER', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);



-- Default Attribute values

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('5','10000','2','sedan','Sedan','Sedan',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('6','10000','2','coupe','Coupe','Coupe',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('7','10000','2','suv','SUV','SUV',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('8','10000','2','truck','Truck','Truck',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('199','10000','2','hatchback','atchback','Hatchback',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('200','10000','2','wagon','Wagon','Wagon',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('201','10000','2','pick-up Truck','Pick-up Truck','Pick-up Truck',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('202','10000','2','Minivan','Minivan','Minivan',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('203','10000','2','Van','Van','Van',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('204','10000','2','Limousine','Limousine','Limousine',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('205','10000','2','Roadster','Roadster','Roadster',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('206','10000','2','Compact','Compact','Compact',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('207','10000','2','Convertible','Convertible','Convertible',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('254','10000','2','Crossover','Crossover','Crossover',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('9','10000','3','blue','Blue','Blue',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('10','10000','3','red','Red','Red',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('11','10000','3','white','White','White',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('12','10000','3','green','Green','Green',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('13','10000','3','brown','Brown','Brown',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('14','10000','3','black','Black','Black',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('15','10000','3','yellow','Yellow','Yellow',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('210','10000','3','gold','Gold','Gold',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('211','10000','3','grey','Grey','Grey',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('213','10000','3','orange','Orange','Orange',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('214','10000','3','pink','Pink','Pink',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('215','10000','3','purple','Purple','Purple',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('216','10000','3','silver','Silver','Silver',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('217','10000','3','tan','Tan','Tan',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('218','10000','3','maroon','Maroon','Maroon',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);



INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('16','10000','4','clean','Clean','Clean',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('17','10000','4','salvage','Salvage','Salvage',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('208','10000','4','','Dismantled','Dismantled',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('209','10000','4','Rebuilt/Reconstructed','Rebuilt/Reconstructed','Rebuilt/Reconstructed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('18','10000','5','inspect','Inspect','Inspect/Repair',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('19','10000','5','hold','Hold','Hold',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('20','10000','5','flr','Front Line Ready','Front Line Ready',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('21','10000','5','sold','Sold','Sold',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('22','10000','6','blue_exterior','Blue','Blue',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('23','10000','6','red_exterior','Red','Red',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('24','10000','6','white_exterior','White','White',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('25','10000','6','green_exterior','Green','Green',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('26','10000','6','brown_exterior','Brown','Brown',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('27','10000','6','black_exterior','Black','Black',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('28','10000','6','yellow_exterior','Yellow','Yellow',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('222','10000','6','gold_exterior','Gold','Gold',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('223','10000','6','grey_exterior','Grey','Grey',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('224','10000','6','orange_exterior','Orange','Orange',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('225','10000','6','pink_exterior','Pink','Pink',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('226','10000','6','purple_exterior','Purple','Purple',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('227','10000','6','Silver_exterior','Silver','Silver',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('228','10000','6','maroon_exterior','Maroon','Maroon',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('229','10000','6','tan_exterior','Tan','Tan',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);



INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('255','10000','20','excellent','Excellent','Excellent',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('234','10000','20','very Good','Very Good','Very Good',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('219','10000','20','good','Good','Good',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('220','10000','20','Fair','Fair','Fair',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('221','10000','20','Poor','Poor','Poor',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


-- miles

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('29','10000','13','automatic','Automatic','Automatic',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('30','10000','13','manual','Manual','Manual',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('31','10000','13','semi_automatic','Semi-Automatic','Semi-Automatic',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('32','10000','14','front_2_wd','Front 2WD','Front 2WD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('33','10000','14','rear_2_wd','Rear 2WD','Rear 2WD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('34','10000','14','4_wd','4WD','4WD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('35','10000','14','awd','AWD','AWD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('36','10000','15','gasoline','Gasoline','Gasoline',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('37','10000','15','diesel','Diesel','Diesel',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('38','10000','15','electric','Electric','Electric',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('39','10000','15','hybrid','Hybrid','Hybrid',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('242','10000','15','Bio-Diesel','Bio-Diesel','Bio-Diesel',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('243','10000','15','Bio-Fuel','Bio-Fuel','Bio-Fuel',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('244','10000','15','compressed Natural Gas','Compressed Natural Gas','Compressed Natural Gas',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('245','10000','15','ethanol','Ethanol','Ethanol',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('246','10000','15','Flex Fuel','Flex Fuel','Flex Fuel',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('247','10000','15','liquified Natural Gas','Liquified Natural Gas','Liquified Natural Gas',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
  INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('248','10000','15','liquified Petroleum','Liquified Petroleum','Liquified Petroleum',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('45','10000','27','show','show','Show',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('46','10000','27','hide','hide','Hide',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('47','10000','17','3','3','3',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('48','10000','17','4','4','4',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('49','10000','17','6','6','6',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('50','10000','17','8','8','8',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('239','10000','17','5','5','5',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('240','10000','17','10','10','10',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('241','10000','17','12','12','12',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('54','10000','21','exterior_good','Good','Good',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('55','10000','21','exterior_bad','Bad','Bad',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('230','10000','21','excellent_exterior','Excellent','Excellent',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('231','10000','21','very Good_exterior','Very Good','Very Good',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('232','10000','21','fair_exterior','Fair','Fair',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('233','10000','21','poor_exterior','Poor','Poor',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('235','10000','23','2','2','2',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('236','10000','23','3','3','3',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('237','10000','23','4','4','4',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('238','10000','23','5','5','5',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('58','10000','26','drive_1','Drive 1','Drive 1',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('59','10000','26','drive_2','Drive 2','Drive 2',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('249','10000','26','2WD','2WD','2WD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('250','10000','26','4WD','4WD','4WD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('251','10000','26','AWD','AWD','AWD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('252','10000','26','FWD','FWD','FWD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('253','10000','26','RWD','RWD','RWD',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.applicantType
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('91','10000','73','individualCredit','individualCredit','Individual Credit',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('92','10000','73','joinCredit','joinCredit','Join Credit',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('93','10000','74','relationShip','relationShip','RelationShip',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('94','10000','74','spouse','spouse','Spouse',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('95','10000','74','relative','relative','Relative',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('96','10000','74','other','other','Other',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.score
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('97','10000','78','score_1','score_1','Above 700',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('98','10000','78','score_2','score_2','699 - 650',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('99','10000','78','score_3','score_3','649 - 600',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('100','10000','78','score_4','score_4','599 - 550',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('101','10000','78','score_5','score_5','549 - 500',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('102','10000','78','score_6','score_6','Below 499',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('103','10000','78','doNotKnow','score_7','Do not know',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.typeCredit
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('104','10000','79','ssn','ssn','SSN',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('105','10000','79','itin','itin','ITIN',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('106','10000','79','btnNone','btnNone','NONE',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.typeId
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('107','10000','80','driveLicence','driveLicence','DRIVE LICENCE',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('108','10000','80','noDriveLicence','noDriveLicence','NO DRIVER LICENCE ID',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('109','10000','80','btnIdNone','btnIdNone','NONE',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.housingStatus
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('110','10000','82','rent','rent','Rent',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('111','10000','82','liveFamily','liveFamily','Live with family',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('112','10000','82','own','own','Own',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('113','10000','82','housingOther','housingOther','Other',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.housingStatus previous
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('149','10000','102','rentPrevious','rentPrevious','Rent',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('150','10000','102','liveFamilyPrevious','liveFamilyPrevious','Live with family',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('151','10000','102','ownPrevious','ownPrevious','Own',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('152','10000','102','housingOtherPrevious','housingOtherPrevious','Other',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.housingStatus another
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('153','10000','111','rentAnother','rentAnother','Rent',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('154','10000','111','liveFamilyAnother','liveFamilyAnother','Live with family',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('155','10000','111','ownAnother','ownAnother','Own',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('156','10000','111','housingOtherAnother','housingOtherAnother','Other',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.employmentStatus
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('114','10000','93','employed','employed','Employed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('115','10000','93','unEmployed','unEmployed','Unemployed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('116','10000','93','retired','retired','Retired / Pension',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('117','10000','93','military','military','Military',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('118','10000','93','selfEmployed','selfEmployed','Self Employed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('119','10000','93','disability','disability','Disability / SS Assistance',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('120','10000','93','onLeave','onLeave','On Leave',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.employmentStatus previous
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('157','10000','122','employedPrevious','employedPrevious','Employed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('158','10000','122','unEmployedPrevious','unEmployedPrevious','Unemployed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('159','10000','122','retiredPrevious','retiredPrevious','Retired / Pension',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('160','10000','122','militaryPrevious','militaryPrevious','Military',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('161','10000','122','selfEmployedPrevious','selfEmployedPrevious','Self Employed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('162','10000','122','disabilityPrevious','disabilityPrevious','Disability / SS Assistance',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('163','10000','122','onLeavePrevious','onLeavePrevious','On Leave',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- values for customer.employmentStatus another
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('164','10000','133','employedAnother','employedAnother','Employed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('165','10000','133','unEmployedAnother','unEmployedAnother','Unemployed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('166','10000','133','retiredAnother','retiredAnother','Retired / Pension',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('167','10000','133','militaryAnother','militaryAnother','Military',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('168','10000','133','selfEmployedAnother','selfEmployedAnother','Self Employed',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('169','10000','133','disabilityAnother','disabilityAnother','Disability / SS Assistance',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('170','10000','133','onLeaveAnother','onLeaveAnother','On Leave',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


-- DEFAULT VALUE TYPE TEXT
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('121','10000','62','customer.middleName','customer.middleName','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('122','10000','39','customer.addressLine1','customer.addressLine1','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('123','10000','65','customer.cellPhone','customer.cellPhone','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('124','10000','66','customer.addressLine2','customer.addressLine2','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('125','10000','67','customer.city','customer.city','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('126','10000','68','customer.homeNumber','customer.homeNumber','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('127','10000','69','customer.state','customer.state','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('128','10000','70','customer.zipCode','customer.zipCode','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('129','10000','71','customer.email','customer.email','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('130','10000','64','customer.address','customer.address','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('132','10000','72','customer.secondPhone','customer.secondPhone','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('133','10000','75','customer.driverLicenseNumber','customer.driverLicenseNumber','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('134','10000','76','customer.driverLicenseState','customer.driverLicenseState','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('135','10000','77','customer.dobmdy','customer.dobmdy','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('136','10000','81','customer.apt','customer.apt','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('137','10000','85','customer.monthlyRent','customer.monthlyRent','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('138','10000','86','customer.employerName','customer.employerName','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('139','10000','87','customer.jobTitle','customer.jobTitle','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('140','10000','88','customer.employerCellPhone','customer.employerCellPhone','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('141','10000','89','customer.customer.employerAddress','customer.employerCellPhone','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('142','10000','90','customer.employerCity','customer.employerCity','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('143','10000','91','customer.employerZipCod','customer.employerZipCod','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('144','10000','96','customer.monthlyInCome','customer.monthlyInCome','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('145','10000','97','customer.addressPrevious','customer.addressPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('146','10000','98','customer.aptPrevious','customer.aptPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('147','10000','99','customer.cityPrevious','customer.cityPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('148','10000','101','customer.zipCodePrevious','customer.zipCodePrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('171','10000','103','customer.yearPrevious','customer.yearPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('172','10000','104','customer.monthPrevious','customer.monthPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('173','10000','105','customer.monthlyRentPrevious','customer.monthlyRentPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('174','10000','106','customer.addressAnother','customer.addressAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('175','10000','107','customer.aptAnother','customer.aptAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('176','10000','108','customer.cityAnother','customer.cityAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('177','10000','110','customer.zipCodeAnother','customer.zipCodeAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('178','10000','112','customer.yearAnother','customer.yearAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('179','10000','113','customer.monthAnother','customer.monthAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('180','10000','114','customer.monthlyRentAnother','customer.monthlyRentAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('181','10000','115','customer.employerNamePrevious','customer.employerNamePrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('182','10000','116','customer.jobTitlePrevious','customer.jobTitlePrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('183','10000','117','customer.employerCellPhonePrevious','customer.employerCellPhonePrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('184','10000','118','customer.employerAddressPrevious','customer.employerAddressPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('185','10000','119','customer.employerCityPrevious','customer.employerCityPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('186','10000','121','customer.employerZipCodePrevious','customer.employerZipCodePrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('187','10000','123','customer.employerYearPrevious','customer.employerYearPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('188','10000','124','customer.employerMonthPrevious','customer.employerMonthPrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('189','10000','125','customer.monthlyInComePrevious','customer.monthlyInComePrevious','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('190','10000','126','customer.employerNameAnother','customer.employerNameAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('191','10000','127','customer.jobTitleAnother','customer.jobTitleAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('192','10000','128','customer.employerCellPhoneAnother','customer.employerCellPhoneAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('193','10000','129','customer.employerAddressAnother','customer.employerAddressAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('194','10000','130','customer.employerCityAnother','customer.employerCityAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('195','10000','132','customer.employerZipCodeAnother','customer.employerZipCodeAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('196','10000','134','customer.employerYearAnother','customer.employerYearAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('197','10000','135','customer.employerMonthAnother','customer.employerMonthAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('198','10000','136','customer.monthlyInComeAnother','customer.monthlyInComeAnother','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('256','10000','91','customer.employerState','customer.employerState','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('257','10000','22','car.stockNumberr','car.stockNumber','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('258','10000','1','car.trim','car.trim','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('259','10000','24','car.liter','car.liter','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('260','10000','14','car.traction','car.traction','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('261','10000','83','customer.year','customer.year','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('262','10000','84','customer.month','customer.month','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('263','10000','94','customer.year','customer.year','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('264','10000','95','customer.month','customer.month','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('265','10000','5','car.status','DEFAULT','Status',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('266','10000','27','car.show','DEFAULT','Visibility',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('270','10000','60','car.kslPrice','car.kslPrice','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_VALUES(id, account_id, attribute_id, code, name, display_value, create_date,modified_date) VALUES
  ('271','10000','9','car.minimumPrice','car.minimumPrice','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- ATTRIBUTE ENTITY TYPE
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('1','10000','1','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('2','10000','2','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('3','10000','3','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('4','10000','4','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('5','10000','5','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('6','10000','6','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('7','10000','9','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('8','10000','13','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('9','10000','14','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('10','10000','15','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('11','10000','17','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('12','10000','20','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('13','10000','21','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('14','10000','22','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('15','10000','23','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('16','10000','24','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('17','10000','26','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('18','10000','27','CAR',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('19','10000','61','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('20','10000','62','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('21','10000','63','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('22','10000','65','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('23','10000','66','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('24','10000','68','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('25','10000','71','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('26','10000','72','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('27','10000','73','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('28','10000','74','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('29','10000','75','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('30','10000','76','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('31','10000','77','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('32','10000','78','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('33','10000','79','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('34','10000','80','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('35','10000','97','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('36','10000','98','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('37','10000','99','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('38','10000','100','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('39','10000','101','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('40','10000','102','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('41','10000','103','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('42','10000','104','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('43','10000','105','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('44','10000','106','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('45','10000','107','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('46','10000','108','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('47','10000','109','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('48','10000','110','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('49','10000','111','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('50','10000','112','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('51','10000','113','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('52','10000','114','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('53','10000','115','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('54','10000','116','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('55','10000','117','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('56','10000','118','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('57','10000','119','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('58','10000','120','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('59','10000','121','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('60','10000','122','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('61','10000','123','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('62','10000','124','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('63','10000','125','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('64','10000','64','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('65','10000','81','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('66','10000','67','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('67','10000','69','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('68','10000','70','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('69','10000','82','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('70','10000','83','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('71','10000','84','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('72','10000','85','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('73','10000','86','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('74','10000','87','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('75','10000','88','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('76','10000','89','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('77','10000','90','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('78','10000','91','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('79','10000','92','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('80','10000','93','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('81','10000','94','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('82','10000','95','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('83','10000','96','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('84','10000','126','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('85','10000','127','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('86','10000','128','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('87','10000','129','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('88','10000','130','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('89','10000','131','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('90','10000','132','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('91','10000','133','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('92','10000','134','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('93','10000','135','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO ATTRIBUTE_ENTITY_TYPES(id, account_id, attribute_id, entity_type, create_date,modified_date) VALUES
('94','10000','136','CUSTOMER',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

