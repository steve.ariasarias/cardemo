UPDATE ACCOUNTS
SET plan_type_id = '101' WHERE id='10000';

INSERT INTO PLAN_TYPES (id, name, listing_number, users_number, price, create_date, modified_date) VALUES ('101','Owner', 2, 1, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO PLAN_TYPES (id, name, listing_number, users_number, price, create_date, modified_date) VALUES ('102','Baby Dealer', 30, 2, 299, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO PLAN_TYPES (id, name, listing_number, users_number, price, create_date, modified_date) VALUES ('103','Dealer', 70, 5, 499, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO PLAN_TYPES (id, name, listing_number, users_number, price, create_date, modified_date) VALUES ('104','Dealer PRO', 10000000, 10000000, 799, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

ALTER TABLE ACCOUNTS
DROP COLUMN sellerType,
DROP COLUMN role_id,
DROP COLUMN image,
DROP FOREIGN KEY fk_account_role_id,
ADD CONSTRAINT fk_plan_type_id FOREIGN KEY (plan_type_id) REFERENCES PLAN_TYPES (id);
